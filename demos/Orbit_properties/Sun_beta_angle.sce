//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Sun beta angle: 
//> It is the angle between the Sun direction and its projection onto the 
//> orbit plane (by convention: positive if the angle between the Sun direction 
//> and the orbit angular momentum is less than 90 degrees). 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


inc = 70 * %CL_deg2rad; // inclination

desc_param = list(..
   CL_defParam("Inclinaison", inc, units=['rad', 'deg'], valid='$x>0 & $x<180')..
);

[inc] = CL_inputParam(desc_param);


// Envelope of evolution of beta on a year (circular orbit)
nbpts = 50;

tab_hlocv = linspace(0,24,nbpts);

tab_decl = linspace(-23.5,23.5,nbpts) * %CL_deg2rad;


result = [];

for decl = tab_decl

  beta_sol = CL_gm_raan2beta(inc*ones(tab_hlocv), zeros(tab_hlocv), -(tab_hlocv-12)*%pi/12, decl*ones(tab_hlocv));
  result = [result; beta_sol * %CL_rad2deg]; // degrees

end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

Nmap = 64;
f.color_map = 0.4+0.6*jetcolormap(Nmap);

Noir = addcolor([1,1,1]*0);
Gris = addcolor([1,1,1]*0.3);
GrisF = addcolor([1,1,1]*0.2);

a=gca();

zmin=min(result); // grayplot min/max
zmax=max(result);

colorbar(zmin,zmax,colminmax=[1,Nmap],fmt="%.1f");
Sgrayplot(tab_hlocv, tab_decl*%CL_rad2deg, result', colminmax=[1,Nmap],zminmax=[zmin,zmax]);

[levels] = CL_autoLevels(min(result), max(result));

contour2d(tab_hlocv, tab_decl*%CL_rad2deg, result', levels, style=Gris*ones(levels));

x_ticks = 0:2:24;
a.x_ticks = tlist(['ticks','locations','labels'], x_ticks, string(x_ticks));

// general setting
CL_g_stdaxes(a)

a.data_bounds = [0,min(tab_decl*%CL_rad2deg);24,max(tab_decl*%CL_rad2deg)];
a.tight_limits="on";
a.title.text = "Beta angle (deg)";
a.x_label.text = "True local time of ascending node (h)";
a.y_label.text = "Sun declination (deg)";

// adjustments
h = CL_g_select(a, "Text");
h.font_style=8;
h.font_size=2;
h.font_foreground=GrisF;

f.immediate_drawing="on";
f.visible = "on";

