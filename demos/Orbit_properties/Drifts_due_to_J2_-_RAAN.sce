//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> RAAN drift (secular drift) due to J2
//
// Auteur: A. Lamy
// -----------------------------------------------------------


smamin = 7000.e3;
smamax = 10000.e3;

incmin = 0 * %CL_deg2rad;
incmax = 90 * %CL_deg2rad;

ecc = 0;  

desc_param = list(..
   CL_defParam("Semi-major axis - min", smamin, units=['m', 'km'], id='$amin', valid='$amin>0'),..
   CL_defParam("Semi-major axis - max", smamax, units=['m', 'km'], id='$amax', valid='$amax>$amin'),..
   CL_defParam("Inclination - min", incmin, units=['rad', 'deg'], id='$imin', valid='$imin>=0 & $imin<=180'),..
   CL_defParam("Inclination - max", incmax, units=['rad', 'deg'], id='$imax', valid='$imax>$imin & $imax<=180'),..
   CL_defParam("Eccentricity", ecc, valid='$x>=0 & $x<1')..
);

[smamin,smamax,incmin,incmax,ecc] = CL_inputParam(desc_param);

nbpts = 60;
sma = linspace(smamin, smamax, nbpts);
inc = linspace(incmin, incmax, nbpts);


res = []; // res(inc, sma) = raan
for i = inc;
  [drift_pom, drift_gom, drift_anm] = CL_op_driftJ2(sma,ecc,i);
  res = [res; drift_gom*%CL_rad2deg*86400]; // deg/j
end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

f.color_map = jetcolormap(32);
Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Noir = addcolor([1,1,1]*0);

a=gca();

[levels, sublevels] = CL_autoLevels(min(res), max(res));

contour2d(sma/1000, inc*%CL_rad2deg, res', levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(sma/1000, inc*%CL_rad2deg, res', sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);

// general setting
CL_g_stdaxes(a);
a.data_bounds = [smamin/1000,incmin*%CL_rad2deg; smamax/1000,incmax*%CL_rad2deg];
a.tight_limits="on";
a.x_label.text = "Semi major axis (km)";
a.y_label.text = "Inclination (deg)";
a.title.text = "RAAN drift rate - ecc = " + string(ecc) + " (deg/day)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text");
h.font_foreground=Coul3;
h.font_size=3;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

