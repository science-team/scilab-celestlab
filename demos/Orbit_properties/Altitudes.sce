//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Various definitions of "altitude": 
//> Meaning of terms used: 
//> - spherical: radius (= distance from Earth centre) minus Earth equatorial radius
//> - geodetic: altitude wrt ellipsoid
//> - mean: value obtained from mean parameters
//> - true: value obtained from true (osculating) parameters
//> - sma: semi-major axis
//>
//> NB: Orbit propagated using "lyddaneLp" model (see help pages for validity). 

// Auteur: A. Lamy
// -----------------------------------------------------------

sma = %CL_eqRad + 700.e3; // sma minus equatorial radius
pom = %pi/2; 
inc = 98*%CL_deg2rad; 
ecc = 1.e-3; 
x_axis = 1; // 1: PSO   2: (true spherical) latitude 

desc_param = list(..
   CL_defParam("Mean semi-major axis", sma, units=['m', 'km'], valid='$x>0'),..
   CL_defParam("Mean eccentricity", ecc,  valid="$x>=0 & $x<1"  ),..
   CL_defParam("Mean inclination", inc, units=['rad', 'deg'], valid="$x>=0 & $x<=180"  ),..
   CL_defParam("Mean argument of perigee", pom, units=['rad', 'deg']),..
   CL_defParam("X-axis (1=mean arg. of lat, 2=true arg. of lat, 3=(sph.) latitude)", x_axis, accv=1:3)..
);

[sma, ecc, inc, pom, x_axis] = CL_inputParam(desc_param);

// -----------------------------------------------------------
// Computation
// -----------------------------------------------------------
ex = ecc*cos(pom); 
ey = ecc*sin(pom); 

circ0 = [sma; ex; ey; inc; 0; 0]; // mean elements adapted to (nearly) circular orbits

T = CL_kp_params('per', sma)/86400;  // orbital period in seconds
t0 = 0; 
mean_cir_T = CL_ex_propagate("lydlp", "cir", t0, circ0, T, "m");  
n = CL_rMod(mean_cir_T(6), %pi, 3*%pi)/T; // rad/day
T = 2*%pi / n; // more exact (mean) period. 

t = linspace(0,T,200); // final times (days)

[mean_cir,osc_cir] = CL_ex_propagate("lydlp", "cir", t0, circ0, t, "mo");  // orbit propagation
pso = CL_rMod(mean_cir(6,:), (t/T) * 2*%pi -%pi, (t/T) * 2*%pi + %pi); // garanteed continuous

[posm,velm] = CL_oe_cir2car(mean_cir);
[pos,vel] = CL_oe_cir2car(osc_cir);

[pos_ell] = CL_co_car2ell(pos);
[pos_sph] = CL_co_car2sph(pos);

[posm_ell] = CL_co_car2ell(posm);
[posm_sph] = CL_co_car2sph(posm);

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------
f=scf();
f.visible="off";
f.immediate_drawing="off";

a=gca();

a.title.text = "Altitude comparison";
a.y_label.text = "Altitude (km)";

if (x_axis == 1)
  x = pso*%CL_rad2deg; 
  a.x_label.text = "Mean argument of latitude (deg)";
elseif (x_axis == 2)
  psov = CL_rMod(pom+CL_kp_M2v(ecc,pso-pom),pso-%pi,pso+%pi); 
  x = psov*%CL_rad2deg; 
  a.x_label.text = "True argument of latitude (deg)";
else
  x = pos_sph(2,:)*%CL_rad2deg; 
  a.x_label.text = "Latitude (deg)";
end


plot2d(x, ((sma-%CL_eqRad) * ones(pso))/1000); 
CL_g_tag(a,0);

plot2d(x, (pos_sph(3,:)-%CL_eqRad)/1000, style=11); 
CL_g_tag(a,1);

plot2d(x, (posm_sph(3,:)-%CL_eqRad)/1000, style=12); 
CL_g_tag(a,2);

plot2d(x, (pos_ell(3,:))/1000, style=28); 
CL_g_tag(a,1);

plot2d(x, (posm_ell(3,:))/1000, style=30); 
CL_g_tag(a,2);

// adjustments

h = CL_g_select(a, "Polyline");
h.thickness = 2;

h = CL_g_select(a, "Polyline", 1);
h.thickness = 3;
h = CL_g_select(a, "Polyline", 0);
h.thickness = 3;


CL_g_stdaxes(a);
a.tight_limits = "on"; 

CL_g_legend(a, ["Spherical: sma", "Spherical: true", "Spherical: mean", "Geodetic: true", "Geodetic: mean"]); 

f.immediate_drawing="on";
f.visible="on";


