//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Some keplerian parameters
// 
// Auteur: A. Lamy
// -----------------------------------------------------------


sma = 24271.e3;
ecc = 0.73;
anom_type = 1;

desc_param = list(..
   CL_defParam("Semi major axis", sma, units=['m', 'km'], valid="$x>0" ),..
   CL_defParam("Eccentricity",    ecc,  valid="$x>=0 & $x<1"  ),..
   CL_defParam("Type of reference anomaly (1=true  2=mean) ", anom_type, accv=[1,2] )..
);

[sma, ecc, anom_type] = CL_inputParam(desc_param);

nbpts = 300;
anm = linspace(0, 2*%pi, nbpts);
anv = CL_kp_M2v(ecc,anm);

tab_anom = list(anv, anm);
anom_titre = ["True", "Mean"];

kep = CL_kp_characteristics(sma,ecc,anv);

f=scf();
f.visible="off";
f.immediate_drawing="off";

result = list(kep.vel/1000, kep.v_slope*%CL_rad2deg, kep.R/1.e6, kep.date/3600);
titres = ['Velocity (km/s)', 'Slope of velocity (deg)', 'Radius (10^3 km)', 'Time (h)'];

for k=1:4
  subplot(2,2,k);

  plot2d(tab_anom(anom_type)*%CL_rad2deg, result(k),style=2);

  a=gca();
  a.x_ticks = tlist(["ticks","locations","labels"], 0:60:360, string(0:60:360));

  // adjust data bounds (add margin to ymin/ymax)
  xmin = 0; // degrees
  xmax = 360; // degrees
  ymin = min(result(k)); 
  ymax = max(result(k)); 
 
  a.data_bounds = [xmin, ymin - (ymax-ymin)*0.03; 360, ymax + (ymax-ymin)*0.03]; 
  
  a.tight_limits = "on";

  CL_g_stdaxes(a);

  a.title.text = titres(k);
  a.x_label.text = anom_titre(anom_type) + ' anomaly (deg)';
  a.margins = [0.12,0.1,0.15,0.2];

  h = CL_g_select(a, "Polyline");
  h.thickness=2;

end

f.immediate_drawing="on";
f.visible="on";



