//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Sun beta angle (evolution over time): 
//> The orbit is circular. 
//> The angle is nearly constant for Sun synchronous orbits (but not quite). 
//> Variations are due to the yearly variations of the Sun direction.  
//
// Auteur: A. Lamy
// -----------------------------------------------------------


h = 820.e3;  // altitude (m)
inc = CL_op_ssoJ2('i', h+%CL_eqRad, 0); // inclination (sun-synchronous, ecc = 0)
hloc0 = 0:3:22; // local time (h)
T = 365; // duration in days

desc_param = list(..
  CL_defParam("Altitude", h, units=['m','km'], id='$h', valid='$h>=0'),..
  CL_defParam("Inclination", inc, units=['rad','deg'], id='$inc', valid='$inc>0 & $inc < 180'),..
  CL_defParam("Values of initial mean local time of asc. node", hloc0, units=['h'], dim=-1, id='$hloc0', valid='$hloc0>=0 & $hloc0<=24'),..
  CL_defParam("Simulation time", T , units=['day','day'], id='$T', valid='$T>0 & $T<3650'),..
  CL_defParam("Show legend (1=yes, 0=no)", 1 , accv=[0,1]),..
  CL_defParam("Show eclipse limit (1=yes, 0=no)", 1 , accv=[0,1])..
);

[h, inc, hloc0, T, show_leg, show_ecl] = CL_inputParam(desc_param);

// Evolution of beta angle

t0 = CL_dat_cal2cjd(2000,3,21,0,0,0);   // initial date
sma = h+%CL_eqRad; // semi major axis
ecc = 0; // eccentricity
[dpomdt, dgomdt, dmadt] = CL_op_driftJ2(sma, ecc, inc);  // compute drifts due to j2

dt = 0:T; // relative dates
pos_sun = CL_eph_sun(t0+dt);  //compute sun position starting at t0 (in ECI)
pos_sun_sph = CL_co_car2sph(pos_sun); // spherical coordinates

f = scf(); //create figure
f.immediate_drawing="off";
f.visible = "off";

nb = length(hloc0);
f.color_map = jetcolormap(min(max(nb,3),100));
Noir = addcolor([0,0,0]);

a = gca();

for k = 1:length(hloc0)
  gom0 = CL_op_locTime(t0, 'mlh', hloc0(k), 'ra');  // initial right ascension of RAAN
  gom = gom0 + dgomdt*dt*86400; // raan over time
  beta_ang = CL_gm_raan2beta(inc, gom, pos_sun_sph(1,:), pos_sun_sph(2,:));
  plot2d(dt, beta_ang * %CL_rad2deg, style=k);
end
CL_g_tag(a,1);

// limit angle for eclipse
if (show_ecl == 1)
beta_ang_lim = asin(%CL_eqRad / sma);
plot2d(dt, beta_ang_lim*ones(dt) * %CL_rad2deg, style=Noir);
plot2d(dt, -beta_ang_lim*ones(dt) * %CL_rad2deg, style=Noir);
CL_g_tag(a,2);
end

a.title.text = "Evolution of beta angle";
a.x_label.text = "Days from March, 21th";
a.y_label.text = "Beta angle (deg)";

if (show_leg== 1)
  CL_g_legend(a, header = "Initial MLT (asc. node):", str=string(hloc0)+" h");
end

CL_g_stdaxes(a);
a.tight_limits = "on";

h=CL_g_select(a, "Polyline");
h.thickness = 2;

h=CL_g_select(a, "Polyline", 2);
h.line_style = 3;
h.thickness = 3;

f.immediate_drawing="on";
f.visible = "on";


