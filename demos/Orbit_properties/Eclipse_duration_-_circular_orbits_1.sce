//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Eclipse duration as a function of altitude and beta (circular orbit). 
//> x axis : altitude (km)
//> y axis : Sun beta angle (deg)
//> Blue curves: eclipse duration (mn)
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hmin = 700.e3;
hmax = 37000.e3;

betamin = 0*%CL_deg2rad;
betamax = 60*%CL_deg2rad;

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>0'),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin'),..
   CL_defParam("Beta angle - min", betamin, units=['rad', 'deg'], id='$bmin', valid='$bmin>-90'),..
   CL_defParam("Beta angle - max", betamax, units=['rad', 'deg'], id='$bmax', valid='$bmax>$bmin & $bmax<90')..
);

[hmin,hmax,betamin,betamax] = CL_inputParam(desc_param);


// -----------------------------------------------------------
// results
// -----------------------------------------------------------
nbpts = 100;

tab_h = linspace(hmin,hmax,nbpts);
tab_beta = linspace(betamin,betamax,nbpts);

result = [];

for betaa = tab_beta
  sma = tab_h + %CL_eqRad;
  semi_ang_ecl = CL_gm_betaEclipse(sma,betaa*ones(sma));
  per = CL_kp_params('per',sma);
  dur_ecl = semi_ang_ecl .* per / %pi;
  result = [ result; dur_ecl/60 ];
end


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

Nmap = 64;
f.color_map = 0.5+0.5*jetcolormap(Nmap);

Noir = addcolor([1,1,1]*0);
GrisC = addcolor([1,1,1]*0.4);
GrisF = addcolor([1,1,1]*0.25);

a=gca();
CL_g_tag(a,0);

zmin=max(min(result),0); // grayplot min/max
zmax=max(result);

colorbar(zmin,zmax,colminmax=[1,Nmap],fmt="%.1f");
Sgrayplot(tab_h/1000, tab_beta*%CL_rad2deg, result', colminmax=[1,Nmap],zminmax=[zmin,zmax], colout=[0,0]);

levels = 20:10:1000;
contour2d(tab_h/1000, tab_beta*%CL_rad2deg, result', levels, style=GrisF*ones(levels));
CL_g_tag(a,1);

sublevels = 10:2:1000;
contour2d(tab_h/1000, tab_beta*%CL_rad2deg, result', sublevels, style=GrisC*ones(sublevels));
CL_g_tag(a,2);

// general setting
CL_g_stdaxes(a);

a.data_bounds = [hmin/1000,min(tab_beta*%CL_rad2deg);hmax/1000,max(tab_beta*%CL_rad2deg)];
a.tight_limits="on";
a.title.text = "Eclipse duration (mn)";
a.x_label.text = "Altitude of circular orbit (km)";
a.y_label.text = "Sun beta angle (deg)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text");
h.font_style=8;
h.font_size=2;
h.font_foreground=Noir;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible = "on";

