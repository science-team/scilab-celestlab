//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Eclipse duration as a function of altitude and beta (circular orbit). 
//> x axis : altitude (km)
//> y axis : - (function of beta angle)
//> Black curves: Sun beta angle (deg)
//> Blue curves: eclipse duration (mn)
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hmin = 500.e3;
hmax = 35000.e3;

betamin = 0*%CL_deg2rad;
betamax = 70*%CL_deg2rad;

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>0'),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin'),..
   CL_defParam("Beta angle - min", betamin, units=['rad', 'deg'], id='$bmin', valid='$bmin>-90'),..
   CL_defParam("Beta angle - max", betamax, units=['rad', 'deg'], id='$bmax', valid='$bmax>$bmin & $bmax<90')..
);

[hmin,hmax,betamin,betamax] = CL_inputParam(desc_param);


// Envelope of evolution of beta on a year (circular orbit)
nbpts = 100;

tab_h = linspace(hmin,hmax,nbpts);
sma = tab_h + %CL_eqRad;

tab_y = linspace(betamin,betamax,nbpts);

// beta = y * K
function [K] = fct_K(R,a)
  K = asin(R./a) / asin(R/a(1));
endfunction

result = [];
res_beta_deg = [];

for y = tab_y
  betaa = y * fct_K(%CL_eqRad, sma);
  semi_ang_ecl = CL_gm_betaEclipse(sma,betaa);
  per = CL_kp_params('per',sma);
  dur_ecl = semi_ang_ecl .* per / %pi;
  result = [ result; dur_ecl/60 ];
  res_beta_deg = [ res_beta_deg ; betaa * %CL_rad2deg ];
end


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

Nmap = 64;
f.color_map = 0.5+0.5*jetcolormap(Nmap);

Noir = addcolor([1,1,1]*0);
Col1F = round(0.1*Nmap);;
Col1C = round(0.25*Nmap);
Col2F = addcolor([1,1,1]*0.25);
Col2C = addcolor([1,1,1]*0.4);

a=gca();

zmin=min(result); // grayplot min/max
zmax=max(result);

colorbar(zmin,zmax,colminmax=[1,Nmap],fmt="%.1f");
Sgrayplot(tab_h/1000, tab_y*%CL_rad2deg, result', colminmax=[1,Nmap],zminmax=[zmin,zmax], colout=[0,0]);
CL_g_tag(a,0);

[levels, sublevels] = CL_autoLevels(min(result), max(result));

contour2d(tab_h/1000, tab_y*%CL_rad2deg, result', levels, style=Col1F*ones(levels));
CL_g_tag(a,1);

contour2d(tab_h/1000, tab_y*%CL_rad2deg, result', sublevels, style=Col1C*ones(sublevels));
CL_g_tag(a,2);

[levels, sublevels] = CL_autoLevels(min(res_beta_deg), max(res_beta_deg), 8, 5);

contour2d(tab_h/1000, tab_y*%CL_rad2deg, res_beta_deg', levels, style=Col2F*ones(levels));
CL_g_tag(a,3);

contour2d(tab_h/1000, tab_y*%CL_rad2deg, res_beta_deg', sublevels, style=Col2C*ones(sublevels));
CL_g_tag(a,4);

// general setting
CL_g_stdaxes(a);
a.data_bounds = [hmin/1000,min(tab_y*%CL_rad2deg);hmax/1000,max(tab_y*%CL_rad2deg)];
a.tight_limits="on";
a.title.text = "Eclipse duration (mn)";
a.x_label.text = "Altitude of circular orbit (km)";
a.y_label.text = "Sun Beta angle (deg)";

a.y_ticks =  tlist("ticks", [], []);
a.grid(2) = -1;

// adjustments
h = CL_g_select(a, "Text", 2);
delete(h);

h = CL_g_select(a, "Text");
h.font_style=8;
h.font_size=2;
h.font_foreground=Col1F;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

h = CL_g_select(a, "Text", 4);
if (~isempty(h)); delete(h); end

h = CL_g_select(a, "Text", 3);
h.font_foreground=Col2F;

h = CL_g_select(a, "Polyline", 3);
h.thickness=2;


f.immediate_drawing="on";
f.visible = "on";

