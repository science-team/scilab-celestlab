//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Eclipse duration for elliptical orbits. 
//> x axis : angle between Sun and perigee (in the orbit plane) (deg)
//> y axis : Sun beta angle (deg)
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hp = 800.e3;
ha = 20000.e3;

betamin = 0*%CL_deg2rad;
betamax = 90*%CL_deg2rad;

desc_param = list(..
   CL_defParam("Perigee altitude", hp, units=['m', 'km'], id='$hp', valid='$hp>0'),..
   CL_defParam("Apogee altitude", ha, units=['m', 'km'], id='$ha', valid='$ha>=$hp'),..
   CL_defParam("Beta angle - min", betamin, units=['rad', 'deg'], id='$bmin', valid='$bmin>=-90'),..
   CL_defParam("Beta angle - max", betamax, units=['rad', 'deg'], id='$bmax', valid='$bmax>$bmin & $bmax<=90')..
);

[hp,ha,betamin,betamax] = CL_inputParam(desc_param);


// -----------------------------------------------------------
// results
// -----------------------------------------------------------
nbpts = 80;

[sma, ecc] = CL_op_rarp2ae(ha+%CL_eqRad, hp+%CL_eqRad); 

// limit beta angle to values for which eclipses exist
betamax = min(betamax, asin(%CL_eqRad/(sma*(1-ecc))));
betamin = max(betamin, -asin(%CL_eqRad/(sma*(1-ecc))));


tab_spom = linspace(0,%pi,nbpts); // Sun to perigee angle
tab_sbeta = linspace(betamin,betamax,nbpts);

result = [];

mm = CL_kp_params('mm',sma); // mean motion

for sbeta = tab_sbeta
  [half_span,mid_pos] = CL_gm_betaEclipse(sma,sbeta,ecc,tab_spom);

  // mean anomaly beginning/end of eclipse
  anm_min = CL_kp_v2M(ecc, mid_pos - half_span - tab_spom); 
  anm_max = CL_kp_v2M(ecc, mid_pos + half_span - tab_spom); 

  dur_ecl = CL_rMod(anm_max-anm_min,0,2*%pi) / mm;
  result = [ result; dur_ecl/60 ];
end


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

Nmap = 64;
f.color_map = 0.5+0.5*jetcolormap(Nmap);

Noir = addcolor([1,1,1]*0);
GrisC = addcolor([1,1,1]*0.4);
GrisF = addcolor([1,1,1]*0.25);

a=gca();
CL_g_tag(a,0);

zmin=max(min(result),0); // grayplot min/max
zmax=max(result);

colorbar(zmin,zmax,colminmax=[1,Nmap],fmt="%.1f");
Sgrayplot(tab_spom*%CL_rad2deg, tab_sbeta*%CL_rad2deg, result', colminmax=[1,Nmap],zminmax=[zmin,zmax], colout=[0,0]);

[levels,sublevels] = CL_autoLevels(zmin,zmax,6,4);

contour2d(tab_spom*%CL_rad2deg, tab_sbeta*%CL_rad2deg, result', levels, style=GrisF*ones(levels));
CL_g_tag(a,1);

contour2d(tab_spom*%CL_rad2deg, tab_sbeta*%CL_rad2deg, result', sublevels, style=GrisC*ones(sublevels));
CL_g_tag(a,2);

// general setting
CL_g_stdaxes(a);

a.data_bounds = [0,min(tab_sbeta*%CL_rad2deg);180,max(tab_sbeta*%CL_rad2deg)];
a.tight_limits="on";
a.title.text = "Eclipse duration (mn) - " + string(hp/1000) + "x" + string(ha/1000) + "km";
a.x_label.text = "Sun to perigee angle in orbit plane (deg)";
a.y_label.text = "Sun beta angle (deg)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text");
h.font_style=8;
h.font_size=2;
h.font_foreground=Noir;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible = "on";

