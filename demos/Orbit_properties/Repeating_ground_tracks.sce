//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Repeating ground tracks:  
//> Altitudes of circular Sun-synchonous orbits (i.e. semi major axis minus 
//> equatorial radius) such that the ground tracks repeat exactly after a 
//> certain number of planet revolutions (Q). 
//> 
//> Move the mouse over the plot and click near a symbol to get more 
//> information. 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hmin = 500.e3;
hmax = 900.e3;
Qmin = 1;
Qmax = 20;
sso = 1; // sun-synchronous
inc = 0; // unused if SSO
ecc = 0; 
model = 2;

desc_param = list(..
   CL_defParam("Altitude (semi-major axis minus equ. radius) - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>=0' ),..
   CL_defParam("Altitude (semi-major axis minus equ. radius) - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin' ),..
   CL_defParam("Eccentricity", ecc, valid='$x>=0 & $x <1' ),..
   CL_defParam("Sun-synchronous orbit? (1=yes, 0=no)", sso, id='$sso', accv=[0,1], valid="$sso == 0 | $model == 2"),..
   CL_defParam("Inclination (if not sun-synchronous)", inc, units=['rad', 'deg'], valid="$sso == 1 | ($x >=0 & $x<=180)"),..
   CL_defParam("Q (number of planet revolutions / node) - min", Qmin, id='$Qmin', accv = 1:100),..
   CL_defParam("Q (number of planet revolutions / node) - max", Qmax, id='$Qmax', accv = 1:100, valid='$Qmax >= $Qmin'),..
   CL_defParam("Model (1=kepler, 2=J2)", model, accv = [1,2], id='$model')..
);

[hmin,hmax,ecc,sso,inc,Qmin,Qmax,model] = CL_inputParam(desc_param);

smaMin = %CL_eqRad + hmin;
smaMax = %CL_eqRad + hmax;

j2 = %CL_j1jn(2);
if (model == 1); j2 = 0; end

result = CL_op_searchRepeatOrbits(smaMin,smaMax,Qmin,Qmax,ecc,sso,inc,j2=j2);


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------
f=scf();
f.visible = "off";
f.immediate_drawing="off";

alt = result(:,1)-%CL_eqRad; 
plot(alt/1000, result(:,6),'d');  
[altmin, altmax] = CL_graduate(min(alt), max(alt));

a = gca();
a.data_bounds=[max(altmin, hmin)/1000,Qmin-1;altmax/1000,Qmax+1];
a.tight_limits = "on";
CL_g_stdaxes(a);

a.grid_position = "background";
a.x_label.text = 'Altitude (semi major axis minus equatorial radius) [km]';
a.y_label.text = 'Number of planet revolutions (Q)';
if (sso == 0)
  a.title.text = 'Repeating orbits (Ecc = ' + string(ecc) + ', Inc = ' + string(inc*%CL_rad2deg) + ' deg)';
else
  a.title.text = 'Sun-synchronous repeating orbits (Ecc = ' + string(ecc) + ')';
end

h = CL_g_select(a, "Polyline");
h.mark_size=6;
h.mark_background=18;
h.mark_foreground=2;

f.immediate_drawing="on";
f.visible = "on";

