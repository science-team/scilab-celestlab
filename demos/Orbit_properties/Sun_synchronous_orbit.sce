//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Inclination of Sun-synchronous orbits (circular orbits). 
//> Note that there is a maximum value for the semi major axis above which 
//> no solution exists.   
//  
// Auteur: A. Lamy
// -----------------------------------------------------------


smamin = 6800.e3;
smamax = 7400.e3;

eccmin = 0;
eccmax = 0.2;

desc_param = list(..
   CL_defParam("Semi major axis - min", smamin, units=['m', 'km'], id='$amin', valid='$amin>0'),..
   CL_defParam("Semi major axis - max", smamax, units=['m', 'km'], id='$amax', valid='$amax>$amin'),..
   CL_defParam("Eccentricity - min", eccmin, id='$emin', valid='$emin>=0 & $emin<1'),..
   CL_defParam("Eccentricity - max", eccmax, id='$emax', valid='$emax>$emin & $emax<1')..
);

[smamin,smamax,eccmin,eccmax] = CL_inputParam(desc_param);

nbpts = 50;
sma = linspace(smamin, smamax, nbpts);
ecc = linspace(eccmin, eccmax, nbpts+1);


res = [];
for e = ecc;
  inc = CL_op_ssoJ2('i',sma,e*ones(sma));
  res = [res; inc * %CL_rad2deg]; // degres
end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

f.color_map = jetcolormap(32);
Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Noir = addcolor([1,1,1]*0);

a=gca();

[levels, sublevels] = CL_autoLevels(min(res), max(res));

contour2d(sma/1000, ecc, res', levels,style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(sma/1000, ecc, res', sublevels,style=Coul2*ones(sublevels));
CL_g_tag(a,2);

// general setting
CL_g_stdaxes(a);
a.data_bounds = [smamin/1000,eccmin; smamax/1000,eccmax];
a.tight_limits="on";
a.x_label.text = "Semi major axis (km)";
a.y_label.text = "Eccentricity";
a.title.text = "Inclination for sun-synchronous orbit (deg)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text");
h.font_foreground=Coul3;
h.font_size=3;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

