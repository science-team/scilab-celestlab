//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> The geoid is an equipotential surface in a frame fixed with
//> respect to the (rotating) Earth. 
//> The equipotential value is determined knowing the height of
//> the geoid at the location given by: longitude = 0 and latitude = 0. 
//> The reference height has been obtained at: 
//> http://earth-info.nga.mil/GandG/wgs84/gravitymod/egm96/intpt.html
//> The plots show the heights of the geoid with respect to the
//> reference ellipsoid, and/or the variations of the potential 
//> on the ellipsoid (0 being the value at the reference position). 
//> 
//> Notes: 
//> - It is assumed that the computation of the potential using spherical 
//> harmonics is valid on the reference ellipsoid and slightly 
//> below. 
//> - For comparison purposes, another map can be found at: 
//> http://en.wikipedia.org/wiki/EGM96
//
// Author: A. Lamy
// -----------------------------------------------------------


// ===========================================================
// Miscellaneous functions
// ===========================================================

// ------------------------------------
// Potential: Gravity + centrifugal force
// pos: cartesian coordinates
// ------------------------------------
function [pot] = potential(pos, nmax)
  // Rotation rate of the Earth (WGS84)
  omega = 0.00007292115; 

  pot = CL_fo_sphHarmPot(pos, [nmax,nmax], inc00=%t) + ..
        CL_fo_centrifugPot(pos, [0;0;omega]);
endfunction 


// ------------------------------------
// radius on ellipsoid at latitude phi (=geocentric latitude)
// a = eq. radius, f = oblateness
// ------------------------------------
function [r] = radius_ell(phi, a, f)
  alpha = f * (2-f) / (1-f)^2;
  r = a ./ sqrt(1 + alpha * sin(phi).^2);
endfunction


// ------------------------------------
// Computes geoid heights
// Such that potential = some reference value
// delta_h: difference of (geodetic) altitude with "pos" 
// ------------------------------------
function [delta_h] = geoid_heights(pos, nmax, potref)

  pos_ell0 = CL_co_car2ell(pos); 

  args = struct(); 
  args.pos_ell0 = pos_ell0; 
  args.nmax = nmax; 
  args.potref = potref; 

  // h and I: same size (h: geodetic)
  function [dpot] = fct(h, I, args)
    pos_ell = args.pos_ell0(:,I); 
    pos_ell(3,:) = pos_ell(3,:) + h; 
    pos = CL_co_ell2car(pos_ell); 
    dpot = potential(pos, args.nmax) - args.potref; 
  endfunction
  
  // altitude range assumed: [-500m, 500m]
  h1 = pos_ell0(3,:) - 500;
  h2 = pos_ell0(3,:) + 500; 

  h = CL_fsolveb(fct, h1, h2, args, dxtol=0.1, meth="s2"); 
  delta_h = h - pos_ell0(3,:); 
    
endfunction


// ------------------------------------
// Plots potential
// lambda, phi: (geocentric) longitude, altitude (rad)
// pot(i,j) = value for lambda(i) and phi(j) 
// ------------------------------------
function plot_potential(lambda, phi, pot, text); 

  f = scf(); 
  f.visible = "off"; 
  f.immediate_drawing = "off"; 
  f.color_map = jetcolormap(100); 

  colorbar(min(pot), max(pot));

  // adjust colorbar labels
  h = CL_g_select(f, "Compound"); 
  h.parent.y_ticks.labels = string(strtod(h.parent.y_ticks.labels));

  Sgrayplot(lambda*%CL_rad2deg, phi*%CL_rad2deg, pot); 

  CL_plot_earthMap(color_id=-1);

  a = gca();
  a.title.text = text; 
  a.x_label.text = "Geocentric longitude"; 
  a.y_label.text = "Geocentric latitude"; 

  f.immediate_drawing = "on"; 
  f.visible = "on"; 

endfunction


// ------------------------------------
// Plots geoid 2d
// lambda, phi: (geocentric) longitude, altitude (rad) 
// alt(i,j) = altitude value for lambda(i) and phi(j) 
// ------------------------------------

function plot_geoid_2d(lambda, phi, height, text); 

  f = scf(); 
  f.visible = "off"; 
  f.immediate_drawing = "off"; 
  f.color_map = jetcolormap(100); 
  a = gca();

  colorbar(min(height), max(height));

  // adjust colorbar labels
  h = CL_g_select(f, "Compound"); 
  h.parent.y_ticks.labels = string(strtod(h.parent.y_ticks.labels));

  Sgrayplot(lambda*%CL_rad2deg, phi*%CL_rad2deg, height); 

  CL_plot_earthMap(color_id=-1);

  a = gca();
  a.title.text = text; 
  a.x_label.text = "Geocentric longitude"; 
  a.y_label.text = "Geocentric latitude"; 
 
  f.immediate_drawing = "on"; 
  f.visible = "on"; 

endfunction


// ===========================================================
// MAIN
// ===========================================================
deff("[n] = maxnmax()", "n = max(size(%CL_cs1nm))"); 

// max degree/order of potential
nmax = 30; 

// height of geoid / ref ellipsoid at position
// longitude = 0 and latitude = 0
dh0 = 17.16; 

// option for computation / plot (see meaning below)
iplot = 1; 

desc_param = list(..
   CL_defParam("Max degree/order of potential", nmax, valid='$x>=2 & $x <= maxnmax() & round($x) == $x'),..
   CL_defParam("Geoid height at location: longitude = 0 and latitude = 0", dh0, units=['m'], valid='$x >= -500 & $x <= 500'), ..
   CL_defParam("Plot: 1=geoid heights, 2=potential variations, 3=both", iplot, accv = 1:3) ..
);

[nmax, dh0, iplot] = CL_inputParam(desc_param);


// ------------------------------------
// Computations / plot
// ------------------------------------

// Value of equipotential
// knowing height = dh0 above ellipsoid at (lon=0,lat=0)
potref = potential([%CL_eqRad+dh0; 0; 0], nmax);

// longitude/latitude mesh
nx = 81; 
ny = 41;
lambda = linspace(-%pi, %pi, nx); 
phi = linspace(-%pi/2, %pi/2, ny); 
[Lambda, Phi] = ndgrid(lambda, phi); 
Rad = radius_ell(Phi, %CL_eqRad, %CL_obla); 

pos_sph = [Lambda(:)'; Phi(:)'; Rad(:)']; 
pos = CL_co_sph2car(pos_sph); 


// potential variations on ellipsoid
if (iplot == 2 | iplot == 3)
  pot = potential(pos, nmax); 
  nmax_str = sprintf("%d", nmax); 
  text = "Potential variations on ref. ellipsoid (m2/s2) - EGM96s ("+nmax_str+"x"+nmax_str+")"; 
  plot_potential(lambda, phi, matrix(pot, nx, ny) - potref, text); 
end


// geoid
if (iplot == 1 | iplot == 3)
  delta_h = geoid_heights(pos, nmax, potref); 
  nmax_str = sprintf("%d", nmax); 
  text = "Geoid heights / reference ellipsoid (m) - EGM96s ("+nmax_str+"x"+nmax_str+")"; 
  plot_geoid_2d(lambda, phi, matrix(delta_h, nx, ny), text); 
end


