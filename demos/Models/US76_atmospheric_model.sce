//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Defaut values
hmin = 0.e3;
hmax = 100.e3;
logscale = 0;

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>=0' ),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin & $hmax < 1000' ),..
   CL_defParam("Logarithmic scale (0=No, 1=Yes)", logscale, accv=0:1)..
);

[hmin,hmax,logscale] = CL_inputParam(desc_param);

nbpts = 200;
alt = linspace(hmin,hmax,nbpts);

[temp,pres,dens] = CL_mod_atmUS76(alt)
res = [temp;pres;dens]

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";
f.figure_size = [600,600];
//f.figure_position = [600,100];

Coul1 = 3;
Coul2 = 11;
Coul3 = 2;
Noir = addcolor([1,1,1]*0);
Gris = addcolor([1,1,1]*0.8);
GrisF = addcolor([1,1,1]*0.3);

a=gca();

tab_logflags = ["nn" , "nn" , "nn"]; 
if(logscale == 1); tab_logflags = ["nn" , "nl" , "nl"]; end   // log scale

// general setting
titles = ["Temperature" , "Pressure" , "Density" ];
x_labels = [ "Altitude (km)" , "Altitude (km)" , "Altitude (km)" ];
y_labels = ["Temperature (K)" , "Pressure (Pa)" , "Density (kg/m^3)" ];

for k = 1 : 3
	subplot(3,1,k);
	a = gca();
	a.axes_bounds = [0.05, (k-1)/3+0.03, 0.95, 0.85*(1/3)-0.03];
	plot2d(alt/1000,res(k,:),style=k);
	a.log_flags = tab_logflags(k);  
      a.title.text = titles(k);
	a.x_label.text = x_labels(k);
	a.y_label.text = y_labels(k);
	h = CL_g_select(a, "Polyline");
	h.thickness=2;
	CL_g_stdaxes(a, colg=GrisF);
	a.data_bounds = [hmin/1000,min(res(k,:));hmax/1000,max(res(k,:))];
	a.tight_limits="on";
end

f.immediate_drawing="on";
f.visible="on";

