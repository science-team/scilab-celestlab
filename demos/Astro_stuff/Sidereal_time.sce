//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Sidereal time (ECI):
//> Angle between the X_axis of the reference frame (ECI) and the Greenwich meridian 
//> in the X-Y plane. 
//> It is approximately 0 on 21 March 12UT. 
//> Variations year after year mostly are mostly due to precession. 
//
// Auteur: A. Lamy
// -----------------------------------------------------------

date_now = CL_dat_now("cal"); 
year = date_now(1);  // year number (current year)

desc_param = list(..
   CL_defParam("Year", year, valid='round($x) == $x')..
);

[year] = CL_inputParam(desc_param);

t0 = CL_dat_cal2cjd(year,01,01,0,0,0);

days = 0:5:366; // at 0h 
hours = 0:0.5:24; 

sidt = []; // sidt(i,j): i<=>hours, j<=>days 
theta = cumsum(ones(days)/length(days)) * 2*%pi;
for h = hours;
  theta = CL_rMod(CL_mod_siderealTime(days+h/24,ut1_tref=0), theta-%pi, theta+%pi); // continuity between lines
  [sidt] = [sidt; theta * %CL_rad2deg];  // degrees
end


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

a=gca();
CL_g_tag(a,0);

f.color_map = jetcolormap(32);

Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Coul0 = addcolor([0,0,0]); // Black
CoulG = addcolor([1,1,1]*0.8); // Gray

zmin=min(sidt); // grayplot min/max
zmax=max(sidt);

[levels, sublevels] = CL_autoLevels(min(sidt), max(sidt));

contour2d(days, hours, sidt', levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(days, hours, sidt', sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);


// --- adjustments 
h = CL_g_select(a, "Text",2);
CL_g_delete(h);

h = CL_g_select(a, "Text", 1);
CL_g_set(h, "text", string(CL_rMod(strtod(h.text), 0, 360)));

h = CL_g_select(a, "Text");
h.font_foreground=Coul3;
h.font_size=2;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

// --- bounds
a.data_bounds = [0,0;365,24];
a.tight_limits="on";

// --- ticks 
t_ticks = [CL_dat_cal2cjd(year*ones(1,12),1:12,ones(1,12),0,0,0)-t0];

x_grid = [t_ticks; t_ticks]; 
y_grid = [0*ones(t_ticks); 24*ones(t_ticks)];  
for j=1:4; plot2d(x_grid+j*7,y_grid, style=CoulG*ones(t_ticks)); end
CL_g_tag(a,4);
plot2d(x_grid, y_grid, style=Coul0*ones(t_ticks)); 
CL_g_tag(a,3);

h3 = CL_g_select(a, "Polyline", 3); 
h4 = CL_g_select(a, "Polyline", 4); 
h3.line_style = 3; 
h4.line_style = 3; 

t_labels = "1/" + string(1:12); 
  
a.x_ticks = tlist(['ticks', 'locations', 'labels'], t_ticks , t_labels); 
a.y_ticks = tlist(['ticks', 'locations', 'labels'], 0:3:24, string(0:3:24)); 

ECI_frame = CL_configGet("ECI_frame");
a.title.text = "ECI (=" + ECI_frame + ") - Sidereal time - Year " + string(year) + "  (deg)";
a.x_label.text = "Day / Month (+ every week from beginning of month)";
a.y_label.text = "Hours (UT1)";


CL_g_stdaxes(a);
a.grid(1) = -1; 
a.sub_ticks(1) = 0; // no substicks for a-axis 


f.immediate_drawing="on";
f.visible="on";

