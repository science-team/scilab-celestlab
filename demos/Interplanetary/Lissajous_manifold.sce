//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Lissajous manifold around a given libration point :
//> Coordinates are given in the adimensional X,Y,Z axes
//> 120 days is the duration of manifold   
// -----------------------------------------------------------


// Defaut values
bodies_system = 1;
Lpoint = 1;
Ax = 150e6;
phix = 0;
Az = 100e6;
phiz = 0;
t_min = 0;
t_max = 182.625*24*3600*7;

desc_param = list(..
   CL_defParam("Bodies system (1=Sun-EarthMoon, 2=Earth-Moon)", bodies_system, accv=1:2 ),...
   CL_defParam("Libration point (1=L1, 2=L2, 3=L3) ", Lpoint, accv=1:3 ),...
   CL_defParam("Amplitude along X axis", Ax, units=['m', 'km'], valid='$x >= 0'),...
   CL_defParam("Phase in X", phix, units=['rad', 'deg'], valid='$x >= 0'),...
   CL_defParam("Amplitude along Z axis", Az, units=['m', 'km'], valid='$x >= 0'),...
   CL_defParam("Phase in Z", phiz, units=['rad', 'deg'], valid='$x >= 0'),...
   CL_defParam("Minimum time (t)", t_min, units=['s', 'day'], valid='$x >= 0'),...
   CL_defParam("Maximum time (t)", t_max, units=['s', 'day'], valid='$x > 0')...
);

[bodies_system,Lpoint,Ax,phix,Az,phiz,t_min,t_max] = CL_inputParam(desc_param);

tab_env = ['S-EM' , 'E-M'];
tab_Lpoint = [ 'L1', 'L2', 'L3'];
env = CL_3b_environment(tab_env(bodies_system),tab_Lpoint(Lpoint));

// Adimensional values :
t_min_adim = t_min * env.OMEGA;
t_max_adim = t_max * env.OMEGA;
Az_adim = Az / env.D;
Ax_adim = Ax / env.D;

nb_dates = 100;
t_orb = linspace(t_min_adim,t_max_adim,nb_dates);



// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";
f.figure_size=[800,600];

epsilon=1e-10;
f.color_map = jetcolormap(10) * 0.8;

Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);

tab_system_names = ['(Sun-EarthMoon)' , '(Earth-Moon)'];

subplot(2,2,1);
a=gca();
a.title.text = "Lissajous orbit at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Y";

subplot(2,2,2);
a=gca();
a.title.text = "Lissajous orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Z";

subplot(2,2,3);
a=gca();
a.title.text = "Lissajous orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);
a.x_label.text = "Y";
a.y_label.text = "Z";

subplot(2,2,4);
a=gca();
a.title.text = "Lissajous orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);

[lissajous_orb,omega,nu] = CL_3b_lissajous(env,Ax_adim,phix,Az_adim,phiz,epsilon,t_orb);

  subplot(2,2,1);
  plot2d(lissajous_orb(1,:),lissajous_orb(2,:),style=1);
  subplot(2,2,2);
  plot2d(lissajous_orb(1,:),lissajous_orb(3,:),style=1);
  subplot(2,2,3);
  plot2d(lissajous_orb(2,:),lissajous_orb(3,:),style=1);
  subplot(2,2,4);
  param3d(lissajous_orb(1,:),lissajous_orb(2,:),lissajous_orb(3,:),leg="X@Y@Z",theta=87,alpha=68);
  e=gce();
  e.foreground = 1;
  // adjustments
  for j = 1 : 4
    subplot(2,2,j);
    a=gca();
    CL_g_stdaxes(a, colg=GrisF, ft=2, fl=1, fg=1);
    h=CL_g_select(a, "Polyline");
    h.thickness=2;
  end
  

f.visible="on";
f.immediate_drawing="on";
f.immediate_drawing="off";

 

// Trace des manifolds 
f=scf();
f.visible="off";
f.immediate_drawing="off";
f.figure_size=[800,600];
f.figure_position = [10,10];
f.color_map = jetcolormap(nb_dates) * 0.8;
Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);
GrisC = addcolor([1,1,1]*0.6);

tint =120*24*3600*env.OMEGA; //120 days
epsilon = 3e-4;

[div_in,conv_in,conv_out,div_out] = CL_3b_manifolds(env,lissajous_orb,t_orb,omega,epsilon,tint,['div','conv','-conv','-div']);


subplot(2,2,1);
a=gca();
a.title.text = "Lissajous manifold (convergent in) ("+tab_Lpoint(Lpoint)+") "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Y";
for i=1:size(conv_in,3)
  plot2d(conv_in(1,:,i),conv_in(2,:,i),i);
end
plot(env.gl,0,'kd');
a.children(1).children(1).mark_style=3;
a.children(1).children(1).mark_size=8;
plot(1,0,'kd');
a.children(1).children(1).mark_style=1;
a.children(1).children(1).mark_size=8;

subplot(2,2,2);
a=gca();
a.title.text = "Lissajous manifold (convergent out) ("+tab_Lpoint(Lpoint)+") "+tab_system_names(bodies_system);
for i=1:size(conv_out,3)
  plot2d(conv_out(1,:,i),conv_out(2,:,i),i);
end
a.x_label.text = "X";
a.y_label.text = "Y";
plot(env.gl,0,'kd');
a.children(1).children(1).mark_style=3;
a.children(1).children(1).mark_size=8;


subplot(2,2,3);
a=gca();
a.title.text = "Lissajous manifold (divergent in) ("+tab_Lpoint(Lpoint)+") "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Y";
for i=1:size(div_in,3)
  plot2d(div_in(1,:,i),div_in(2,:,i),i);
end
plot(env.gl,0,'kd');
a.children(1).children(1).mark_style=3;
a.children(1).children(1).mark_size=8;
plot(1,0,'kd');
a.children(1).children(1).mark_style=1;
a.children(1).children(1).mark_size=8;

subplot(2,2,4);
a=gca();
a.title.text = "Lissajous manifold (divergent out) ("+tab_Lpoint(Lpoint)+") "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Y";
for i=1:size(div_out,3)
  plot2d(div_out(1,:,i),div_out(2,:,i),i);
end
plot(env.gl,0,'kd');
a.children(1).children(1).mark_style=3;
a.children(1).children(1).mark_size=8;

// adjustments
for j = 1 : 4
  subplot(2,2,j);
  a=gca();
  CL_g_stdaxes(a, colg=GrisC, ft=2, fl=1, fg=1);
  h=CL_g_select(a, "Polyline");
  h.thickness=2;
end
f.immediate_drawing="on";
f.visible="on";


