//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> DV needed for an insertion around Mars:  
//> The satellite is initially on a hyperbolic orbit. 
//> A tangential manoeuvre is performed at the periapsis in order 
//> to reach the target orbit around the planet (defined by the altitude 
//> of the apoapsis).
// -----------------------------------------------------------


// Defaut values
hp = 500.e3;
vinfmin = 1.e3;
vinfmax = 8.e3;
tab_ha = [500 1000 2000 5000 10000 20000 100000] * 1.e3;
planet = 4;

desc_param = list(..
   CL_defParam("Altitude of periapsis", hp, units=['m', 'km'], valid='$x >= 0' ),...
   CL_defParam("Velocity at infinity - min", vinfmin, units=['m/s', 'km/s'], id='$vinfmin', valid='$vinfmin>0'  ),...
   CL_defParam("Velocity at infinity - max", vinfmax, units=['m/s', 'km/s'], id='$vinfmax', valid='$vinfmax>$vinfmin'  ),...
   CL_defParam("Levels of altitude of apoapsis", tab_ha, units=['m', 'km'], dim=-1, valid='$x >= 0'),...
   CL_defParam("Planet (2=Venus, 3=Earth, 4=Mars)", planet, accv=2:4)...
);

[hp,vinfmin,vinfmax,tab_ha,planet] = CL_inputParam(desc_param);


nbpts = 30;
tab_vinf = linspace(vinfmin,vinfmax,nbpts);


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

nb = length(tab_ha);
f.color_map = jetcolormap(min(max(nb,3),100));

Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);

a=gca();

%CL_body = CL_dataGet("body");

planets_eqrad = [%CL_body.Mercury.eqRad , %CL_body.Venus.eqRad , %CL_body.Earth.eqRad , %CL_body.Mars.eqRad];
planets_mu = [%CL_body.Mercury.mu, %CL_body.Venus.mu , %CL_body.Earth.mu , %CL_body.Mars.mu];

for k = 1:length(tab_ha);
  ha = tab_ha(k);
  sma = (2*planets_eqrad(planet) + ha + hp)/2;
  rph = planets_eqrad(planet) + hp;
  [dv] = CL_ip_insertionDv(tab_vinf, rph, sma, mu=planets_mu(planet));
  
  plot2d(tab_vinf/1000,dv/1000,style=k);
end

CL_g_legend(a, string(tab_ha/1000)+" km", header="Altitude of apoapsis");

// adjustments
CL_g_stdaxes(a, colg=GrisF);
planets = ["Mercury" , "Venus" , "the Earth" , "Mars"];
a.title.text = "DV needed for an insertion around "+planets(planet)+" (km/s)";
a.x_label.text = "Velocity at infinity (km/s)";
a.y_label.text = "DV (km/s)";

// proprietes des courbes
h=CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

