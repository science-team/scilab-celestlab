//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Halo orbits around a given libration point.
//> Coordinates are given in the adimensional X,Y,Z axes  
// -----------------------------------------------------------


// Defaut values
bodies_system = 1;
Lpoint = 1;
Az_min = 150e6;
Az_max = 450e6;
t_min = 0;
t_max = 182.625*24*3600;
direction = 1;

desc_param = list(..
   CL_defParam("Bodies system (1=Sun-EarthMoon, 2=Earth-Moon)", bodies_system, accv=1:2 ),...
   CL_defParam("Libration point (1=L1, 2=L2, 3=L3) ", Lpoint, accv=1:3 ),...
   CL_defParam("Minimum amplitude along Z axis", Az_min, units=['m', 'km'], valid='$x > 0'),...
   CL_defParam("Maximum amplitude along Z axis", Az_max, units=['m', 'km'], valid='$x > 0'),...
   CL_defParam("Minimum time (t)", t_min, units=['s', 'day'], valid='$x >= 0'),...
   CL_defParam("Maximum time (t)", t_max, units=['s', 'day'], valid='$x > 0'),...
   CL_defParam("Direction (0=prograde, 1=retrograde)",direction, accv=0:1)...
);

[bodies_system,Lpoint,Az_min,Az_max,t_min,t_max,direction] = CL_inputParam(desc_param);

tab_env = ['S-EM' , 'E-M'];
tab_Lpoint = [ 'L1', 'L2', 'L3'];
env = CL_3b_environment(tab_env(bodies_system),tab_Lpoint(Lpoint));

// Adimensional values :
t_min_adim = t_min * env.OMEGA;
t_max_adim = t_max * env.OMEGA;
Az_min_adim = Az_min / env.D;
Az_max_adim = Az_max / env.D;

nb_dates = 100;
t_orb = linspace(t_min_adim,t_max_adim,nb_dates);
nb_halo = 5;
Az = linspace(Az_min_adim,Az_max_adim,nb_halo);


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";
f.figure_size=[800,600];


f.color_map = jetcolormap(min(max(nb_halo,3),100)) * 0.8;

Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);

tab_system_names = ['(Sun-EarthMoon)' , '(Earth-Moon)'];

subplot(2,2,1);
a=gca();
a.title.text = "Halo orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Y";
plot(env.gl,0,'kd');
a.children(1).children(1).mark_style=3;

subplot(2,2,2);
a=gca();
a.title.text = "Halo orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);
a.x_label.text = "X";
a.y_label.text = "Z";
plot(env.gl,0,'kd');
a.children(1).children(1).mark_style=3;


subplot(2,2,3);
a=gca();
a.title.text = "Halo orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);
a.x_label.text = "Y";
a.y_label.text = "Z";
plot(0,0,'kd');
a.children(1).children(1).mark_style=3;



subplot(2,2,4);
a=gca();
a.title.text = "Halo orbits at "+tab_Lpoint(Lpoint)+" "+tab_system_names(bodies_system);

for k = 1 : nb_halo
  [halo_orb,omega_halo] = CL_3b_halo(env,Az(k),direction,t_orb);
  subplot(2,2,1);
  plot2d(halo_orb(1,:),halo_orb(2,:),style=k);
  subplot(2,2,2);
  plot2d(halo_orb(1,:),halo_orb(3,:),style=k);
  subplot(2,2,3);
  plot2d(halo_orb(2,:),halo_orb(3,:),style=k);
  subplot(2,2,4);
  param3d(halo_orb(1,:),halo_orb(2,:),halo_orb(3,:),leg="X@Y@Z",theta=87,alpha=68);
  e=gce();
  e.foreground = k;
  // adjustments
  for j = 1 : 4
    subplot(2,2,j);
    a=gca();
    CL_g_stdaxes(a, colg=GrisF, ft=2, fl=1, fg=1);
    h=CL_g_select(a, "Polyline");
    h.thickness=2;
  end
  f.visible="on";
  f.immediate_drawing="on";
  f.immediate_drawing="off";
end

//CL_g_legend(a, string(tab_vinf/1000)+" km/s", header="Velocity at infinity");



f.immediate_drawing="on";
f.visible="on";

