//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> DV needed to escape from a planet :  
//> The satellite is initially on a circular orbit. 
//> A tangential manoeuvre is performed to reach the escape velocity
//> (so that the velocity on the hyperbola at infinity has the desired 
//> value).
// -----------------------------------------------------------


// Defaut values
hmin = 200.e3;
hmax = 2000.e3;
tab_vinf = (1:1:8) * 1000;
planet = 3;

desc_param = list(..
   CL_defParam("Altitude of circular orbit - min", hmin, units=['m', 'km'], id='hmin', valid='hmin>=0' ),...
   CL_defParam("Altitude of circular orbit - max", hmax, units=['m', 'km'], id='hmax', valid='hmax>hmin'  ),...
   CL_defParam("Levels of velocity at infinity", tab_vinf, units=['m/s', 'km/s'], dim=-1, valid='$x > 0'),...
   CL_defParam("Planet (2=Venus, 3=Earth, 4=Mars)", planet, accv=2:4)...
);

[hmin,hmax,tab_vinf,planet] = CL_inputParam(desc_param);


nbpts = 30;
tab_h = linspace(hmin,hmax,nbpts);


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

nb = length(tab_vinf);
f.color_map = jetcolormap(min(max(nb,3),100));

Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);

a=gca();

%CL_body = CL_dataGet("body");

planets_eqrad = [%CL_body.Mercury.eqRad , %CL_body.Venus.eqRad , %CL_body.Earth.eqRad , %CL_body.Mars.eqRad];
planets_mu = [%CL_body.Mercury.mu, %CL_body.Venus.mu , %CL_body.Earth.mu , %CL_body.Mars.mu];

for k = 1:length(tab_vinf);
  vinf = tab_vinf(k);
  sma = planets_eqrad(planet) + tab_h;
  [dv] = CL_ip_escapeDv(sma, ecc=0, vinf,mu=planets_mu(planet));
  plot2d(tab_h/1000,dv/1000,style=k);
end

CL_g_legend(a, string(tab_vinf/1000)+" km/s", header="Velocity at infinity");

// adjustments
planets = ["Mercury" , "Venus" , "the Earth" , "Mars"];
CL_g_stdaxes(a, colg=GrisF);
a.title.text = "DV needed to escape from "+planets(planet)+" (km/s)";
a.x_label.text = "Altitude of circular orbit (km)";
a.y_label.text = "DV (km/s)";

// proprietes des courbes
h=CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

