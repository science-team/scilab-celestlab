//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Image of vector by 2 rotations
//> Red curve:   (normalized) image of initial vector by 1st rotation
//> Blue curves: (normalized) image of initial vector by the 2 rotations
//
// Auteur: A. Lamy
// -----------------------------------------------------------

// sphere
function plot_sphere(R)
  F = 0.9999;
  u = linspace(-%pi/2,%pi/2,37); // every 5 deg
  v = linspace(0,2*%pi,73); // every 5 deg
  x = F*R*cos(u)'*cos(v);
  y = F*R*cos(u)'*sin(v);
  z = F*R*sin(u)'*ones(v);
  plot3d2(x,y,z);
  e=gce();
  e.color_flag = 0; 
  e.color_mode = color("grey95"); //12;
  e.foreground = color("grey92"); //18;

endfunction


// trajectory
function plot_traj(traj,F,col,th)
  param3d(F*traj(1,:), F*traj(2,:), F*traj(3,:)); 
  e=gce();
  e.foreground=col;
  e.thickness=th;
endfunction


// view adjustments
function plot_settings(a)
  CL_g_stdaxes(a);
  a.x_label.text = "X";
  a.y_label.text = "Y";
  a.z_label.text = "Z";
  a.title.text = ""; 
  a.grid = [-1,-1,-1];
  d = 1.1; 
  a.data_bounds = [-d, -d, -d; d, d, d]; 
  a.tight_limits = "on"; 
  //a.axes_visible = ["off", "off", "off"]; 
  //a.box = "off"; 
  a.isoview = "on"; 
  a.rotation_angles = [65, -50]; 
endfunction


// -----------------------------------------------------------

u = ([1;1;1])'; 
n1 = 1;
n2 = 2; 
interv1 = [0, 2*%pi]; 
interv2 = [0, 2*%pi]; 

desc_param = list(..
   CL_defParam("Vector", u, dim=3),..
   CL_defParam("Axis 1", n1, accv = [1,2,3]),..
   CL_defParam("Interval 1", interv1, units=['rad', 'deg'], dim=2),..
   CL_defParam("Axis 2", n2, accv = [1,2,3]),..
   CL_defParam("Interval 2", interv2, units=['rad', 'deg'], dim=2)..
);

[u,n1,interv1,n2,interv2] = CL_inputParam(desc_param);
u = CL_unitVector(u'); 


N1 = max(ceil(abs(interv1(1)-interv1(2)) / CL_deg2rad(5)),3); 
N2 = max(ceil(abs(interv2(1)-interv2(2)) / CL_deg2rad(5)),3); 

ang1 = linspace(interv1(1), interv1(2), N1);
ang2 = linspace(interv2(1), interv2(2), N2);


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

rep0 = eye(3,3);
plot_sphere(1); 

// rotation 1
axis1 = rep0(:,n1);
q1 = CL_rot_axAng2quat(axis1, ang1); 
u1 = CL_rot_rotVect(q1, u); 
axis2 = CL_rot_rotVect(q1, rep0(:,n2)); 

// rotation 2
for k = 1 : length(ang1)
  q2 = CL_rot_axAng2quat(axis2(:,k), ang2); 
  u2 = CL_rot_rotVect(q2, u1(:,k)); 
  plot_traj(u2,1.01,col=2,th=1); 
end

// image by 1st rotation
plot_traj(u1,1.011,col=5,th=2); 


a=gca(); 
plot_settings(a); 

f.immediate_drawing="on";
f.visible="on";




