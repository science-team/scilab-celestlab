//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> View parameters as a function of elevation for the Earth.  
//> Results computed for a location on the ground. 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


// Defaut values
hmin = 500.e3;
hmax = 2000.e3;
elevmin = 0 * %CL_deg2rad;
elevmax = 90 * %CL_deg2rad;
show = 1;

levels = []; // deg
sublevels = []; // deg

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>0' ),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin' ),..
   CL_defParam("Elevation - min", elevmin, units=['rad', 'deg'], id='$elmin', valid='$elmin>=-90' ),...
   CL_defParam("Elevation - max", elevmax, units=['rad', 'deg'], id='$elmax', valid='$elmax<=90 & $elmax>$elmin' ),...
   CL_defParam("Show (1=Angle from satellite, 2=Angle from Earth centre, 3=Distance)", show, accv=1:3)...
);

[hmin,hmax,elevmin,elevmax,show] = CL_inputParam(desc_param);

nbpts = 40;
h = linspace(hmin,hmax,nbpts);
elev = linspace(elevmin,elevmax,nbpts); // elevation

// generate matrices: x = %CL_eqRad + h (rows), y = elev (columns)
[mat_elev, mat_h] = ndgrid(elev, %CL_eqRad + h); 

type_outputs = ["sat" , "cen" , "dist"];

result = CL_gm_visiParams(mat_h, %CL_eqRad*ones(mat_h), 'elev', mat_elev, type_outputs(show));


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

f.color_map = jetcolormap(32);

Coul1 = 3;
Coul2 = 11;
Coul3 = 2;
Noir = addcolor([1,1,1]*0);
Gris = addcolor([1,1,1]*0.8);
GrisF = addcolor([1,1,1]*0.3);

conv_units = [%CL_rad2deg , %CL_rad2deg , 1/1000];

[levels, sublevels] = CL_autoLevels(min(result)*conv_units(show), max(result)*conv_units(show));

a=gca();

contour2d(elev*%CL_rad2deg, h/1000, result*conv_units(show), levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(elev*%CL_rad2deg, h/1000, result*conv_units(show), sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);

// general setting
titles = [ "View angle from satellite (deg)" , "View angle from Earth centre (deg)" , "Distance (km)" ];
CL_g_stdaxes(a, colg=GrisF);
a.data_bounds = [elevmin*%CL_rad2deg,hmin/1000;elevmax*%CL_rad2deg,hmax/1000];
a.tight_limits="on";
a.title.text = titles(show);
a.x_label.text = "Elevation (deg)";
a.y_label.text = "Altitude (km)";


// adjustments
h = CL_g_select(a, "Text", 2);
delete(h);

h = CL_g_select(a, "Text", 1);
CL_g_set(h, "text", string(abs(strtod(h.text))));

h = CL_g_select(a, "Text");
h.font_foreground=Coul3;
h.font_size=3;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

