//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Difference of argument of latitude so that the radius vectors 
//> of 2 satellites are aligned (or so that the 2 satellites are at the same 
//> position if the orbits are circular). 
//> difference of RAAN: raan2 - raan1
//> difference of (true) arg. of latitude: arg_lat2 - arg_lat1 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


imin = 97 * %CL_deg2rad;
imax = 99 * %CL_deg2rad;

dgmin = 0  * %CL_deg2rad;
dgmax = 10 * %CL_deg2rad;

desc_param = list(..
   CL_defParam("Inclination - min", imin, units=['rad', 'deg'], id='$imin', valid='$imin>=0 & $imin<=180'),..
   CL_defParam("Inclination - max", imax, units=['rad', 'deg'], id='$imax', valid='$imax>$imin & $imax<=180'),..
   CL_defParam("Difference of RAAN - min", dgmin, units=['rad', 'deg'], id='$dgmin', valid='$dgmin>=-180 & $dgmin<=180'),..
   CL_defParam("Difference of RAAN - max", dgmax, units=['rad', 'deg'], id='$dgmax', valid='$dgmax>dgmin & $dgmax<=180')..
);

[imin, imax, dgmin, dgmax] = CL_inputParam(desc_param);


nbpts = 40;
tab_inc = linspace(imin,imax,nbpts);
tab_dgom = linspace(dgmin,dgmax,nbpts+1); // +1 : pour verif dimensions matrice !!

result = [];
for dgom = tab_dgom;
  [pso1,pso2,inters] = CL_gm_intersectPlanes(tab_inc,zeros(tab_inc),tab_inc,dgom*ones(tab_inc));
  res = pso2-pso1;
  ind = find (inters == 0);
  res(ind) = 0; // if no intersection : sat1 and sat2 must be at the same position
  result = [result; res];
end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

f.color_map = jetcolormap(32);

Coul0 = 1;
Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Noir = addcolor([0,0,0]);

a=gca();

[levels, sublevels] = CL_autoLevels(min(result * %CL_rad2deg), max(result * %CL_rad2deg));

contour2d(tab_dgom * %CL_rad2deg, tab_inc * %CL_rad2deg, result * %CL_rad2deg, levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(tab_dgom * %CL_rad2deg, tab_inc * %CL_rad2deg, result * %CL_rad2deg, sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);

// general setting
CL_g_stdaxes(a);
a.data_bounds = [dgmin*%CL_rad2deg,imin*%CL_rad2deg;dgmax*%CL_rad2deg,imax*%CL_rad2deg];
a.tight_limits="on";
a.title.text = "Difference of (true) arg. of lat. for alignment at intersection (deg)";
a.x_label.text = "Difference of RAAN (deg)";
a.y_label.text = "Common inclination (deg)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text");
h.font_foreground=Coul0;
h.font_size=3;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

