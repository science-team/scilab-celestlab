//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.



// Demo file for Clohessy-Wiltshire functions
// CW propagation  - Continous thrust

//input data
tini = 0;
tpv_ini = [-35000,0,6000,10.070665,0,0]';
alt_ci = 450000;
pv_closing = [-250,0,0,0,0,0]';
pv_homing = [-3500,0,0,0,0,0]';

[Hl_imp,Hl_cont] = CL_cw_hohmannLine();
periode = CL_kp_params('per',%CL_eqRad+alt_ci);

//continuous burn transfer

//Drift up to the start of homing
d = tpv_ini(3)/tan(Hl_cont)+3500;
[xp] = CL_cw_circularDrift(alt_ci,tpv_ini(3));
tfin_bal = (-tpv_ini(1)-d)/xp;
t = linspace(tini,tfin_bal,50)/86400;
pv = CL_cw_propagate(tini,tpv_ini,t,alt_ci);
traj = pv;
[moyen_mvt] = CL_kp_params(['mm'],%CL_eqRad+alt_ci);

//Homing
acc_diff = tpv_ini(3)*moyen_mvt**2/(4*%pi); //Gx = w2/4pi*Dz
gama = [acc_diff,0,0]';
//tfin = periode;  
tt = linspace(t($),t($)+periode/86400,50);
pv = CL_cw_propagate(t($),pv(:,$), tt, alt_ci, acc = gama);
traj = [traj, pv];

//Closing
acc_diff = (traj(1,$)+250)*moyen_mvt**2/(4*%pi); //Gx = w2/4pi*Dx
gama = [0,0,-acc_diff]';

ttt = linspace(tt($),tt($)+periode/86400,50);
pv = CL_cw_propagate(tt($),pv(:,$), ttt, alt_ci, acc = gama);
traj = [traj, pv]; 

//Plots
f1=scf();
f1.visible="off";
plot2d(traj(1,:)/1000,traj(3,:)/1000,2);
h = CL_g_select(gce(), "Polyline"); 
h.thickness = 2;

a = gca();
a.title.text = "Rendezvous trajectory - Continuous thrust";
a.x_label.text = "x (km)";
a.x_location  =  "bottom";
a.y_label.text = "z (km)";
a.y_location  =  "right";
a.axes_reverse  =  ["off","on","off"];

CL_g_stdaxes();
f1.visible="on";



