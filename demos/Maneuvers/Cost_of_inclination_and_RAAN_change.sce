//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Cost of modification of inclination and RAAN (circular orbit): 
//> Shows the total cost (DV >=0) needed for the orbit change,
//> and (optionally) the corresponding argument of latitude.
//> Blue curves: DV (m/s)
//> Brown curves (optional): arg of latitude (degrees)
//
// Auteur: A. Lamy
// -----------------------------------------------------------


h = 700.e3;
inc0 = 98*%CL_deg2rad;
dgmin = 0 * %CL_deg2rad;
dgmax = 1 * %CL_deg2rad;
dimin = 0 * %CL_deg2rad;
dimax = 1 * %CL_deg2rad;
show_arglat = 0;

desc_param = list(..
   CL_defParam("Altitude", h, units=['m', 'km'], id='$h', valid='$h >= 0'),..
   CL_defParam("Inclinaison", inc0, units=['rad', 'deg'], id='$inc0', valid='$inc0>=0 & $inc0<=180'),..
   CL_defParam("Delta RAAN - min", dgmin, units=['rad', 'deg'], id='$dgmin', valid='$dgmin>=-180'),..
   CL_defParam("Delta RAAN - max", dgmax, units=['rad', 'deg'], id='$dgmax', valid='$dgmax>$dgmin & $dgmax<=180'),..
   CL_defParam("Delta inclination - min", dimin, units=['rad', 'deg'], id='$dimin', valid='$inc0+$dimin>=0'),..
   CL_defParam("Delta inclination - max", dimax, units=['rad', 'deg'], id='$dimax', valid='$dimax>$dimin & $inc0+$dimax<=180'),..
   CL_defParam("Show argument of latitude (1=yes, 0=no)", show_arglat, accv=[0,1])..
);

[h, inc0, dgmin, dgmax, dimin, dimax, show_arglat] = CL_inputParam(desc_param);

nbpts = 40;
tab_dg = linspace(dgmin,dgmax,nbpts);
tab_di = linspace(dimin,dimax,nbpts);

resdv = [];
respso = [];

for di = tab_di;
  inci = inc0 * ones(tab_dg);
  incf = (inc0 + di) * ones(tab_dg);
  raani = zeros(tab_dg);
  raanf = tab_dg;
  sma = (h + %CL_eqRad) * ones(tab_dg);

  [deltav, _dv_, arg_lat] = CL_man_dvIncRaanCirc(sma,inci,raani,incf,raanf);
  resdv = [resdv; deltav];
  respso = [respso; arg_lat*%CL_rad2deg]; // degrees

end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

a=gca();
CL_g_tag(a,0);

f.color_map = jetcolormap(32);
Gris = addcolor([0,0,0]);

Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Coul4 = 30;
Coul5 = 24;

[levels, sublevels] = CL_autoLevels(min(resdv), max(resdv));

contour2d(tab_dg*%CL_rad2deg, tab_di*%CL_rad2deg, resdv', levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(tab_dg*%CL_rad2deg, tab_di*%CL_rad2deg, resdv', sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);

if (show_arglat == 1)
// arg of latitude (deg)
[levels, sublevels] = CL_autoLevels(min(respso), max(respso));

contour2d(tab_dg*%CL_rad2deg, tab_di*%CL_rad2deg, respso', levels, style=Coul4*ones(sublevels));
CL_g_tag(a,3);

contour2d(tab_dg*%CL_rad2deg, tab_di*%CL_rad2deg, respso', sublevels, style=Coul5*ones(sublevels));
CL_g_tag(a,4);
end

// general setting
CL_g_stdaxes(a);
a.data_bounds=[dgmin,dimin;dgmax,dimax] * %CL_rad2deg;
a.tight_limits="on";
a.title.text = "DV needed for a RAAN/inclination change (m/s)";
a.x_label.text = "Change of RAAN (deg)";
a.y_label.text = "Change of inclination (deg)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text", 1);
CL_g_set(h, "text", string(abs(strtod(h.text))));

h = CL_g_select(a, "Text",1);
h.font_foreground=Coul3;
h.font_size=2;
h.font_style=8;

h = CL_g_select(a, "Polyline",1);
h.thickness = 2;

if (show_arglat == 1)
// arg of lat
h = CL_g_select(a, "Text", 4);
CL_g_delete(h);

h = CL_g_select(a, "Polyline",3);
h.thickness = 2;

h = CL_g_select(a, "Text",3);
h.font_foreground=Coul4;
end

f.immediate_drawing="on";
f.visible="on";

