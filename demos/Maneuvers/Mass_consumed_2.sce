//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Mass consumed as a function of DV
//> (varying initial mass) 
//> Notes: 
//> - dm is proportional to the initial mass. 
//> - dv is forced to be greater than 1.e-6 m/s so than dm/dv can be computed. 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


dvmin = 0;
dvmax = 1000;
isp = 300.;
tab_mass0 = 500:500:3000;
restype = 3;

desc_param = list(..
   CL_defParam("DV - min", dvmin, units=['m/s'], id='$dvmin', valid='$dvmin >= 0'),..
   CL_defParam("DV - max", dvmax, units=['m/s'], id='$dvmax', valid='$dvmax > $dvmin'),..
   CL_defParam("Isp", isp, units=['s'], valid='$x > 0'),..
   CL_defParam("Levels of initial mass", tab_mass0, units=['kg'],  dim=-1, valid='$x > 0'),..
   CL_defParam("Result type (1=dm, 2=dm/m, 3=dm/dv)", restype, accv=[1,2,3])..
);

[dvmin, dvmax, isp, tab_mass0, restype] = CL_inputParam(desc_param);
dvmin = max(1.e-6, dvmin);
dvmax = max(dvmin,dvmax);

nbpts = 50;
dv = linspace(dvmin,dvmax,nbpts);

if (restype == 1)
  titrey = "Mass consumed (kg)";
elseif (restype == 2)
  titrey = "Mass consumed / Initial mass (%)";
else
  titrey = "Mass consumed / velocity increment (kg/(m/s))";
end

function [K] = fct_K(mass0)
if (restype == 1)
  K = 1;
elseif (restype == 2)
  K = 100 / mass0;
else
  K = 1.0 ./ dv;
end
endfunction

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

nb = length(tab_mass0);
f.color_map = jetcolormap(min(max(nb,3),100));
Noir = addcolor([0,0,0]);

a=gca();

dmmin = %inf;
dmmax = -%inf;

for k=1:length(tab_mass0)
  m0 = tab_mass0(k);
  dm = CL_man_consumption('dm', dv, m0, isp);
  K = fct_K(m0);
  plot2d(dv, dm.*K, style=k);
  dmmin = min(min(dm.*K), dmmin);
  dmmax = max(max(dm.*K), dmmax);
end

// general setting
a1 = CL_g_legend(a, string(tab_mass0)+" kg", header="Initial mass");

CL_g_stdaxes(a);
a.title.text = "Mass consumed (Isp=" + string(isp) + "s)";
a.x_label.text = "Velocity increment (m/s)";
a.y_label.text = titrey;

// adjustments
h = CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

