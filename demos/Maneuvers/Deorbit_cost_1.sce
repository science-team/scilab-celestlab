//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> DV needed to change the initial orbit into an orbit with a low 
//> enough (adjustable) perigee
//> Y-axis : DV
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hmin = 500.e3;
hmax = 850.e3;
tab_hp = (400:50:650) * 1000;

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>=0' ),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin' ),..
   CL_defParam("Perigee altitude", tab_hp, units=['m', 'km'], dim=-1, id='$hp', valid='$hp>=0' )..
);

[hmin,hmax,tab_hp] = CL_inputParam(desc_param);


nbpts = 100; // nombre de points en x
tab_h = linspace(hmin,hmax,nbpts);

ind = find(tab_hp < hmax);
tab_hp = tab_hp(ind);

// -----------------------------------------------------------
// results / plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

a=gca();

nb = length(tab_hp);
f.color_map = jetcolormap(min(max(nb,3),100)) * 0.95; // min 3 valeurs

Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);

dvmin = %inf;
dvmax = -%inf;

for k = 1:length(tab_hp);

  ai = %CL_eqRad + tab_h; // initial orbit
  ei = zeros(ai);
  af = (%CL_eqRad + tab_hp(k) + ai)/2; 

  [_X_, dv] = CL_man_dvSma(ai, ei, af); // returned norm ignored
  deltav = -dv(2,:); // signed: >0 if sma decreases

  dvmin = max( min(min(deltav),dvmin), 0);
  dvmax = max( max(max(deltav),dvmax), 0);

  plot2d(tab_h/1000, deltav, style=k);
end


// legends
a1 = CL_g_legend(a, string(tab_hp/1000)+" km", header="Perigee altitude");

// general setting
CL_g_stdaxes(a, colg=GrisF)
a.data_bounds = [hmin/1000,dvmin; hmax/1000,dvmax];
a.tight_limits="on";
a.title.text = "Deorbit cost for a circular orbit";
a.x_label.text = "Initial altitude (km)";
a.y_label.text = "Dv (m/s)";

// change properties
h=CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

