//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Cost of inclination change 
//> The maneuver is performed at the ascending node or descending node. 
//> The result (dv) is given as a fraction of a "reference velocity"
//> (The reference velocity is in fact: Vref = sqrt(mu/sma) 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


// -----------------------------------------------------------
// Parameters input
// -----------------------------------------------------------

dinc = 1 * %CL_deg2rad; 
tabecc = 0 : 0.1 : 0.7; 
smamin = 7000.e3; 
smamax = 42000.e3; 
pommin = 0; 
pommax = %pi; 
manpos = 1; 

desc_param = list(..
   CL_defParam("Change of inclination", dinc, units=['rad', 'deg'], valid='$x >= 0 & $x <= 180'),..
   CL_defParam("Semi major axis - min", smamin, units=['m', 'km'], id='$smamin', valid='$smamin > 0'),..
   CL_defParam("Semi major axis - max", smamax, units=['m', 'km'], id='$smamax', valid='$smamax > 0 & $smamax > $smamin '),..
   CL_defParam("Argument of perigee - min", pommin, units=['rad', 'deg'], id='$pommin'),..
   CL_defParam("Argument of perigee - max", pommax, units=['rad', 'deg'], id='$pommax', valid='$pommax > $pommin'),..
   CL_defParam("Values of eccentricity", tabecc, valid = '$x >= 0 & $x < 1', dim=-1),..
   CL_defParam("Maneuver position: 1=>asc node, 2=>desc node, 3=>minimal dv", manpos, accv = 1:3)..
);

[dinc, smamin, smamax, pommin, pommax, tabecc, manpos] = CL_inputParam(desc_param);


// -----------------------------------------------------------
// computations
// -----------------------------------------------------------

tabpom = linspace(pommin, pommax, 200); 
tabsma = linspace(smamin, smamax, 200); 

opt_manpos = ["an", "dn", "opt"];

inc = 0; 

dv_ratio = zeros(length(tabecc), length(tabpom)); 

v_mean = sqrt(%CL_mu ./ tabsma); 

for k = 1 : length(tabecc)
  ecc = tabecc(k); 
  sma = %CL_mu; // OK !!! 

  deltav = CL_man_dvInc(%CL_mu, ecc, inc, tabpom, inc + dinc, posman = opt_manpos(manpos), icheck=%f); 
  dv_ratio(k,:) = deltav; 
end


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f = scf(); 
f.visible="off";
f.immediate_drawing="off";

nbcol = max(length(tabecc), 3); 
f.color_map = jetcolormap(nbcol); 

subplot(2,1,1); 

plot2d(tabsma/1000, v_mean, style=1); 


subplot(2,1,2); 

for k = 1:length(tabecc);
  ecc = tabecc(k); 
  plot2d(tabpom*%CL_rad2deg, dv_ratio(k,:)*100, style=k); 
end



// plot adjustments

subplot(2,1,1);
a = gca(); 
a.axes_bounds = [0, 0.02, 1, 0.3]; 

a.x_label.text = "Semi major axis (km)"; 
a.title.text = "Reference velocity (m/s)"; 

h = CL_g_select(a, "Polyline"); 
h.thickness = 2; 

a.data_bounds = [min(tabsma/1000), min(v_mean); max(tabsma/1000), max(v_mean)]; 
a.tight_limits = "on"; 

CL_g_stdaxes(a);
a.margins(4) = 0.25;
a.margins(3) = 0.2;

subplot(2,1,2);
a = gca(); 
a.axes_bounds = [0, 0.34, 1, 0.64]; 
//a.margins(4) = 0.25;


// titles
a.x_label.text = "Argument of periapsis (deg)"; 
a.title.text = sprintf("Ratio: DeltaV / ref. velocity (%%) - dinc = %.1f deg, pos=%s", dinc*%CL_rad2deg, opt_manpos(manpos)); 

h = CL_g_select(a, "Polyline"); 
h.thickness = 2; 

a.data_bounds = [min(tabpom*%CL_rad2deg), min(dv_ratio*100); max(tabpom*%CL_rad2deg), max(dv_ratio*100)]; 
a.tight_limits = "on"; 

CL_g_stdaxes(a);

CL_g_legend(a, str=string(tabecc), header="Eccentricity"); 


f.immediate_drawing="on";
f.visible="on";


