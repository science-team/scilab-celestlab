//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Cost of Hohmann transfer:
//> Total cost (DV >=0) needed for the transfer
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hmin = 0.e3;       // m
hmax = 35000.e3;   // m

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin >= 0'),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax > $hmin')..
);

[hmin, hmax] = CL_inputParam(desc_param);

nbpts = 30;
tab_h = linspace(hmin,hmax,nbpts);

dv = [];
for h = tab_h;
  dga_i = %CL_eqRad + tab_h;
  dga_f = ones(tab_h) * (%CL_eqRad + h);
  [dv] = [dv; CL_man_dvHohmann(dga_i, dga_f)];

  ind = find(dga_f < dga_i);
  dv($,ind) = -dv($,ind);
end

[levels, sublevels] = CL_autoLevels(min(dv), max(dv));

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

a=gca();
CL_g_tag(a,0);

f.color_map = jetcolormap(32);

Coul1 = 5;
Coul2 = 10;
Coul3 = 2;

zmin=min(dv); // grayplot min/max
zmax=max(dv);

contour2d(tab_h/1000, tab_h/1000, dv', levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(tab_h/1000, tab_h/1000, dv', sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);

// adjustments
CL_g_stdaxes(a);

a.data_bounds = [hmin/1000,hmin/1000;hmax/1000,hmax/1000];
a.tight_limits="on";
a.title.text = "DV needed for a Hohmann transfer (m/s)";
a.x_label.text = "Initial altitude (km)";
a.y_label.text = "Final altitude (km)";

// actions sur les lignes de niveau
h = CL_g_select(a, "Text",2);
CL_g_delete(h);

h = CL_g_select(a, "Text", 1);
CL_g_set(h, "text", string(abs(strtod(h.text))));

h = CL_g_select(a, "Text");
h.font_foreground=Coul3;
h.font_size=2;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

