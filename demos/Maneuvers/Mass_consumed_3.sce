//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Mass consumed as a function of DV (varying Isp)
//> Notes: 
//> - dm is proportional to the initial mass. 
//> - dv is forced to be greater than 1.e-6 m/s so than dm/dv can be computed. 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


dvmin = 0;
dvmax = 1000;
mass0 = 1000.;
tab_isp = 200:50:400;
restype = 3;

desc_param = list(..
   CL_defParam("DV - min", dvmin, units=['m/s'], id='$dvmin', valid='$dvmin >= 0'),..
   CL_defParam("DV - max", dvmax, units=['m/s'], id='$dvmax', valid='$dvmax > $dvmin'),..
   CL_defParam("Initial mass", mass0, units=['kg'], valid='$x > 0'),..
   CL_defParam("Levels of Isp", tab_isp, units=['s'], dim=-1, valid='$x > 0'),..
   CL_defParam("Result type (1=dm, 2=dm/m, 3=dm/dv)", restype, accv=[1,2,3])..
);

[dvmin, dvmax, mass0, tab_isp, restype] = CL_inputParam(desc_param);

//> DV is silently forced to be greater than 1.e-6 m/s
dvmin = max(1.e-6, dvmin);
dvmax = max(dvmin,dvmax);

nbpts = 50;
dv = linspace(dvmin,dvmax,nbpts);

if (restype == 1)
  K = 1;
  titrey = "Mass consumed (kg)";
elseif (restype == 2)
  K = 100 / mass0;
  titrey = "Mass consumed / Initial mass (%)";
else
  K = 1.0 ./ dv;
  titrey = "Mass consumed / velocity increment (kg/(m/s))";
end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

nb = length(tab_isp);
f.color_map = jetcolormap(min(max(nb,3),100));
Noir = addcolor([0,0,0]);

a=gca();

zmin = %inf;
zmax = -%inf;

for k=1:length(tab_isp)
  isp = tab_isp(k);
  dm = CL_man_consumption('dm', dv, mass0, isp);
  plot2d(dv, dm.*K, style=k);
  zmin = min(min(dm.*K), zmin);
  zmax = max(max(dm.*K), zmax);
end

// general setting
CL_g_legend(a, string(tab_isp)+" s", header="Specific impulse");

CL_g_stdaxes(a);
//a.data_bounds = [dvmin,zmin;dvmax,zmax];
//a.tight_limits="on";
a.title.text = "Mass consumed (Mass=" + string(mass0) + "kg)";
a.x_label.text = "Velocity increment (m/s)";
a.y_label.text = titrey;

// adjustments
h = CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

