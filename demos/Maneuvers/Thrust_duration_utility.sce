//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Thrust duration (utility)
//> Computes the thust duration and the mass consummed. 
//
// Auteur: A. Lamy
// -----------------------------------------------------------


dv = 100;
isp = 300.;
mass = 1.e3;
F = 250;

desc_param_in = list(..
   CL_defParam("DV", dv, units=['m/s'], valid='$x>=0'),..
   CL_defParam("Isp", isp, units=['s'], valid='$x > 0'),..
   CL_defParam("Initial mass", mass, units=['kg'], valid='$x > 0'),..
   CL_defParam("Thrust", F, units=['N'], valid='$x > 0')..
);

[dv, isp, mass, F] = CL_inputParam(desc_param_in);

dm = CL_man_consumption('dm', dv, mass, isp);
dt = CL_man_thrustDuration('dt',dm, F, isp);

desc_param_out = list(..
   CL_defParam("DV", dv, units=['m/s'], valid=''),..
   CL_defParam("Isp", isp, units=['s'], valid=''),..
   CL_defParam("Initial mass", mass, units=['kg'], valid=''),..
   CL_defParam("Thrust", F, units=['N'], valid=''),..
   CL_defParam("Mass consumed", dm, units=['kg'], valid=''),..
   CL_defParam("Duration", dt, units=['s'], valid='')..
);

[dv, isp, mass, F, dm, dt] = CL_inputParam(desc_param_out);



