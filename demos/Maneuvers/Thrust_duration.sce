//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Thrust duration (simple plot)
//
// Auteur: A. Lamy
// -----------------------------------------------------------


dvmin = 0;
dvmax = 100;
isp = 300.;
mass = 1.e3;
F = 250;

desc_param = list(..
   CL_defParam("DV - min", dvmin,  units=['m/s'], id='$dvmin', valid='$dvmin >= 0'),..
   CL_defParam("DV - max", dvmax,  units=['m/s'], id='$dvmax', valid='$dvmax > $dvmin'),..
   CL_defParam("Isp", isp, units=['s'], valid='$x > 0'),..
   CL_defParam("Initial mass", mass, units=['kg'], valid='$x > 0'),..
   CL_defParam("Thrust", F, units=['N'], valid='$x > 0')..
);

[dvmin, dvmax, isp, mass, F] = CL_inputParam(desc_param);

nbpts = 50;
dv = linspace(dvmin,dvmax,nbpts);

dm = CL_man_consumption('dm', dv, mass, isp);
dt = CL_man_thrustDuration('dt',dm, F, isp);

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

f.color_map = jetcolormap(32);

Noir = addcolor([1,1,1]*0);
Bleu = 2;

a=gca();

plot2d(dv, dt, style=Bleu);

// general setting
CL_g_stdaxes(a);
a.data_bounds = [dvmin,min(dt);dvmax,max(dt)];
a.tight_limits="on";
a.title.text = "Thrust duration (Isp=" + string(isp) + "s, Mass=" + string(mass) +"kg, F="+string(F) + "N)";
a.x_label.text = "Velocity increment (m/s)";
a.y_label.text = "Thrust duration (s)";

// adjustments
h = CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

