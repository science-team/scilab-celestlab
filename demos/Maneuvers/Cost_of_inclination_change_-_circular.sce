//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Cost of change of inclination for a circular orbit:
//> The maneuver is performed at the ascending or descending node.  
//> NB: "altitude" means: semi major axis minus equatorial radius
//
// Auteur: A. Lamy
// -----------------------------------------------------------


hmin = 0;
hmax = 40000.e3;
dincmin = 0;
dincmax = 10 * %CL_deg2rad; 

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='$hmin', valid='$hmin>=0'),..
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='$hmax', valid='$hmax>$hmin'),..
   CL_defParam("Change of inclination - min", dincmin, units=['rad', 'deg'], id='$dincmin', valid='$dincmin>=0'),..
   CL_defParam("Change of inclination - max", dincmax, units=['rad', 'deg'], id='$dincmax', valid='$dincmax>$dincmin & $dincmax<=180')..
);

[hmin, hmax, dincmin, dincmax] = CL_inputParam(desc_param);

nbpts = 40;
tab_h = linspace(hmin,hmax,nbpts);
tab_dinc = linspace(dincmin,dincmax,nbpts);

resdv = [];

for k = 1 : length(tab_dinc);
  dinc = tab_dinc(k); 
  inci = 0; 
  incf = inci + dinc;
  sma = tab_h + %CL_eqRad;
  ecc = 0; 
  pom = 0; 

  deltav = CL_man_dvInc(sma,ecc,inci,pom,incf,posman="an"); 
  resdv(k,:) = deltav;
end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

a=gca();
CL_g_tag(a,0);

f.color_map = jetcolormap(32);
Gris = addcolor([0,0,0]);

Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Coul4 = 30;
Coul5 = 24;

[levels, sublevels] = CL_autoLevels(min(resdv), max(resdv));

contour2d(tab_h/1000, tab_dinc*%CL_rad2deg, resdv', levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(tab_h/1000, tab_dinc*%CL_rad2deg, resdv', sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);


// general setting
CL_g_stdaxes(a);
a.data_bounds=[hmin/1000,dincmin*%CL_rad2deg;hmax/1000,dincmax*%CL_rad2deg];
a.tight_limits="on";
a.title.text = "Cost of an inclination change - circular orbit (m/s)";
a.x_label.text = "Altitude (km)";
a.y_label.text = "Change of inclinaison (deg)";

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text", 1);
CL_g_set(h, "text", string(abs(strtod(h.text))));

h = CL_g_select(a, "Text",1);
h.font_foreground=Coul3;
h.font_size=2;
h.font_style=8;

h = CL_g_select(a, "Polyline",1);
h.thickness = 2;


f.immediate_drawing="on";
f.visible="on";

