//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Cost of changing the RAAN drift rate (circular orbits only)
//> Three maneuver types are possible: 
//> - semi major axis only
//> - inclination only
//> - combined semi major axis and inclination (optimal DV)
//> 
//> Note : valid for "small" increments (1st order approximation) 
// 
// Auteur: A. Lamy
// -----------------------------------------------------------

smamin = 6900.e3;
smamax = 7100.e3;

incmin = 95 * %CL_deg2rad;
incmax = 105 * %CL_deg2rad;

raandrift = 0.1 * %CL_deg2rad / 86400;
mantype = 0; 

desc_param = list(..
   CL_defParam("Semi-major axis - min", smamin, units=['m', 'km'], id='$amin', valid='$amin>=0'),..
   CL_defParam("Semi-major axis - max", smamax, units=['m', 'km'], id='$amax', valid='$amax>$amin'),..
   CL_defParam("Inclination - min", incmin, units=['rad', 'deg'], id='$imin', valid='$imin>=0 & $imin<=180'),..
   CL_defParam("Inclination - max", incmax, units=['rad', 'deg'], id='$imax', valid='$imax>$imin & $imax<=180'),..
   CL_defParam("RAAN drift rate change", raandrift, units=['rad/s', 'deg/day'], valid='$x > 0'),..
   CL_defParam("Maneuver type (1=sma, 2=inc, 0=both)", mantype, accv=[0,1,2])..
);

[smamin,smamax,incmin,incmax,raandrift,mantype] = CL_inputParam(desc_param);

nbpts = 50;
sma = linspace(smamin, smamax, nbpts);
inc = linspace(incmin, incmax, nbpts);

ecc = zeros(sma);

nmoy = CL_kp_params('mm',sma); 
V = nmoy .* sma; 

res = []; 

for i = inc;
  
  [dpomdt, dgomdt, danmdt, ddpomdtdaei, ddgomdtdaei, ddanmdtdaei] = CL_op_driftJ2(sma,ecc,i);
  
  Ca = ddgomdtdaei(1,:) .* (2 ./ nmoy); // d (dgomdt) / d DVa
  Ci = ddgomdtdaei(3,:) ./ V;           // d (dgomdt) / d DVi

  if (mantype == 1)
    C = abs(Ca); 
  elseif (mantype == 2)
    C = abs(Ci); 
  else 
    C = sqrt(Ca.^2 + Ci.^2);  
  end

  dv = %nan * ones(C); 
  if (C > 0); dv = abs(raandrift ./ C); end

  res = [res; dv];  
end

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

f.color_map = jetcolormap(32);
Coul1 = 5;
Coul2 = 10;
Coul3 = 2;
Noir = addcolor([1,1,1]*0);

a=gca();

[levels, sublevels] = CL_autoLevels(min(res), max(res));

contour2d(sma/1000, inc*%CL_rad2deg, res', levels, style=Coul1*ones(levels));
CL_g_tag(a,1);

contour2d(sma/1000, inc*%CL_rad2deg, res', sublevels, style=Coul2*ones(sublevels));
CL_g_tag(a,2);

// general setting
CL_g_stdaxes(a);
a.data_bounds = [smamin/1000,incmin*%CL_rad2deg; smamax/1000,incmax*%CL_rad2deg];
a.tight_limits="on";
a.x_label.text = "Semi major axis (km)";
a.y_label.text = "Inclination (deg)";

raandrift_degday = sprintf("%.3g", raandrift*%CL_rad2deg*86400); 
infos = ["(sma+inc)", "(sma)", "(inc)"]; 
a.title.text = "Cost (m/s) of RAAN drift change of " + string(raandrift_degday) + " deg/day " + infos(mantype+1);

// adjustments
h = CL_g_select(a, "Text", 2);
CL_g_delete(h);

h = CL_g_select(a, "Text");
h.font_foreground=Coul3;
h.font_size=3;
h.font_style=8;

h = CL_g_select(a, "Polyline", 1);
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

