//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> DV needed for an altitude change 
//> (circular orbits, Hohmann transfer)
//
// Auteur: A. Lamy
// -----------------------------------------------------------


// Defaut values
hmin = 0.e3;
hmax = 10000.e3;

tab_dh = (50:50:300) * 1000;

desc_param = list(..
   CL_defParam("Altitude - min", hmin, units=['m', 'km'], id='hmin', valid='hmin>=0' ),...
   CL_defParam("Altitude - max", hmax, units=['m', 'km'], id='hmax', valid='hmax>hmin'  ),...
   CL_defParam("Levels of altitude change", tab_dh, units=['m', 'km'], dim=-1)...
);

[hmin,hmax,tab_dh] = CL_inputParam(desc_param);


nbpts = 30;
tab_h = linspace(hmin,hmax,nbpts);


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="off";
f.immediate_drawing="off";

nb = length(tab_dh);
f.color_map = jetcolormap(min(max(nb,3),100));

Noir = addcolor([0,0,0]);
GrisF = addcolor([1,1,1]*0.4);

a=gca();

for k = 1:length(tab_dh);
  dh = tab_dh(k);
  dga_i = %CL_eqRad + tab_h;
  dga_f = dga_i + dh;
  deltav = CL_man_dvHohmann(dga_i, dga_f);
  plot2d(tab_h/1000,deltav,style=k);
end

CL_g_legend(a, string(tab_dh/1000)+" km", header="Altitude variation");

// adjustments
CL_g_stdaxes(a, colg=GrisF);
a.title.text = "DV needed for an altitude change (Hohmann) (m/s)";
a.x_label.text = "Initial altitude (km)";
a.y_label.text = "Dv (m/s)";

// proprietes des courbes
h=CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

