//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Cost of change of inclination for an elliptical orbit: 
//> The maneuver is performed at the ascending node. 
//> The perigee altitude is specified and the maneuver if performed 
//> at the opposite position (apogee). 
//> NB: 
//> - depending on the actual values, the perigee may be the apogee (and conversely)!
//> - "altitude" means: semi major axis minus equatorial radius
//
// Auteur: A. Lamy
// -----------------------------------------------------------


// -----------------------------------------------------------
// Parameters input
// -----------------------------------------------------------

hp = 700.e3; // heure locale moyenne of asc node
hamin = 0.e3; 
hamax = 2000.e3; 
dinc = 1 * %CL_deg2rad; 
tabpom = [0:30:180] * %CL_deg2rad; 

desc_param = list(..
   CL_defParam("Altitude of (peri/apo)gee", hp, units=['m', 'km'], id='$hp', valid='$hp >= 0'),..
   CL_defParam("Altitude of (apo/peri)gee - min", hamin, units=['m', 'km'], id='$hamin', valid='$hamin >= 0'),..
   CL_defParam("Altitude of (apo/peri)gee - max", hamax, units=['m', 'km'], id='$hamax', valid='$hamax > $hamin'),..
   CL_defParam("Change of inclination", dinc, units=['rad', 'deg'], valid='$x >= 0 & $x <= 180'),..
   CL_defParam("Values of argument of perigee", tabpom, units=['rad', 'deg'], dim=-1)..
);

[hp, hamin, hamax, dinc, tabpom] = CL_inputParam(desc_param);


// -----------------------------------------------------------
// computations
// -----------------------------------------------------------

ha = linspace(hamin, hamax, 200); 
[sma, ecc] = CL_op_rarp2ae(%CL_eqRad+ha, %CL_eqRad+hp); 
inc = 0; 

dv = zeros(length(tabpom), length(ha)); 

for k = 1:length(tabpom);
  pom = tabpom(k); 

  deltav = CL_man_dvInc(sma, ecc, inc, pom, inc + dinc, posman = "an", icheck=%f); 
  dv(k,:) = deltav; 
end


// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f = scf(); 
f.visible="off";
f.immediate_drawing="off";

nbcol = max(length(tabpom), 3); 
f.color_map = jetcolormap(nbcol); 

for k = 1:length(tabpom);
  pom = tabpom(k); 
  plot2d(ha/1000, dv(k,:), style=k); 
end

// plot adjustments

a = gca(); 

// titles
a.x_label.text = "Altitude of apogee (km)"; 
a.y_label.text = "DV (m/s)"; 
a.title.text = sprintf("Cost of %.1f deg inclination change (asc. node) - hp = %.0f km", dinc * %CL_rad2deg, hp/1000); 

h = CL_g_select(a, "Polyline"); 
h.thickness = 2; 

a.data_bounds = [min(ha/1000), min(dv); max(ha/1000), max(dv)]; 
a.tight_limits = "on"; 

CL_g_stdaxes(a);

CL_g_legend(a, str=string(tabpom * %CL_rad2deg), header="Arg of perigee (deg)"); 

f.immediate_drawing="on";
f.visible="on";


