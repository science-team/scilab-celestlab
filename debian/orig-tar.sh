#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=celestlab
TAR=../scilab-celestlab_$2.orig.tar.bz2

# clean up the upstream tarball
tar zxvf $3
find $DIR -type f -exec chmod -x {} \;
tar -c -j -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $TAR $origDir
    echo "moved $TAR to $origDir"
fi

exit 0
