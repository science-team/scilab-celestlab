<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_oe_convert.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_oe_convert" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_oe_convert</refname><refpurpose>Conversion of orbital elements</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [oe2,jacob] = CL_oe_convert(type_oe1,type_oe2,oe1 [,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Converts orbital elements from a given type to orbital elements of another type.</para>
<para>Available types of orbital elements are: "kep", "cir", "cireq", "equin" and "pv".</para>
<para>Orbital elements of type "pv" are position and velocity in a 6xN vector ([pos;vel])</para>
<para>See <link linkend="Orbital elements">Orbital elements</link> for more details on orbital elements.</para>
<para></para></listitem>
<listitem>
<para>Notes:</para>
<para>- mu is only needed when converting to or from "pv" orbital elements.</para>
<para>- Only the conversions available in functions CL_oe_xxx are available.
(i.e conversion to and from keplerian orbital element or cartesian orbital elements)</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>type_oe1:</term>
      <listitem><para> (string) Type of input orbital elements ("kep", "cir", "cireq", "equin" or "pv") (1x1)</para></listitem></varlistentry>
   <varlistentry><term>type_oe2:</term>
      <listitem><para> (string) Type of output orbital elements ("kep", "cir", "cireq", "equin" or "pv") (1x1)</para></listitem></varlistentry>
   <varlistentry><term>oe1:</term>
      <listitem><para> Input orbital elements (6xN)</para></listitem></varlistentry>
   <varlistentry><term>oe2:</term>
      <listitem><para> Output orbital elements (6xN)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2]. (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>jacob:</term>
      <listitem><para> (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Example 1
pos = [7000.e3;1000.e3;-500.e3];
vel = [1.e3;2.e3;7e3];
kep = CL_oe_convert("pv","kep",[pos;vel]);

// Example 2
pos = [7000.e3;1000.e3;-500.e3];
vel = [1.e3;2.e3;7e3];
[kep,jacob1] = CL_oe_convert("pv","kep",[pos;vel]);
[pv2,jacob2] = CL_oe_convert("kep","pv",kep);
pv2 - [pos;vel] // zero
jacob2 * jacob1 // identity
   ]]></programlisting>
</refsection>
</refentry>
