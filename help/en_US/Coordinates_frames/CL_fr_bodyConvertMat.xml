<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_fr_bodyConvertMat.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_fr_bodyConvertMat" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_fr_bodyConvertMat</refname><refpurpose>Transformation matrix and angular velocity from a body frame to another</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [M,omega] = CL_fr_bodyConvertMat(body,frame1,frame2,cjd [,tt_tref])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the frame transformation from <emphasis role="bold">frame1</emphasis> to <emphasis role="bold">frame2</emphasis>, which is characterized by
a frame transformation matrix <emphasis role="bold">M</emphasis> and an angular velocity vector <emphasis role="bold">omega</emphasis>. </para>
<para>By convention: </para>
<para>- multiplying <emphasis role="bold">M</emphasis> by coordinates relative to <emphasis role="bold">frame1</emphasis> yields coordinates relative to <emphasis role="bold">frame2</emphasis>. </para>
<para>- <emphasis role="bold">omega</emphasis> is the angular velocity vector of <emphasis role="bold">frame2</emphasis> wrt <emphasis role="bold">frame1</emphasis>, with coordinates in <emphasis role="bold">frame1</emphasis>. </para>
<para></para></listitem>
<listitem>
<para>Available frames are : </para>
<para>    'ICRS': International Celestial Reference System</para>
<para>    'BCI': Body Centered Inertial</para>
<para>    'BCF': Body Centered body Fixed</para>
<para></para>
<para>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</para>
<para></para>
<para>Available bodies are: "Mercury","Venus","Mars","Jupiter","Saturn","Uranus", "Neptune", "Sun" and "Moon" </para>
<para></para></listitem>
<listitem>
<para>Notes : </para>
<para>- The date "cjd" is relative to the TREF time scale. </para>
<para>- The frame names and body names are case sensitive</para>
<para>- Earth is not a valid body for this function. See CL_fr_convert.</para>
<para>- GCRS (identical to ICRS in CelestLab) is also accepted as a frame name.</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>body:</term>
      <listitem><para> (string) Name of the body. ("Mercury","Venus","Mars","Jupiter","Saturn","Uranus", "Neptune", "Sun" or "Moon") (1x1)</para></listitem></varlistentry>
   <varlistentry><term>frame1:</term>
      <listitem><para> (string) Name of the initial frame. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>frame2:</term>
      <listitem><para> (string) Name of the final frame. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>cjd:</term>
      <listitem><para> Modified (1950.0) julian day (Time scale: TREF) (1xN)</para></listitem></varlistentry>
   <varlistentry><term>tt_tref:</term>
      <listitem><para> (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>Report of the IAU/IAG working group on cartographic coordinates and rotational elements: 2009</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_mod_IAUBodyAngles">CL_mod_IAUBodyAngles</link></para></member>
   <member><para><link linkend="CL_fr_bodyConvert">CL_fr_bodyConvert</link></para></member>
   <member><para><link linkend="CL_fr_convertMat">CL_fr_convertMat</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);

// ICRS to BCF
[M,omega] = CL_fr_bodyConvertMat("Mars","ICRS","BCI",cjd);
   ]]></programlisting>
</refsection>
</refentry>
