<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_op_frozenOrbitDer.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_op_frozenOrbitDer" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_op_frozenOrbitDer</refname><refpurpose>Derivatives of eccentricity and argument of periapsis with respect to time</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [deccdt,dpomdt] = CL_op_frozenOrbitDer(sma,ecc,inc,pom [,er,mu,j1jn])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the time derivatives of eccentricity and argument of periapsis
resulting from the gravitational effects due to J2 and J3.</para>
<para>The time derivative of the argument of periapsis is undefined (%nan) if the orbit is circular (ecc = 0) or equatorial
(inc = 0 or pi).</para>
<para>(See the formulas, where: R = equatorial radius, n = keplerian mean motion)</para>
<para><inlinemediaobject><imageobject><imagedata fileref="driftJ2J3.gif"/></imageobject></inlinemediaobject></para>
<para></para></listitem>
<listitem>
<para>Warning :</para>
<para> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>sma:</term>
      <listitem><para> Semi-major axis [m] (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>ecc:</term>
      <listitem><para> Eccentricity (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>inc:</term>
      <listitem><para> Inclination [rad] (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>pom:</term>
      <listitem><para> Argument of pariapsis [rad] (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>er:</term>
      <listitem><para> (optional) Equatorial radius [m] (default is %CL_eqRad)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>j1jn:</term>
      <listitem><para> (optional) Vector of zonal coefficients J1 to Jn, troncated to J3. Default is %CL_j1jn(1:3)). (1xNz)</para></listitem></varlistentry>
   <varlistentry><term>deccdt:</term>
      <listitem><para> Time derivative of eccentricity [s^-1] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>dpomdt:</term>
      <listitem><para> Time derivative of argument of periapsis [rad/s] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) "Frozen orbits in the J2+J3 problem", Krystyna Kiedron and Richard Cook, AAS 91-426.</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
sma = [7000.e3, 73000.e3];
inc = CL_deg2rad([51.6, 91.6]);
[ecc,pom] = CL_op_frozenOrbit(sma,inc);
ecc = 0.999*ecc;
pom = 0.999*pom;
[deccdt,dpomdt] = CL_op_frozenOrbitDer(sma,ecc,inc,pom)
   ]]></programlisting>
</refsection>
</refentry>
