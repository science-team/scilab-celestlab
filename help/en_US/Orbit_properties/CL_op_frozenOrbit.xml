<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_op_frozenOrbit.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_op_frozenOrbit" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_op_frozenOrbit</refname><refpurpose>Eccentricity and argument of periapsis of a frozen orbit</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [ecc,pom] = CL_op_frozenOrbit(sma,inc [,er,j1jn])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the eccentricity and argument of periapsis of a "frozen"
orbit, so that the mean value of eccentricity and argument of periapsis remain
constant over time. </para>
<para> Freezing the orbit is possible by balancing the effects of harmonic even
and odd terms of the potential (J1=0, J2, J3, ...). </para>
<para> The argument of periapis returned is 90 degrees or -90 degrees. The eccentricity
is usually small: of the order of 1.e-3 for the Earth. </para>
<para> If j1jn are limited to J2 and J3, the frozen eccentricity is given by: </para>
<para> eccg = -0.5*sin(inc)*(Req/sma)*J3/J2  </para>
<para> NB: The relative accuracy decreases as the number of zonal terms increases.
The result can be very inaccurate for 40 terms or more. </para>
<para></para></listitem>
<listitem>
<para>Warning :</para>
<para> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>sma:</term>
      <listitem><para> Semi major axis [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>inc:</term>
      <listitem><para> Inclination [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>er:</term>
      <listitem><para> (optional) Equatorial radius [m]. Default is %CL_eqRad.</para></listitem></varlistentry>
   <varlistentry><term>j1jn:</term>
      <listitem><para> (optional) Vector of zonal coefficients J1 to Jn (1xNz or Nzx1). Default is %CL_j1jn(1:3).</para></listitem></varlistentry>
   <varlistentry><term>ecc:</term>
      <listitem><para> Mean eccentricity (1xN)</para></listitem></varlistentry>
   <varlistentry><term>pom:</term>
      <listitem><para> Mean argument of periapsis [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
sma = [7000.e3, 7300.e3];
inc = CL_deg2rad(98);
[ecc,pom] = CL_op_frozenOrbit(sma,inc)
j1jn = CL_dataGet("j1jn");
[ecc,pom] = CL_op_frozenOrbit(sma,inc,j1jn=j1jn(1:20))
   ]]></programlisting>
</refsection>
</refentry>
