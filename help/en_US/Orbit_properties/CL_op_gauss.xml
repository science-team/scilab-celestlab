<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_op_gauss.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_op_gauss" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_op_gauss</refname><refpurpose>Gauss equations</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [M, N] = CL_op_gauss(type_oe,oe,frame [,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes Gauss equations for a given type of orbital elements, in the "qsw" or "tnw" frame.</para>
<para>The function returns M and N such that d(oe)/dt = M*acc + N, where
acc = acceleration with components in "qsw", "tnw" or "inertial" frame.</para>
<para>The "inertial frame" option is only present for convenience (and is less efficient). </para>
<para></para>
<para>Available types of orbital elements are: "kep", "cir", "cireq" and "equin".</para>
<para>See <link linkend="Orbital elements">Orbital elements</link> for the definition of orbital elements</para>
<para></para>
<para>Notes: </para>
<para>- For orbital elements that have singularities (e.g Keplerian orbital elements with an eccentricity of zero)
the parameters that cannot be computed are set to %nan.</para>
<para>- For orbital elements that have singularities (e.g Keplerian orbital elements with an eccentricity of zero)
the parameters that cannot be computed are set to %nan.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>type_oe:</term>
      <listitem><para> (string) Type of input orbital elements ("kep", "cir", "cireq", "equin" or "car") (1x1)</para></listitem></varlistentry>
   <varlistentry><term>oe:</term>
      <listitem><para> Input orbital elements (6xN)</para></listitem></varlistentry>
   <varlistentry><term>frame:</term>
      <listitem><para> (string) Coordinates frame for acceleration : "qsw", "tnw" or "inertial". (1x1)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>M:</term>
      <listitem><para> Effects of acceleration on orbital elements (6x3xN)</para></listitem></varlistentry>
   <varlistentry><term>N:</term>
      <listitem><para> Mean motion (6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) Les Equations du Mouvement Orbital Perturbe - Cours de l'Ecole GRGS 2002 - Pierre Exertier, Florent Deleflie</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_oe_convert">CL_oe_convert</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Keplerian orbital elements
kep = [7000.e3;0.01;1.8;0.1;0.2;0.3];
[M,N] = CL_op_gauss("kep",kep,"qsw");

// d(kep)/dt due to some acceleration:
acc = [1e-3 ; 1e-4; -2e-3]; // in qsw frame
dkepdt = M*acc + N;

// Same orbit, circular orbital elements
[cir, dcirdkep] = CL_oe_kep2cir(kep);
[M2,N2] = CL_op_gauss("cir",cir,"qsw");

// Check consistency using jacobian:
M2 - dcirdkep*M   // --> zero
   ]]></programlisting>
</refsection>
</refentry>
