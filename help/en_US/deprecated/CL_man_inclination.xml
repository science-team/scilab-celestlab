<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_man_inclination.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_man_inclination" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_man_inclination</refname><refpurpose>Inclination maneuver (elliptical orbits) - DEPRECATED</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [dv,anv] = CL_man_inclination(ai,ei,inci,pomi,incf [,posman,icheck,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>This function is deprecated. </para>
<para>Replacement function: <link linkend="CL_man_dvInc">CL_man_dvInc</link></para>
<para></para></listitem>
<listitem>
<para>Computes the velocity increment needed to change the inclination.</para>
<para> The maneuver consists in making the orbit rotate around the line of nodes, thus changing the velocity direction at one of the nodes. </para>
<para>The output argument <emphasis role="bold">dv</emphasis> is the velocity increment vector in spherical coordinates in the "qsw" local orbital frame. </para>
<para><emphasis role="bold">anv</emphasis> is the true anomaly at the maneuver position.</para>
<para>The optional flag <emphasis role="bold">posman</emphasis> can be used to define the position of the maneuver (maneuver at ascending node, descending node, or "best choice"). </para>
<para>The optional flag <emphasis role="bold">icheck</emphasis> is used to enforce no checking
on the final inclination value. If the targetted inclination is less
than 0 or more than pi, the ascending node will rotate 180 degrees.
If icheck is true, the right ascension of the ascending node is not changed by the maneuver. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>ai :</term>
      <listitem><para> Semi major-axis [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>ei:</term>
      <listitem><para> Eccentricity (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>inci :</term>
      <listitem><para> Initial inclination [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pomi:</term>
      <listitem><para> Argument of periapsis [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>incf :</term>
      <listitem><para> Final inclination [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>posman:</term>
      <listitem><para> (optional) Flag specifying the position of the maneuver: 1 or "an" -&gt; ascending node, -1 or "dn" -&gt; descending node, 0 or "opt" -&gt; minimal |dv|. Default is at the ascending node. (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>icheck:</term>
      <listitem><para> (optional, boolean) Flag specifying if incf must be checked in the standard range for inclination values ([0, pi]). Default is %t. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>mu :</term>
      <listitem><para> (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>dv :</term>
      <listitem><para> Velocity increment vector in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>anv:</term>
      <listitem><para> True anomaly at maneuver position [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_man_biElliptic">CL_man_biElliptic</link></para></member>
   <member><para><link linkend="CL_man_hohmann">CL_man_hohmann</link></para></member>
   <member><para><link linkend="CL_man_sma">CL_man_sma</link></para></member>
   <member><para><link linkend="CL_man_hohmannG">CL_man_hohmannG</link></para></member>
   <member><para><link linkend="CL_man_apsidesLine">CL_man_apsidesLine</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
ai = 7200.e3;
ei = 0.1;
inci = 1;
pomi = 1.4;
incf = 1.1;
[dv,anv] = CL_man_inclination(ai,ei,inci,pomi,incf,posman=0)
// Check results :
anm = CL_kp_v2M(ei,anv);
kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
kep1 = CL_man_applyDv(kep,dv)
   ]]></programlisting>
</refsection>
</refentry>
