<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_ex_lyddaneInfos.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_ex_lyddaneInfos" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_ex_lyddaneInfos</refname><refpurpose>Informations about Lyddane orbit propagation analytical model</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   infos = CL_ex_lyddaneInfos(mean_kep [,er,mu,j1jn])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes data related to Lyddane orbit propagation analytical model.</para>
<para> These data are: </para>
<para> - Eccentricity vector of frozen orbit </para>
<para> - Secular drifts of some angular orbital elements </para>
<para></para>
<para>The output is a structure with the following fields:</para>
<para> - infos.eccf: Mean eccentricity of frozen orbit</para>
<para> - infos.pomf: Mean argument of periapsis of frozen orbit</para>
<para> - infos.dgomdt: Secular drift of mean right ascension of ascending node [rad/s]</para>
<para> - infos.dpsodt: Secular drift of mean argument of latitude [rad/s]</para>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Note:</emphasis></para>
<para>The outputs only depend on semi major-axis, eccentricity and inclination. </para>
<para></para></listitem>
<listitem>
<para>The orbital elements used are the following:</para>
<para><inlinemediaobject><imageobject><imagedata fileref="kep_par.gif"/></imageobject></inlinemediaobject></para>
<para></para></listitem>
<listitem>
<para>See <link linkend="CL_ex_lyddane">CL_ex_lyddane</link> for more details on the propagation model.</para>
<para></para></listitem>
<listitem>
<para>Warning :</para>
<para> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>mean_kep:</term>
      <listitem><para> Mean orbital elements (6xN).</para></listitem></varlistentry>
   <varlistentry><term>er:</term>
      <listitem><para> (optional) Equatorial radius [m] (default is %CL_eqRad)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>j1jn:</term>
      <listitem><para> (optional) Vector of zonal coefficients J1 to Jn (troncated to J6) to be used (default is %CL_j1jn(1:6)) (1xNz)</para></listitem></varlistentry>
   <varlistentry><term>infos:</term>
      <listitem><para> (structure) Data for the given orbital elements (containing (1xN) vectors)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_ex_lyddane">CL_ex_lyddane</link></para></member>
   <member><para><link linkend="CL_ex_meanLyddane">CL_ex_meanLyddane</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Example :
mean_kep = [7.e6; 1.e-2; 1.7; 0; 0; 0 ]; // sma, ecc, inc...
infos = CL_ex_lyddaneInfos(mean_kep);
   ]]></programlisting>
</refsection>
</refentry>
