<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_gm_stationVisiLocus.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_gm_stationVisiLocus" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_gm_stationVisiLocus</refname><refpurpose>Satellite position for visibility in specified local direction</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pos_sat] = CL_gm_stationVisiLocus(station, azim, elev, rsat [, er, obla])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the position of the satellite on a sphere (with a given radius) given the direction
of the satellite in the local frame of some location. </para>
<para></para>
<para><emphasis role="bold">station</emphasis> is a location defined by its elliptical (geodetic) coordinates. </para>
<para><emphasis role="bold">azim</emphasis> (azimuth, posive towards West) and <emphasis role="bold">elev</emphasis> (elevation) define the direction
of the satellite in the local (topocentric North) frame. The corresponding unit vector in
spherical coordinates is [azim; elev; 1]. </para>
<para><emphasis role="bold">pos_sat</emphasis> is the position on the sphere of radius <emphasis role="bold">rsat</emphasis> in cartesian coordinates. </para>
<para></para>
</listitem>
<listitem>
<para><emphasis role="bold">Notes:</emphasis></para>
<para>This function can be used to compute "visibility circles". </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>station:</term>
      <listitem><para> Station position, in elliptical (geodetic) coordinates  [long,lat,alt] [rad,m] (3x1 or 3xN)</para></listitem></varlistentry>
   <varlistentry><term>azim:</term>
      <listitem><para> First spherical coordinate of the pointing direction in the topocentric North frame [rad] (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>elev:</term>
      <listitem><para> Second spherical coordinate of the pointing direction in the topocentric North frame [rad] (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>rsat:</term>
      <listitem><para> Satellite radius (= distance from body center) [m] (1x1 or 1xN)</para></listitem></varlistentry>
   <varlistentry><term>er :</term>
      <listitem><para> (optional) Equatorial radius (default is %CL_eqRad) [m] (1x1)</para></listitem></varlistentry>
   <varlistentry><term>obla :</term>
      <listitem><para> (optional) Oblateness (default is %CL_obla) (1x1)</para></listitem></varlistentry>
   <varlistentry><term>pos:</term>
      <listitem><para> Satellite position (cartesian coordinates) [m] (3xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_gm_stationPointing">CL_gm_stationPointing</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
station = [10 * %pi/180; 45 * %pi/180; 0];
azim = linspace(0, 2*%pi, 100);
rsat = 8000.e3;

// Elevation = constant value
elev = 10 * %pi/180;
pos_sat = CL_gm_stationVisiLocus(station, azim, elev, rsat);

// Elevation = f(azimuth)
elev = 10 * (%pi/180) * (1+sin(4*azim));
pos_sat2 = CL_gm_stationVisiLocus(station, azim, elev, rsat);

scf();
CL_plot_earthMap(color_id=color("grey60"));
CL_plot_ephem(pos_sat, color_id=2);
CL_plot_ephem(pos_sat2, color_id=5);
   ]]></programlisting>
</refsection>
</refentry>
