<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_chebyshevEval.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_chebyshevEval" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2013/03/27 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_chebyshevEval</refname><refpurpose>Evaluation of Chebyshev polynomials (of first or second kind)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [y, ydot, y2dot] = CL_chebyshevEval(x, n, kind)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Evaluates Chebyshev polynomials of first or second kind at given abscissae.</para>
<para>The polynomials are defined as follows:</para>
<para>P0(x) = 1  (degree 0)</para>
<para>P1(x) = kind * x, with kind = 1 or 2  (degree 1)</para>
<para>Pn(x) = 2 * x * Pn-1(x) - Pn-2(x), for n &gt;= 2 </para>
<para></para>
<para>The function evaluates the polynomials of degree <emphasis role="bold">n</emphasis> at each abscissa <emphasis role="bold">x</emphasis>. <emphasis role="bold">y</emphasis>(i,j) is the value of the polynomial of degree n(i) at x(j).</para>
<para>The function optionally evaluates the first and second derivatives of the polynomials.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x:</term>
      <listitem><para> Abscissae (1xN)</para></listitem></varlistentry>
   <varlistentry><term>n:</term>
      <listitem><para> (integer) Degrees of the polynomials in [0,100] (1xP)</para></listitem></varlistentry>
   <varlistentry><term>kind:</term>
      <listitem><para> (optional) 1 for first kind, 2 for second kind. Default is 1 (1x1)</para></listitem></varlistentry>
   <varlistentry><term>y:</term>
      <listitem><para> Values of Chebyshev polynomials (PxN)</para></listitem></varlistentry>
   <varlistentry><term>pdot:</term>
      <listitem><para> Values of polynomials first derivatives (PxN)</para></listitem></varlistentry>
   <varlistentry><term>p2dot:</term>
      <listitem><para> Values of polynomials second derivatives (PxN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_chebyshevPoly">CL_chebyshevPoly</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Evaluate Chebyshev polynomials (degrees 0 to 5) of first kind
x = linspace(-1,1,11);
y = CL_chebyshevEval(x, 0:5)
   ]]></programlisting>
</refsection>
</refentry>
