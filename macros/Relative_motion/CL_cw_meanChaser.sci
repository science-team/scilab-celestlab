//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Zm] = CL_cw_meanChaser(Z0,dX0,alt, er,mu)
// Mean chaser
//
// Calling Sequence
// [Zm] = CL_cw_meanChaser(Z0,dX0,alt [,er,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the so-called "mean chaser" which is the relative altitude of 
// the chaser that corresponds to the mean drift resulting from an impulsive maneuver 
// in the tangential direction.</p> 
// <p>The local orbital reference frame tied to the target is the LVLH (See <link linkend="CL_fr_lvlhMat">CL_fr_lvlhMat</link>).</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="meanchaser.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// Z0: Altitude of chaser at initial time in target frame [m] (1xN)
// dX0: First component of chaser velocity in target frame at initial time [m/s] (1xN)
// alt: Target altitude (from planet surface) [m] (1xN)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// Zm: Mean chaser [m] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_kp_params
// CL_cw_twoImpulse
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, 16.3.2.4.2
//
// Examples
// Z0 = [30 20 15];
// dX0 = [7 8 9];
// alt = [260000 180000 300000];
// zm = CL_cw_meanChaser(Z0,dX0,alt);
// zm = CL_cw_meanChaser(Z0,dX0,alt(1));
// zm = CL_cw_meanChaser(Z0,dX0(1),alt(3));
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('mu','local') then mu=%CL_mu; end

omega = CL_kp_params('mm',er+alt,mu)
Zm = 4.0 * Z0 - 2.0 * dX0 ./ omega

endfunction
