//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [hl_imp,hl_cont] = CL_cw_hohmannLine()
// Hohmann line
//
// Calling Sequence
// [Hl_imp,Hl_cont] = CL_cw_hohmannLine()
//
// Description
// <itemizedlist><listitem>
// <p>Gives the Hohmann line angle for a two-impulse and for a continuous burn transfer.</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="hohmannline.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// hl_imp: Hohmann line for two-impulse transfer [rad]
// hl_cont: Hohmann line for continuous burn transfer [rad]
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cw_twoImpulse
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, 16.3.3.1.1 and 16.3.3.1.2
//
// Examples
// [Hl_imp,Hl_cont]= CL_cw_hohmannLine()
//

// Declarations:


// Code:

[lhs,rhs] = argn()

hl_imp = atan(4/3/%pi)

if lhs > 1
  hl_cont = atan(2/3/%pi)
end

endfunction


