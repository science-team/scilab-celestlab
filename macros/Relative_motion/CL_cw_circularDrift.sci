//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [xp] = CL_cw_circularDrift(alt,z0, er)
// Clohessy-Wiltshire circular drift
//
// Calling Sequence
// xp = CL_cw_circularDrift(alt,z0 [,er])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the relative drift on a circular orbit. </p>
// <p>The chaser is on a circular orbit, in the same plane as the target. </p>
// <p>The local orbital reference frame tied to the target is LVLH (See <link linkend="CL_fr_lvlhMat">CL_fr_lvlhMat</link>).</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="circular_drift.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p> 
// <p> The relative motion is done at constant speed (dX0/dt). </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="circular_drift2.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// alt: Target altitude [m] (1xN)
// z0: Chaser relative altitude [m] (1xN)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// xp: Relative velocity [m/s] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cw_ballisticPropa
// CL_cw_twoImpulse
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, 16.3.2.4.1
//
// Examples
// alt_ci=[450000,460000];
// z=1000;
// [xp]=CL_cw_circularDrift(alt_ci,z)
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end

// Code:
if ~exists('er','local') then er=%CL_eqRad; end

omega = CL_kp_params('mm',alt+er)
xp = 1.5.*omega.*z0

endfunction
