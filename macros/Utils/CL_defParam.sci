//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [par] = CL_defParam(text,val,id,units,dim,typ,accv,valid)
// Definition of parameter structure for CL_inputParam
//
// Calling Sequence
// par = CL_defParam(text,val,id,units,dim,typ,accv,valid)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Defines a parameter structure. </p>
// <p>Notes: </p>
// <p> - All the arguments are optional! </p>
// <p> - For all arguments: "empty value" ([]) is equivalent to "default value". </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// text: Text defining the parameter. Empty string by default. 
// val: Initial value (given in internal unit). 0 or " " (empty string) or current UTC date by default. 
// id: Identifier (string) useable in the 'valid' expression. Empty string by default. 
// units: [unit1, [unit2]] (strings), where unit1 is the internal unit (unit of val), and unit2 the unit used for input (unit2 is the same as unit1 by default). By default: no units. If unit2 is defined, CL_unitConvert is used to convert from unit1 to unit2. 
// dim: Number of expected values. Default is 1. A value of -1 means 'any number including 0' (vector).
// typ: Type of parameter: "r": real, "s": string, "cal": calendar format (string): year/month/day [hours:minutes:seconds]). 
// accv: Vector (1xN) of accepted values. By default: [], meaning that everything is accepted. NB: if units are defined, values in accv must be expressed in 'input' units (i.e. unit2). 
// valid: Expression (string) used to check the validity of the value. The expression used should be compatible with 'find' if many values are expected (i.e. if dim <> 1). NB: if units are defined, the values are checked while expressed in 'input' unit (i.e unit2), not internal unit. 
// par: Parameter structure. 
//
// Authors
// CNES - DCT/SB (AL)
//
// See also
// CL_inputParam
//
// Examples
// par=CL_defParam("param 1", val=1, accv=1:10);
//
// par=CL_defParam("param 2", val=[1,2], ..
// units=["m", "km"], valid="$x == round($x)", dim=-1);
//
// par=CL_defParam("param 3",val=2000,units=['m', 'km']);
//
// par=CL_defParam("param 4", val="2012/1/1 12:0:0", typ="cal");


// Declarations:


// Code:

  // remove spaces inside strings
  // str: vector of strings
  function [str2] = strip(str)
    str2 = str;
    for i = 1 : size(str, "*")
       str2(i) = strcat(tokens(str(i), " "), "");
    end
  endfunction

  // text: "" by default
  if (~exists("text", "local")); text = []; end
  if (text == []); text = ""; end
  
  // type = "r" (real) by default
  if (~exists("typ", "local")); typ = []; end
  if (typ == []); typ = "r"; end

  // check type
  if (typ <> "r" & typ <> "cal" & typ <> "s")
    CL__error("Type not managed");
  end

  // Default value: type dependent
  if (~exists("val", "local")); val = []; end
  if (val == [])
    if (typ == "r"); val = 0; end
    if (typ == "s"); val = ""; end  
    if (typ == "cal")
      // current date/time (TREF)
      cjd = CL_dat_now();
      val = CL_dat_cal2str(CL_dat_cjd2cal(cjd));
    end 
  end
  if (size(val,1) > 1)
     CL__error("Invalid argument: val");
  end
 
  unitf = []; // unit conversion factor
  if (~exists("units", "local")), units = []; end
  if (units <> [])
    if (typeof(units) <> "string" | size(units,1) <> 1 | size(units,2) > 2 )
      CL__error("Invalid argument: unit)");
    elseif (size(units,2) == 1)
      units = stripblanks([units(1), units(1)], %t);
    else
      units = stripblanks(units, %t); // remove spaces
      unitf = CL_unitConvert(1, units(2), units(1));
      if isempty(unitf)
         CL__error("Units (" + strcat(units,",") + ") not handled");
      end
    end
  end

  // identifier
  if (~exists("id", "local")); id = []; end
  if (id <> [])
    if (typeof(id) <> "string" | size(id,'*') <> 1)
       CL__error("Invalid argument: id)");
    end
  end

  // dimension
  if (~exists("dim", "local")); dim = []; end
  if (dim == [])
    if (typ == "r" | typ == "s") 
    dim = 1; // one value
  end
  end
  if (typeof(dim) <> "constant")
     CL__error("Invalid type: dim");
  end
  if (dim <> [])
  if (length(dim) <> 1 | dim <> round(dim) | dim < -1)
      CL__error("Invalid value: dim");
    end
  end

  // accepted values
  if (~exists("accv", "local")); accv = []; end  // no restriction

  // validity code
  if ~exists("valid", "local") valid = []; end


  // consistency
  if (dim <> [] & dim <> -1 & size(val,2) <> dim)
     CL__error("Invalid number of values");
  end
  if (typ == "cal" & (dim <> [] | accv <> [] | units <> []))
     CL__error("Inconsistency (calendar type)");
  end
  if (typ == "s" & units <> [])
     CL__error("Inconsistency (string type)");
  end
  if (typ == "s" & dim <> 1)
     CL__error("Inconsistency (string / dim)");
  end
  
  par = struct(); 

  par.text   = text;
  par.val    = val;
  par.id     = id;
  par.units  = units;
  par.dim    = dim;
  par.typ    = typ;
  par.accv   = accv;
  par.valid  = valid;
  par.unitf  = unitf;

endfunction
