//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function AT = %hm_t(A)
// transposition of hypermatrix
// A is considered as a set of matrices : A = [A1, A2, ...Ak] 
// dimension of A : NxPxK
// AT is defined as: AT = [A1', A2'... Ak']
// dimension of AT : PxNxK
// NOTA: %hm_t has no default definition in Scilab

// Declarations:

// Code:

  if (size(A.dims,'*') ~= 3)
     CL__error("Only hypermatrices with 3 dimensions are handled");
  end

  [N,P,K] = size(A);
  
  AT = hypermat([P,N,K]); 

  if (P <= N)  
    for (i=1:P)
      AT(i,:,:) = A(:,i,:);
    end
  else
    for (j=1:N)
      AT(:,j,:) = A(j,:,:);
    end
  end 


endfunction

