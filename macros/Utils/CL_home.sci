//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [path] = CL_home()
// CelestLab home directory
//
// Calling Sequence
// path = CL_home()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Returns the name of the directory where CelestLab has been installed. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// path: (string) CelestLab home directory (1x1)
//
// Authors
// CNES - DCT/SB
//
// Examples
// CL_home() 


// Declarations:


// Code: 

// this function's (source) path
my_name = "CL_home"; 
my_path = get_function_path(my_name); 

// up 3 directories to get home directory
path = dirname(dirname(dirname(my_path))); 

// check path OK (in case...)
if (path == "" | path == "." | isempty(path))
  CL__error("Invalid CelestLab home directory")
end

// long path name (if windows)
if (getos() == "Windows")
  path = getlongpathname(path); 
end

endfunction


