//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [x_rad] = CL_deg2rad(x_deg)
// Degrees to radians unit conversion 
//
// Calling Sequence
// x_rad = CL_deg2rad(x_deg)
//
// Description
// <itemizedlist><listitem>
// <p>Conversion from degrees to radians.</p>
// <p>Note: %CL_deg2rad can also be used.</p>
// </listitem></itemizedlist>
//
// Parameters
// x_deg: Quantity expressed in degrees (PxNxK)
// x_rad: Quantity expressed in radians (PxNxK)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_rad2deg
//
// Examples
// CL_deg2rad(45)

// Declarations:
global %CL__PRIV; 
DEG2RAD = %CL__PRIV.DATA.deg2rad;

// Code:
x_rad = x_deg * DEG2RAD;

endfunction
