//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [values] = CL_covDraw(means,cov,num)
// Random values from covariance 
//
// Calling Sequence
// values = CL_covDraw(means,cov,num)
//
// Description
// <itemizedlist><listitem>
// <p>Returns randomly drawn samples of the random variable X, knowing the expectation of X and its 
// covariance matrix. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// means : Vector of expectations (Nx1)
// cov: Covariance matrix (NxN)
// num : (integer) Number of random samples to draw
// values : Matrix containing the random samples. Each sample is a column vector. The number of columns is num. (NxP)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cov2cor
//
// Examples
// // draw 10000 position samples knowing the covariance matrix.
// pos =  [-1877901; -3909428; -5026025]; //position
// cov = [1.0, -0.46, -0.44;-0.46, 1.0, -0.43;-0.44, -0.43, 1.0];
// [drawn_pos] = CL_covDraw(pos,cov,10000);  // drawn values 
//
// // estimate mean values and covariance from samples
// [pos2, cov2] = CL_stat(drawn_pos); 
//

// Declarations:


// Code:

[cor,sd] = CL_cov2cor(cov);
R = chol(cor)';
X = rand(size(sd,1),num,'normal');
values = (R*X).*(sd.*.ones(1,num)) + means.*.ones(1,num);

endfunction
