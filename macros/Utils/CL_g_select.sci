//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [h] = CL_g_select(root,typ,tag)
// Selection of graphic entities
//
// Calling Sequence
// h = CL_g_select(root,typ,tag)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Selects graphic entities according to 2 criteria: type of graphic entity and tag (user_data value).</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// root: (optional) Root of the hierarchy of graphic entities to be examined. Can also be an array of handles. Default = current axes (gca()). 
// typ: (optional) Type (string) of graphic entity: "Polyline", "Text", "Legend", etc... Default is '*', meaning 'all'.
// tag: (optional) User_data value (see <link linkend="CL_g_tag">CL_g_tag</link>). Default is '*', meaning 'any value'.  
// h: Vector of the selected graphic entities.
//
// Authors
// CNES - DCT/SB (AL)
//
// See also
// CL_g_tag
//
// Examples
// f=scf();
// a=gca();
// x = 0:0.1:5;
// plot(x, x.^2);
// CL_g_tag(a,1);
// plot(x, x.^3, '-r');
// CL_g_tag(a,2); 
// h = CL_g_select(a, "Polyline"); 
// h.thickness = 2;
// h = CL_g_select(a, "Polyline", 2); 
// h.thickness = 4;


// Declarations:
function [h] = select1(root, typ, tag)

  h = []; 

  root_type = get(root, "type"); 
  root_ud = get(root, "user_data"); 

  if ((typ == '*' | root_type == typ) & ..
      (tag == '*' | root_ud == tag))
    h = [h, root]; 
  end

  n = length(root.children);

  for k = 1:n
    h1 = select1(root.children(k), typ, tag);
    h = [h, h1];
  end

endfunction


// Code:

  if (~exists('root', 'local')); root=gca(); end
  if (~exists('typ', 'local')); typ = '*'; end
  if (~exists('tag', 'local')); tag= '*'; end

  h = [];

  if (root == [])
    return; 

  elseif (length(root) > 1)
    for k = 1 : length(root)
      h1 = select1(root(k), typ, tag); 
      h = [h, h1];
    end

  else
    h = select1(root, typ, tag); 

  end

endfunction
