//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [hl] = CL_g_legend(h,str, pos,header,new_axes)
// Defines a legend adequately positioned with optional title
//
// Calling Sequence
// hl = CL_g_legend(h,str [,pos,header,new_axes])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Defines a legend positioned adequately by changing some properties of the axes it is derived from (right margin).</p>
// <p>A title may be optionally defined.</p>
// <p>The legend is optionally created in a new axes entity in order to avoid having the title
// move when zooming in/out. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// h: Graphic handle of an Axes entity or vector of handles on polyline entities (same as legend). Default = current axes (gca()).  
// str: Strings used for the legend (1xN or Nx1: as many as there are 'Polyline' entities in 'a'). 
// pos: (optional) Position of the legend: [x,y], where x: (normalized coordinates) distance from the left, y: (normalized coordinate) distance from the top. Default is [0.75, 0.2]. 
// header: (optional) Title of the legend.
// new_axes: (optional) %t if the legend must be created in a new axes entity. Default is %t if 'header' is defined, else it is %f. 
// hl: Handle of the legend entity created. 
//
// Authors
// CNES - DCT/SB (AL)
//
// Examples
// f=scf();
// a=gca();
// x = 0:0.1:5;
// y = x;
// plot(x, y);
// plot(x, 2*y, '-r');
// CL_g_legend(a, str=["plot 1", "plot 2"]);


// Declarations:


// Code:

  if (~exists('h', 'local'))
     h=gca();
  end

  if ~exists('pos', 'local')
    pos = [0.75, 0.2]; 
  end

  if ~exists('header', 'local')
    header = []; 
  end

  if ~exists('new_axes', 'local')
    new_axes = %t; 
    if isempty(header); new_axes = %f; end
  end

  if (new_axes) 
    x = 0; // position relative to new axes
  else
    x = pos(1); 
  end
  y = pos(2);

  hl = legend(h, str);

  hl.legend_location = 'by_coordinates';
  hl.position = [x,pos(2)];
  hl.fractional_font = "on"; 
  hl.font_size = 2.;
  hl.line_mode = "off"; // no line drawn

  a = gca(); // current axes
  a.margins(2) = (1 - pos(1)) + 0.02; 


  if (new_axes)
    a1 = newaxes();
    a1.axes_bounds = a.axes_bounds;
    a1.axes_bounds(1) = a.axes_bounds(1) + a.axes_bounds(3) * pos(1); // shifted along x
    a1.filled = "off";
    relocate_handle(hl, a1); 
    swap_handles(a1, a);
  else
    a1 = a; 
  end

  sca(a1); // change current axes

  // Title to legend: 
  if ~isempty(header)

    dim = a1.parent.axes_size; // width, height of axes in pixels
    bd = a1.axes_bounds; // left, top, width, height (normalized)
 
    xpix = dim(1) * (bd(1) + x * bd(3)); 
    ypix = dim(2) * (bd(2) + y * bd(4)); 

    // "user" coordinates
    [xu, yu] = xchange(xpix, ypix, "i2f");
  
    for k = size(header, 2):-1:1
      xstring(xu, yu, header(k)); 
      e = gce();
      e.clip_state = "off"; 
      e.font_size = 2; 
      e.font_style = 8; 
      rect = stringbox(e);
      yu = max(rect(2,:)); // y must increase towards top
    end

  end

  sca(a); // restore axes

endfunction
