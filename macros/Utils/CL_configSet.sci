//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [] = CL_configSet(name, val)
// Sets a CelestLab's configuration parameter
//
// Calling Sequence
// CL_configSet(name, val)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Set a CelestLab's configuration parameter. </p>
// <p>Available configuration parameters are "display_menu", "warning_mode", 
// "verbose_mode", "warn_deprecated" and "ECI_frame".</p>
// <p>See <link linkend="Configuration">Configuration</link> for more details on 
// the configuration parameters.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (string) Name of the configuration parameter. (1x1)
// val: (string) Value of the configuration parameter. (1x1)  
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_configGet
//
// Examples
// CL_configSet("warning_mode", "silent");

// Declarations:
global %CL__PRIV;
      
// Code
if (typeof(name) <> "string" | typeof(val) <> "string")
  CL__error("Invalid inputs (strings expected)");
end

if (size(name, "*") <> 1 | size(val, "*") <> 1)
  CL__error("Invalid inputs (strings expected)");
end


// NB: %CL__PRIV.PREF must have been created before.
// Done by CL__configInit in etc/celestlab.start file

// Check that %CL__PRIV is a structure and %CL__PRIV.PREF exists
if (~isstruct(%CL__PRIV)); 
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); 
end
if (~isfield(%CL__PRIV, "PREF"))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab.")
end

// get the configuration description
config_desc = CL__configGetDesc(); 

// Check that name of config param exists
// ind = index of "name" in config_desc
ind = find(name == config_desc.names)
if (ind == [])
  CL__error("Invalid configuration parameter");
end

// Check that config parameter can be modified
if (~config_desc.settable(ind))
  CL__error("This configuration parameter cannot be modified by this function. See help for more details.");
end

// Check that the new value for this config param is valid
if (find(val == config_desc.accval(ind)) == [])
  CL__error("Invalid value for this configuration parameter");
end

// Set the new value
%CL__PRIV.PREF(name) = val;

endfunction
