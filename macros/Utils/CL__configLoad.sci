//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Loads configuration file an returns a structure of 
// (key, value) pairs
// => error if all names and values are not correct

function [conf] = CL__configLoad()

// get the configuration description
config_desc = CL__configGetDesc(); 

fname = config_desc.fpath; 

if (~isfile(fname))
  CL__error("Configuration file not found");
end

// Intialize configuration structure
// (variable name "conf" must be the same as in configuration file) 
conf = struct();

// Execute file (fills "conf" structure)
err = exec(fname, -1, 'errcatch');
if (err <> 0)
  CL__error("Error loading configuration file");
end


// Check that all configuration parameters exist in conf
for (name = config_desc.names)
  if (~isfield(conf, name))
    CL__error("Configuration parameter ''" + name + "'' missing in configuration file");
  end
end

// Check that all fields in conf are valid (name is correct)
for (name = matrix(fieldnames(conf),1,-1))
  if (find(name == config_desc.names) == [])
    CL__error("Invalid parameter name (''" + name + "'') in configuration file");
  end
end

// Check the values
for (name = matrix(fieldnames(conf),1,-1))
  ind = find(name == config_desc.names);  
  if (find(conf(name) == config_desc.accval(ind)) == [])
    CL__error("Invalid value for parameter ''" + name + "'' in configuration file");
  end
end


endfunction
