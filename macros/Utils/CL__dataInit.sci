//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [] = CL__dataInit()
// Loads CelestLab data (constants) from files
//
// Calling Sequence
// CL__dataInit()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Loads CelestLab data (constants) from files.</p>
// <p>The function loads data from files "physical_data.scd", "constants.scd", "time_data.scd" and "solar_system_data.scd" 
// located in CL_home()/data/</p>
// <p></p>
// <p>Notes: 
// - This function initializes the global variable %CL__PRIV.DATA and 
//   is supposed to be run only once, when CelestLab starts.</p>
// - A call to CL__configInit should have been done before calling this function.
//   Reason: some data initialization (see physical_data.scd) depend on the configuration of CelestLab.</p>
// </listitem>
// </itemizedlist>
//
// Authors
// CNES - DCT/SB
//
// See also
// CL__configInit
// CL_init


// Declarations:

// Code:

// Internal function: Loading of data 
// returns structure containing all the data
function [data] = dataInit_load(nomfic)
  data = struct();
  //-- fprintf(%io(2), "CelestLab: loading %s\n", nomfic);  
  err = exec(nomfic, -1, 'errcatch');
  if (err <> 0)
    CL__error("Error loading file: " + nomfic);
  end
endfunction


// --------------
// MAIN FUNCTION
// --------------

// Initialize %CL__PRIV if it did not exist
// (If a global variable does not exist, Scilab initializes it with [])
global %CL__PRIV;
if (isempty(%CL__PRIV)); %CL__PRIV = struct(); end;

// Check that %CL__PRIV is a structure
if (~isstruct(%CL__PRIV)); CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); end;

// Check that preferences exists
if (~isfield(%CL__PRIV, "PREF"))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab.")
end


// Directory where files are stored
DIR = fullfile(CL_home(), "data")

nam_glob = [];     // vector of names
val_glob = list(); // list of corresponding values

// ----------------------------------------------------------------------------
// Loading of data from .scd files
for nomfic = ["physical_data.scd", "constants.scd", "time_data.scd", "solar_system_data.scd"]
  data = dataInit_load(fullfile(DIR, nomfic));
  names = fieldnames(data); 
  for name = names'
    nam_glob($+1) = name;
    val_glob($+1) = data(name);
  end
end

// ----------------------------------------------------------------------------
// Store all the variables in the global variable %CL__PRIV.DATA
 
%CL__PRIV.DATA = struct();
for k = 1:length(val_glob)
  %CL__PRIV.DATA(nam_glob(k)) = val_glob(k); 
end

endfunction
