//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cl_ver, cl_dat] = CL_version(opt)
// Returns celestLab's version 
//
// Calling Sequence
// [cl_ver, cl_dat] = CL_version([opt])
//
// Description
// <itemizedlist><listitem>
// <p>CelestLab's version (fist output argument) can be returned as a string or as a 1x4 vector. </p>
// <p></p>
// <p>The "string" form can be long (opt = "cl") or short (opt = "str"). </p>
// <p>The 1x4 vector form has the following meaning: </p>
// <p> - First 3 numbers: give the latest "official" version, </p>
// <p> - Fourth number: 0 for an "official" version, revision number of an intermediate version. </p>
// <p>The second output argument is the calendar date of the release.</p>
// <p></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// opt: (string, optional) Type of output: "cl", "str" or "num".  Default is "cl". (1x1)
// cl_ver: (string or integer) CelestLab's version (1x1 or 1x4)
// cl_dat: (string) CelestLab's date of release (1x1)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Version as a string 
// CL_version()
// CL_version("str")
//
// // Version as 4 numbers 
// CL_version("num")
// 

function [vcl, vstr, vnum, vdat] = version_read()

  vnum = [0,0,0,0]; 
  vdat = ""; 
  vcl = ""; 
  vstr = ""; 

  fpath = fullfile(CL_home(), "etc", "celestlab.version"); 
  
  if (~isfile(fpath))
    CL__error("Version file does not exist"); 
  end
  
  // read infos from file "celestlab.version" in "etc"
  fic_txt = mgetl(fpath); 
  fic_txt = stripblanks(fic_txt, %t);
  
  // Remove blank lines and lines that start with "//"
  I = find(part(fic_txt,1:2) == "//" | length(fic_txt) == 0);
  fic_txt(I) = [];

  // add values to avoid errors ! 
  fic_txt = [fic_txt; ""; ""; ""];  
  
  // Version number - revision number - release date
  v = msscanf(fic_txt(1),"%d %d %d");
  r = msscanf(fic_txt(2),"$Rev: %d ");
  vdat = msscanf(fic_txt(3),"$Date: %s");
  vdat = strsubst(vdat, "-", "/");
   
  vnum = [v, r];

  // fills variables with something 
  if (size(vnum,"*") <> 4 | typeof(vdat) <> "string")
    vnum = [0, 0, 0, 0]; 
    vdat = "?/?/?"; 
  end
  
  // If revision number is 0 (for official releases), 
  // revision number is not printed
  if (r(1) == 0)
    vcl = msprintf("CelestLab-v%d.%d.%d", v(1), v(2), v(3)); 
    vstr = msprintf("%d.%d.%d", v(1), v(2), v(3)); 
  else
    vcl = msprintf("CelestLab-v%d.%d.%d-r%06d", v(1), v(2), v(3), r(1)); 
    vstr = msprintf("%d.%d.%d.%06d", v(1), v(2), v(3), r(1)); 
  end
 
endfunction


// -------------------------------------
// Main
// -------------------------------------

global %CL__PRIV;

if (~exists("opt", "local")); opt = "cl"; end

if (typeof(opt) <> "string"); 
  CL__error("Invalid argument type"); 
end
if (opt <> "cl" & opt <> "str"  & opt <> "num"); 
  CL__error("Invalid argument"); 
end

// Creates %CL__PRIV is not yet created
if (isempty(%CL__PRIV)); %CL__PRIV = struct(); end

// 1st call: store version 
if (~isfield(%CL__PRIV, "VERSION"))
  [vcl, vstr, vnum,  vdat] = version_read(); 
  %CL__PRIV.VERSION = struct(); 
  %CL__PRIV.VERSION.vnum = vnum; 
  %CL__PRIV.VERSION.vcl = vcl; 
  %CL__PRIV.VERSION.vstr = vstr; 
  %CL__PRIV.VERSION.vdat = vdat; 
end


if (opt == "cl")
  cl_ver = %CL__PRIV.VERSION.vcl; 
elseif (opt == "str")
  cl_ver = %CL__PRIV.VERSION.vstr; 
else
  cl_ver = %CL__PRIV.VERSION.vnum; 
end

cl_dat = %CL__PRIV.VERSION.vdat; 
 
endfunction
