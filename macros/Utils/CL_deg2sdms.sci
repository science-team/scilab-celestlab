//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [sdms] = CL_deg2sdms(degs)
// Decimal degrees to sign, degrees, arcminutes, arcseconds conversion 
//
// Calling Sequence
// sdms = CL_deg2sdms(degs)
//
// Description
// <itemizedlist><listitem>
// <p>Conversion from decimal degrees to sign, degrees, arcminutes, arcseconds.</p>
// <p>The output argument sdms is such that:</p>
// <p>- The first row is the sign of the angle (1 => positive, -1 => negative).</p>
// <p>- The second row is the number of degrees (positive integer).</p>
// <p>- The third row is the number of arcminutes (integer in [0,59]).</p>
// <p>- The fourth row is the number of arcseconds (double in [0,60[).</p>
// </listitem></itemizedlist>
//
// Parameters
// degs: Decimal degrees (1xN)
// sdms: Sign, degrees, arcminutes and arcseconds (4xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_sdms2deg
//
// Examples
// degs = -40.325;
// sdms = CL_deg2sdms(degs)

// Declarations:

// Code:

[P,N] = size(degs);
if (P <> 1); CL__error("Invalid input argument"); end;

sdms = zeros(4,N); 

sdms(1,:) = sign(degs);

// Scilab's sign function returns 0 if value is 0. 
// --> Replace with 1
I = find(sdms(1,:) == 0);
sdms(1,I) = 1;

degs = abs(degs);

sdms(2,:) = floor(degs);
sdms(3,:) = floor((degs - sdms(2,:)) * 60); 
sdms(4,:) = (degs - sdms(2,:))*3600 - sdms(3,:)*60;

endfunction
