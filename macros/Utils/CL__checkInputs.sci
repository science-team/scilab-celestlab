//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL__checkInputs(varargin)
// Internal function used to check inputs (number of rows,columns) and to resize them
//
// Calling Sequence
// [u1,u2, ... uN , ncMax] = CL__checkInputs(u1,nr1 , u2,nr2, ... uN,nrN)
//
// Description
// u1,u2, ... uN : inputs to be checked.
// Their number of columns should either be 1 or the maximum of number of columns ncMax.
// Their number of rows should be nr1,nr2, ... nrN.
// If either of these conditions is not satisfied : an error is returned.
//
// Note : nr1,nr2, ... nrN can be vectors.
// Exemple : nr1 = [1,2] --> u1 should have one or two rows.
// 
// The output vectors are the same vectors resized to ncMax
//
// Note :
// [] are always accepted
//
// Example :
// A = rand(3,4);
// B = rand(2,4);
// C = rand(5,1);
// [A2,B2,C2, N] = CL__checkInputs(A,3,B,2,C,5)

[lhs,rhs]=argn();

// Number of inputs should be even
if (modulo(rhs,2) ~= 0); CL__error("Invalid number of inputs"); end;

s = lstsize(varargin);

// Maximum number of columns
N = 0; 
for k = 1 : 2 :s
  N = max(N , size(varargin(k),2));
end

// Perform checks and resizing :
for k = 1 : 2 : s
  arg = varargin(k);  
  iout = ceil(k/2); 

  if (arg == [])
    varargout(iout) = [];
  else
    if (intersect(size(arg, 1), varargin(k+1)) == [])
      CL__error("Invalid input argument size (number of rows)"); 
    end
    if (size(arg, 2) <> 1 &  size(arg, 2) <> N) 
      CL__error("Invalid input argument size (number of columns)"); 
    end
    
    if (size(arg, 2) == 1) 
      varargout(iout) = arg * ones(1,N);
    else
      varargout(iout) = arg;
    end
  end
end

varargout( s/2+1 ) = N;

endfunction
