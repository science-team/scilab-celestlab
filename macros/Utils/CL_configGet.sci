//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [val] = CL_configGet(name)
// Get a CelestLab's configuration parameter 
//
// Calling Sequence
// val = CL_configGet(name)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Get a CelestLab's configuration parameter. </p>
// <p>Available configuration parameters are "display_menu", "warning_mode", 
// "verbose_mode", "warn_deprecated" and "ECI_frame".</p>
// <p>See <link linkend="Configuration">Configuration</link> for more details on 
// the configuration parameters.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (string) Name of configuration parameter. (1x1)  
// val: (string) Value of configuration parameter. (1x1)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_configSet
//
// Examples
// warning_mode = CL_configGet("warning_mode");

// Declarations:
global %CL__PRIV;

// Code:
if (size(name, "*") <> 1)
  CL__error("Invalid name (string expected)");
end

// NB: %CL__PRIV.PREF must have been created before.
// Done by CL__configInit in etc/celestlab.start file

// Check that %CL__PRIV is a structure and %CL__PRIV.PREF exists
if (~isstruct(%CL__PRIV)); 
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); 
end
if (~isfield(%CL__PRIV, "PREF"))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab.")
end

if (~isfield(%CL__PRIV.PREF, name))
  CL__error("Requested configuration parameter does not exist")
end

val = %CL__PRIV.PREF(name);

endfunction
