//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [h] = CL_g_tag(root,tag)
// Tags graphic entities
//
// Calling Sequence
// h = CL_g_tag(root,tag)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Puts a tag on graphic entities (i.e. sets the "user_data" property) if that entity has not already been tagged (that is if the user_data value is []).</p>
// <p>This function may be used in conjunction with CL_g_select to change some graphic properties without having to go through the graphic hierarchy (which avoids dealing with expressions like a.children(1).children...). </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// root: Root of the hierarchy of graphic entities to be examined. Default = current axes (gca()). 
// tag: (optional) Value to be used to tag the graphic entities. The value can be any value (and of any type) except '*'. Default is [] (user_data value not changed).
// h: Vector of the graphic entities that have been tagged.
//
// Authors
// CNES - DCT/SB (AL)
//
// See also
// CL_g_select
//
// Examples
// f=scf();
// a=gca();
// x = 0:0.1:5;
// plot(x, x.^2);
// CL_g_tag(a,1);
// plot(x, x.^3, '-r');
// CL_g_tag(a,2);
// h = CL_g_select(a, "Polyline", 1);
// h.thickness = 3;


// Declarations:


// Code:

  if (~exists('root', 'local'))
     root=gca();
  end
  if (~exists('tag', 'local'))
     tag=[];
  end
  h = CL_g_select(root, tag=[]); // select all entities with user_data = []
  CL_g_set(h, "user_data", tag); // sets user_data

endfunction
