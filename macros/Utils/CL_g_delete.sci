//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function CL_g_delete(h)
// Deletes graphic entities
//
// Calling Sequence
// CL_g_delete(h)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Deletes the selected graphic entities. </p>
// <p>Identical to 'delete' except that the argument can be empty (in which case nothing happens).</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// h: Vector of graphic entities (1xN).
//
// Authors
// CNES - DCT/SB (AL)
//
// Examples
// f=scf();
// a=gca();
// x = 0:0.1:5;
// y = x;
// plot2d(x, y);
// h = CL_g_select(a, "Polyline");
// CL_g_delete(h); // deletes the curve
// h = CL_g_select(a, "Text");
// CL_g_delete(h); // does nothing


// Declarations:


// Code:

  if (~isempty(h))
    delete(h);
  end

endfunction
