//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function CL_plot_earthMap(win_id,color_id,tick_steps,data_bounds,thickness,res,coord)
// Plots an Earth map
//
// Calling Sequence
// CL_plot_earthMap([win_id,color_id,tick_steps,data_bounds,thickness,res,coord])
//
// Description
// <itemizedlist><listitem>
// <p>Plots an Earth map in the selected figure (win_id). </p>
// <p>The x-axis and y-axis are respectively the longitude and latitude 
// in degrees. </p>
// <p> Depending on the "coord" option, the "y" coordinates are assumed to be 
// spherical or elliptical (i.e. geodetic) latitudes. </p>
// <p>There are 2 available resolutions: </p>
// <p>- "low": outlines of Earth continents and major islands</p>
// <p>- "high": outlines of Earth continents, islands and lakes</p>
// </listitem>
// <listitem>
// <p><b>Note</b>: </p>
// <p>Only one graphical entity is created. </p>
// <p>Call gce() to get it and possibly change graphical properties. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// win_id: (optional) Figure ID, or ID of the current window if omitted.
// color_id: (optional) Color index. Default is 2.
// tick_steps: (optional) Steps of the grid in longitude and latitude in degrees (2x1), or [] (automatic). Default: []. 
// data_bounds: (optional) Definition of the view area: [longmin, latmin; longmax, latmax] in degrees (2x2). Default is [-180, -90; 180, 90].
// thickness: (optional) Line thickness. Default is 1.
// res: (optional, string) Resolution: "low" or "high". Default is "low". 
// coord: (optional, string) Type of coordinates: "sph"=spherical, "ell"=elliptical. Default is "sph". 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_plot_ephem
//
// Examples
// // basic plot
// scf(); 
// CL_plot_earthMap();
//
// // plot in figure 10, with color 2
// CL_plot_earthMap(win_id=10, color_id=2);
//
// // plot in figure 10, with color 1, and focus on Europe
// CL_plot_earthMap(win_id=10, color_id=1, ..
// data_bounds=[-10,30;40,60], tick_steps=[10,10]);
//
// // plot in high res, elliptical coordinates and focus on Europe
// CL_plot_earthMap(color_id=5,data_bounds=[-10,30;40,60], ..
// tick_steps=[10,10], res = "high", coord = "ell");
 

// Declarations:

// Code: 

// CODE FOR COMPATIBILITY
if exists('winId','local'); win_id=winId; end
if exists('colorId','local'); color_id=colorId; end
if exists('tickInterval','local'); tick_steps=tickInterval; end
if exists('dataBounds','local'); data_bounds=dataBounds; end


if exists('win_id','local') then
  f = scf(win_id)
else
  f = gcf();
end

if ~exists('color_id','local') then
  color_id = 2;
end

if ~exists('tick_steps','local') then
  tick_steps = []; // degrees
end

if ~exists('data_bounds','local') then
  data_bounds = [-180, -90; 180, 90]; // degrees
end

if ~exists('thickness','local'); thickness = 1; end

if ~exists('res','local'); res = "low"; end 

if ~exists('coord','local'); coord = "sph"; end 

// error checking

if (size(data_bounds,1) <> 2 | size(data_bounds,2) <> 2)
  CL__error('Invalid size for data_bounds');
end
if (data_bounds(2,1)<=data_bounds(1,1) | data_bounds(2,1)>data_bounds(1,1)+360)
  CL__error('Invalid data bounds in longitude');
end 
if (data_bounds(2,2)<=data_bounds(1,2) | data_bounds(1,2) < -90 | data_bounds(2,2) > 90)
  CL__error('Invalid data bounds in latitude');
end 

if (tick_steps <> [])
  if (length(tick_steps) <> 2)
    CL__error('Invalid size for tick_steps');
  end 
  if (tick_steps(1) <= 0 | tick_steps(2) <= 0)
    CL__error('Invalid values for tick_steps');
  end 
end

// plot
immediate_drawing_save = f.immediate_drawing; // store field
f.immediate_drawing = "off"; 

// loading of the map (blocks separated by %nan)
// pts: size = 3xN
pts = CL_getEarthMap(res, coord); 

// interval in longitude containing the view
bmin = (data_bounds(1,1)+data_bounds(2,1))/2 - 180; 
bmax = (data_bounds(1,1)+data_bounds(2,1))/2 + 180; 

 // plot curve (degrees)
[x, y] = CL__plot_genxy(pts(1,:)*180/%pi, pts(2,:)*180/%pi, bmin, bmax);  
plot2d(x, y);
h = CL_g_select(gce(), "Polyline"); 
h.foreground = color_id; 
h.thickness = thickness; 


// adjustments (grid, databounds)
a = gca();
a.data_bounds = data_bounds;
a.tight_limits = "on";

if (tick_steps <> [])
  bticks = [ tick_steps(1)*floor(data_bounds(1,:)/tick_steps(1)); 
             tick_steps(2)*ceil((data_bounds(2,:))/tick_steps(2)) ];

  x_ticks = bticks(1,1) : tick_steps(1) : bticks(2,1);
  y_ticks = bticks(1,2) : tick_steps(2) : bticks(2,2);

  // strsplit+msprintf : used to avoid "string"
  x_ticks_labels = strsplit( stripblanks( msprintf("%.8g ",x_ticks')) , " ")' ; 
  y_ticks_labels = strsplit( stripblanks( msprintf("%.8g ",y_ticks')) , " ")' ;

  a.x_ticks = tlist(["ticks", "locations", "labels"], x_ticks, x_ticks_labels ); 
  a.y_ticks = tlist(["ticks", "locations", "labels"], y_ticks, y_ticks_labels ); 
end

CL_g_stdaxes(a);

f.immediate_drawing = immediate_drawing_save; // restore field

endfunction


