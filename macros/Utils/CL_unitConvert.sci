//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [val2] = CL_unitConvert(arg1,arg2,arg3)
// Unit conversion
//
// Calling Sequence
// val2 = CL_unitConvert(val1,unit1,unit2)
// val2 = CL_unitConvert(unit1,val1,unit2)
// val2 = CL_unitConvert(unit1,unit2,val1)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Converts a quantity expressed in unit1 into unit2. The recognized units are:</p>
// <p>m: meter</p>
// <p>km: kilometer (1000 meters)</p>
// <p>s: second</p>
// <p>mn: minute (60 seconds) </p>
// <p>h: hour (60 minutes) </p>
// <p>day: day (24 hours)</p>
// <p>yr: year (365.25 days)</p>
// <p>g: gramme</p>
// <p>kg: kilogramme (1000 grammes) </p>
// <p>au: astronomical unit (%CL_au)</p>
// <p>N: newton</p>
// <p>Hz: hertz</p>
// <p>kHz: kilohertz (1000 Hz)</p>
// <p>rad: radian</p>
// <p>deg: degree (pi/180 radians)</p>
// <p>arcmin: arcminute (1/60 degree)</p>
// <p>arcsec: arcsecond (1/3600 degree)</p>
// <p>mas: milli arcsecond (1/1000 arcsecond)</p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- The consistency between unit1 and unit2 is not checked. Thus CL_unitConvert(1, "kg", "m") generates no error!.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// val1: Value of some quantity 'X' expressed in unit1. (PxN)
// unit1: (string) Initial unit. (1x1)
// unit2: (string) Final unit. (1x1)
// val2: Value of 'X' expressed in unit2. (PxN)
//
// Authors
// CNES - DCT/SB (AL)
//
// Examples
// val = CL_unitConvert(1, "m/s", "km/h")
// val = CL_unitConvert(1, "au^2", "km^2")
// val = CL_unitConvert(0:90:360, "deg", "rad")
//
// // Other calling sequences:
// val = CL_unitConvert("deg", "rad", 0:90:360)
// val = CL_unitConvert("deg", 0:90:360, "rad")

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_au")); %CL_au = %CL__PRIV.DATA.au; end

// Code:
// reference units
m   = 1;
s   = 1;
kg  = 1;
rad = 1;
Hz  = 1;

// derived units
km  = 1000      * m;
au  = %CL_au    * m;  // NB: %CL_au must be defined in meters ! 
h   = 3600      * s;
mn  = 60        * s;
day = 86400     * s;
yr =  365.25    * day;
g   = 1.e-3     * kg;
deg = (%pi/180) * rad;
N   = 1         * kg*m/s^2;
kHz = 1000      * Hz;
arcmin = (1/60) * deg; 
arcsec = (1/3600) * deg; 
mas = (1/1000) * arcsec; 

// Get input arguments depending on the calling sequence
// val2 = CL_unitConvert(val1,unit1,unit2)
if (typeof(arg2) == "string" & typeof(arg3) == "string")
  val1 = arg1;
  unit1 = arg2;
  unit2 = arg3;

// val2 = CL_unitConvert(unit1,val1,unit2)
elseif (typeof(arg1) == "string" & typeof(arg3) == "string")
  unit1 = arg1;
  val1 = arg2;
  unit2 = arg3;
  
// val2 = CL_unitConvert(unit1,unit2, val1)
elseif (typeof(arg1) == "string" & typeof(arg2) == "string")
  unit1 = arg1;
  unit2 = arg2;
  val1 = arg3;
  
else
  CL__error("Invalid calling sequence");
end

if (size(unit1,"*") <> 1 | size(unit2,"*") <> 1)
  CL__error("Invalid size for unit1 or unit2");
end

// The two following lines are equivalent to 
// f1 = evstr(unit1); 
// f2 = evstr(unit2); 
// except that errors can be retrieved properly (avoid try/catch)
ier1 = execstr("f1 = " + unit1, "errcatch");
ier2 = execstr("f2 = " + unit2, "errcatch");

if (ier1 == 0 & ier2 == 0)
  val2 = val1 * f1 / f2;
else
  CL__error("Unable to compute (possibly invalid units).")
end

endfunction
