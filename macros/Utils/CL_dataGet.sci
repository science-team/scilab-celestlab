//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [val] = CL_dataGet(name)
// Retrieve the value of CelestLab predefined data
//
// Calling Sequence
// [val] = CL_dataGet([name])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Retrieves the value of CelestLab predefined data. </p>
// <p>The names of the data that can be retrieved can be found 
// in the data files accessible through CelestLab menu "Data files (predefined variables)"</p>
// <p></p>
// <p>Notes:</p>
// <p>- The data files (*.scd) are located in the folder: CL_home()/data/</p>
// <p>- Any field is accessible: CL_dataGet("body") returns the whole "body" structure,
//    CL_dataGet("body.Sun.mu") returns only the value of the field "body.Sun.mu"</p>
// <p>- CL_dataGet() returns CelestLab's whole structure of data.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (optional, string) Data name (1x1)
// val: Value of the field (can be a structure or a matrix of any size)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_init
//
// Examples
// muSun = CL_dataGet("body.Sun.mu");
// au = CL_dataGet("au");
// body = CL_dataGet("body");
//
// // Retrieve CelestLab's whole data structure:
// s = CL_dataGet();

// Declarations:
global %CL__PRIV;

// Code:

// No input argument --> return whole structure
if (argn(2) == 0)
  val = %CL__PRIV.DATA;
  return; // <-- RETURN!
end


// Return internal variable
// NB: Do not use val = %CL__PRIV.DATA(name) because name can be name1.name2.name3 ...
ier = execstr("val = %CL__PRIV.DATA." + name, "errcatch");

if (ier <> 0)
  CL__error("Requested name does not exist");
end


endfunction
