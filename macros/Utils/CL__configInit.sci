//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [] = CL__configInit()
// Loads CelestLab's configuration from a file
//
// Calling Sequence
// CL__configInit()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Loads CelestLab's configuration parameters from a file.</p>
// <p>See <link linkend="Configuration">Configuration</link> for more details on the configuration parameters.</p>
// <p>Note: This function initializes the global variable %CL__PRIV.DATA.PREF and 
// is supposed to be run only once, when CelestLab starts.</p>
// </listitem>
// </itemizedlist>
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_configGet
// CL_configSet


// NB:  do not use configSet or configGet in this function ! 

// Declarations:
global %CL__PRIV;

// Code:

// loads configuration (name of config file is implict)
conf = CL__configLoad(); 

// Initialize %CL__PRIV if it did not exist
// (NB: if a global variable does not exist, Scilab initializes it with [])
if (isempty(%CL__PRIV)); 
  %CL__PRIV = struct(); 
end

// Check that %CL__PRIV is a structure
if (~isstruct(%CL__PRIV)); 
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); 
end

// Set the configuration parameters structure (as a whole)
%CL__PRIV.PREF = conf;

endfunction
