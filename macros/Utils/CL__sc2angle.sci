//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function  [angle] = CL__sc2angle(x,y)
// Sinus and cosinus to angle
//
// Calling Sequence
// angle = CL__sc2angle(x,y)
//
// Description
// <itemizedlist><listitem>
// Computes the angle in [0, 2*%pi[ between the x-axis and a vector in the x-y plane. 
// </listitem>
// </itemizedlist>
//
// Parameters
// x: Vector x-component (1xN)
// y: Vector y-component (1xN)
// angle: angle so that x=r*cos(angle) and y=r*sin(angle), r being the norm of the vector. (1xN) [rad]
//
// Authors
// CNES - DCT/SB
//
// Examples
// angle = CL__sc2angle(2*cos(4),2*sin(4)) // returns 4 
//

// Declarations:


// Code:

// Renvoie 0 si x et y sont simultanement nuls
angle = CL_rMod(atan(y, x),0,2*%pi);

endfunction
