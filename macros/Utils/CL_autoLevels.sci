//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [levels,sublevels] = CL_autoLevels(vmin,vmax,ns1,ns2)
// Automatic generation of level values 
//
// Calling Sequence
// [levels,sublevels] = CL_autoLevels(vmin,vmax,ns1,ns2)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Generates sensible level values than can be used for contour plots.</p> 
// <p>The numbers of levels or sublevels returned are never equal to 1 (so that contour2d doesn't confuse the level values with the number of values).</p>
// <p>The intersection between levels and sublevels is empty: there are no values belonging both to levels and sublevels.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// vmin: User data minimum value.
// vmax: User data maximum value.
// ns1: (optional) Approximate number of steps. Default is 7. 
// ns2: (optional) Approximate number of sub-steps. Default is 5. 
// levels: Level values.
// sublevels: Sub-level values.
//
// Authors
// CNES - DCT/SB (AL)
//
// See also
// CL_graduate
//
// Examples
// [levels, sublevels] = CL_autoLevels(0, 6)
// [levels, sublevels] = CL_autoLevels(1.14, 2.27, 5, 5)

// Declarations:


// Code:

  if ~exists('ns1', 'local'); ns1=7; end
  if ~exists('ns2', 'local'); ns2=5; end

  [vmin1, vmax1, nb1, nb2] = CL_graduate(vmin, vmax, ns1, ns2);
  levels = linspace(vmin1, vmax1, nb1+1);
  sublevels = linspace(vmin1, vmax1, nb1*nb2+1);

  I = 1:nb2:nb1*nb2+1;
  sublevels(I) = []; // eliminate values already in levels

  // eliminate values outside [vmin, vmax] to minimize the number of values
  I = find(levels < vmin | levels > vmax); 
  levels(I) = []; 
  I = find(sublevels < vmin | sublevels > vmax); 
  sublevels(I) = []; 

  // duplicate the values if there is only one
  if (length(levels) == 1)
    levels = [levels(1), levels(1)];
  end

  if (length(sublevels) == 1)
    sublevels= [sublevels(1), sublevels(1)];
  end

endfunction
