//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.



function CL__message(fmt, varargin)
// Prints CelestLab message
//
// The calling sequence is the same as the mprintf function:
// CL__message(fmt, arg1, arg2, ..., argn)
// fmt: Format of the string
// arg1,...argn: Variables to be printed accordingly to fmt
//
// NB: It is up to the caller to use a "\n" at the end of the fmt to create an end of line.
//
// Example:
// // Simple example
// CL__message("Semi major axis = %.15g\n", 7000.002e3);
//
// // Example with strings containing %
// CL__message("This is a CelestLab message: %s\n", "Variable %CL_j2 exists");
// --> OK
// BUT:
// CL__message("This is a CelestLab message: Variable %CL_j2 exists\n");
// DOES NOT WORK because of the single % in the format. Use % instead.
// CL__message("This is a CelestLab message: Variable %CL_j2 exists\n");

  verbose_mode = CL_configGet("verbose_mode");
  
  // NB : if verbose_mode == "silent": do nothing
  
  if (verbose_mode == "standard")
  
    // Build the command string to call mprintf
    cmd = "mprintf(fmt";
    for k = 1 : lstsize(varargin)
      cmd = cmd + msprintf(",varargin(%d)",k);
    end
    cmd = cmd + ");" ;
    
    // Execute the commad and catch any error 
    // in order to print a CelestLab error
    ierr = execstr(cmd,"errcatch", "n");
    if (ierr <> 0)
      CL__error(lasterror());
    end
  end
endfunction
