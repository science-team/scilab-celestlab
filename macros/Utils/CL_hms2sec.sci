//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [secs] = CL_hms2sec(hms)
// Hours, minutes, seconds to seconds conversion 
//
// Calling Sequence
// secs = CL_hms2sec(hms)
//
// Description
// <itemizedlist><listitem>
// <p>Conversion from hours, minutes, seconds to seconds.</p>
// <p>Input argument hms is such that:</p>
// <p>- The first row is the number of hours.</p>
// <p>- The second row is the number of minutes.</p>
// <p>- The third row is the number of seconds.</p>
// <p></p>
// <p>Notes:</p> 
// <p>- The number of hours and minutes should be integers but non-integers are allowed as well.</p>
// <p>- The function only checks that all values are positive.</p>
// </listitem></itemizedlist>
//
// Parameters
// hms: Hours, minutes and seconds (3xN)
// secs: Seconds (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_sec2hms
//
// Examples
// hms = [17 ; 19 ; 30.5];
// secs = CL_hms2sec(hms)

// Declarations:

// Code:

// Check input argument size
if (size(hms,1) <> 3); CL__error("Invalid input argument (3 rows expected)"); end;

// Check that all values are positive
if (find(hms < 0) <> []); CL__error("Invalid input argument value (negative value)"); end

secs = hms(1,:)*3600 + hms(2,:)*60 + hms(3,:);

endfunction
