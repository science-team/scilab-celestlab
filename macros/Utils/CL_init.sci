//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function CL_init(init_glob)
// Creation of useful (local) variables
//
// Calling Sequence
// CL_init()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Convenience function that creates a few (local) variables containing useful data.</p>
// <p>These data consist of physical quantities and constants that are commonly used
// in practice.</p>
// <p></p>
// <p>The created variables are:</p>
// </listitem>
// <itemizedlist>
//   <listitem><emphasis role="bold">%CL_mu</b>: Gravitational constant default value </listitem>
//   <listitem><b>%CL_eqRad</b>: Equatorial radius default value </listitem>
//   <listitem><b>%CL_obla</b>: Oblateness default value </listitem>
//   <listitem><b>%CL_j1jn</b>: Zonal harmonics default value </listitem>
//   <listitem><b>%CL_cs1nm</b>: Tesseral and sectorial harmonics default value </listitem>
//   <listitem><b>%CL_rotrBody</b>: Body rotation rate default value </listitem>
//   <listitem><b>%CL_rotrBodySun</b>: Body to Sun direction (mean) rotation rate default value </listitem>
//   <listitem><b>%CL_TT_TREF</b>: TT time scale minus TREF time scale defaut value </listitem>
//   <listitem><b>%CL_UT1_TREF</b>: UT1 time scale minus TREF time scale defaut value </listitem>
//   <listitem><b>%CL_deg2rad</b>: Degrees to radians conversion factor </listitem>
//   <listitem><b>%CL_rad2deg</b>: Radians to degrees conversion factor </listitem>
//   <listitem><b>%CL_au</b>: Astronomical unit (mean Earth to Sun distance) </listitem>
//   <listitem><b>%CL_body</b>: Structure of solar system bodies data (eqRad,mu,obla)</listitem>
// </itemizedlist>
// </itemizedlist>
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_dataGet
//
// Examples
// CL_init();
// whos -name "%CL"


// Declarations:
global %CL__PRIV;


// Code: 

// NB: init_glob not used anymore but still in the calling sequence for backward compatibility

// Check that %CL__PRIV is a structure
if (~isstruct(%CL__PRIV))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); 
end

// Check that data exists
if (~isfield(%CL__PRIV, "DATA"))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab.")
end

// Variables returned as locals = 
// variables that are used as default arguments of CelestLab functions
str_var = ["deg2rad", "rad2deg", "eqRad", "mu", "obla", "j1jn", "cs1nm", ...
          "au", "rotrBody", "rotrBodySun", "TT_TREF", "UT1_TREF", "body" ];
          
for str = str_var
  if (~isfield(%CL__PRIV.DATA,str)); CL__error("Variable " + str + " missing in %CL__PRIV.DATA. Restart CelestLab."); end
end
          
str_loc = strcat("%CL_" + str_var, ","); 
str_glob = strcat("%CL__PRIV.DATA." + str_var, ","); 
cmd = "[" + str_loc + "] = return(" + str_glob + ")"; 

// WARNING: Do NOT use errcatch argument of execstr 
// (Scilab bug with 'return' instruction)
execstr(cmd);

endfunction
