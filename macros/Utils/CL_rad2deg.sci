//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [x_deg] = CL_rad2deg(x_rad)
// Radians to degrees unit conversion 
//
// Calling Sequence
// x_deg = CL_rad2deg(x_rad)
//
// Description
// <itemizedlist><listitem>
// <p>Conversion from radians to degrees. </p>
// <p>Note: %CL_rad2deg can also be used</p>
// </listitem></itemizedlist>
//
// Parameters
// x_rad: Quantity expressed in radians (PxNxK)
// x_deg: Quantity expressed in degrees (PxNxK)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_deg2rad
//
// Examples
// CL_rad2deg(%pi/4)

// Declarations:
global %CL__PRIV; 
RAD2DEG = %CL__PRIV.DATA.rad2deg;

// Code:
x_deg = x_rad * RAD2DEG;

endfunction

