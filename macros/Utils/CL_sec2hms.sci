//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [hms] = CL_sec2hms(secs)
// Seconds to hours, minutes, seconds conversion 
//
// Calling Sequence
// hms = CL_sec2hms(secs)
//
// Description
// <itemizedlist><listitem>
// <p>Conversion from seconds to hours, minutes, seconds.</p>
// <p>Output argument hms is such that:</p>
// <p>- The first row is the number of hours (positive integer).</p>
// <p>- The second row is the number of minutes (integer in [0,59]).</p>
// <p>- The third row is the number of seconds (double in [0,60[).</p>
// <p></p>
// <p>Note: the input argument secs must be positive.</p>
// </listitem></itemizedlist>
//
// Parameters
// secs: Seconds (1xN)
// hms: Hours, minutes and seconds (3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_hms2sec
//
// Examples
// secs = 76521.23;
// hms = CL_sec2hms(secs)

// Declarations:

// Code:

[P,N] = size(secs);
if (P <> 1); CL__error("Invalid input argument"); end;

if (find(secs < 0) <> []); CL__error("Invalid input argument (positive value expected)"); end;

hms = zeros(3,N); 
hms(1,:) = floor(secs/3600);
hms(2,:) = floor((secs-hms(1,:)*3600)/60); 
hms(3,:) = secs - hms(1,:)*3600 - hms(2,:)*60;

endfunction
