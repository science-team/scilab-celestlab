//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [phi,theta,psi] = CL_rot_quat2eul(q)
// Quaternion to Euler '123' angles - DEPRECATED
//
// Calling Sequence
// [phi,theta,psi] = CL_rot_quat2eul(q)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_rot_quat2angles">CL_rot_quat2angles</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the Euler angles (rotation order: 1,2,3) that corresponds to a given quaternion.</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="q2eul.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - One should be aware of singularities in the Euler angles when the second angle 
// (theta) approaches +pi, -pi, +pi/2 or -pi/2. These cases should be
// handled specifically or results might not be accurate! </p>
// <p> - See <link linkend="Data types">Data types</link> or <link linkend="CL_rot_defQuat">CL_rot_defQuat</link> for more details on quaternions. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// q : Quaternion (dim N)
// phi : Rotation angle around X axis [rad] (1xN)
// theta : Rotation angle around Y axis [rad] (1xN)
// psi : Rotation angle around Z axis [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Mecanique spatiale - CNES Cepadues 1995, Tome I, 7.2.2.3 Les quaternions
//
// See also
// CL_rot_eul2quat
// CL_rot_defQuat
//
// Examples
// M = CL_rot_angles2matrix([1,2,3], [0.5; 1.1; 2.3]); 
// q = CL_rot_matrix2quat(M); // quaternion of this rotation
// [phi,theta,psi] = CL_rot_quat2eul(q) 



// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

if (typeof(q) <> "CLquat")
  CL__error("Wrong type of input argument. Quaternion expected");
end

q0 = q.r
q1 = q.i(1,:)
q2 = q.i(2,:)
q3 = q.i(3,:)

phi = atan( ( 2.*(q0.*q1-q2.*q3) ),( 1-2.*(q1.^2+q2.^2) ) );
theta = asin( 2.*(q0.*q2+q3.*q1) );
psi = atan( ( 2.*(q0.*q3-q1.*q2) ),( 1-2.*(q2.^2+q3.^2) ) );

if or( (abs(theta)>%pi-0.0001) | ((abs(theta)>%pi/2-0.0001)&(abs(theta)<%pi/2+0.0001)) )
  CL__warning("theta near pi or pi/2: results may not be accurate!");
end



endfunction
