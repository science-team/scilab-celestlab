//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [W] = CL_mod_polarMotionMatrix(xp,yp)
// Polar motion matrix - DEPRECATED
//
// Calling Sequence
// W = CL_mod_polarMotionMatrix(xp,yp)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Builds a polar motion matrix from polar coordinates <b>xp</b>, <b>yp</b></p>
//<p> The matrix <b>W</b> is such that multiplying W by 
// coordinates relative to ITRS yields coordinates relative to TIRS</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// xp : x polar coordinate [rad] (1xN)
// yp : y polar coordinate [rad] (1xN)
// W: polar motion matrix (3x3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_mod_precessionMatrix
// CL_mod_nutationMatrix
//

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs]=argn(0);
if rhs~=2 CL__error("check number of input arguments"); end,

// Old erroneous code:
// W = CL_rot_angles2matrix(1,yp)*CL_rot_angles2matrix(2,xp);

// The matrix W1 = CL_rot_angles2matrix([2,1], [-xp; -yp]) is such that multiplying M by 
// coordinates relative to TIRS yields coordinates relative to ITRS
// Reference: CelestLab function CL__fr_tirs2itrs
// Here we want the transposed matrix:
W = CL_rot_angles2matrix([1,2], [yp; xp]);

endfunction
