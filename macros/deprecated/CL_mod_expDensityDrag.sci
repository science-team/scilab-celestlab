//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [acc] = CL_mod_expDensityDrag(altitude,cbal, er,mu)
// Atmospheric drag using exponential density model - DEPRECATED
//
// Calling Sequence
// acc = CL_mod_expDensityDrag(altitude,cbal [,er,mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Computes the acceleration <b>acc</b> due to atmospheric drag
// using an exponential density model (expDensity with default values, 
// see <link linkend="CL_mod_expDensity">CL_mod_expDensity</link>).</p>
// <p> Inputs are the satellite's ballistic coefficient <b>cbal</b> and 
// the satellite's altitude <b>altitude</b></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// altitude : Satellite's altitude [m] (1xN)
// cbal : Satellite's ballistic coefficient : S*cx/m with S=equivalent drag surface, cx = drag coefficient, m = mass [m^2/kg](1xN)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// acc: Drag acceleration [m/s^2] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cw_diffDrag
// CL_mod_expDensity
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, 13.4.1.3
//
// Examples
// [acc]=CL_mod_expDensityDrag([350000,360000,350000,360000],[2.2,2.3,2.3,2.2])
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('mu','local') then mu=%CL_mu; end

v2 = mu ./ (er+altitude);
ro = CL_mod_expDensity(altitude);
acc = -1.0 .* ro .* cbal .*v2 / 2; 

endfunction
