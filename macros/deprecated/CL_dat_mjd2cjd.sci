//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cjd] = CL_dat_mjd2cjd(mjd)
// Modified Julian day to modified (1950.0) Julian day - DEPRECATED
//
// Calling Sequence
// cjd = CL_dat_mjd2cjd(mjd)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_dat_convert">CL_dat_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts a Modified Julian day (<b>mjd</b>: Julian day - 2400000.5) into 
// a modified (1950.0) Julian day (<b>cjd</b>: number of days elapsed since 1950.0). </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// mjd: (real) Modified Julian day (1xN)
// cjd: (real) Modified (1950.0) Julian day (1xN)
//
// See also
// CL_dat_cjd2mjd
//
// Authors
// CNES - DCT/SB
//
// Examples
// mjd=[53282.45687,53283.26789];
// cjd = CL_dat_mjd2cjd(mjd);
// mjd2 = CL_dat_cjd2mjd(cjd);

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs]=argn();
if rhs~=1 then CL__error("Wrong number of input arguments"), end

cjd = mjd + (2400000.5 - 2451545) + 18262.5;

endfunction
