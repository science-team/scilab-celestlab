//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [eq_overlap] = CL_gm_equatorialOverlap(alt,inc,view_angle)
// Ratio between sensor footprint and gap between ground tracks - DEPRECATED
//
// Calling Sequence
// eq_overlap = CL_gm_equatorialOverlap(alt,inc,view_angle)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_op_equatorialSwath">CL_op_equatorialSwath</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the ratio Q = Leq/Ded where: </p>
// <p> - Leq: portion of the equator seen by a sensor during the time it crosses the equator. </p>
// <p> - Deq: gap between 2 consecutive ground tracks at equator. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p> 
// <p> - The planet is assumed spherical, and the orbit circular. </p> 
// <p> - The sensor is conic and oriented towards the descending vertical. </p> 
// <p> - The gap between 2 consecutive ground tracks is computed taking the effect of J2 into account. </p> 
// <p> - The result is undefined (%nan) if inclination is 0 or %pi.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// alt: Satellite's altitude [m] (1xN or 1x1)
// inc: Orbit's inclination [rad](1xN or 1x1)
// view_angle: Sensor half view angle = angle from the descending vertical [rad] (1xN or 1x1)
// eq_overlap: ratio (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Satellites orbites et missions - Michel Capderou
//
// See also
// CL_gm_equatorialSwath
//
// Examples
// alt = [822.e3 630.e3] ;
// inc = [CL_deg2rad(98.72) CL_deg2rad(96.72)];
// view_angle = [CL_deg2rad(50.5) CL_deg2rad(30.5)];
// [eq_overlap] = CL_gm_equatorialOverlap(alt,inc,view_angle);

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_rotrBody")); %CL_rotrBody = %CL__PRIV.DATA.rotrBody; end

// Compatibility: old constant must not exist
if (exists("%CL_rotrEarth")); CL__warning("Variable %CL_rotrEarth exists: use %CL_rotrBody instead"); %CL_rotrBody = %CL_rotrEarth; end

// Code:

CL__warnDeprecated(); // deprecated function

N = max(size(alt,2), size(inc,2), size(view_angle,2));
alt = alt .* ones(1,N);
inc = inc .* ones(1,N);
view_angle = view_angle .* ones(1,N);

I = find (alt <= 0 | inc < 0-%eps | inc > %pi+%eps | view_angle < 0); 
if ~isempty(I); CL__error('Arguments out of range'); end

I = find (inc >=%pi-%eps & inc <= %eps); 
if ~isempty(I)
  eq_overlap = %nan * ones(1,N);
  return;
end

sma = alt+%CL_eqRad;
ecc = zeros(size(alt,1),size(alt,2));

// Calcul de l'inclinaison apparente :
[mmj2] = CL_op_paramsJ2('mm',sma,ecc,inc);
[drpom, drift_gom, drM] = CL_op_driftJ2(sma,ecc,inc);
incapp = atan((mmj2.*sin(inc))./(mmj2.*cos(inc)-(%CL_rotrBody-drift_gom)));
if inc > %pi/2 then
  incapp = incapp+%pi;
end

[dlong] = CL_op_paramsJ2('lgap',sma,ecc,inc);
[alpha] = CL_gm_visiParams(sma,%CL_eqRad,"sat",view_angle,"cen");
eq_swath = 2*alpha./(sin(inc)+cos(inc).*tan(inc-incapp));
eq_overlap = eq_swath./dlong;

endfunction

