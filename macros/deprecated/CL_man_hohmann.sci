//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(ai,af, mu)
// Hohmann transfer - DEPRECATED
//
// Calling Sequence
// [delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(ai,af [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_man_dvHohmann">CL_man_dvHohmann</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the maneuvers of a Hohmann transfer from an initial
// circular orbit with semi major axis <b>ai</b> to a final circular 
// orbit with semi major axis <b>af</b>.</p>
// <p>The output argument <b>delta_v</b> is the sum of the norms 
// of the velocity increments required (|<b>dv1</b>| + |<b>dv2</b>|).</p>
// <p>Velocity increments are expressed in spherical coordinates in the "qsw" frame. </p>
// <para/><inlinemediaobject><imageobject><imagedata fileref="hohmann.gif"/></imageobject></inlinemediaobject></listitem>
// </itemizedlist>
//
// Parameters
// ai : Semi-major axis of initial circular orbit. [m] (1xN or 1x1)
// af : Semi-major axis of final circular orbit. [m] (1xN or 1x1)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// delta_v : Total delta-v required = |dv1| + |dv2|. [m/s] (1xN)
// dv1: First velocity increment, in spherical coordinates in the "qsw" local orbital frame [lambda;phi;|dv|] [rad,rad,m/s]. (3xN)
// dv2: Second velocity increment, in spherical coordinates in the "qsw" local orbital frame [lambda;phi;|dv|]. [rad,rad,m/s] (3xN)
// anv1: True anomaly at the location of the first velocity increment (in the initial orbit): as the initial orbit is circular, anv1 is arbitrary. (1xN)
// anv2: True anomaly at the location of the second velocity increment (in the intermediate orbit). [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_man_biElliptic
//
// Examples
// // 7200km to 7000km :
// ai = 7200.e3;
// af = 7000.e3;
// [delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(ai,af);
// // Check results :
// kep = [ai ; 0 ; %pi/2 ; 0 ; 0 ; anv1];
// kep1 = CL_man_applyDv(kep,dv1);
// kep1(6) = anv2;
// kep2 = CL_man_applyDv(kep1,dv2)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if (~exists("mu", "local")); mu = %CL_mu; end

// checks arguments sizes are OK / resizes
[ai,af] = CL__checkInputs(ai,1,af,1); 

if (find(ai <= 0 | af <= 0) <> [])
  CL__error("Invalid input arguments"); 
end

// 1st man : 
// initial orbit: ai / ai
// maneuver: ai -> af (opposite man position)
//
// 2nd man : 
// initial orbit: af / ai (maneuver at af)
// maneuver: ai -> af (opposite man position)

[dv1, anv1] = CL__man_raps(ai, ai, af, mu); 
[dv2, anv2] = CL__man_raps(af, ai, af, mu); 

// sum of dv norms
delta_v = dv1(3,:) + dv2(3,:); 


endfunction
