//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos_ter,vel_ter,jacob] = CL_fr_G502ter(cjd,pos_G50, vel_G50,ut1_utc)
// Gamma50 (Veis) to terrestrial frame vector transformation - DEPRECATED
//
// Calling Sequence
// [pos_ter,vel_ter,jacob] = CL_fr_G502ter(cjd,pos_G50 [,vel_G50,ut1_utc])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_fr_convert">CL_fr_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts position and (optionally) velocity vectors from Gamma50 (Veis) to 
// the terrestrial ("Earth fixed") reference frame. </p>
// <p>The jacobian of the transformation is optionally computed.</p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> Transformation from Gamma50 (Veis) to terrestrial frame consists in a single rotation 
// of the Veis sideral time (see <link linkend="CL_mod_sidTimeG50">CL_mod_sidTimeG50</link>) around the Z axis.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: modified julian day from 1950.0 (TUC) (1xN or 1x1)
// pos_G50: position vector relative to Gamma50 (Veis) frame [m] (3xN or 3x1)
// vel_G50: (optional) velocity vector relative to Gamma50 (Veis) frame [m/s] (3xN or 3x1)
// ut1_utc: (optional) ut1-utc [seconds] (default is 0) (1xN or 1x1)
// pos_ter: position vector relative to terrestrial frame [m] (3xN)
// vel_ter: (optional) velocity vector relative to terrestrial frame [m/s] (3xN)
// jacob: (optional) jacobian of the transformation [d(x,y,z,vx,vy,vz)_ter/d(x,y,z,vx,vy,vz)_G50] (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume R (mr_veis_TerVrai)
//
// See also
// CL_fr_ter2J2000Mat
// CL_fr_G502terMat
// CL_fr_ter2G50
// CL_mod_sidTimeG50
//
// Examples
// // Conversion of position G50 to terrestrial
// pos_G50 = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// cjd = [21010 , 21011];
// pos_ter = CL_fr_G502ter(cjd,pos_G50);
//
// // Conversion of position and velocity + jacobian
// pos_G50 = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// vel_G50 = [[1.e3;3.e3;7.e3] , [2.e3;3.e3;6.e3]];
// cjd = [21010 , 21011];
// [pos_ter,vel_ter,jacob] = CL_fr_G502ter(cjd,pos_G50,vel_G50);


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

vel_present = %t;
if ~exists('vel_G50','local') then vel_present = %f; end
if ~exists('ut1_utc','local') then ut1_utc=zeros(cjd); end

compute_vel = %f;
compute_jacob = %f;
vel_ter = [];
jacob = [];

[lhs,rhs] = argn()
if (rhs<2 | rhs > 4) then CL__error('check number of input arguments'); end


if (lhs >=2)
  compute_vel = %t;
end
if (lhs ==3)
  compute_jacob = %t;
end

if (~vel_present & compute_vel)
  CL__error("Input velocity missing");
end


// Check argument sizes, and resize if necessary: 
if (vel_present)
  [cjd,pos_G50,vel_G50,ut1_utc] = CL__checkInputs(cjd,1, pos_G50,3, vel_G50,3, ut1_utc,1);
else
  [cjd,pos_G50,ut1_utc] = CL__checkInputs(cjd,1, pos_G50,3, ut1_utc,1);
end


// pos_ter computation :
cjd_ut1 = cjd + ut1_utc/86400.;
[tsid,tsidt] = CL_mod_sidTimeG50(cjd_ut1);

costsid = cos(tsid);
sintsid = sin(tsid);

pos_ter = [ costsid .* pos_G50(1,:) + sintsid .* pos_G50(2,:);
           -sintsid .* pos_G50(1,:) + costsid .* pos_G50(2,:);
            pos_G50(3,:) ];


// Velocity/jacobian computation
if (compute_vel)
  
  n = size(pos_G50,2); 

  // omega ter/G50 (coordinates in either frame)
  omega = [ zeros(1,n); zeros(1,n); tsidt.*ones(1,n) ];

  // next line: vel_ter = vel/G50, coordinates in ter
  vel_ter = [ costsid .* vel_G50(1,:) + sintsid .* vel_G50(2,:);
             -sintsid .* vel_G50(1,:) + costsid .* vel_G50(2,:);
              vel_G50(3,:) ];

  // next line: vel_ter = vel/ter, coordinates in ter
  vel_ter = vel_ter - CL_cross(omega, pos_ter); 

  // jacobian computation
  if (compute_jacob)

    // transf matrix G50 -> ter 
    // X_ter = mat * X_G50 
    mat = zeros(3,3,n); 
    mat(1,1,1:n) = costsid; 
    mat(1,2,1:n) = sintsid; 
    mat(2,1,1:n) = -sintsid; 
    mat(2,2,1:n) = costsid; 
    mat(3,3,1:n) = 1; 

    jacob = CL_rot_pvJacobian(mat, omega); 
  
  end

end


endfunction
