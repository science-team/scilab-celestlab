//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [acc_diff] = CL_cw_diffDrag(altitude,ballistic_coef_target,ballistic_coef_chaser, er,mu)
// Differential acceleration drag - DEPRECATED
//
// Calling Sequence
// [acc_diff] = CL_cw_diffDrag(altitude,cbal1,cbal2 [,er,mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Computes the differential acceleration of a chaser relative to a target due to atmospheric drag. </p>
// <p> The atmospheric density model used is an exponential model. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// altitude: Target altitude [m] (1xN)
// ballistic_coef_target: Target ballistic coefficient S*cx/m S=equivalent drag surface, cx = drag coefficient, m = mass [m^2/kg] (1xN)
// ballistic_coef_chaser:  Chaser ballistic coefficient S*cx/m S=equivalent drag surface, cx = drag coefficient, m = mass [m^2/kg] (1xN)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// acc_diff: Differential acceleration (chaser relative to target) [m/s^2] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_mod_expDensityDrag
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, 16.3.2.4.7
//
// Examples
// [acc_diff]=CL_cw_diffDrag(350000,2.25,2.15)
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('mu','local') then mu=%CL_mu; end

acc_diff = CL_mod_expDensityDrag(altitude,ballistic_coef_target,er,mu) - CL_mod_expDensityDrag(altitude,ballistic_coef_chaser,er,mu)

endfunction


