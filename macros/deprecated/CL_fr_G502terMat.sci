//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M] = CL_fr_G502terMat(cjd, ut1_utc)
// Gamma50 (Veis) to terrestrial frame transformation matrix - DEPRECATED
//
// Calling Sequence
// M = CL_fr_G502terMat(cjd [,ut1_utc])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_fr_convertMat">CL_fr_convertMat</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the frame transformation matrix <b>M</b> from Gamma50 (Veis) to the 
// terrestrial ("Earth fixed") reference frame.</p>
// <p> By convention, <b>M</b> multiplied by coordinates relative to G50 yields coordinates relative to the terrestrial frame. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> Transformation from Gamma50 (Veis) to terrestrial frame consists in a single rotation 
// of the Veis sideral time (see <link linkend="CL_mod_sidTimeG50">CL_mod_sidTimeG50</link>) around the Z axis.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: modified julian day from 1950.0 (UTC) (1xN)
// ut1_utc : (optional) ut1-utc [seconds] (default is 0) (1xN)
// M: Gamma50 (Veis) to terrestrial frame transformation matrix (3x3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume R (mr_tsid_veis)
//
// See also
// CL_fr_ter2J2000Mat
// CL_fr_G502ter
// CL_fr_ter2G50
// CL_mod_sidTimeG50
// CL_fr_H0n2J2000Mat
//
// Examples
// // Conversion G50 to terrestrial
// pos_G50 = [3500.e3;2500.e3;5800.e3];
// cjd = 21010;
// M = CL_fr_G502terMat(cjd);
// pos_ter = M * (pos_G50);
// pos_ter = CL_fr_G502ter(cjd,pos_G50);
//
// // Conversion terrestrial to G50
// pos_ter = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// cjd = [21010 , 21011];
// M = CL_fr_G502terMat(cjd);
// pos_G50 = M' * (pos_ter);
// pos_G50 = CL_fr_ter2G50(cjd,pos_ter);

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('ut1_utc','local') then ut1_utc=zeros(cjd); end

cjd_ut1 = cjd + ut1_utc/86400.;
tsid = CL_mod_sidTimeG50(cjd_ut1);
M = CL_rot_angles2matrix(3,tsid);

endfunction
