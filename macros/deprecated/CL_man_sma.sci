//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [dv,anv] = CL_man_sma(ai,ei,af,posman,mu)
// Change of semi-major axis (elliptical orbits) - DEPRECATED
//
// Calling Sequence
// [dv,anv] = CL_man_sma(ai,ei,af [,posman,mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_man_dvSma">CL_man_dvSma</link></p>
// <p></p></listitem>
// <listitem>
// <p>Delta-v required to change the semi major axis by 
// one maneuver at the peripasis or apoapsis.</p>
// <p>The output argument <b>dv</b> is the delta-V in spherical coordinates in the "qsw" local orbital frame. </p>
// <p><b>anv</b> is the true anomaly at the position of the maneuver.</p>
// </listitem>
// <listitem>
// <p> Note: The results are set to %nan if the targeted semi major-axis is unreachable.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ai : Initial semi major axis [m] (1xN or 1x1)
// ei: Initial eccentricity (1xN or 1x1)
// af : Final semi major axis [m] (1xN or 1x1)
// posman: (optional) Flag specifying the position of the maneuver: 0 or "per" -> periapsis, 1 or "apo" -> apoapsis. Default is at the periapsis. (1xN or 1x1)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// dv : Delta_v in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)
// anv: True anomaly at the position of maneuver [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_man_biElliptic
// CL_man_hohmann
// CL_man_hohmannG
//
// Examples
// ai = 7200.e3;
// af = 7000.e3;
// ei = 0.1;
// [dv,anv] = CL_man_sma(ai,ei,af)
// // Check results :
// kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv];
// kep1 = CL_man_applyDv(kep,dv)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if (~exists("posman", "local")); posman = 0; end
if (~exists("mu", "local")); mu = %CL_mu; end

// convert posman type to "real"
if (typeof(posman) == "string")
  str = posman; 
  posman = %nan * ones(str); 
  posman(find(str == "per")) = 0; 
  posman(find(str == "apo")) = 1; 
end

// checks arguments sizes are OK / resizes
[ai,ei,af,posman] = CL__checkInputs(ai,1,ei,1,af,1,posman,1); 

if (find(ai <= 0 | ei < 0 | ei >= 1 | af <= 0) <> [])
  CL__error("Invalid input arguments (orbital elements)"); 
end

if (find(posman <> 0 & posman <> 1) <> [])
  CL__error("Invalid value for ''posman''"); 
end


// sgn: (man at periapsis) => 1, apoapsis => -1
sgn = ones(posman); 
I = find(posman == 1); 
sgn(I) = -1;

// r: radius at maneuver position
// r_opp: opposite radius before maneuver
// r_opp2: opposite radius after maneuver (af = (r+r_opp2)/2)
r      = ai .* (1 - sgn .* ei); 
r_opp  = ai .* (1 + sgn .* ei); 
r_opp2 = 2 * af - r; 

// computation only if r_opp2 > 0, otherwise: %nan
// note: CL__man_opp called with whole vector to simplify code

I = find(r_opp2 <= 0);
r_opp2(I) = r_opp(I); // arbitrary, to enable computation

[dv, anv] = CL__man_raps(r, r_opp, r_opp2, mu); 

dv(:,I) = %nan; 
anv(I) = %nan; 


endfunction
