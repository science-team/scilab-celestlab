//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M] = CL_fr_H0n2J2000Mat(lon,cjd, ut1_utc,xp,yp,dPsi,dEps,conv_iers)
// H0-n to EME2000 (J2000) transformation matrix - DEPRECATED
//
// Calling Sequence
// M = CL_fr_H0n2J2000Mat(lon,cjd [,ut1_utc,xp,yp,dPsi,dEps,conv_iers])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Computes the frame transformation matrix <b>M</b> from H0-n to EME2000.</p>
// <p> By convention, multiplying <b>M</b> by coordinates relative to H0-n yields coordinates relative to EME2000. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// lon: east longitude that defines the X axis of the H0-n frame [rad]
// cjd: modified julian day from 1950.0 (UTC). Reference time for H0-n frame. (1xN)
// ut1_utc : (optional) ut1-utc [seconds] (default is 0) (1xN)
// xp : (optional) x polar coordinate [radians] (default is 0) (1xN)
// yp : (optional) y polar coordinate [radians] (default is 0) (1xN)
// dPsi : (optional) Nutation corrections [radians] (default is 0) (1xN)
// dEps : (optional) Nutation corrections [radians] (default is 0) (1xN)
// conv_iers : (optional) Convention IERS. Only iers 1996 (Lieske/Wahr) is implemented (default is "iers_1996")
// M: H0-n to EME200 transformation matrix (3x3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_ter2J2000Mat
// CL_fr_G502terMat
//
// Examples
// // Conversion of 3 coordinates of Arianne5 ATV launch
// launch_date = CL_dat_cal2cjd(2006,4,14,19,13,43);
// pos_H31 = [3952930.5 ; 3127929.25 ; 4128420.75];
// pos_K22 = [-3755942.75 ; -3308836.25 ; -4361083.5 ];
// pos_H41 = [-1877905.62 ; -3909427.35 ; -5026024.14 ];
// vel_H31 = [-6244.61914 ; 3103.701904 ; 3619.265381];
// vel_K22 = [6356.73584 ; -2869.25708 ; -3293.44238];
// vel_H41 = [7427.6591 ; -1541.7598 ; -1576.4651];
// pos_H03 = [pos_H31 pos_K22 pos_H41] ;
// vel_H03 = [vel_H31 vel_K22 vel_H41];
// // Launch pad ELA3 at the French Guayana Space Centre
// CSG = [CL_deg2rad(-52.768602),CL_deg2rad(5.240322),-4.215];
// M = CL_fr_H0n2J2000Mat(CSG(1),launch_date);
// pos_J2000 = M*pos_H03;
// vel_J2000 = M*vel_H03;


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('ut1_utc','local') then ut1_utc=zeros(cjd); end
if ~exists('xp','local') then xp=zeros(cjd); end
if ~exists('yp','local') then yp=zeros(cjd); end
if ~exists('dPsi','local') then dPsi=zeros(cjd); end
if ~exists('dEps','local') then dEps=zeros(cjd); end
if ~exists('conv_iers','local') then conv_iers="iers_1996"; end

NP = CL_fr_ter2J2000Mat(cjd,ut1_utc,xp,yp,dPsi,dEps,conv_iers);

R = CL_rot_angles2matrix(3,-lon.*.ones(1,size(cjd,2)));
//.*.ones... is to make R same size as NP, which could be hypermatrix. If NP is hypermatrix and R is matrix,
//NP*R does not work.

M = NP*R;

endfunction
