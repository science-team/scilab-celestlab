//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [gmst,gmstp] = CL_mod_sidTimeG50(cjd_ut1)
// sidereal time in Gamma50 (Veis) frame - DEPRECATED
//
// Calling Sequence
// [gmst,gmstp] = CL_mod_sidTimeG50(cjd_ut1)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_mod_siderealTime">CL_mod_siderealTime</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the sidereal time relative to Gamma50 Veis reference frame.</p> 
// <p>It also optionally computes the first derivative of the sidereal time : <b>gmstp</b>, which is a constant value. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd_ut1: Modified (1950.0) julian day in UT1 time scale (1xN)
// gmst: Gamma50 Veis sidereal time [rad] (1xN)
// gmstp: Gamma50 Veis sidereal time first derivative [rad/s] (1xN)
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume R (mr_tsid_veis)
//
// See also
// CL_mod_sidTime
//
// Authors
// CNES - DCT/SB
//
// Examples
// cjd_utc = CL_dat_cal2cjd(2008,10,25,15,30,25)  // 25 oct 2008 at 15h30min25s
// sec_ut1_minus_utc = 0.2;
// cjd_ut1 = cjd_utc + sec_ut1_minus_utc/86400.;  // ut1 time
// gmst = CL_mod_sidTimeG50(cjd_ut1);
//
// // With first time derivative :
// [gmst,gmstp] = CL_mod_sidTimeG50(cjd_ut1);

// ************************************************************************
//               EXPLICATIONS SUR LES CONSTANTES UTILISEES
//      .................................................................
//      la reference Georges Veis - the system reference,
//      in Sao special report 200, 1er volume, 1966
//      donne les constantes
//              a1=100.075542 degres
//              a2=360.985612288 degres par jour
//
//      pour un calcul du temps sideral:
//                         gmst = ( a1 + a2*t ) modulo 2.pi
//      t est ici donne dans l'echelle TU1 en jours
//
//      on pose t = jj + frac,  avec jj entier
//
//      Pour des raisons de precision numerique, on ecrit:
//
//      gmst  = a1 + a2 * t                                      [mod 2*pi]
//                  = a1 + a2 * (jj + frac)                      [mod 2*pi]
//                  = a1 + (a2-2*pi)* jj + a2 * frac             [mod 2*pi]
//                  = a1 + (a2-2*pi)* (t-frac) + a2 * frac       [mod 2*pi]
//                  = a1 + (a2-2*pi)* t + 2*pi * frac            [mod 2*pi]
//                  = a1 + eps*t + 2*pi * frac                   [mod 2*pi]
//                    (avec eps = a2 - 2*pi rad/jour)
//
//       remarque1: on trouve souvent dans la litterature la valeur
//                  erronnee a3 = 0.004177 degres par seconde
//                  qui conduit a une
//                  valeur erronne = 0.729024d-04 radians par secondes
//       remarque2: les valeurs en radians des parameter ont ete verifiees
//                  sous maple


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

cjd_ut1_jour = int(cjd_ut1);
cjd_ut1_frac  = cjd_ut1 - cjd_ut1_jour;

// calcul du temps sidereal veis -> terrestre vrai
a1 = 1.7466477086178711333726904629689443812586; // radians, soit 100.075542 degres
a2 = 6.3003874867533010742986650751563511671780; // rad/j, soit 360.985612288 deg/j
eps = .172021795737145973733783085973453987844E-1; // = a2 - 2*pi, soit 0.985612288 degres par jour
a3 = 0.729211514670520957673456605920873977E-4;  // a2/86400
tsid_int = a1 + 2*%pi * cjd_ut1_frac + eps * cjd_ut1;

gmst = pmodulo(tsid_int,2*%pi)
gmstp = a3*ones(cjd_ut1);

endfunction
