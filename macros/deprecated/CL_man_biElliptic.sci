//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [delta_v,dv1,dv2,dv3,anv1,anv2,anv3]=CL_man_biElliptic(ai,af,rt, mu)
// Bi-elliptic transfer - DEPRECATED
//
// Calling Sequence
// [delta_v,dv1,dv2,dv3,anv1,anv2,anv3]=CL_man_biElliptic(ai,af,rt [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_man_dvBiElliptic">CL_man_dvBiElliptic</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the maneuvers of a bi-elliptical transfer from a circular 
// orbit with semi-major axis <b>ai</b> 
// to a circular orbit with semi-major axis <b>af</b>.</p> 
// <p> The apogee radius of the elliptical transfer orbit is 
// <b>rt</b>. </p>
// <p><b>delta-v</b> is the sum of the norms 
// of the velocity increments.</p>
// <p>Velocity increments are expressed in spherical coordinates in the "qsw" local orbital frame. </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="bielliptic.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ai : Semi-major axis of initial circular orbit  [m] (1xN or 1x1)
// af : Semi-major axis of final circular orbit [m] (1xN or 1x1)
// rt : Radius at the position of the second maneuver [m] (1xN or 1x1)
// mu : (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// delta_v : Total |delta-V| (=|dv1|+|dv2|+|dv3]) [m/s] (1xN)
// dv1: First delta-V, in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)
// dv2: Second delta-V, in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)
// dv3: Third delta-V, in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)
// anv1: True anomaly at the position of the 1st maneuver: initial orbit is circular so this is an arbitrary value of 0  (1xN)
// anv2: True anomaly at the position of the 2nd maneuver (either 0 or pi) [rad] (1xN)
// anv3: True anomaly at the position of the 3rd maneuver (either 0 or pi) [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for engineering students, H D Curtis, Chapter 6
//
// See also
// CL_man_hohmann
// CL_man_hohmannG
//
// Examples
// // 7000 km to 98 000km through a 280 000 transfer orbit:
// ai = 7000.e3;
// af = 98000.e3;
// rt = 280000.e3;
// [delta_v,dv1,dv2,dv3,anv1,anv2,anv3]=CL_man_biElliptic(ai,af,rt)
// // Check results :
// kep = [ai ; 0 ; %pi/2 ; 0 ; 0 ; anv1];
// kep1 = CL_man_applyDv(kep,dv1);
// kep1(6) = anv2;
// kep2 = CL_man_applyDv(kep1,dv2);
// kep2(6) = anv3;
// kep3 = CL_man_applyDv(kep2,dv3)
//
// // Same example with a Hohmann transfer:
// // more expensive !
// [delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(ai,af) 

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if (~exists("mu", "local")); mu = %CL_mu; end

// checks arguments sizes are OK / resizes
[ai,af,rt] = CL__checkInputs(ai,1,af,1,rt,1); 

if (find(ai <= 0 | af <= 0 | rt <= 0) <> [])
  CL__error("Invalid input arguments"); 
end

// 1st man : 
// initial orbit: ai / ai
// maneuver: ai -> rt (opposite man position)
//
// 2nd man : 
// initial orbit: rt / ai (maneuver at rt)
// maneuver: ai -> af (opposite man position)
//
// 3rd man : 
// initial orbit: af / rt (maneuver at af)
// maneuver: rt -> af (opposite man position)

[dv1, anv1] = CL__man_raps(ai, ai, rt, mu); 
[dv2, anv2] = CL__man_raps(rt, ai, af, mu); 
[dv3, anv3] = CL__man_raps(af, rt, af, mu); 

// sum of dv norms
delta_v = dv1(3,:) + dv2(3,:) + dv3(3,:);


endfunction
