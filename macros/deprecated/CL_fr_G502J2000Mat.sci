//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M] = CL_fr_G502J2000Mat(cjd, dPsi,dEps,conv_iers)
// Gamma50 (Veis) to EME2000 (J2000) transformation matrix - DEPRECATED
//
// Calling Sequence
// M = CL_fr_G502J2000Mat(cjd [,dPsi,dEps,conv_iers])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_fr_convertMat">CL_fr_convertMat</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the frame transformation matrix <b>M</b> from Gamma50 (Veis) to EME2000.</p>
// <p> By convention, multiplying <b>M</b> by coordinates relative to G50 yields coordinates relative to EME2000. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: modified julian day from 1950.0 (UTC) (1xN)
// ut1_utc : (optional) ut1-utc [seconds] (default is 0) (1xN)
// xp : (optional) x polar coordinate [radians] (default is 0) (1xN)
// yp : (optional) y polar coordinate [radians] (default is 0) (1xN)
// dPsi : (optional) Nutation corrections [radians] (default is 0) (1xN)
// dEps : (optional) Nutation corrections [radians] (default is 0) (1xN)
// conv_iers : (optional) Convention IERS. Only iers 1996 (Lieske/Wahr) is implemented (default is "iers_1996")
// M: Gamma50 (Veis) to EME2000 transformation matrix (3x3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume R (mr_tsid_veis)
//
// See also
// CL_fr_ter2J2000Mat
// CL_fr_G502J2000
// CL_fr_J20002G50
// CL_fr_H0n2J2000Mat
//
// Examples
// pos_G50 = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// cjd = [21010 , 21011];
// M = CL_fr_G502J2000Mat(cjd);
// pos_J2000 = M*pos_G50;
//
// pos_J2000=CL_fr_G502J2000(cjd,pos_G50);
//

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('dPsi','local') then dPsi=zeros(cjd); end
if ~exists('dEps','local') then dEps=zeros(cjd); end
if ~exists('conv_iers','local') then conv_iers="iers_1996"; end

xp = zeros(cjd); // No polar motion in ter2G50 => must be 0
yp = zeros(cjd);
ut1_utc = zeros(cjd); // Arbitrary value

cjd_ut1 = cjd + ut1_utc/86400.;
tsid = CL_mod_sidTimeG50(cjd_ut1)
Veis = CL_rot_angles2matrix(3,tsid);

// Terrestrial to J2000 matrix :
NP = CL_fr_ter2J2000Mat(cjd,ut1_utc,xp,yp,dPsi,dEps,conv_iers);

// G50 to J2000 matrix :
M = NP*Veis;

endfunction
