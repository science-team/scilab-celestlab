//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos_J2000,vel_J2000,jacob] = CL_fr_G502J2000(cjd,pos_G50, vel_G50,dPsi,dEps,conv_iers)
// Gamma50 (Veis) to EME2000 (J2000) vector transformation - DEPRECATED
//
// Calling Sequence
// [pos_J2000,vel_J2000,jacob] = CL_fr_G502J2000(cjd,pos_G50 [,vel_G50,dPsi,dEps,conv_iers])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_fr_convert">CL_fr_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts position and (optionally) velocity vectors from Gamma50 (Veis) to EME2000. </p>
// <p>The jacobian of the transformation is optionally computed.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: modified julian day from 1950.0 (UTC) (1xN or 1x1)
// pos_G50: position vector relative to Gamma50 (Veis) [m] (3xN or 3x1)
// vel_G50: (optional) velocity vector relative to Gamma50 (Veis) [m/s] (3xN or 3x1)
// dPsi : (optional) Nutation corrections [radians] (default is 0) (1xN or 1x1)
// dEps : (optional) Nutation corrections [radians] (default is 0) (1xN or 1x1)
// conv_iers : (optional) Convention IERS. Only iers 1996 (Lieske/Wahr) is implemented (default is "iers_1996")
// pos_J2000: position vector relative to EME2000 [m] (3xN)
// vel_J2000: (optional) velocity vector relative to EME2000 [m/s] (3xN)
// jacob: (optional) jacobian of the transformation (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume R (mr_tsid_veis)
//
// See also
// CL_fr_G502J2000Mat
// CL_fr_J20002G50
//
// Examples
// // Conversion of position G50 to J2000
// pos_G50 = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// cjd = [21010 , 21011];
// pos_J2000=CL_fr_G502J2000(cjd,pos_G50);
//
// // Conversion of position and velocity : G50 to J2000  + jacobian
// pos_G50 = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// vel_G50 = [[1.e3;3.e3;7.e3] , [2.e3;3.e3;6.e3]];
// cjd = [21010 , 21011];
// [pos_J2000,vel_J2000,jacob]=CL_fr_G502J2000(cjd,pos_G50,vel_G50);

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs]=argn();

vel_present = %t;

if ~exists('vel_G50','local') then vel_present=%f; end
if ~exists('dPsi','local') then dPsi=zeros(cjd); end
if ~exists('dEps','local') then dEps=zeros(cjd); end
if ~exists('conv_iers','local') then conv_iers="iers_1996"; end

if(~vel_present & lhs >=2)
  CL__error("Input velocity missing");
end

// Check argument sizes, and resize if necessary: 
if (vel_present)
  [cjd,pos_G50,vel_G50,dPsi,dEps] = CL__checkInputs(cjd,1, pos_G50,3, vel_G50,3, dPsi,1, dEps,1);
else
  [cjd,pos_G50,dPsi,dEps] = CL__checkInputs(cjd,1, pos_G50,3, dPsi,1, dEps,1);
end


xp = zeros(cjd); // No polar motion in G502ter => must be 0
yp = zeros(cjd);
ut1_utc = zeros(cjd); // Arbitrary value

select lhs
  // position only :
  case 1
    vel_G50 = zeros(pos_G50);
    vel_ter = zeros(pos_G50);
    [pos_ter] = CL_fr_G502ter(cjd,pos_G50,vel_G50,ut1_utc);
    [pos_J2000] = CL_fr_ter2J2000(cjd,pos_ter,vel_ter,ut1_utc,xp,yp,dPsi,dEps,conv_iers);
  // position and velocity :  
  case 2
    [pos_ter,vel_ter] = CL_fr_G502ter(cjd,pos_G50,vel_G50,ut1_utc);
    [pos_J2000,vel_J2000] = CL_fr_ter2J2000(cjd,pos_ter,vel_ter,ut1_utc,xp,yp,dPsi,dEps,conv_iers);
  // with jacobian :
  case 3  
    [pos_ter,vel_ter,jacob1] = CL_fr_G502ter(cjd,pos_G50,vel_G50,ut1_utc);
    [pos_J2000,vel_J2000,jacob2] = CL_fr_ter2J2000(cjd,pos_ter,vel_ter,ut1_utc,xp,yp,dPsi,dEps,conv_iers);
    jacob = jacob2*jacob1;
  else
     CL__error("Invalid number of output arguments");
  end


endfunction
