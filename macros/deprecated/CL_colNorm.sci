//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [norms] = CL_colNorm(M)
// Norm of column vectors - DEPRECATED
//
// Calling Sequence
// norms = CL_colNorm(M)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_norm">CL_norm</link></p>
// </listitem></itemizedlist>
//
// Parameters
// M: Matrix (PxNxK) 
// norms: Norms of column vectors (1xNxK), where norms(1,j,k) = norm(M(:,j,k))
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_norm
//
// Examples
// // With a matrix :
// M = rand(3,5);
// norms = CL_colNorm(M);
//
// // With an hypermatrix :
// M = rand(3,5,2);
// norms = CL_colNorm(M);


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

norms = CL_norm(M);

endfunction
