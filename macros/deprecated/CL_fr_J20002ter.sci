//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos_ter,vel_ter,jacob] = CL_fr_J20002ter(cjd,pos_J2000, vel_J2000,ut1_utc,xp,yp,dPsi,dEps,conv_iers)
// EME2000 (J2000) to terrestrial frame vector transformation - DEPRECATED
//
// Calling Sequence
// [pos_ter,vel_ter,jacob] = CL_fr_J20002ter(cjd,pos_J2000 [,vel_J2000,ut1_utc,xp,yp,dPsi,dEps,conv_iers])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_fr_convert">CL_fr_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts position and (optionally) velocity vectors from EME2000 to the 
// terrestrial ("Earth fixed") reference frame.</p> 
// <p>The jacobian of the transformation is optionally computed.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: modified julian day from 1950.0 (UTC) (1xN or 1x1)
// pos_J2000: position vector relative to EME2000 [m] (3xN or 3x1)
// vel_J2000: (optional) velocity vector relative to EME2000 [m/s] (3xN or 3x1)
// ut1_utc : (optional) ut1-utc [seconds] (default is 0) (1xN or 1x1)
// xp : (optional) x polar coordinate [radians] (default is 0) (1xN or 1x1)
// yp : (optional) y polar coordinate [radians] (default is 0) (1xN or 1x1)
// dPsi : (optional) Nutation corrections [radians] (default is 0) (1xN or 1x1)
// dEps : (optional) Nutation corrections [radians] (default is 0) (1xN or 1x1)
// conv_iers : (optional) Convention IERS. Only iers 1996 (Lieske/Wahr) is implemented (default is "iers_1996")
// pos_ter: position vector relative to terrestrial frame [m] (3xN)
// vel_ter: (optional) velocity vector relative to terrestrial frame [m/s] (3xN)
// jacob: (optional) jacobian of the transformation (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) IERS Conventions (1996), Dennis D. McCarthy
// 2) Explanatory Supplement to the Astronomical Almanac, Seidelman (1992)
//
// See also
// CL_fr_ter2J2000Mat
// CL_fr_J20002ter
//
// Examples
// // Position
// pos_J2000 = [7000.e3; 0; 0];
// cjd = [21010 , 21011];
// pos_ter = CL_fr_J20002ter(cjd, pos_J2000);
//
// // Position, velocity and jacobian
// pos_J2000 = [7000.e3; 0; 0];
// vel_J2000 = [0; 7000; 0];
// cjd = [21010 , 21011];
// [pos_ter, vel_ter, jacob] = CL_fr_J20002ter(cjd, [pos_J2000,pos_J2000], [vel_J2000,vel_J2000])


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

vel_present = %t;

if ~exists('vel_J2000','local') then vel_J2000 = []; vel_present = %f; end
if ~exists('ut1_utc','local') then ut1_utc = zeros(cjd); end
if ~exists('xp','local') then xp = zeros(cjd); end
if ~exists('yp','local') then yp = zeros(cjd); end
if ~exists('dPsi','local') then dPsi = zeros(cjd); end
if ~exists('dEps','local') then dEps = zeros(cjd); end
if ~exists('conv_iers','local') then conv_iers = "iers_1996"; end

if (conv_iers <> "iers_1996") then
  CL__error("Only iers_1996 model is implemented");
end

vel_ter = [];
jacob = [];

compute_vel = %f; 
compute_jac = %f; 

if (argn(1) == 1)
  flag = "n";  // means that only the matrix will be computed
end
if (argn(1) >= 2)
  compute_vel = %t; 
  flag = "p";  // means that the matrix and angular velocity vectors will be computed
end
if (argn(1) >= 3)
  compute_jac = %t; 
end

if (compute_vel & ~vel_present)
  CL__error("Input velocity missing");
end

// Check argument sizes, and resize if necessary: 
if (vel_present)
  [cjd,pos_J2000,vel_J2000,ut1_utc,xp,yp,dPsi,dEps] = CL__checkInputs(cjd,1, pos_J2000,3, vel_J2000,3, ut1_utc,1, xp,1, yp,1, dPsi,1, dEps,1 );
else
  [cjd,pos_J2000,ut1_utc,xp,yp,dPsi,dEps] = CL__checkInputs(cjd,1, pos_J2000,3, ut1_utc,1, xp,1, yp,1, dPsi,1, dEps,1 );
end

// pole - associated angular velocity = 0
W = CL_mod_polarMotionMatrix(xp,yp);

//Conversion TUC --> TAI
[cjdtai]=CL_dat_utc2tai(cjd);
jj_tai = floor(cjdtai);
sec_tai = (cjdtai-jj_tai)*86400.0;

cjdut1 = cjd + ut1_utc/86400.0;

[TempsSideralMoyen,TSMP] = CL_mod_sidTime(cjdut1);

[eps0,eps0p] = CL_mod_meanObliquity(jj_tai,sec_tai,flag);

[NUT,F,NUTP,FP] = CL_mod_nutationAngles(jj_tai,sec_tai,flag);

[equi,equip] = CL_mod_equinoxesEquation(NUT(1,:),eps0,F(5,:),NUTP(1,:),eps0p,FP(5,:));
TempsSideralVrai = TempsSideralMoyen+equi;
R = CL_rot_angles2matrix(3,-TempsSideralVrai);

// angular rotation vector that corresponds to R matrix
// same conventions as for other angles
OM_rot = [zeros(TempsSideralVrai); zeros(TempsSideralVrai); -(equip+TSMP).*ones(TempsSideralVrai)];   

NUT = NUT+[dPsi;dEps];
[N,OM_nut] = CL_mod_nutationMatrix([eps0; NUT;],[eps0p ; NUTP],[],flag);

[K,KP,KPP] = CL_mod_precessionAngles(jj_tai,sec_tai,flag);
[PREC,OM_prec] = CL_mod_precessionMatrix(K,KP,KPP,flag);


// NOTE: 
//
// frames:  ter  -->  Fa  -->   Fb  -->   Fc  -->     J2000
// mat:           W        R         N        PREC      
// omega:         0      OM_rot    OM_nut    OM_prec
//
// omega: ang velocity F2/F1 with coordinates in F2 with:  
//        X_F2 = mat * X_F1 
// example: 
//   X_J2000 = PREC * X_Fc
//   OM_prec = omega J2000/Fc with coordinates in J2000


// NOTE: omega in CL_rot_pvConvert call:
// final/initial, coordinates in initial frame

// Precession (PREC)
// OM_prec = omega J2000/Fc (coord in J2000)
// PREC = transf matrix Fc->J2000 <=> X_J2000 = PREC * X_Fc
[pos_tmp, vel_tmp, jacob] = CL_rot_pvConvert(pos_J2000, vel_J2000, PREC', -OM_prec, ...
                            cvel=compute_vel, cjac=compute_jac); 

// Nutation (N)
// OM_nut = omega Fc/Fb (coord in Fc)
// N = transf matrix Fb->Fc <=> X_Fc = N * X_Fb
[pos_tmp, vel_tmp, jacob_tmp] = CL_rot_pvConvert(pos_tmp, vel_tmp, N', -OM_nut, ...
                                cvel=compute_vel, cjac=compute_jac); 
jacob = jacob_tmp * jacob; 

// Earth rotation (R*W)
// OM_rot = omega Fb/ter (coord in Fb)
// R*W = transf matrix ter->Fb <=> X_Fb = (R*W) * X_ter
[pos_ter, vel_ter, jacob_tmp] = CL_rot_pvConvert(pos_tmp, vel_tmp, (R*W)', -OM_rot, ...
                                cvel=compute_vel, cjac=compute_jac); 
jacob = jacob_tmp * jacob; 



endfunction




