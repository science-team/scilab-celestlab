//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ro] = CL_mod_expDensity(altitude, prctl,h_ro)
// Exponential density model - DEPRECATED
//
// Calling Sequence
// ro = CL_mod_expDensity(altitude [,prctl,h_ro])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_mod_atmUS76">CL_mod_atmUS76</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the density value at a given altitude using an exponential model.</p> 
// <p> The confidence index is given by the input percentile (<b>prctl</b>). 
// The other parameters of the exponential model (reference density and altitude) are function of the input altitude and prctl values. </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="expAtm.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// altitude: Altitude [m]
// prctl: (optional) Percentile level. Possible values are 1, 5, 33, 67, 95, 99, 100. Default is 100
// h_ro: (optional) Inverse of scale height (beta) [km^-1]. Default is 0.0153
// ro : Density [kg/m^3]
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_mod_atmUS76
// CL_mod_expDensityDrag
// CL_cw_diffDrag
//
// Bibliography
// 1) ATV-CC, Technical Note, Mission analysis input data ANNEX (Time, reference frames, conventions and models for mission analysis) [DCT/SB/MS 2004-543]
// 2) Space Station Program Natural Environment Definition for Desing, International Space Station, Alpha (Revision B, February 8, 1994) [SSP 30425 Revision B]
//
// Examples
// altitude=[350000 360000];
// [ro]=CL_mod_expDensity(altitude)
// [ro]=CL_mod_expDensity(altitude,67,0.0155)
// [ro]=CL_mod_expDensity(altitude,h_ro=0.0155)
//

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('prctl','local') then prctl=100; end
if ~exists('h_ro','local') then h_ro=0.0153; end

altitude = altitude./1000 //altitude in [km]

//alt mean 1%sd 5%sd 33%sd 67%sd 95%sd 99%sd 100%sd
coeffs=[250 8.629e-11 -4.066e-11 -3.559e-11 -1.718e-11 1.921e-11 5.291e-11 6.621e-11 8.741e-11
        275 4.869e-11 -2.576e-11 -2.277e-11 -1.135e-11 1.350e-11 3.946e-11 5.060e-11 6.971e-11
        300 2.868e-11 -1.658e-11 -1.476e-11 -7.570e-12 9.510e-12 2.934e-11 3.854e-11 5.542e-11
        325 1.748e-11 -1.085e-11 -9.725e-12 -5.100e-12 6.730e-12 2.183e-11 2.932e-11 4.397e-11
        350 1.095e-11 -7.212e-12 -6.504e-12 -3.488e-12 4.790e-12 1.628e-11 2.234e-11 3.487e-11
        375 7.008e-12 -4.856e-12 -4.404e-12 -2.408e-12 3.442e-12 1.221e-11 1.709e-11 2.770e-11
        400 4.567e-12 -3.306e-12 -3.015e-12 -1.681e-12 2.496e-12 9.203e-12 1.312e-11 2.205e-11
        425 3.019e-12 -2.270e-12 -2.081e-12 -1.182e-12 1.823e-12 6.979e-12 1.013e-11 1.761e-11
        450 2.021e-12 -1.570e-12 -1.446e-12 -8.380e-13 1.339e-12 5.321e-12 7.854e-12 1.411e-11
        475 1.366e-12 -1.091e-12 -1.010e-12 -5.955e-13 9.900e-13 4.080e-12 6.122e-12 1.135e-11
        500 9.322e-13 -7.616e-13 -7.087e-13 -4.257e-13 7.348e-13 3.142e-12 4.793e-12 9.168e-12];

prctl_i = [1 5 33 67 95 99 100;3:9] //line1: possible values for percentile
                                    //line2: column index in coeffs for each possible percentile

if find(prctl_i(1,:)==prctl)==[] then CL__error('non valid percentile'); end

j_coeffs = prctl_i(2,find(prctl_i(1,:)==prctl))
i_coeffs = interp1(coeffs(:,1),(1:size(coeffs,1))',altitude,'nearest','extrap') //nearest altitude coeff
z_zero = coeffs(i_coeffs,1)
ro_zero = coeffs(i_coeffs,2) + coeffs(i_coeffs,j_coeffs) //mean+deviation

ro = ro_zero'.*exp(-1.0.*h_ro.*(altitude-z_zero'))

endfunction
