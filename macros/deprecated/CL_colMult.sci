//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [N] = CL_colMult(coef,M)
// Multiplication of column vectors by coefficients - DEPRECATED
//
// Calling Sequence
// N = CL_colMult(coef,M)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_dMult">CL_dMult</link></p>
// </listitem></itemizedlist>
//
// Parameters
// coef: Coefficients (1 x N)
// M: Matrix (P x N)
// N: Matrix (P x N) such that: N(:,j) =  coef(j) * M(:,j)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_dMult
//
// Examples
// coef = [ 1 2 3];
// v = [ 1 2 3 ; 4 5 6 ; 7 8 9];
// w = CL_colMult(coef,v)


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

N = CL_dMult(coef,M);

endfunction
