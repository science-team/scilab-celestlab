//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [equi,equip] = CL_mod_equinoxesEquation(dPsi,eps0,F5, dPsip,eps0p,F5p)
// Equation of the equinoxes (IERS 1996) - DEPRECATED
//
// Calling Sequence
// [equi,equip] = CL_mod_equinoxesEquation(dPsi,eps0,F5 [,dPsip,eps0p,F5p])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Computes equation of the equinoxes: Greenwich sideral time - Mean Greenwich sideral time (standard IERS 1996)</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// dPsi: longitude (see <link linkend="CL_mod_nutationAngles">CL_mod_nutationAngles</link>) [rad](1xN)
// dPsip: (optional with eps0p and F5p) longitude first time derivative (see <link linkend="CL_mod_nutationAngles">CL_mod_nutationAngles</link>) [rad/s] (1xN)
// eps0: mean obliquity (see <link linkend="CL_mod_meanObliquity">CL_mod_meanObliquity</link>) [rad] (1xN)
// eps0p: (optional with dPsip and F5p) mean obliquity first time derivative (see <link linkend="CL_mod_meanObliquity">CL_mod_meanObliquity</link>) [rad/s] (1xN)
// F5: Mean longitude of Moon's ascending node (see <link linkend="CL_mod_nutationArg">CL_mod_nutationArg</link>) [rad] (1xN)
// F5p: (optional with eps0p and dPsip) Mean longitude of Moon's ascending node first time derivative (see <link linkend="CL_mod_nutationArg">CL_mod_nutationArg</link>) [rad/s] (1xN)
// equi: equinoxes equation (1xN)
// equip: (optional, needs dPsip, eps0p and F5p) first time derivative of equi (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) IERS Conventions (1996), Dennis D. McCarthy
// 2) Explanatory Supplement to the Astronomical Almanac, Seidelman (1992)
//
// See also
// CL_mod_nutationAngles
// CL_mod_nutationArg
// CL_mod_meanObliquity
//
// Examples
// jj_tai = [19500:1:20500];
// ss_tai = 43200*ones(jj_tai);
// // Nutation angles (dPsi,dEps) :
// [NUT,F,NUTP,FP,NUTPP,FPP]=CL_mod_nutationAngles(jj_tai,ss_tai,"s"); 
//
// // Mean obliquity :
// [eps0,eps0p,eps0pp]=CL_mod_meanObliquity(jj_tai,ss_tai,"s");
//
// // Equinoxes equation :
// [equi,equip]=CL_mod_equinoxesEquation(NUT(1,:),eps0,F(5,:),NUTP(1,:),eps0p,FP(5,:)); 
// plot(jj_tai'+ss_tai'/86400,equi');
//

// Declarations:

// Code:

CL__warnDeprecated(); // deprecated function

[lhs rhs]=argn(0);
if (rhs~=6 & rhs~=3) CL__error("check number of input arguments"); end,

sec2rad=(1/3600)*(%pi/180);
A = sec2rad*0.00264; B=sec2rad*0.000063;


equi =dPsi.*cos(eps0);  //equation des equinoxes
equi = equi + A*sin(F5) + B*sin(2*F5) ; //rajout des corrections

equip=[];
if rhs==6
  equip=dPsip.*cos(eps0) - dPsi.*eps0p.*sin(eps0);
  equip = equip + A*F5p.*cos(F5) + 2*B*F5p.*cos(2*F5);
end

endfunction
