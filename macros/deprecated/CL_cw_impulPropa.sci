//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [t,rel_pos_vel] = CL_cw_impulPropa(t_ini,rel_pos_vel_ini,man_dates,man,alt,t_end,delta_t,ballistic_coef_chaser,ballistic_coef_target,er,mu)
// Propagation of a chaser relatively to a target, with impulsives maneuvers - DEPRECATED
//
// Calling Sequence
// [t,rel_pos_vel] = CL_cw_impulPropa(t_ini,rel_pos_vel_ini,...
//        man_dates,man,alt,t_end,...
//        [delta_t,ballistic_coef_chaser,ballistic_coef_target,er,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the position and velocity vector of a chaser relative to a target when 
// impulsive maneuvres are performed by the chaser. </p>
// <p>The output argument <b>rel_pos_vel</b> contains
// the positions and velocities of the chaser relative to the target in the target's LVLH 
// reference frame at the times <b>t</b>. </p>
// <p>If no ballistic coefficients are given, acceleration due to drag is not taken into account. </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="impulPropa_X.gif"/></imageobject></inlinemediaobject></p>
// <p><inlinemediaobject><imageobject><imagedata fileref="impulPropa_Z.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// t_ini: Initial time [seconds] (1x1)
// rel_pos_vel_ini: Initial position and velocity vector of chaser in target's LVLH frame [X;Y;Z;Vx;Vy;Vz] [m;m/s] (6x1)
// man_dates: Maneuvers times [seconds] (1xM)
// man: Velocity increments [dvx, dvy, dvz] [m/s] (3xM)
// alt: Altitude of target [m]
// t_end: Final time [seconds] (1x1)
// delta_t: (optional) Time step [s] (default is 100)
// ballistic_coef_chaser: (optional) Ballistic coefficient of chaser : S*cx/m S=equivalent drag surface, cx = drag coefficient, m = mass [m^2/kg] (default is 0)
// ballistic_coef_target: (optional) Ballistic coefficient of target : S*cx/m S=equivalent drag surface, cx = drag coefficient, m = mass [m^2/kg] (default is 0)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// t: Computation times [seconds] (1xN)
// rel_pos_vel: Position and velocity vectors of chaser in target's LVLH frame at corresponding times [X;Y;Z;Vx;Vy;Vz] [m,m/s] (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cw_ballisticPropa
// CL_cw_contPropa
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II
//
// Examples
// t_ini = 0;
// rel_pos_vel_ini = [0;0;0;0;0;0];
// alt = 450.e3;
// ballistic_coef_chaser = 100 * 1 / 20.e3;
// ballistic_coef_target = 200 * 1 / 400.e3;
// t_end = 4*3600; // 4 hours of ballistic propagation
// delta_t = 100; // 100 seconds time step
// man_dates = 100;
// man = [1;0;0];
// [t,rel_pos_vel] = CL_cw_impulPropa(t_ini, ..
// rel_pos_vel_ini,man_dates,man,alt,t_end,delta_t, ..
// ballistic_coef_chaser,ballistic_coef_target)
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('ballistic_coef_chaser','local') then ballistic_coef_chaser=0; end
if ~exists('ballistic_coef_target','local') then ballistic_coef_target=0; end
if ~exists('delta_t','local') then delta_t=100; end
if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('mu','local') then mu=%CL_mu; end

// This functions deals with ballistic arcs between impulse maneuvers

// First point :
t = t_ini;
rel_pos_vel = rel_pos_vel_ini;

// Number of maneuvers :
nbman = size(man,2);

if (nbman>0)
  for i=1:nbman
    [dates,pos_vel] = CL_cw_ballisticPropa(t_ini,rel_pos_vel_ini,alt,man_dates(i),delta_t,ballistic_coef_chaser,ballistic_coef_target,er,mu); //ballistic arc until maneuver
    t = [ t(1:$-1) , dates ];
  rel_pos_vel = [ rel_pos_vel(:,1:$-1) , pos_vel ];
  rel_pos_vel(4:6,$) = rel_pos_vel(4:6,$) + man(:,i); //add maneuver at last ephemeride
    t_ini = t($);             //set last ephemeride position and velocity for next propagation
  rel_pos_vel_ini = rel_pos_vel(:,$);
  end
end

// Ballistic propa until end :
[dates,pos_vel] = CL_cw_ballisticPropa(t_ini,rel_pos_vel_ini,alt,t_end,delta_t,ballistic_coef_chaser,ballistic_coef_target,er,mu);
t = [ t(1:$-1) , dates ];
rel_pos_vel = [ rel_pos_vel(:,1:$-1) , pos_vel ];

endfunction
