//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function kep_dv = CL_man_applyDv(kep,dv_local, mu)
// Effect of maneuver on orbital elements - DEPRECATED
//
// Calling Sequence
// kep_dv = CL_man_applyDv(kep,dv_local [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_man_applyDvKep">CL_man_applyDvKep</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the effect of an impulsive maneuver (instantaneous velocity increment) on orbital elements.</p>
// <p> The (osculating) orbit elements must be given at the time of maneuver as there 
// is no orbit propagation. </p>
// <p> The maneuver is defined by the components of the delta-V in the QSW local frame in 
// spherical coordinates: </p>
// <p> <b>dv_local </b> = [lambda;phi;dv] (lambda: in-plane angle 
// (pi = towards planet and pi/2 ~ along velocity), phi: out-of-plane angle, positive towards 
// the angular momentum vector (the angular momentum vector is perpendicular to the orbit plane 
// and oriented according to the right hand rule), dv: norm of delta-V). </p>
// <p></p></listitem>
// <listitem> The orbital elements are the following:  
// <p><inlinemediaobject><imageobject><imagedata fileref="kep_par.gif"/></imageobject></inlinemediaobject></p></listitem>
// </itemizedlist>
//
// Parameters
// kep: Initial (osculating) Keplerian elements [m,rad] (6xN)
// dv_local: delta-V in spherical coordinates in the QSW local frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)
// mu : (optional) Gravitational constant [m^3/s^2] (default is %CL_mu)
// kep_dv: (Osculating) Keplerian elements including the effect of the maneuver [m,rad] (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_qswMat
// CL_fr_inertial2qsw
//
// Examples
// // Manoeuvre : 7200km to 7000km :
// ai = 7200.e3;
// af = 7000.e3;
// [delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(ai,af);
//
// // Check results using CL_man_applyDv:
// kep = [ai ; 0 ; %pi/2 ; 0 ; 0 ; anv1];
// kep1 = CL_man_applyDv(kep,dv1);
// kep1(6) = anv2;
// kep2 = CL_man_applyDv(kep1,dv2)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('mu','local') then mu=%CL_mu; end

Nkep = size(kep,2);
Ndv = size(dv_local,2);
N = max(Nkep,Ndv);
coherence = (Nkep==N|Nkep==1) & (Ndv==N|Ndv==1)
if ~coherence then CL__error('bad dimension of input arguments'); end
if N~=1
  if Nkep==1 then kep=kep*ones(1,N); end
  if Ndv==1 then dv_local=dv_local*ones(1,N); end
end

dv_cart_local = CL_co_sph2car(dv_local);  //convert spheric into cartesian coordinates
[pos,vel] = CL_oe_kep2car(kep,mu);  //convert orbital keplerian parameters to cartesien coordinates
dv_cart_inertiel = CL_fr_qsw2inertial(pos,vel,dv_cart_local);  //dv from orbital to inertial frame
vel_dv = vel + dv_cart_inertiel; //add velocity impulsion to velocity in inertial frame
//kep_dv = CL_oe_car2kep(pos,vel_dv,mu); //convert cartesian to keplerian parameters
cirequa_dv = CL_oe_car2cirEqua(pos,vel_dv,mu); //convert cartesian to circ equa parameters
kep_dv = CL_oe_cirEqua2kep(cirequa_dv); //convert circ equa to keplerian parameters

endfunction
