//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cir_equa,jacob] = CL_oe_kep2cirEqua(kep)
// Keplerian to circular equatorial adapted orbital elements - DEPRECATED
//
// Calling Sequence
// [cir_equa,jacob] = CL_oe_kep2cirEqua(kep)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_oe_kep2cireq">CL_oe_kep2cireq</link></p>
// <p>"cirEqua" orbital elements are identical to "cireq" orbital elements except that 
// ix=2*sin(i/2)*cos(raan) and iy=2*sin(i/2)*sin(raan)</p>
// <p></p></listitem>
// <listitem>
// <p>Converts classical keplerian orbital elements to orbital elements 
// adapted to near-circular near-equatorial orbits.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// kep: classical keplerian orbital elements [sma;ecc;inc;pom;raan;anm] [m,rad] (6xN)
// cir_equa: orbital elements adapted to near-circular near-equatorial orbits [sma;ex;ey;ix;iy;pom+raan+anm] [m,rad] (6xN)
// jacob: (optional) transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume V (mv_kep_cir_equa)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_cirEqua2kep
// CL_oe_kep2car
//
// Examples
// // Example 1
// kep=[24464560;0.7311;0.122138;3.10686;1.00681;0.048363];
// [cirequa]=CL_oe_kep2cirEqua(kep);
//
// // Example 2
// kep=[24464560;0.7311;0.122138;3.10686;1.00681;0.048363];
// [cirequa,jacob1]=CL_oe_kep2cirEqua(kep);
// [kep2,jacob2]=CL_oe_cirEqua2kep(cirequa);

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs] = argn();
if lhs==2
  compute_jacob=%t
else
  compute_jacob=%f
end

N = size(kep,2)

cir_equa = zeros(6,N)

cir_equa(1,:) = kep(1,:)

pom_plus_gom = kep(4,:)+kep(5,:)
ex = kep(2,:).*cos(pom_plus_gom)
cir_equa(2,:) = ex

ey = kep(2,:).*sin(pom_plus_gom)
cir_equa(3,:) = ey

sinus_i_sur_2 = sin(kep(3,:)./2)
ix = 2.*sinus_i_sur_2.*cos(kep(5,:))
cir_equa(4,:) = ix

iy = 2.*sinus_i_sur_2.*sin(kep(5,:))
cir_equa(5,:) = iy

cir_equa(6,:) = CL_rMod(pom_plus_gom + kep(6,:),2*%pi)

//jacobien calcul
if compute_jacob

   jacob = hypermat([6 6 N])
   jacob(1,1,1:N) = 1
   jacob(2,2,1:N) = cos(pom_plus_gom)
   jacob(2,4,1:N) = - ey
   jacob(2,5,1:N) = - ey
   jacob(3,2,1:N) = sin(pom_plus_gom)
   jacob(3,4,1:N) = ex
   jacob(3,5,1:N) = ex
   jacob(4,3,1:N) = cos(kep(3,:)./2).*cos(kep(5,:))
   jacob(4,5,1:N) = - iy
   jacob(5,3,1:N) = cos(kep(3,:)./2).*sin(kep(5,:))
   jacob(5,5,1:N) = ix
   jacob(6,4,1:N) = 1
   jacob(6,5,1:N) = 1
   jacob(6,6,1:N) = 1

   if (N==1) then jacob=jacob(:,:,1); end

end

endfunction
