//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [jd] = CL_dat_cjd2jd(cjd)
// Modified (1950.0) Julian day to Julian day - DEPRECATED
//
// Calling Sequence
// jd = CL_dat_cjd2jd(cjd)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_dat_convert">CL_dat_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts modified (1950.0) julian day (<b>cjd</b>: number of days elapsed since 1950.0) 
// into a Julian day (<b>jd</b>: number of days elapsed since 1st January -4712 12h).</p> 
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: (real) Modified (1950.0) Julian day (1xN)
// jd: (real) Julian day (1xN)
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Chapitre 3, section 3.3.3 Les annees
//
// See also
// CL_dat_jd2cjd
// CL_dat_jd2cal
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1
// CL_dat_cjd2jd([20000;20001])
//
// // Example 2
// cjd=[18262.5 18000];
// jd = CL_dat_cjd2jd(cjd);
// cjd2 = CL_dat_jd2cjd(jd);

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs]=argn();
if rhs~=1 then CL__error("check number of input arguments"), end

jd = cjd + 2451545 - 18262.5;

endfunction
