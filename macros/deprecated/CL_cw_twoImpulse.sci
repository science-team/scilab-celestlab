//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [delta_vi,delta_vf] = CL_cw_twoImpulse(pv_ini,pv_fin,alt,t_transfer, er,mu)
// Two-impulse transfer - DEPRECATED
//
// Calling Sequence
// [delta_vi,delta_vf]=CL_cw_twoImpulse(pv_ini,pv_fin,alt,t_transfer [,er,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the delta_v needed for a two impulse transfer.</p>
// <p>The transfer is performed with a maneuver at the starting point (delta_vi at 
// <b>A</b>) and a maneuver at the end point (delta_vf at 
// <b>B</b>). </p>
// <p>The local orbital reference frame tied to the target is LVLH. </p>
// <para/><inlinemediaobject><imageobject><imagedata fileref="twoImpulse.gif"/></imageobject></inlinemediaobject>
// <para/><inlinemediaobject><imageobject><imagedata fileref="twoimpulH.gif"/></imageobject></inlinemediaobject>
// <para/><inlinemediaobject><imageobject><imagedata fileref="twoimpulC.gif"/></imageobject></inlinemediaobject>
// </listitem>
// </itemizedlist>
//
// Parameters
// pv_ini: Initial chaser position and velocity vector (A in the figure) in the target's LVLH frame  [rx;ry;rz;vx;vy;vz]_ini [m] (6x1) 
// pv_fin: Final chaser position and velocity vector (B in the figure) in the target's LVLH frame  [rx;ry;rz;vx;vy;vz]_fin [m] (6x1) 
// alt: Target's altitude [m]
// t_transfer: Transfer time (time between the two maneuvers) [s]
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// delta_vi: Delta_v for the first maneuver [m/s] (3x1) 
// delta_vf: Delta_v for the second maneuver [m/s] (3x1) 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cw_Nmatrix
// CL_cw_Mmatrix
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, 16.3
// 2) Orbital Mechanics for engineering students, H D Curtis, Chapter 7 (section 7.5)
//
// Examples
// pv_ini = [-30e3;0;0;10;0;0];
// pv_fin = [-15860;0;0;0;0;0];
// eqRad = CL_dataGet("eqRad");
// alt = 450e3;
// omega = CL_kp_params("mm",eqRad+alt);
// periode = 2*%pi/omega;
// t_transfer = periode/4;
// [delta_vi,delta_vf]=CL_cw_twoImpulse(pv_ini,pv_fin,alt,t_transfer)
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('mu','local') then mu=%CL_mu; end

M = CL_cw_Mmatrix(alt,t_transfer,er,mu);

mat_xx = M(1:3,1:3)
mat_xv = M(1:3,4:6)
mat_vx = M(4:6,1:3)
mat_vv = M(4:6,4:6)

xi = pv_ini(1:3);
vi = pv_ini(4:6);
xf = pv_fin(1:3);
vf = pv_fin(4:6);

delta_vi = inv(mat_xv)* (xf - mat_xx*xi - mat_xv*vi)

delta_vf = vf-(mat_vx*xi + mat_vv*(vi+delta_vi))

endfunction
