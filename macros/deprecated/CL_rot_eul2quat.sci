//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [q] = CL_rot_eul2quat(phi,theta,psi)
// Euler '123' angles to quaternion - DEPRECATED
//
// Calling Sequence
// [q] = CL_rot_eul2quat(phi,theta,psi)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_rot_angles2quat">CL_rot_angles2quat</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the quaternion that corresponds to the combination of 3 successive rotations 
// of respective angles <b>phi</b>, 
// <b>theta</b>, <b>psi</b> around
// axes number 1 (x-axis), 2 (y-axis), 3 (z-axis) respectively. </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="eul2q.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - See <link linkend="Data types">Data types</link> or <link linkend="CL_rot_defQuat">CL_rot_defQuat</link> for more details on quaternions. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// phi : Rotation angle around the first (X) axis [rad] (1xN)
// theta : Rotation angle around the second (Y) axis [rad] (1xN)
// psi : Rotation angle around the third (Z) axis [rad] (1xN)
// q : Quaternion (dim N)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Mecanique spatiale - CNES Cepadues 1995, Tome I, 7.2.2.3 Les quaternions
//
// See also
// CL_rot_quat2eul
// CL_rot_defQuat
//
// Examples
// // Example 1
// phi = 0.2; // radians
// theta = 0.3;
// psi = 0.4;
// q = CL_rot_eul2quat(phi,theta,psi)
// 
// // Same computation with matrix:  
// M = CL_rot_angles2matrix([1,2,3], [phi;theta;psi]);
// q = CL_rot_matrix2quat(M) 


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

cphi2 = cos(0.5.*phi);
sphi2 = sin(0.5.*phi);
ctheta2 = cos(0.5.*theta);
stheta2 = sin(0.5.*theta);
cpsi2 = cos(0.5.*psi);
spsi2 = sin(0.5.*psi);

q0 = cpsi2 .* ctheta2 .* cphi2 - spsi2 .* stheta2 .* sphi2;
q1 = cpsi2 .* ctheta2 .* sphi2 + spsi2 .* stheta2 .* cphi2;
q2 = cpsi2 .* stheta2 .* cphi2 - spsi2 .* ctheta2 .* sphi2;
q3 = spsi2 .* ctheta2 .* cphi2 + cpsi2 .* stheta2 .* sphi2;

q = CL_rot_defQuat([q0;q1;q2;q3]);

endfunction




