//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [loctime_asc,loctime_desc]=CL_gm_lat2locTime(lat,inc,loctime_asc_node)
// Satellite's local time at given latitude - DEPRECATED
//
// Calling Sequence
// [loctime_asc,loctime_desc]=CL_gm_lat2locTime(lat,inc,loctime_asc_node)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_gm_orbSphTriangle">CL_gm_orbSphTriangle</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the satellite's local time at a given latitude. </p>
// <p>The satellite local time is computed from the local time of the ascending node 
// (<b>loctime_asc_node</b>), the orbit's inclination 
// (<b>inc</b>) and the satellite's latitude 
// (<b>lat</b>). </p>
// <p> Two solutions exist, that correspond to the satellite latitude increasing 
// (<b>loctime_asc</b>) or decreasing 
// (<b>loctime_desc</b>). </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - A spherical planet is assumed. </p>
// <p> - The model used is Keplerian and the Sun direction is constant relative to the orbit plane. </p>
// <p> - The latitude in absolute value must be less than max(inclination, pi-inclination). </p>
// <p> - A satellite local time equal to 12h means than the satellite's right ascension is equal to the Sun's right ascension. </p>
// <p> - "Local time" here means "true local time". </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// lat: Satellite latitude [rad] (1xN)
// inc: Inclination [rad] (1xN)
// loctime_asc_node: Local time of ascending node [hours] (1xN)
// loctime_asc: Satellite local time (ascending path) [hours] (1xN)
// loctime_desc: Satellite local time (descending path) [hours] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Satellites orbites et missions - Michel Capderou
//
// Examples
// inc = CL_deg2rad(98.6);
// lat = CL_deg2rad(-90+9:1:90-9);
// loctime_asc_node = 22;
// [lt_asc,lt_desc] = CL_gm_lat2locTime(lat,inc,loctime_asc_node);
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_rad2deg")); %CL_rad2deg = %CL__PRIV.DATA.rad2deg; end

// Code:
CL__warnDeprecated(); // deprecated function

inc_test = inc;
ind = find(inc_test > %pi/2);
inc_test(ind) = %pi - inc_test(ind);

ind = find(abs(lat) > abs(inc_test));

if(~isempty(ind))
  CL__error('error : lat > inc (satellite cant reach a latitude higher that its inclination)');
end

deltatau=(%CL_rad2deg/15)*asin(tan(lat)./tan(inc));
loctime_asc=loctime_asc_node + deltatau;
loctime_asc=pmodulo(loctime_asc,24);
loctime_desc=loctime_asc_node + 12-deltatau;
loctime_desc=pmodulo(loctime_desc,24);

endfunction
