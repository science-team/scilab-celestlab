//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [view_angle] = CL_gm_equatorialSwath(alt,inc, er,rotr_pla)
// Sensor view angle for an overlap ratio of 1 at equator - DEPRECATED
//
// Calling Sequence
// [view_angle] = CL_gm_equatorialSwath(alt,inc,[er,rotr_pla])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_op_equatorialSwath">CL_op_equatorialSwath</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the view angle as seen from the satellite (angle from the descending vertical) so that 
// the overlap ratio at equator is 1.</p>
// <p> The overlap ratio is the ratio Q=Leq/Ded where:</p> 
// <p> - Leq: portion of the equator seen by the sensor during the time it crosses the equator. </p>
// <p> - Deq: gap between 2 consecutive ground tracks at equator. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p> 
// <p> - The planet is assumed spherical, and the orbit circular. </p> 
// <p> - The sensor is conic and oriented towards the descending vertical. </p> 
// <p> - The gap between 2 consecutive ground tracks is computed taking the effect of J2 into account. </p> 
// <p> - The result is undefined (%nan) if inclination is 0 or pi.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// alt: Satellite's altitude [m] (1xN)
// inc: Orbit's inclination [rad] (1xN)
// view_angle: Sensor half view angle [rad] (1xN)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// rotr_pla : (optional) Rotation rate of the planet (default is %CL_rotrBody)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Satellites orbites et missions - Michel Capderou
//
// See also
// CL_gm_equatorialOverlap
//
// Examples
// alt = [822.e3 630.e3] ;
// inc = [CL_deg2rad(98.72) CL_deg2rad(96.72)];
// [view_angle] = CL_gm_equatorialSwath(alt,inc)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_rotrBody")); %CL_rotrBody = %CL__PRIV.DATA.rotrBody; end

// Compatibility: old constant must not exist
if (exists("%CL_rotrEarth")); CL__warning("Variable %CL_rotrEarth exists: use %CL_rotrBody instead"); %CL_rotrBody = %CL_rotrEarth; end

// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('rotr_pla','local') then rotr_pla=%CL_rotrBody; end

N = max(size(alt,2), size(inc,2));
alt = alt .* ones(1,N);
inc = inc .* ones(1,N);

I = find (alt <= 0 | inc < 0-%eps | inc > %pi+%eps); 
if ~isempty(I); CL__error('Arguments out of range'); end

I = find (inc >=%pi-%eps & inc <= %eps); 
if ~isempty(I)
  view_angle = %nan * ones(1,N);
  return;
end


sma = er+alt;
ecc = 0;

// Calcul de l'inclinaison apparrente :
[mmj2] = CL_op_paramsJ2('mm',sma,ecc,inc);
[drift_pom,drift_gom,drift_M] = CL_op_driftJ2(sma,ecc,inc);
incapp = atan((mmj2.*sin(inc)),(mmj2.*cos(inc)-(rotr_pla-drift_gom)));
[dlong] = CL_op_paramsJ2('lgap',sma,ecc,inc);
alpha=dlong./(2.*(sin(inc)+cos(inc).*tan(inc-incapp)));
[view_angle]=CL_gm_visiParams(sma,er,"cen",alpha,"sat");

endfunction

