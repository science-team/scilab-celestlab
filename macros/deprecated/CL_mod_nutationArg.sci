//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [F,FP,FPP] = CL_mod_nutationArg(t, flag)
// Fundamental arguments for nutation - DEPRECATED
//
// Calling Sequence
// [F,FP,FPP] = CL_mod_nutationArg(t [,flag])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Computes the fundamental arguments for nutation (<b>F</b>)
// (Sun and Moon position), and their derivatives (<b>FP</b>,<b>FPP</b>).</p>
// <p>F(1,:) -> l  = Mean Anomaly of the Moon</p>
// <p>F(2,:) -> l' = Mean Anomaly of the Sun</p>
// <p>F(3,:) -> F  = L - OMEGA (where L is the mean longitude of the moon)</p>
// <p>F(4,:) -> D  = Mean Elongation of the Moon from the Sun</p>
// <p>F(5,:) -> OMEGA = Mean Longitude of the Ascending Node of the Moon</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// t : Time from 1st January 2000 12h in centuries (number of days / 36525), (TT time scale) (1xN)
// flag : (optional) "n" -> F only; "p" -> F and Fp; "s" -> F, Fp and Fpp (default depends on number of output variables)
// F : Fundamental arguments for nutation (5xN) [rad]
// FP : (optional) First time derivative of F (5xN) [rad/s]
// FPP : (optional) Second time derivative of F (5xN) [rad/s^2]
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) IERS Conventions (1996), Dennis D. McCarthy
//
// See also
// CL_mod_nutationAngles
//
// Examples
// t=-0.05:0.001:0.05;
// [F,FP,FPP]=CL_mod_nutationArg(t,"s")
//

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs]=argn(0);

if ~exists('flag','local')
  select lhs
    case 1
      flag='n';
    case 2
      flag='p';
    case 3
      flag='s';
    else
      CL__error("check number of output arguments");
  end
end

if rhs>2 CL__error("check number of input arguments"); end,

if (flag~="s" & flag~="p" & flag~="n") then
  CL__error("value of parameter ''flag'' = "+flag+" unknown");
end

COEFF=[134.96340251d0, 1717915923.2178d0, 31.8792d0, 0.051635d0, -0.00024470d0;
       357.52910918d0,  129596581.0481d0, -0.5532d0,-0.000136d0, -0.00001149d0;
        93.27209062d0, 1739527262.8478d0,-12.7512d0,-0.001037d0,  0.00000417d0;
       297.85019547d0, 1602961601.2090d0, -6.3706d0, 0.006593d0, -0.00003169d0;
       125.04455501d0,   -6962890.2665d0,  7.4722d0,  0.007702d0,-0.00005939d0;];

//evaluation de polynomes avec la methode de horner pour minimiser les
//erreurs numeriques

//changement unitees
COEFF = [(%pi/180)*COEFF(:,1)   (%pi/180)*(1/3600)*COEFF(:,2:5)];

//evaluation du polynom
F=CL_evalPoly(COEFF,t,0);
F=modulo(F,2*%pi);
FP=[]; FPP=[];
if (flag == "p" | flag =="s") then
  FP=CL_evalPoly(COEFF,t,1);
  FP=FP/(86400*36525);
end,
if (flag =="s") then
  FPP=CL_evalPoly(COEFF,t,2);
  FPP=FPP/((86400*36525)^2);
end

endfunction
