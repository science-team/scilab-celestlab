//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [B,I] = CL_matSort(A,num, option,direction);
// Matrix sorting by row or column - DEPRECATED
//
// Calling Sequence
// [B,I] = CL_matSort(A,num [,option,direction]);
//
// Description
// <itemizedlist><listitem>
// <p>Sorts elements of a matrix.</p>
// <p></p>
// <p>The matrix <b>A</b> can be considered as a set of column vectors or row vectors. </p>
// <p>- If <b>option</b> is 'c', A is considered as a set of column vectors. <b>num</b> is then 
// the number of the column to be sorted (by rows!). All the other columns are sorted accordingly. </p>
// <p>- If <b>option</b> is 'r', A is considered as a set of row vectors. <b>num</b> is then 
// the number of the row to be sorted (by columns!). All the other rows are sorted accordingly. </p>
// <p></p>
// <p>The sorting order is determined by <b>direction</b> (increasing by default). </p>
// <p></p></listitem>
// <listitem>
// <p><b>Warning! </b> </p>
// <p> The meaning of <b>option</b> may be confusing. The meaning 
// is different from that of the 'option' argument of Scilab's gsort function. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// A: Matrix (PxN)
// num: Row or column number (1x1)
// option: (string, optional) Indicates if num is a column number ('c') or row number ('r'). Default is 'c' (1x1)
// direction: (string, optional) Sorting order: 'i' = increasing, 'd' = decreasing (1x1). Default is 'i' (1x1)
// B: Sorted matrix (PxN)
// I: Index such that A(:,I) = B if option='r' or A(I,:) = B if option='c' (1xN or Px1)
//
// Authors
// CNES - DCT/SB
//
// Examples
// A=[1,3,2,4; 1000,1010,1020,990; 10,80,15,45];
// // Sorts 2nd column increasingly, other columns sorted accordingly
// [B] = CL_matSort(A,2);
//
// // Sorts 3rd row decreasingly, other rows sorted accordingly
// [B,I] = CL_matSort(A,3,'r','d');
// A(:,I) - B;  // == 0


// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

if ~exists('option','local'); option='c'; end;
if ~exists('direction','local'); direction='i'; end;

// Handle [] case
if (A == [])
  B = [];
  return;  // < -- RETURN !
end

li = size(A,'r');
co = size(A,'c');

if (option == 'c' & (num < 1 | num > co))
  CL__error('Invalid column number');
end

if (option == 'r' & (num < 1 | num > li))
  CL__error('Invalid row number');
end

if (option == 'c')
  par_gsort = 'r';   
  vect = A(:,num);
else
  par_gsort = 'c';  
  vect = A(num,:);
end

[C,I] = gsort(vect, par_gsort, direction);

if (option == 'c')
  B = A(I,:);
else
  B = A(:,I);
end


endfunction

