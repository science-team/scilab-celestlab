//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [kep_t2] = CL_ex_secularJ2(t1,kep_t1,t2, er,mu,j2)
// Orbit propagation using "secular J2" model
//
// Calling Sequence
// kep_t2 = CL_ex_secularJ2(t1,kep_t1,t2 [,er,mu,j2])
//
// Description
// <itemizedlist><listitem>
// <p>Propagates orbital elements considering the mean secular effects due to J2 only. </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>  
// <p> - There can be 1 or N initial times, and 1 or N final times. </p>
// <p> - This function works for elliptical orbits only. </p>
// </listitem>
// <listitem>
// <p>The type of orbital elements is the following:</p>  
// <p><inlinemediaobject><imageobject><imagedata fileref="kep_par.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1 or 1xN)
// kep_t1: Orbital elements at time t1 (6x1 or 6xN).  
// t2: Final time [days] (1xN or 1x1).  
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// j2: (optional) (Unnormalized) zonal coefficient (second zonal harmonic) (default is %CL_j1jn(2))
// kep_t2: Orbital elements propagated to time t2. (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_kepler
// CL_ex_lyddane
// CL_ex_eckHech
// CL_op_driftJ2
//
// Examples
// // propagation of one satellite to several times:
// dga = 7070 * 1000;
// ecc = 0.001;
// inc = CL_deg2rad(98);
// pom = CL_deg2rad(90);
// gom = 0;
// anm = 0;
// t1 = 21915;
// t2 = t1:0.1:t1+1;
// kep_t1=[dga;ecc;inc;pom;gom;anm];
// [kep_t2] = CL_ex_secularJ2(t1, kep_t1, t2);
//
// // propagation of 2 element sets to one final time:
// t2 = t1+1;
// t1=[t1,t1+0.5];
// kep_t1=[kep_t1,[dga+100*1000;ecc+0.1;inc;pom;gom;anm]];
// [kep_t2] = CL_ex_secularJ2(t1, kep_t1, t2);

// Gestion arguments optionnels

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Compatibility: old constant must not exist
if (exists("%CL_j2")); CL__warning("Variable %CL_j2 exists: use %CL_j1jn instead"); %CL_j1jn(2) = %CL_j2; end

// Code:
if ~exists('er','local'); er = %CL_eqRad; end
if ~exists('mu','local'); mu = %CL_mu; end
if ~exists("j2","local"); j2 = %CL_j1jn(2); end

// Inputs checking
// adjust sizes : same for t1 and kep_t1
[t1, kep_t1, N1] = CL__checkInputs(t1, 1, kep_t1, 6); 
[t2, N2] = CL__checkInputs(t2, 1); // check nb of rows == 1

if ~(N1 == 1 | N2 == 1 | N1 == N2)
   CL__error('Wrong size of input arguments'); 
end
N = max(N1, N2);

// drifts due to J2
[dpomdt, dgomdt, danmdt] = CL_op_driftJ2(kep_t1(1,:), kep_t1(2,:), kep_t1(3,:), er=er, mu=mu, j2=j2);

// orbital elements at final times
kep_t2 = [ kep_t1(1,:) .* ones(1,N);
           kep_t1(2,:) .* ones(1,N);
           kep_t1(3,:) .* ones(1,N);
           kep_t1(4,:) + dpomdt .* (t2-t1) * 86400;
           kep_t1(5,:) + dgomdt .* (t2-t1) * 86400;
           kep_t1(6,:) + danmdt .* (t2-t1) * 86400  ];

endfunction
