//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2, er,mu,j1jn)
// Eckstein-Hechler propagation model
//
// Calling Sequence
// [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2 [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Propagates orbital elements using Eckstein-Hechler analytical model. </p>
// <p></p></listitem>
// <listitem> 
// <p>The type of orbital elements is the following:</p>  
// <p><inlinemediaobject><imageobject><imagedata fileref="cir_par.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p><b>Important note:</b></p>
// <p>The original algorithm has been modified so that the perigee of a frozen orbit remains perfectly constant over time. 
// This resulted in a change of the mean eccentricity vector (x component) of about 4.e-7, which is small compared to the 
// model's accuracy. </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- There can be 1 or N initial times, and 1 or N final times. </p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn". </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1 or 1xN)
// mean_cir_t1: Mean orbital elements at time t1 (6x1 or 6xN). 
// t2: Final time [days] (1xN or 1x1). 
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_cir_t2: Mean orbital elements at t2  (6xN)
// osc_cir_t2: Osculating orbital elements at t2 (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_meanEckHech
//
// Examples
// // Example 1: one orbit, several final time instants:
// t1 = 12584;
// mean_cir_t1 = [7.e6; 0; 1.e-3; 1; 0.1; 0.2]; 
// t2 = 12587:1:12590
// [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2)
//
// // Example 2: one orbit, several final time instants:
// t1 = 12584;
// mean_cir_t1 = [7.e6; 0; 1.e-3; 0.1; 0.2; 0.3] * [1,1]; 
// t2 = [12587, 12588];
// [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2)


// Declarations:
global %CL__PRIV; 

if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end
if ~exists('er','local'); er = %CL_eqRad; end

// Backward compatibility code
// if j1jn does not exist but zonals exist: take this value
if (exists("zonals","local") & ~exists("j1jn","local"))
  j1jn = zonals;
end

if (~exists("j1jn","local")); j1jn = %CL_j1jn; end;

// NB: arguments sizes/validity: checked in low level functions

// osculating elements: not computed if not necessary
lhs = argn(1);
compute_osc = %t;
if (lhs < 2); compute_osc = %f; end


// -------------------------------
// Main
// -------------------------------

// Propagate using Eckstein-Hechler model
numres = 1; 
if (compute_osc); numres = 2; end

[mean_cir_t2, osc_cir_t2] = CL__ex_eckHech(t1, mean_cir_t1, t2, er, mu, j1jn, numres); 

endfunction
