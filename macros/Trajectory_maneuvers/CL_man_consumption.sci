//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [output] = CL_man_consumption(type_output,par1,par2,par3, g0)
// Mass consumed as function of delta-V
//
// Calling Sequence
// output = CL_man_consumption(type_output,par1,par2,par3 [,g0])
// dm = CL_man_consumption('dm',dv,m,isp [,g0])
// dv = CL_man_consumption('dv',dm,m,isp [,g0])
// m = CL_man_consumption('m',dm,dv,isp [,g0])
// isp = CL_man_consumption('isp',dm,dv,m [,g0])
//
// Description
// <itemizedlist><listitem>
// <p>The mass consumed by a maneuver can be computed by the following equation:</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="consumption.gif"/></imageobject></inlinemediaobject></p>
// <p> Given 3 of the 4 variables that appear in the equation (mass consumed, 
// norm of velocity increment, Isp or initial mass), the function computes the 4th one. </p> 
// <p>The output argument <b>type_output</b> defines the parameter 
// to be computed. It can be one of the following: </p>
// <p>- <b>'dm'</b>: mass consumed</p>
// <p>- <b>'dv'</b>: norm of delta-V</p>
// <p>- <b>'isp'</b>: propellant specific impulse. </p>
// <p>- <b>'m'</b>: initial mass</p>
// <p>Note: The input arguments are always in the same order: dm, dv, m, isp. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// type_output: String defining the parameter to be computed. It can be 'dm,'dv','m' or 'isp'. (1x1)
// par1: First input parameter: dm, dv, m or isp [kg m/s kg or s] (PxN)
// par2: Second input parameter: dv, m or isp [m/s kg or s] (PxN)
// par3: Third input parameter: m or isp [kg or s] (PxN)
// g0: (optional) Value of gravity acceleration. [m/s^2] (default value is CelestLab predefined data "g0") (1x1)
// output: Value of computed parameter: dm, dv, m or isp [kg m/s kg or s] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for Engineering Students, H D Curtis, Chapter 6, equation 6.1
// 2) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 4.8.6
//
// See also
// CL_man_thrustDuration
//
// Examples
// // Compute mass consumed
// isp = 220; // seconds
// dv = 1; // m/s
// m = 180; // kg
// dm = CL_man_consumption('dm',dv,m,isp); // mass consumed in kg
//
// // Compute delta V
// isp = 220; // seconds
// dm = 1; // kg
// m = 180; // kg
// dv = CL_man_consumption('dv',dm,m,isp); // delta V in m/s
//
// // Compute initial mass
// isp = 220; // seconds
// dv = 1; // m/s
// dm = 1.8; // kg
// m = CL_man_consumption('m',dm,dv,isp); // initial mass in kg
//
// // Compute Isp
// dm = 1; // kg
// dv = 1; // m/s
// m = 180; // kg
// isp = CL_man_consumption('isp',dm,dv,m); // isp in sec



// Declarations:
global %CL__PRIV; 
DEFAULT_G0 = %CL__PRIV.DATA.g0;

// Code:
if ~exists('g0','local'); g0 = DEFAULT_G0; end

sz = [ size(par1) ; size(par2) ; size(par3)];
P = max(sz(:,1));
N = max(sz(:,2));
I = find( (sz(:,1) <> P & sz(:,1) <> 1)  |  (sz(:,2) <> N & sz(:,2) <> 1));
if ( I <> [])
  CL__error("Bad size of input arguments");
end


select type_output
  case 'dm'
    dv = par1;
    m = par2;
    isp = par3;
    dm = m.*(1 - exp(-dv ./ (g0.*isp)));
    output = dm;
  case 'dv'
    dm = par1;
    m = par2;
    isp = par3;
    dv = -g0.*isp.*log(1-dm./m);
    output = dv;
  case 'm'
    dm = par1;
    dv = par2;
    isp = par3;
    m = dm./(1 - exp(-dv ./ (g0.*isp)));
    output = m;
  case 'isp'
    dm = par1;
    dv = par2;
    m = par3;
    isp = -dv./(g0.*log(1-dm./m));
    output = isp;
  else
    CL__error('unknown type_output');
end

endfunction
