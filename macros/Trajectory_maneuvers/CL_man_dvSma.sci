//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_man_dvSma(ai,ei,af,posman,mu,res)
// Change of semi-major axis (elliptical orbits)
//
// Calling Sequence
// [deltav,dv,anv] = CL_man_dvSma(ai,ei,af [,posman,mu])
// man = CL_man_dvSma(ai,ei,af [,posman,mu], res="s")
//
// Description
// <itemizedlist><listitem>
// <p>Delta-v required to change the semi major axis by 
// one maneuver at the peripasis or apoapsis.</p>
// <p>The output argument <b>dv</b> is the velocity increment in cartesian coordinates in the "qsw" local frame. </p>
// <p><b>anv</b> is the true anomaly at the position of the maneuver.</p>
// <p>If the argument <b>res</b> is present and is equal to "s", all the output data are returned in a structure. </p>
// </listitem>
// <listitem>
// <p> Note: The results are set to %nan if the targeted semi major-axis is unreachable.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ai : Initial semi major axis [m] (1xN or 1x1)
// ei : Initial eccentricity (1xN or 1x1)
// af : Final semi major axis [m] (1xN or 1x1)
// posman: (optional) Flag specifying the position of the maneuver: 0 or "per" -> periapsis, 1 or "apo" -> apoapsis. Default is at the periapsis. (1xN or 1x1)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// res : (string, optional) Type of output: "d" or "s" for . Default is "d".
// deltav : Norm of velocity increment. [m/s] (1xN)
// dv : Velocity increment (in cartesian coordinates) in the "qsw" local frame [m/s] (3xN)
// anv : True anomaly at the position of maneuver [rad] (1xN)
// man : Structure containing all the output data. 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_man_dvBiElliptic
// CL_man_dvHohmann
// CL_man_dvHohmannG
//
// Examples
// ai = 7200.e3;
// af = 7000.e3;
// ei = 0.1;
// [deltav,dv,anv] = CL_man_dvSma(ai,ei,af)
// // Check results :
// kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv];
// kep1 = CL_man_applyDvKep(kep,dv)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if (~exists("posman", "local")); posman = 0; end
if (~exists("mu", "local")); mu = %CL_mu; end
if (~exists("res", "local")); res = "d"; end

// check "res" argument
if (res <> "d" & res <> "s"); 
  CL__error("Invalid value for argument ''res''"); 
end
if (argn(1) > 1 & res == "s"); 
  CL__error("Invalid number of output arguments"); 
end

// convert posman type to "real"
if (typeof(posman) == "string")
  str = posman; 
  posman = %nan * ones(str); 
  posman(find(str == "per")) = 0; 
  posman(find(str == "apo")) = 1; 
end

// checks arguments sizes are OK / resizes
[ai,ei,af,posman] = CL__checkInputs(ai,1,ei,1,af,1,posman,1); 

if (find(ai <= 0 | ei < 0 | ei >= 1 | af <= 0) <> [])
  CL__error("Invalid input arguments (orbital elements)"); 
end

if (find(posman <> 0 & posman <> 1) <> [])
  CL__error("Invalid value for ''posman''"); 
end


// sgn: (man at periapsis) => 1, apoapsis => -1
sgn = ones(posman); 
I = find(posman == 1); 
sgn(I) = -1;

// r: radius at maneuver position
// r_opp: opposite radius before maneuver
// r_opp2: opposite radius after maneuver (af = (r+r_opp2)/2)
r      = ai .* (1 - sgn .* ei); 
r_opp  = ai .* (1 + sgn .* ei); 
r_opp2 = 2 * af - r; 

// computation only if r_opp2 > 0, otherwise: %nan
// note: CL__man_dvRaps called with whole vector to simplify code

I = find(r_opp2 <= 0);
r_opp2(I) = r_opp(I); // arbitrary, to enable computation

[dv, anv] = CL__man_dvRaps(r, r_opp, r_opp2, mu); 
deltav = CL_norm(dv); 

dv(:,I) = %nan; 
anv(I) = %nan; 
deltav(I) = %nan; 

// output
if (res == "d")
  varargout = list(deltav, dv, anv); 
else
  varargout(1) = struct("deltav",deltav, "dv", dv, "anv", anv); 
end


endfunction
