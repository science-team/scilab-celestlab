//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [param2,cov2] = CL_dsp_covCoord(param1,cov1,conv1to2, mu)
// Conversion of type of orbital elements in a covariance matrix
//
// Calling Sequence
// [param2,cov2]=CL_dsp_covCoord(param1,cov1,conv1to2 [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Performs a change of type of orbital elements in a covariance matrix.</p> 
// <p>Allowed conversions are those handled by <link linkend="CL_oe_convert">CL_oe_convert</link>.</p>
// <p></p>
// <p>The argument <b>conv1to2</b> is a string built with the following template:</p>
// <p><b>conv1to2</b> = type1 + "2" + type2.</p>
// <p>with type1 and type2 being one of the following strings: "kep", "cir", "cireq", "equin" and "pv".</p>
// <p></p>
// <p>Note: The use of "car" orbital elements is deprecated. Use "pv" instead.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// param1: Orbital elements of type 1. (6xN)
// cov1: Covariance matrix associated with param1. (6x6xN)
// conv1to2: (string) Type of orbital element conversion from type1 to type2 (1x1)
// mu : (optional) Gravitational constant [m^3/s^2]. (default value is %CL_mu)
// param2: Orbital elements of type 2. (6xN)
// cov2: Covariance matrix associated with param2.  (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_convert
// CL_cor2cov
// CL_stat
//
// Examples
// // Simple example (cartesian to keplerian)
// A = rand(6,6);
// param1 = [7000.e3;1000.e3;200.e3; 3.e3; 2.e3; 1.e3];
// cov1 = A*A'; // symmetric matrix
// [param2,cov2] = CL_dsp_covCoord(param1,cov1,"car2kep")
//
//
//
// // Elaborate example
// // Change correlation matrix: cartesian to circular-adapted
// bul = [-1877901 -3909428 -5026025 7428.157 -1541.857 -1576.532]'; 
// cor = [1 -0.467016 -0.447601  0.960396  0.987145  0.995826;...
//        0     1     -0.088751 -0.359696 -0.412472 -0.540655;...
//        0     0         1     -0.248472 -0.582834 -0.431908;...
//        0     0         0          1     0.915197  0.943178;...
//        0     0         0          0         1      0.980679;...
//        0     0         0          0         0          1   ]; //upper triangle of correlation matrix
// cor = cor+cor'-eye(cor); // correlation matrix (symmetric)
//
// // Standard  deviation : 
// sd = [15939.681;2912.099;3079.494;6.819104;9.500176;12.146244];
//
// // Covariance matrix in cartesian parameters
// cov_car = CL_cor2cov(cor,sd); 
// [bul_cir,cov_cir] = CL_dsp_covCoord(bul,cov_car,'car2cir');
// [cor_cir,sd_cir] = CL_cov2cor(cov_cir);
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end

if (typeof(conv1to2) <> "string"); CL__error("Invalid input argument: conv1to2"); end;

// Extract type1 and type2 from input string
type_oe = strsplit(conv1to2,"2");
if (size(type_oe,"*") <> 2); CL__error("Invalid input argument: conv1to2"); end;

// Deprecated use of "car" --> replace with "pv"
if (type_oe(1) == "car"); type_oe(1) = "pv"; end;
if (type_oe(2) == "car"); type_oe(2) = "pv"; end;

[param2,jacob] = CL_oe_convert(type_oe(1), type_oe(2), param1, mu);

cov2 = jacob * cov1 * jacob';

endfunction
