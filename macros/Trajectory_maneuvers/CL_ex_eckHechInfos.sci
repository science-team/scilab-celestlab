//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [infos] = CL_ex_eckHechInfos(mean_cir, er,mu,j1jn)
// Informations about Eckstein-Hechler orbit propagation analytical model
//
// Calling Sequence
// infos = CL_ex_eckHechInfos(mean_cir [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Computes data related to Eckstein Hechler orbit propagation analytical model.</p>
// <p>These data are: </p>
// <p> - Eccentricity vector of frozen orbit </p>
// <p> - Secular drifts of some angular orbital elements </p>
// <p></p>
// <p>The output is a structure with the following fields:</p>
// <p> - infos.exf: Mean eccentricity vector x-component of frozen orbit</p>
// <p> - infos.eyf: Mean eccentricity vector y-component of frozen orbit</p>
// <p> - infos.dgomdt: Secular drift of mean right ascension of ascending node [rad/s]</p>
// <p> - infos.dpsodt: Secular drift of mean argument of latitude [rad/s]</p>
// <p></p></listitem>
// <listitem> 
// <p><b>Note:</b></p>  
// <p>The outputs only depend on semi major-axis and inclination. </p>
// <p></p></listitem>
// <listitem> 
// <p>The orbital elements used are the following:</p>  
// <p><inlinemediaobject><imageobject><imagedata fileref="cir_par.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem> 
// <p>See <link linkend="CL_ex_eckHech">CL_ex_eckHech</link> for more details on the propagation model. </p>
// <p></p></listitem>
// <listitem>
// <p>Warning :</p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</p>
// </listitem> 
// </itemizedlist>
//
// Parameters
// mean_cir: Mean orbital elements (6xN) 
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// j1jn: (optional) Vector of zonal coefficients J1 to Jn (troncated to J6) to be used (default is %CL_j1jn(1:6)) (1xNz)
// infos: (structure) Data for the given orbital elements (containing (1xN) vectors)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_eckHech
// CL_ex_meanEckHech
//
// Examples
// // Example :
// mean_cir = [7.e6; 0; 0; 1.7; 0; 0]; // sma, ex, ey, inc... 
// infos = CL_ex_eckHechInfos(mean_cir);


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Code:
if ~exists('mu','local') then mu=%CL_mu; end
if ~exists('er','local') then er=%CL_eqRad; end

// Backward compatibility code
// if j1jn does not exist but zonals exist: take this value
if (exists("zonals","local") & ~exists("j1jn","local"))
  j1jn = zonals;
end

if ~ (exists("j1jn","local")); j1jn = %CL_j1jn(1:6); end;

// Ensure that j1jn has at least 6 elements (fill with zeros)
j1jn = [matrix(j1jn, 1, -1), zeros(1,6)];

// Valeur de J2
if (j1jn(2) == 0)
  CL__error("Invalid value for j1jn(2) (zero).");
end

// *** CODE TAKEN FROM CL_ex_eckHech *** 
// NB: results only depend on sma and inc (mean elements)

cn0 = -j1jn; 

a0   = mean_cir(1,:);
i0   = mean_cir(4,:);
                  
q  = er ./ a0;

c  = cos(i0);
b1 = sin(i0);
b2 = b1 .^2;
b4 = b1 .^4; 
b6 = b1 .^6; 

g2 = cn0(2) * q^2;
g3 = cn0(3) * q^3;
g4 = cn0(4) * q^4;
g5 = cn0(5) * q^5;
g6 = cn0(6) * q^6;

xn  = sqrt(mu./(a0.^3));  

// exf / eyf / dpomfdt 
delta_pom  = -0.75 .* g2 .* ( 4 - 5 * b2 );
delta_pomp = 7.5 * g4 .* ( 1 - (31/8) * b2 + (49/16) * b4 ) - ...
             13.125 * g6 .* (1 - 8 * b2 + (129/8) * b4 - (297/32) * b6);
q = (3/8) * b1 ./ delta_pom;
eyf = q .* g3 .* ( 4 - 5 * b2 ) - q .* g5 .* ( 10 - 35 * b2 + 26.25 * b4 );
exf = 0; 

// dgomdt 
c  = cos(i0);
q   = 1.5 * g2 - 2.25 * (g2.^2) .* ( 2.5 - (19/6) * b2 ) + ...
      (15/16) * g4 .* ( 7 * b2 - 4 ) + ...
      (105/32) * g6 .* ( 2 - 9 * b2 + (33/4) * b4 );
dgomdt = q .* c .* xn; 

// dpsodt 
delta_l = 1 - 1.5 * g2 .* ( 3 - 4 * b2 );
q   = delta_l + 2.25 * (g2.^2) .* ( 9 - (263/12) * b2 + (341/24) * b4 ) + ...
      (15/16) * g4 .* ( 8 - 31 * b2 + 24.5 * b4 ) + ...
      (105/32) * g6 .* ( -(10/3) + 25 * b2 - 48.75 * b4 + 27.5 .* b6 );
dpsodt  = q .* xn; 


infos = struct(); 
infos.exf = exf; 
infos.eyf = eyf;
infos.dgomdt = dgomdt; 
infos.dpsodt = dpsodt; 
// maybe in the future...
//infos.dpomdt = delta_pom + delta_pomp;

endfunction


