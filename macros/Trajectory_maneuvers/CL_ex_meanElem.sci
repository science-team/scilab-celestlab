//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_oe] = CL_ex_meanElem(mod, type_oe, osc_oe, er, mu, j1jn)
// Mean orbital elements (all analytical models)
//
// Calling Sequence
// mean_oe = CL_ex_meanElem(mod, type_oe, osc_oe [, er, mu, j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Computes mean orbital elements from osculating orbital elements.</p>
// <p></p>
// <p>This function is a wrap-up for all available analytical models. </p>
// <p></p>
// <p>The available models are: </p>
// <p>"central": Central force (osculating elements = mean elements) </p>
// <p>"j2sec": Secular effects of J2 (osculating elements = mean elements by convention)</p>
// <p>"lydsec": Lyddane (mean elements include secular effects only) </p>
// <p>"lydlp": Lyddane (mean elements include secular and long period effects) </p>
// <p>"eckhech": Eckstein-Hechler  (mean elements include secular and long period effects) </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- There can be 1 or N initial times, and 1 or N final times. </p>
// <p> - Conversions take place if the type of orbital elements is not the "natural" type for the model. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mod: (string) Model name: "central", "j2sec", "lydsec", "lydlp", "eckhech". (1x1)
// type_oe: (string) Type of orbital elements used for input/output: "kep" or "cir" (1x1)
// osc_oe: Osculating orbital elements (6xN)
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_oe: Mean orbital elements (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_propagate
//
// Examples
// mean_kep = [7.e6; 1.e-3; 1; %pi/2; 0.1; 0.2] 
// osc_kep = CL_ex_propagate("eckhech","kep",0,mean_kep,0,res="o"); 
// CL_ex_meanElem("eckhech","kep",osc_kep) // => mean_kep

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Code:
Models = [ "central", "j2sec", "lydsec", "lydlp", "eckhech" ];  
Imodels = [ 1, 2, 3, 4, 5]; 
// "natural" types for each model
Types_oe_nat = ["-", "kep", "kep", "kep", "cir"]; // "-" => any

if (~exists("er","local")); er = %CL_eqRad; end
if (~exists("mu","local")); mu = %CL_mu; end
if (~exists("j1jn","local")); j1jn = %CL_j1jn; end;

if (type_oe <> "kep" & type_oe <> "cir")
  CL__error("Invalid type of orbital elements"); 
end

imod = find(mod == Models); 
if (imod == [])
  CL__error("Invalid model name"); 
end

nat_type = Types_oe_nat(imod); 
convert = (type_oe <> nat_type & nat_type <> "-"); 

if (convert)
  // converts to "natural" type
  osc_oe = CL_oe_convert(type_oe, nat_type, osc_oe, mu); 
end


if (imod == 1) 
  // central force (osc = mean)
  mean_oe = osc_oe; 

elseif (imod == 2) 
  // Secular J2 (osc = mean by convention) 
  mean_oe = osc_oe; 

elseif (imod == 3) 
  // Lyddane (mean = secular)
  mean_oe = CL_ex_meanLyddane(osc_oe, er, mu, j1jn); 

elseif (imod == 4) 
  // Lyddane (mean = secular + long periods)
  mean_oe = CL_ex_meanLyddaneLp(osc_oe, er, mu, j1jn); 

elseif (imod == 5) 
  // Eckstein-Hechler
  mean_oe = CL_ex_meanEckHech(osc_oe, er, mu, j1jn); 

end

if (convert)
  // converts back to "initial" type 
  mean_oe = CL_oe_convert(nat_type, type_oe, mean_oe, mu); 
end

endfunction
