//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Change of periapsis/apoapsis radius 
// (1 maneuver - elliptical orbit)
// r : Radius where maneuver occurs (periapsis or apoapsis) (1xN)
// ropp1 : Radius at opposite position, before maneuver (1xN)
// ropp2 : Radius at opposite position, after maneuver (1xN)
// dv : velocity increment in "qsw" - spherical coordinates (3xN) 
// anv = True anomaly of maneuver (1xN)
// NB : No check on arguments 
function [dv,anv] = CL__man_raps(r,ropp1,ropp2,mu)

// v1: velocity at maneuver position before maneuver
// v2: velocity at maneuver position after maneuver
// v = sqrt(mu * (2 / r - 1 / sma)), 
// with sma = (r + ropp)/2

v1 = sqrt(2 * mu * ropp1 ./ (r .* (r + ropp1))); 
v2 = sqrt(2 * mu * ropp2 ./ (r .* (r + ropp2))); 

// dv in "qsw"
// v2-v1 = component of dv on "s" axis (= tangential axis 
// at periapsis or apoapsis)

dv = CL_co_car2sph([zeros(v1); v2-v1; zeros(v1)]); 

// anomaly
anv = zeros(v1); // periapsis
I = find(r > ropp1); // apoapsis 
anv(I) = %pi; 


endfunction

