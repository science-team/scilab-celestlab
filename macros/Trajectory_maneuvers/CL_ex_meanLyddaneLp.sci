//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_kep] = CL_ex_meanLyddaneLp(osc_kep, er,mu,j1jn)
// Lyddane propagation model - mean elements (mean = secular + LP)
//
// Calling Sequence
// mean_kep = CL_ex_meanLyddaneLp(osc_kep [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the mean orbital elements from the osculating orbital elements, using Lyddane model. </p>
// <p>The mean elements for this model include secular and long period effects. </p>
// <p></p></listitem>
// <listitem> 
// <p>The type of orbital elements is the following: </p> 
// <p><inlinemediaobject><imageobject><imagedata fileref="kep_par.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// osc_kep: Osculating Keplerian elements [sma;ecc;inc;pom;raan;anm] (6xN)
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_kep: Mean Keplerian elements [sma;ecc;inc;pom;raan;anm] (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_lyddaneLp
//
// Examples
// mean_kep = [7.e6; 1.e-3; 1; 0.1; 0.2; 0.3] 
// [mean_kep2,osc_kep] = CL_ex_lyddaneLp(0,mean_kep,0); 
// CL_ex_meanLyddaneLp(osc_kep) // => mean_kep
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Code:
if ~exists("er","local"); er = %CL_eqRad; end
if ~exists("mu","local"); mu = %CL_mu; end

// Backward compatibility code
// if j1jn does not exist but zonals exist: take this value
if (exists("zonals","local") & ~exists("j1jn","local"))
  j1jn = zonals;
end

if (~exists("j1jn","local")); j1jn = %CL_j1jn; end;

// -------------------------------
// Main
// -------------------------------

// Secular => osculating
function [osc_kep] = lyd2_fct_osc(secu_kep, args)
  [X, Y, osc_kep] = CL__ex_lyddane(0, secu_kep, 0, args.er, args.mu, args.j1jn, 3); 
endfunction 

// Arguments for lyd2_fct_osc
args = struct(); 
args.er = er; 
args.mu = mu; 
args.j1jn = j1jn; 

// Compute secular elements from osculating elements
secu_kep = CL__ex_inverse("kep", osc_kep, lyd2_fct_osc, args);

// Converts secular elements to mean elements
[X, mean_kep, Y] = CL__ex_lyddane(0, secu_kep, 0, er, mu, j1jn, 2); 

// adjust angles mod 2*pi as default values may be wrong.
mean_kep(4:6,:) = CL_rMod(mean_kep(4:6,:), osc_kep(4:6,:)-%pi, osc_kep(4:6,:)+%pi); 


endfunction
