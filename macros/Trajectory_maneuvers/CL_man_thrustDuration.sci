//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [output] = CL_man_thrustDuration(type_output,par1,par2,par3, g0)
// Thrust duration as function of mass consumed
//
// Calling Sequence
// output = CL_man_thrustDuration(type_output,par1,par2,par3 [,g0])
// dt = CL_man_thrustDuration('dt',dm,F,isp [,g0])
// dm = CL_man_thrustDuration('dm',dt,F,isp [,g0])
// F = CL_man_thrustDuration('F',dt,dm,isp [,g0])
// isp = CL_man_thrustDuration('isp',dt,dm,F [,g0])
//
// Description
// <itemizedlist><listitem>
// <p>The thrust duration can be computed as a function of mass consumed, thrust and propellant
// specific impulse by the following equation: </p>  
// <p><inlinemediaobject><imageobject><imagedata fileref="propulsion.gif"/></imageobject></inlinemediaobject></p>
// <p> Given 3 of the 4 variables that appear in the equation (thrust duration, 
// mass consumed, thrust value and Isp), the function computes the 4th one. </p> 
// <p> The output argument <b>type_output</b> defines the parameter to be computed. It can be among the following: </p>
// <p> - <b>'dt'</b> : thrust duration.</p>
// <p> - <b>'dm'</b> : mass consumed</p>
// <p> - <b>'F'</b> : thrust value.</p>
// <p> - <b>'isp'</b> : propellant specific impulse. </p>
// <p> Notes: </p>
// <p> - Input arguments are always in the same order: dt, dm, F, isp </p>
// <p> - The flow rate (mass consumed per unit of time) can be computed by : flow = dm./dt</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// type_output: String defining the parameter to be computed. It can be 'dt,'dm','F' or 'isp'. (1x1)
// par1: First input parameter: dt, dm, F or isp [s kg N or s] (PxN)
// par2: Second input parameter: dm, F or isp [kg N or s] (PxN)
// par3: Third input parameter: F or isp [N or s] (PxN)
// g0: (optional) value of gravity acceleration. [m/s^2] (default value is %CL_g0) (1x1)
// output: Value of computed parameter: dt, dm, F or isp [s kg N or s] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for Engineering Students, H D Curtis, Equations 11.10, 11.12, 11.22, 11.30
// 2) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 4.8.6
//
// See also
// CL_man_consumption
//
// Examples
// // mass consumed in kg
// isp = 220; // seconds
// dv = 1; // m/s
// m = 180; // kg
// dm = CL_man_consumption('dm',dv,isp,m); 
//
// // thrust duration in seconds
// F = 1 ; // N
// [dt] = CL_man_thrustDuration('dt',isp,F,dm) 

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_g0")); %CL_g0 = %CL__PRIV.DATA.g0; end

// Code:
sz = [ size(par1) ; size(par2) ; size(par3)];
P = max(sz(:,1));
N = max(sz(:,2));
I = find( (sz(:,1) <> P & sz(:,1) <> 1)  |  (sz(:,2) <> N & sz(:,2) <> 1));
if ( I <> [])
  CL__error("Bad size of input arguments");
end

if ~exists('g0','local') then g0=%CL_g0; end

select type_output
  case 'dt'
    dm = par1;
    F = par2;
    isp = par3;
    dt = g0.*isp.*dm./F;
    output=dt;
  case 'dm'
    dt = par1;
    F = par2;
    isp = par3;
    dm = F.*dt./(g0.*isp);
    output=dm;
  case 'F'
    dt = par1;
    dm = par2;
    isp = par3;
    F = g0.*isp.*dm./dt;
    output=F;
  case 'isp'
    dt = par1;
    dm = par2;
    F = par3;
    isp = F.*dt./(g0.*dm);
    output=isp
  else
    CL__error('unknown input parameter');
end

endfunction
