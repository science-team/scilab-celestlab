//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_cir] = CL_ex_meanEckHech(osc_cir, er,mu,j1jn)
// Eckstein-Hechler propagation model - mean elements 
//
// Calling Sequence
// mean_cir = CL_ex_meanEckHech(osc_cir [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the mean orbital elements from the osculating orbital elements, using Lyddane model. </p>
// <p></p></listitem>
// <listitem>
// <p>The type of orbital elements is the following: </p> 
// <p><inlinemediaobject><imageobject><imagedata fileref="cir_par.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn". </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// osc_cir: Osculating circular-adapted elements [sma;ex;ey;inc;raan;pom+anm] (6xN)
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_cir: Mean circular-adapted elements [sma;ex;ey;inc;raan;pom+anm] (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_meanEckHech
//
// Examples
// mean_cir = [7.e6; 0; 1.e-3; 1; 0.2; 0.3] 
// [mean_cir2,osc_cir] = CL_ex_eckHech(0,mean_cir,0); 
// CL_ex_meanEckHech(osc_cir) // => mean_cir
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end
if ~exists('er','local'); er = %CL_eqRad; end

// Backward compatibility code
// if j1jn does not exist but zonals exist: take this value
if (exists("zonals","local") & ~exists("j1jn","local"))
  j1jn = zonals;
end
if (~exists("j1jn","local")); j1jn = %CL_j1jn; end;


// -------------------------------
// Main
// -------------------------------

// Mean => osculating
function [osc_cir] = eh1_fct_osc(mean_cir, args)
  [X, osc_cir] = CL__ex_eckHech(0, mean_cir, 0, args.er, args.mu, args.j1jn, 2); 
endfunction 

// Arguments for eh1_fct_osc
args = struct(); 
args.er = er; 
args.mu = mu; 
args.j1jn = j1jn; 

// Compute mean elements from osculating elements
mean_cir = CL__ex_inverse("cir", osc_cir, eh1_fct_osc, args); 

endfunction
