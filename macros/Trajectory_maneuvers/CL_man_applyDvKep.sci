//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [kep_dv] = CL_man_applyDvKep(kep,dv, dv_frame,mu)
// Effect of maneuver on orbital elements
//
// Calling Sequence
// kep_dv = CL_man_applyDvKep(kep,dv [,dv_frame,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the effect of an impulsive maneuver (instantaneous velocity increment) on keplerian orbital elements.</p>
// <p> The (osculating) orbital elements must be given at the time of maneuver as there 
// is no orbit propagation. </p>
// <p> The maneuver is defined by the components of the velocity increment one local frame (default is "qsw"). </p>
// <p></p></listitem>
// <listitem> The orbital elements are the following:  
// <p><inlinemediaobject><imageobject><imagedata fileref="kep_par.gif"/></imageobject></inlinemediaobject></p></listitem>
// </itemizedlist>
//
// Parameters
// kep : Initial (osculating) Keplerian elements [m,rad] (6xN)
// dv : Velocity increment (in cartesian coordinates) in the local frame [m/s] (3xN)
// dv_frame : Local frame in which dv is defined. Default is "qsw" 
// mu : (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// kep_dv : (osculating) Keplerian elements with the effect of the maneuver included [m,rad] (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_locOrbMat
//
// Examples
// // Manoeuvre : 7200km to 7000km :
// ai = 7200.e3;
// af = 7000.e3;
// [deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmann(ai,af);
//
// // Check results:
// // NB: here, true anomaly = mean anomaly
// kep = [ai ; 0 ; %pi/2 ; 0 ; 0 ; anv1];
// kep1 = CL_man_applyDvKep(kep,dv1,dv_frame="qsw");
// kep1(6) = anv2;
// kep2 = CL_man_applyDvKep(kep1,dv2,dv_frame="qsw")

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('dv_frame','local'); dv_frame="qsw"; end
if ~exists('mu','local'); mu=%CL_mu; end

// check inputs sizes
[kep, dv] = CL__checkInputs(kep,6,dv,3); 


// convert from keplerian to cartesian coordinates
[pos,vel] = CL_oe_kep2car(kep, mu=mu);  

// dv in inertial frame
dv_inert = (CL_fr_locOrbMat(pos,vel,dv_frame)') * dv; 

// add velocity increment in inertial frame
// and convert to keplerian coordinates
kep_dv = CL_oe_car2kep(pos,vel + dv_inert, mu=mu); 


endfunction
