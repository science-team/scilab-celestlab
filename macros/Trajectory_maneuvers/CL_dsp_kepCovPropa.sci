//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [kep_t2,cov_t2]=CL_dsp_kepCovPropa(t1,kep_t1,cov_t1,t2, mu)
// Keplerian covariance propagation
//
// Calling Sequence
// [kep_t2,cov_t2]=CL_dsp_kepCovPropa(t1,kep_t1,cov_t1,t2 [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Performs the propagation of orbital elements and associated covariance matrix using a Keplerian model:</p> 
// <p> Given a set of Keplerian orbital elements <b>kep_t1</b> and 
// the associated covariance matrix (<b>cov_t1</b>) at time t1, 
// the function computes the orbital elements <b>kep_t2</b> and the 
// associated covariance matrix (<b>cov_t2</b>) at time 
// <b>t2</b>. </p>
// <p>Note: cov_t2(:,:,i) corresponds to kep_t2(:,i).</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="kepCovPropa.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1)
// kep_t1: Keplerian elements at time t1 [sma;ecc;inc;pom;raan;anm] (6x1)
// cov_t1: Covariance matrix at time t1 (6x6)
// t2: Final time [days] (1xN)
// mu : (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// kep_t2: Keplerian elements at t2 [sma;ecc;inc;pom;raan;anm] (6xN)
// cov_t2: Covariance matrix at time t2 (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cor2cov
// CL_stat
//
// Examples
// // Example 1 : Covariance propagation
// t1 = 21892.7;
// kep_t1 = [7029796.3;0.0036;CL_deg2rad([97.9;47.2;277.8;-59.0])];
// cov_t1 = [
//    8.3D+06   0.90471  -0.25296  -153.815  -0.08734   142.552  
//    0.90471   1.3D-07  -2.6D-08  -9.9D-06  -1.5D-08   8.7D-06  
//   -0.25296  -2.6D-08   2.7D-07   7.3D-06   2.6D-08  -6.9D-06  
//   -153.815  -9.9D-06   7.3D-06   0.00519   1.1D-07  -0.00498  
//   -0.08734  -1.5D-08   2.6D-08   1.1D-07   7.4D-09   5.3D-09  
//    142.552   8.7D-06  -6.9D-06  -0.00498   5.3D-09   0.00479  ]
// t2 = t1 : 10/86400 :t1+10/1440;
// [kep_t2,cov_t2] = CL_dsp_kepCovPropa(t1,kep_t1,cov_t1,t2);
//
// // Example 2 :
// //   Propagation of pointing errors seen from a ground station
// //   This involves several changes of reference frame
// //   The jacobian needed for covariance transformation is computed
// //   Note: covariances and jacobian are modeled as hypermatrix (6,6,n)
//
// // If the previous values were in J2000 : 
// // Transformation Keplerian to position, velocity : 
// [pos_car,vel_car,jacobkp2car] = CL_oe_kep2car(kep_t2);
// cov_car = jacobkp2car * cov_t2 * jacobkp2car';
// // Frame transformation : J2000 ==> terrestrial frame
// [pos_ter,vel_ter,jacobJ20002ter] = CL_fr_convert("EME2000", "ECF", t2,pos_car,vel_car);
//
// // Ground station definition
// KRU = [CL_deg2rad(-52.64);CL_deg2rad(5.1);200];
//
// // Terrestrial to topocentric north :
// [pos_car,vel_car,jacobter2topo] = CL_fr_ter2topoN(KRU,pos_ter,vel_ter);
// jacobter2topo_t=hypermat([6 6 size(kep_t2,2)]);
// for j=1:size(kep_t2,2)
//   jacobter2topo_t(:,:,j)=jacobter2topo(1:6,1:6);
// end
//
// // Change of coordinate(cartesian to spherical)
// [pos_sph,vel_sph,jacobcar2sph] = CL_co_car2sph(pos_car,vel_car);
//
// // Global Jacobian computation and covariance transformation
// jacobiennetot=jacobcar2sph*jacobter2topo_t*jacobJ20002ter*jacobkp2car;
// covariancetopoNsph=jacobiennetot*cov_t2*jacobiennetot';
//
// // Conversions for plot
// err_az=matrix(covariancetopoNsph(1,1,:),1,size(covariancetopoNsph(6,6,:),3));
// err_elev=matrix(covariancetopoNsph(2,2,:),1,size(covariancetopoNsph(6,6,:),3));
// clear jacobter2topo jacobter2topo_t jacobter2topo covariancetopoNcar
//
// //-----------PLOTS-------------------------
// scf();
// plot2d((t2-t2(1))*24,CL_rad2deg(sqrt(err_az)),2)
// plot2d((t2-t2(1))*24,CL_rad2deg(sqrt(err_elev)),5)
// ax=gca();
// CL_g_stdaxes();
// plots = CL_g_select(ax,"Polyline");
// plots.thickness = 2;
// ax.title.text= "Pointing error";
// ax.x_label.text= "Time (h)";
// ax.y_label.text= "Pointing error (deg)";
// ax.font_size=2;
// ax.title.font_size=2;
// ax.x_label.font_size=2;
// ax.y_label.font_size=2;
// legends(["azimuth" "elevation"],[2,5],1);
// 


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local') then mu=%CL_mu; end

// error and consistency checks
Nt1 = size(t1,2);
if( Nt1 > 1 ); CL__error("t1 must of size (1x1)");end
Nkep_t1 = size(kep_t1,2);
if( Nkep_t1 > 1 ); CL__error("kep_t1 must of size (6x1)");end
Ncov_t1 = size(cov_t1,2);
if( Ncov_t1 ~= 6 ); CL__error("cov_t1 must of size (6x6)");end
Nt2 = size(t2,2);
Nt2_2 = size(t2,1);
if( Nt2_2 ~= 1 ); CL__error("t2 must of size (1xN)");end


sma = kep_t1(1);  //semi major axis
mm = (mu/(sma^3))^(0.5);  //mean motion

Pt1_t2 = zeros(6,6,Nt2);
for k = 1 : 6
  Pt1_t2(k,k,1:Nt2) = 1; // diagonal = 1
end

Pt1_t2(6,1,1:Nt2) = -3/2*mm/sma * (t2 - t1*ones(1,Nt2))*86400.0;

cov_t1_hm = zeros(6,6,Nt2);
for k = 1 : 6
  for j = 1 : 6
    cov_t1_hm(k,j,1:Nt2) = cov_t1(k,j); // cov_t1_hm = cov_t1 as an hypermatrix (6x6xNt2) :
  end
end

cov_t2 = Pt1_t2 * cov_t1_hm * Pt1_t2';

[kep_t2] = CL_ex_kepler(t1,kep_t1,t2,mu);


endfunction
