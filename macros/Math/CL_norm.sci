//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [u_norm] = CL_norm(u)
// Norm of column vectors
//
// Calling Sequence
// u_norm = CL_norm(u)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the norm of vectors (or matrices considered as set of column vectors).</p>
// <p> The arguments may be matrices or hypermatrices of dimension 3. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// u: Matrix (considered as a set of column vectors) (PxNxK)
// u_norm: Matrix (norms of column vectors) (1xNxK)
//
// See also
// CL_dot
//
// Authors
// CNES - DCT/SB
//
// Examples
// u = [1,2;3,4]
// CL_norm(u)
//
// u = rand(2,3,4)
// CL_norm(u)


// Declarations:


// Code:

rhs = argn(2); // number of input arguments

if (rhs <> 1)
  CL__error('Wrong number of input arguments (1 expected)');
end

u_norm = sqrt(CL_dot(u,u));

endfunction


