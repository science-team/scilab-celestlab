//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [y] = CL__csc(x)
// CL__csc = 1/sin 
// (like Scilab function "csc", but: CL__csc(0) = %inf)

  sinx = sin(x); 
  I = find(sinx == 0); 
  sinx(I) = %nan; 
  y = 1 ./ sinx; 
  y(I) = %inf; 
endfunction

