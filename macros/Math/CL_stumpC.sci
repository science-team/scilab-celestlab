//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [c] = CL_stumpC(z)
// Stumpff function C(z)
//
// Calling Sequence
// c = CL_stumpC(z)
//
// Description
// <itemizedlist><listitem>
// <p>Evaluates the Stumpff function C(z) according to the following equation :</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="stumpC.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// z: input argument (1xN)
// c: value of C(z) (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for engineering students, H D Curtis, Chapter 3 and Appendix D.4
// 2) Modern astrodynamics Fundamentals and perturbation methods, V Bond and M Allman, Chapter 6.2
//
// See also
// CL_stumpS
//
// Examples
// z=0:1:500;
// c=CL_stumpC(z);
// plot(z,c);
// a=gca();
// a.data_bounds(4)=0.04;
//

// Declarations:


// Code:

c = %nan * ones(z);  // CL_stumpC(%nan) = %nan

I1 = find( z > 0) ;
I2 = find( z < 0) ;
I3 = find( z == 0) ;  // CL_stumpC(0) = 1/2

c(I1) = (1 - cos(sqrt(z(I1))))./z(I1);
c(I2) = (cosh(sqrt(-z(I2))) - 1)./(-z(I2));
c(I3) = 1/2;


endfunction

