//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [y] = CL_rMod(x, a, b)
// Modulo with result in range
//
// Calling Sequence
// y = CL_rMod(x, a, b)
// y = CL_rMod(x, a)
//
// Description
// <itemizedlist><listitem>
// <p>Computes a modulo in a given range. </p>
// <p>The result y is such that y = x modulo [b-a] with y in [a,b[. 
// That is to say that y = x + k*(b-a) with k integer such that y belongs to [a, b[. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- x can be a row or a column vector, a and b can be scalars or vectors with same sizes as x.</p>
// <p>- CL_rMod(x, a) is the same as CL_rMod(x, 0, a) </p>
// <p>- CL_rMod([], a, b) = [] </p>
// </listitem></itemizedlist>
//
// Parameters
// x: Vector of real values (1xN or Nx1)
// a: Minimum value (1x1 or same dimension as x)
// b: Maximum value (strictly greater than a) (1x1 or same size as a)
// y: Result of "x modulo (b-a)" belonging to [a,b[ (same size as x)
//
// Authors
// CNES - DCT/SB
//
// Examples
// CL_rMod(2*%pi, -%pi, %pi)
// 
// y = CL_rMod(1:20, -5, 5)


// Declarations:


// Code:

 if ~exists('b','local') then
   b = a;
   a = 0; // OK if scalar 
 end

 I = find(b-a <= 0); 
 if ~isempty(I) 
   CL__error('Invalid interval bounds');
 end
 
 if isempty(x)
   y = [];
 else
   ecart = b-a;
   n = floor((x - a) ./ ecart);
   y = x - n .* ecart;
 end
 
endfunction

