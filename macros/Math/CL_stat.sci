//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [means,cov] = CL_stat(samples)
// Statistics on samples
//
// Calling Sequence
// [means,cov] = CL_stat(samples)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the mean and the covariance matrix from samples.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// samples: Each row (sample) contains "drawn" values corresponding to one random variable (PxN)
// means: Estimated mean values of each random variable (Px1)
// cov: Estimated covariance matrix (PxP)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cor2cov
//
// Examples
// // 2 independent random variables 
// // (Gaussian law, mean = 0, stdev = 1)
// samples = grand(2,1000,"nor", 0, 1); 
// [means,cov] = CL_stat(samples)
// [cor,sd] = CL_cov2cor(cov)

// Declarations:


// Code:

means = mean(samples,2);
cov = mvvacov(samples');

endfunction
