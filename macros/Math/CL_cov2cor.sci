//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cor,sd] = CL_cov2cor(cov)
// Covariance to correlation matrix
//
// Calling Sequence
// [cor,sd] = CL_cov2cor(cov)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the correlation matrix and the standard deviations vector from the covariance matrix.</p> 
// <p> The correlation matrix and standard deviation vector are built as follows: </p>
// <p> - <b>cor</b>(i,i) = 1 </p>
// <p> - <b>cor</b>(i,j) = <b>cov</b>(i,j) /  (<b>sd</b>(i) * <b>sd</b>(j)) </p>
// <p> - <b>sd</b>(i) = cov(i,i)^(1/2) </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cov: Covariance matrix (NxNxK)
// cor: Correlation matrix (NxNxK)
// sd: Standard deviations vectors (NxK)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cor2cov
//
// Examples
// // Random covariance matrix :
// N=6; K=2;
// mat = rand(N,N,K,key="normal");
// cov = mat'*mat;  // symetrical
// [cor,sd] = CL_cov2cor(cov);
//
// // Retrieving the covariance :
// cov2 = CL_cor2cov(cor,sd) ;


// Declarations:


// Code:

[p p2]=size(cov);
if(p ~= p2)
  CL__error('first and second dimension must be of same size');
end

// Matrix :
if(length(size(cov))==2)
  w = inv(sqrt(diag(diag(cov))));
  cor = w*cov*w;
  sd = sqrt(diag(cov));
// Hypermatrix :
else
    [p p2 N] = size(cov);
  cor = zeros(cov);
  sd = zeros(p,N);
  for I = 1 : p
    for J = 1 : p
    if I == J
      cor(I,J,:) = matrix(ones(N,1),1,1,N);
    else
      cor(I,J,:) = cov(I,J,:) ./ ( sqrt( cov(I,I,:) .* cov(J,J,:)) );
    end
    end
    sd(I,:) = sqrt(cov(I,I,:));
  end
end


endfunction
