//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [c] = CL_dot(u,v)
// Dot product of column vectors
//
// Calling Sequence
// c = CL_dot(u,v)
// c = CL_dot(u)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the dot product of vectors (or matrices considered as set of column vectors).</p>
// <p> The arguments may be matrices or hypermatrices of dimension 3. </p>
// <p> The second argument can be omitted: CL_dot(u) = CL_dot(u,u) (self dot product). </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// u: Matrix (considered as a set of column vectors) (Px1 or PxN) or hypermatrix (PxNxK)
// v: (optional) Matrix (considered as a set of column vectors). (Px1 or PxN) or hypermatrix (PxNxK)
// c: Matrix (dot products of column vectors) (1xN or 1xNxK)
//
// See also
// CL_cross
// CL_norm
//
// Authors
// CNES - DCT/SB
//
// Examples
// // u and v : set of column vectors
// u = [ [1;2] , [3;4]];
// v = [ [-1;5] , [-6;2]];
// CL_dot(u,v)
//
// // u : set of column vectors, v : one column vector
// u = [ [1;2;3] , [3;4;5]];
// v = [1;2;3];
// CL_dot(u,v)
// 
// // u and v hypermatrix : set of column vectors
// u = rand(2,3,4);
// v = rand(2,3,4);
// CL_dot(u,v)

// Declarations:


// Code:

rhs = argn(2); // number of input arguments

if (rhs < 1 | rhs > 2)
  CL__error('Wrong number of input arguments');
end

if (rhs == 1) // only one input => self product
  c = CL_dot(u,u); // recursive call to simplify the code

else // general case 

  u_type = typeof(u);
  v_type = typeof(v);

  if (u_type ~= v_type) 
    CL__error('Arguments must be of the same type');
  end

  if (u_type ~= 'constant' & u_type ~= 'hypermat' ) 
    CL__error('Invalid argument types (constant or hypermat expected)');
  end
  
  
  if (u_type == 'constant') // matrix
    [Pu, Nu] = size(u);
    [Pv, Nv] = size(v);

    if ~(Nu == 1 | Nv == 1 | Nu == Nv)
      CL__error("Matrices have inconsistent sizes");
    end

    if (Nu == 1 & Nv > 1) u = u * ones(1,Nv); end
    if (Nv == 1 & Nu > 1) then v = v * ones(1,Nu); end

    c = sum(u.*v,'r'); // result

  else // hypermatrix
    [Pu,Nu,Ku] = size(u);
    [Pv,Nv,Kv] = size(v);

    if ~(Pu == Pv & Nu == Nv & Ku == Kv)
      CL__error("Hypermatrices have inconsistent sizes");
    end

    u = matrix(u,Pu,Nu*Ku); // Transform u and v into matrices
    v = matrix(v,Pv,Nv*Kv);

    c = sum(u.*v,'r'); // result
    c = matrix(c,1,Nu,Ku); // transform result into hypermatrix

  end

end

endfunction


