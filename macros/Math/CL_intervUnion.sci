//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ires] = CL_intervUnion(varargin)
// Union of sets of intervals
//
// Calling Sequence
// ires = CL_intervUnion(i1 [,i2,i3,...,ip])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the union of sets of intervals.</p>
// <p>The intervals are gathered in different sets (i1, i2,...,ip). </p>
// <p>A real number x belongs to the union of (i1, i2,...,ip) if it belongs to at least one interval in one of the sets. </p>
// <inlinemediaobject><imageobject><imagedata fileref="union.gif"/></imageobject></inlinemediaobject>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - Only one set may be given. The result is then the union of the intervals that belong to the set. </p>
// <p> - Intervals of length equal to 0 are discarded. (length = end of interval minus beginning of interval) </p>
// <p> - Sets of intervals may be empty: CL_intervUnion(i1,[]) = CL_intervUnion(i1)</p>
// <p> - Resulting set of intervals are sorted in ascending order (first row) </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// i1: Set of intervals [start ; end] (2xN)
// i2: Set of intervals [start ; end] (2xN2)
// ip: Set of intervals [start ; end] (2xNp)
// ires: Union of i1,i2...,iN (2xM)
//
// See also
// CL_intervInters
// CL_intervInv
//
// Authors
// CNES - DCT/SB
//
// Examples
// i1=[ [1;3] , [5;6] , [10;12]];
// i2=[ [2;4] , [5.5;5.7] , [5.6;15]];
// ires = CL_intervUnion(i1,i2);

// Declarations:


// Code:

for i=1:size(varargin)
  if (varargin(i) <> [])
    if (size(varargin(i),1) <> 2)
      CL__error("Invalid size for interval " + string(i));
    end
  end
end

itot = [];
for i=1:size(varargin)
  itot = [itot,varargin(i)];
end

// suppression of intervals of zero length
itot(:, find(itot(2,:)-itot(1,:)==0) ) = []; 
isort = CL_sortMat(itot, itot(1,:));

N = size(isort,2);

// Union of intervals :
ires = isort(:,1);
j = 1; // index for ires
k = 1; // index for isort
while (k < N)
  ii = 0;
  while (ii <> find(isort(1,:) <= ires(2,j)))
    // ii: indices for which lower bound is less than current upper bound
    ii = find(isort(1,:) <= ires(2,j)) ; 
    if (ii <> [])
      // current upper bound is now the biggest upper bounds (for these indices)
      ires(2,j) = max(isort(2,ii)) 
    end
  end
  j = j+1;

  ind2 = find(isort(1,:) > ires(2,j-1));
  
  if (ind2 <> [])
    ires = [ires, isort(:,ind2(1))];
    k = ind2(1);
  else
    k=N+1;
  end
  
end

 
endfunction
