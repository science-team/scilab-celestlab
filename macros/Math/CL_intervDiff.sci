//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ires] = CL_intervDiff(i1,i2)
// Difference of sets of intervals
//
// Calling Sequence
// ires = CL_intervDiff(i1,i2)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the difference of sets of intervals.</p>
// <p>The intervals are gathered in two sets (i1 and i2). </p>
// <p>A real number x belongs to the difference of i1 and i2 if it belongs to an interval from i1, and does not belong to any interval from i2.  </p>
// <inlinemediaobject><imageobject><imagedata fileref="diffInterval.gif"/></imageobject></inlinemediaobject>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - The intervals in each set should have empty intersections (use CL_intervUnion if needed). </p>
// <p> - Sets of intervals may be empty. CL_intervDiff(i1,[]) = i1. CL_intervDiff([],i2) = []. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// i1: Set of intervals [start ; end] (2xN1)
// i2: Set of intervals [start ; end] (2xN2)
// ires: Difference of i1 and i2 (i1 minus i2) (2xM)
//
// See also
// CL_intervInters
// CL_intervUnion
//
// Authors
// CNES - DCT/SB
//
// Examples
// i1=[ [ 0;20] , [21; 25]];
// i2=[ [2;4] , [5.5;5.7] , [6.6;15] , [19;20.5]];
// ires = CL_intervDiff(i1,i2);

// Declarations:

// Interval including all the intervals
i_min_max = [min([i1,i2]) ; max([i1,i2])];

// Complementary intervals of i2
i2_comp = CL_intervInv(i_min_max, i2);

// Result is the intersection of i1 and i2_comp
ires = CL_intervInters(i1, i2_comp);

endfunction
