//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cov] = CL_cor2cov(cor,sd)
// Correlation to covariance matrix
//
// Calling Sequence
// cov = CL_cor2cov(cor,sd)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the covariance matrix from the correlation matrix and the standard deviations. </p>
// <p> The covariance matrix is built as follows: </p>
// <p> - <b>cov</b>(i,i) = <b>sd</b>(i)^2 </p>
// <p> - <b>cov</b>(i,j) = <b>cor</b>(i,j) * <b>sd</b>(i) * <b>sd</b>(j) </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cor: Correlation matrix (NxNxK)
// sd: Standard deviations vector (NxK)
// cov: Covariance matrix (NxNxK)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cov2cor
//
// Examples
// // Random covariance matrix :
// N=6; K=2;
// mat = rand(N,N,K,key="normal");
// cov = mat'*mat;  // symetrical
// [cor,sd] = CL_cov2cor(cov);
//
// // Retrieving the covariance :
// cov2 = CL_cor2cov(cor,sd) ;

// Declarations:


// Code:

[p p2] = size(cor);
[p3 p4] = size(sd);
if(p ~= p2 | p ~= p3)
  CL__error('first and second dimension must be of same size');
end
// Matrix :
if(length(size(cor))==2)
  w = diag(sd);
  cov = w*cor*w;
// Hypermatrix :
else
    [p p2 N] = size(cor);
  cov = zeros(cor);
  for I = 1 : p
    for J = 1 : p
    if I == J
      cov(I,J,:) = sd(I,:).^2;
    else
      cov(I,J,:) = cor(I,J,:) .* matrix(sqrt(sd(I,:).^2 .* sd(J,:).^2 ),1,1,N);
    end
    end
  end
end

endfunction
