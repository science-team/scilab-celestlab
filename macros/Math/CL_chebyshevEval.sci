//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [y, ydot, y2dot] = CL_chebyshevEval(x, n, kind)
// Evaluation of Chebyshev polynomials (of first or second kind)
//
// Calling Sequence
// [y, ydot, y2dot] = CL_chebyshevEval(x, n, kind)
//
// Description
// <itemizedlist><listitem>
// <p>Evaluates Chebyshev polynomials of first or second kind at given abscissae.</p>
// <p>The polynomials are defined as follows:</p>
// <p>P0(x) = 1  (degree 0)</p>
// <p>P1(x) = kind * x, with kind = 1 or 2  (degree 1)</p>
// <p>Pn(x) = 2 * x * Pn-1(x) - Pn-2(x), for n >= 2 </p>
// <p></p>
// <p>The function evaluates the polynomials of degree <b>n</b> at each abscissa <b>x</b>. <b>y</b>(i,j) is the value of the polynomial of degree n(i) at x(j).</p>
// <p>The function optionally evaluates the first and second derivatives of the polynomials.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// x: Abscissae (1xN)
// n: (integer) Degrees of the polynomials in [0,100] (1xP)
// kind: (optional) 1 for first kind, 2 for second kind. Default is 1 (1x1)
// y: Values of Chebyshev polynomials (PxN)
// pdot: Values of polynomials first derivatives (PxN)
// p2dot: Values of polynomials second derivatives (PxN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_chebyshevPoly
//
// Examples
// // Evaluate Chebyshev polynomials (degrees 0 to 5) of first kind
// x = linspace(-1,1,11); 
// y = CL_chebyshevEval(x, 0:5)

if (~exists("kind", "local")); kind = 1; end

if (size(x,1) <> 1)
  CL__error("Invalid input argument: x"); 
end

// p, pdot, p2dot = Scilab polynoms
// NB: arguments n and kind are checked in subfunction
// degrees passed = columns matrix => p = column matrix
p = CL_chebyshevPoly(matrix(n,-1,1), kind); 

lhs = argn(1); // number of output arguments

y = horner(p, x); 

if (lhs >= 2)
  pdot = derivat(p); 
  ydot = horner(pdot, x); 
end

if (lhs >= 3)
  p2dot = derivat(pdot); 
  y2dot = horner(p2dot, x); 
end

endfunction

