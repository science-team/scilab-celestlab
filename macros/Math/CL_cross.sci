//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [w] = CL_cross(u,v)
// Cross product of column vectors
//
// Calling Sequence
// w = CL_cross(u,v)
//
// Description
// <itemizedlist><listitem>
// <p>Cross product of vectors.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// u: Matrix (considered as a set of column vectors) (PxN or Px1, P >= 3)
// v: Matrix (considered as a set of column vectors) (PxN or Px1, P >= 3)
// w: Matrix (cross products of column vectors): w(:,j) = u(:,j)^v(:,j) (PxN, P >= 3)
//
// See also
// CL_dot
//
// Authors
// CNES - DCT/SB
//
// Examples
// u = [ [1;0;0] , [0;1;0] ] ;
// v = [ [0;1;0] , [0;0;1] ] ;
// w = CL_cross(u,v);

// Declarations:


// Code:

nr_u = size(u,1); 
nr_v = size(v,1);
nc_u = size(u,2); 
nc_v = size(v,2);

if (nc_u == 0 & nc_v == 0)

  w = []; // CL_cross([], []) == [] (by convention)

else  

  if (nc_u == 1 & nc_v > 1) then u = u * ones(1,nc_v); nc_u = nc_v; end;
  if (nc_v == 1 & nc_u > 1) then v = v * ones(1,nc_u); nc_v = nc_u; end;

  if ~(nr_u == nr_v & nc_u == nc_v  & nr_u >= 3 & nc_u >=1)
     CL__error("Invalid argument sizes");
  end

  I = [2:nr_u,1];
  J = [3:nr_u,1,2]; 

  w = u(I,:) .* v(J,:) - u(J,:) .* v(I,:); 

end


endfunction
