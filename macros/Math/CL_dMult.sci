//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [C] = CL_dMult(A,B)
// Dot multiplication
//
// Calling Sequence
// C = CL_dMult(A,B)
//
// Description
// <itemizedlist><listitem>
// <p>Multiplies matrices elementwise (extends ".*"). </p>
// <p> If A (resp B) contains 1 row (A=[r]), A (resp B) is 
// construed as [r;r;r...r] (P times, where P = maximum number of rows between A and B). </p>
// <p> If A (resp B) contains 1 column ([c]), A (resp B) is 
// construed as [c,c,c...c] (N times, where N = maximum number of columns between A and B). </p>
// <p> If A or B is [], the result is []. </p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// A: First matrix (PxN) or (1xN) or (Px1)
// B: Second matrix (PxN) or (1xN) or (Px1)
// C: result (PxN) 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_unitVector
// CL_norm
//
// Examples
// coef = [ 1 2 3 ];
// v = [ 1 2 3 ; 4 5 6 ; 7 8 9 ];
// w = CL_dMult(coef,v)


// Declarations:


// Code:

[nra, nca] = size(A);
[nrb, ncb] = size(B);

nr = max(nra, nrb);
nc = max(nca, ncb);

if ~(nra == nrb | nra <= 1 | nrb <= 1)
  CL__error("Invalid number of rows in input arguments");
end

if ~(nca == ncb | nca <= 1 | ncb <= 1)
  CL__error("Invalid number of columns in input arguments");
end

if (nra == 1); A = ones(nr, 1) * A; end
if (nrb == 1); B = ones(nr, 1) * B; end

if (nca == 1); A = A * ones(1, nc); end
if (ncb == 1); B = B * ones(1, nc); end


C = A .* B;

endfunction

