//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Traj] = CL__3b_formatOrbits(XXtt,t,env)
//Author
// 12/02/09 A. Blazquez( CNES, DCT/SB/MO)

//fonction qui recalcule tous les points d'une orbite stabilisee (Lissajous ou halo par exemple),
//en suprimant le premier morceau (jusqu'a la premiere annulation de z) qui est toujours mauvais:
//  entree: matrice XXtt, de dim 7xn, d'une orbite stabilisee (sortie de la generation de la Lissajous par exemple ou de la generation de halo)
//               dont chacune des n colonnes est un des points
//             t vecteur qui contient les instant ou on veut calculer l'orbit lissajous
//  sortie: matrice TrajLiss, de dim 7xn, d'une orbite stabilisee dont chaque colonne est un point de la trajectoire en
//    ses six composantes de positions-vitesses et date


// Declarations:


// Code:

Traj= [];
j=1;
tmp=[];
for k=1:size(t,2)
     while XXtt(7,j)<t(k)
     j=j+1;
     end
     // Modification for the last point
     if j==size(XXtt,2)
        if XXtt(7,j)<=t(k)
           j
        else
         j=j-1
        end
     else
        j=j-1;
     end
     //modification for the first point
     if j==0, j=1;end


  X = XXtt(1:6,j);
  deff("[u]=r(t,X)","u=XXtt($,j+1)-t");
  tinit = XXtt(7,j);
  tint = t(k)-XXtt(7,j);
  if tint==0.
    tmp=XXtt(1:7,j)
  else
    [Xtraj,ttraj,rdtraj] = CL__3b_integratorFix(X,tinit,tint,CL__3b_RHS,r,env.pas,env.MU);
    tmp=[Xtraj(:,$);ttraj($)];
  end

  Traj=[Traj,tmp];
end
  if size(Traj,2)-size(t) <>0
    CL__message("Fake formatting extrapolation");
  end
endfunction
