//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Y,t,rdYfin] = CL__3b_integratorFix(Y0,t0,tint,f,g,pas,MU)
// Author:
// B. Meyssignac (CNES DCT/SB/MO)

//fonction qui utilise l'integration "root" avec les parametres ODEOPTIONS fixes
//  entrees: Y0: vecteur colonne des 6 conditions initiales positions et vitesses
//     t0: temps initial
//     tint: duree de l'integration
//     f: second membre: YCL_dot = f(t,Y) definit dans choix_point_col.sce
//     g: fonction dont "root" cherche la racine
//  sorties: Y: matrice resultat de l'integration: matrice 6xn : position vitesse x dates
//                  nota: si l arret est provoque par g la date d arret et le Y correspondant sont dans rd et sont absent de Y et t
//     t: vecteur des dates associees a chaque pas de l'integration
//               rd: non vide si l integrateur s arrette a cause de g: dim 1X8
//                   date du dernier point, nombre de surface annulee, position vitesse du dernier point


// Declarations:


// Code:

%ODEOPTIONS = [1,0,0,%inf,0,2,500000,12,5,0,-1,-1];
sizeY = size(Y0,1);
rtol = 1.e-8*ones(sizeY,1);
atol = 1.e-9*ones(sizeY,1);

t = t0:sign(tint)*pas:t0+tint;
ng = 1;

[Y,rd] = ode("root",Y0,t0,t,rtol,atol,f,ng,g);

if rd(2)>0
  sizeY = size(Y,2);
  rdYfin = [rd,Y(:,$)'];
  Y = Y(:,1:sizeY-1);
  t = t(1:sizeY-1);
else
  rdYfin = [];
end

endfunction
