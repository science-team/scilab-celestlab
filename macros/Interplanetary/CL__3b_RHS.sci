//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function XCL_dot = CL__3b_RHS(t,X,MU)
//  Inputs: t: time
//      X: vector of 6 lines (position+velocity)
//          MU: from tb_environnement
//
//  Outputs: XCL_dot: vector of 6 lines (position+velocity)
//
// Author:
// B. MEYSSIGNAG (CNES DCT/SB/MO)

// Declarations:


// Code:

r0 = sqrt((X(1)+MU)^2+X(2)^2+X(3)^2);
r1 = sqrt((X(1)-1+MU)^2+X(2)^2+X(3)^2);

XCL_dot = [X(4);
        X(5);
        X(6);
        2*X(5)+X(1)-(1-MU)*(X(1)+MU)/(r0^3)-MU*(X(1)-1+MU)/(r1^3);
       -2*X(4)+X(2)-(1-MU)*X(2)/(r0^3)-MU*X(2)/(r1^3);
       -(1-MU)*X(3)/(r0^3)-MU*X(3)/(r1^3)];

endfunction
