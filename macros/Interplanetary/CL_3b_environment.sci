//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [env] = CL_3b_environment(bodies,Lpoint)
// Structure creation for the 3-body problem functions
//
// Calling Sequence
// env = CL_3b_environment(bodies,Lpoint)
//
// Description
// <itemizedlist><listitem>
// <p>Creates a structure to be used by other "3b" functions.</p> 
// <p>This structure contains the necessary information about a specific libration point in a specific system.</p>
// <p>At the moment, only (Earth + Moon), (Sun + barycenter of Earth-Moon) and (Sun + Jupiter) systems are included, 
// and only the colinear libration points (L1, L2 and L3) are defined.</p>
// <p><imageobject><imagedata fileref="3b_frame.png"/></imageobject></p>
// <p></p></listitem>
// <listitem>
// <p> Description of output structure fields:</p>
// <p>- bodies: (string) System of primaries ('S-EM', 'E-M' or 'S-J')</p>
// <p>- Lpoint: (string) Libration point ('L1', 'L2' or 'L3')</p>
// <p>- gammal: Adimensional distance between the libration point and the closest primary (m1-L1 or m1-L2 or m2-L3)</p>
// <p>- MU: constant mu: Ratio of the mass of the biggest primary to the mass of the smallest primary (mu1/mu2)</p>
// <p>- omega_init: Used for internal computations.Theoretical pulsation of in-plane motion for orbits around the libration point</p>
// <p>- k: Used for internal computations </p>
// <p>- gl: Distance from the center of mass of the primaries to the libration point (G-L1 or G-L2 or G-L3)</p>
// <p>- pas: Integration step</p>
// <p>- OMEGA: Pulsation (rad/s) of the primaries around their center of mass G. Used to "adimensionalyse" time (1 year => 365.25*24*3600*OMEGA)</p>
// <p>- D: Distance (m) between the primaries. Used to "adimensionalyse" distances and velocities</p>
// <p>- c2: Used for internal computations </p>
// <p>- nu:  Used for internal computations</p>
// <p>- invM:  Used for internal computations</p>
// <p>- ESCAPEDIR: Used for internal computations </p>
// <p>- ESCAPEDIRNORM: Used for internal computations </p>
// <p><imageobject><imagedata fileref="3b_L2_param.png"/></imageobject></p>
// <p></p></listitem>
// <listitem>
// <p><b> Notes: </b> </p>
// <p>- The stacksize is changed to 5e7 in order to perform the Manifold computations.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// bodies: (string) Body system: 'S-EM', 'E-M' or 'S-J' 
// Lpoint: (string) Type of libration point. It can be 'L1', 'L2' or 'L3'
// env: Resulting structure. 
//
// See also
// CL_3b_halo
// CL_3b_lissajous
// CL_3b_manifolds
//
// Authors
// CNES - DCT/SB
//
// Examples
// envL1 = CL_3b_environment('S-EM','L1')
//

Lpoint = strsubst(Lpoint,'l','L');
stacksize(5e7);
select bodies
  case 'S-EM' //Sun-EarthMoon system
    OMEGA = 0.0000001990986687;
    MU = 0.000003040357143;
    D = 149597871410;
    environment=bodies;
  case 'E-M'  //Earth-Moon system
    OMEGA = 0.0000026617;
    MU = 0.012150668;
    D = 384399060;
  case 'S-J' //Sun-Jupiter system 
    OMEGA = 0.00145029924/86400;  // enem de SWLibration
    MU = 0.00095368390550742; // muSun physical data, muJup planetary data
    D = 149597871410*5.2; // mean distance Sun-Jupiter 5.2 AU
  else
    CL__error('unknown bodies system');
end

gammal = CL__3b_gamma(MU,Lpoint);

select Lpoint
case 'L1'
    c2 = 1/(gammal^3)*(MU+(1-MU)*(gammal^3)/((1-gammal)^3));
    gl = 1 -gammal - MU;
  case 'L2'
    c2 = 1/(gammal^3)*(MU+(1-MU)*(gammal^3)/((1+gammal)^3));
    gl = gammal + 1 - MU;
  case 'L3'
    c2 = 1/(gammal^3)*(1-MU+MU*(gammal^3)/((1+gammal)^3));
    gl= -gammal -MU;
  else
    CL__error('not valid Libration point');
end

lambda = sqrt(1/2*(-2+c2+sqrt(9*c2^2-8*c2)));
omega_init = sqrt(1/2*(2-c2+sqrt(9*c2^2-8*c2)));
nu = sqrt(c2);
c = (lambda^2-1-2*c2)/(2*lambda);
k = (omega_init^2+1+2*c2)/(2*omega_init);
b1 = c*lambda + k*omega_init;
b2 = c*omega_init - k*lambda;

//Linearisation du mouvement
//matrice des coordonnees x-gl, y, z, vx, vy, vz,(lignes)
//dans la base des vecteurs propres (colonnes):
//  A*e^(lambda*t), B*e^-(lambda*t), C*e^(i*omega_init*t), D*e^(-i*omega_init*t), E*e^(i*nu*t), F*e^(-i*nu*t)
M = [1         1         1         1        0      0;
     c        -c         %i*k     -%i*k     0      0;
     0         0         0         0        1      1;
     lambda   -lambda    %i*omega_init -%i*omega_init 0      0;
     lambda*c  lambda*c -omega_init*k  -omega_init*k  0      0;
     0         0         0         0        %i*nu -%i*nu];

//matrice des vecteurs propres (lignes):
//  A*e^(lambda*t), B*e^-(lambda*t), C*e^(i*omega_init*t), D*e^(-i*omega_init*t), E*e^(i*nu*t), F*e^(-i*nu*t)
//dans la base des coordonnees x-gl, y, z, vx, vy, vz, (colonnes): (inverse de la precedente en analytique)
invM = 1/2* [k*omega_init/b1   omega_init/b2     0 -k/b2     1/b1  0;
             k*omega_init/b1  -omega_init/b2     0  k/b2     1/b1  0;
             c*lambda/b1  %i*lambda/b2 0 -%i*c/b2 -1/b1  0;
             c*lambda/b1 -%i*lambda/b2 0  %i*c/b2 -1/b1  0;
             0            0            1  0        0    -%i/nu;
             0            0            1  0        0     %i/nu];

//vecteur escape_direction dans le plan vx, vy
ESCAPEDIR = real(invM(1,4:5).');
ESCAPEDIRNORM = 1/norm(ESCAPEDIR)*ESCAPEDIR;

//vecteur non_escape_direction dans le plan vx, vy
NESCAPEDIR = [1/b1;k/b2];
NESCAPEDIRNORM = 1/norm(NESCAPEDIR)*NESCAPEDIR;

//definition de la variable globale pas
//pas = 1.e-3;
pas = 1.e-2;  //pas integrateur_fix


//definition structure environment
env = struct('bodies',bodies, 'Lpoint',Lpoint, 'gammal',gammal, 'MU',MU, ...
             'omega_init',omega_init, 'k',k, 'gl',gl, 'pas',pas, 'OMEGA',OMEGA, 'D',D, ...
       'c2',c2, 'nu',nu, 'invM',invM, 'ESCAPEDIR',ESCAPEDIR, ...
       'ESCAPEDIRNORM',ESCAPEDIRNORM);

endfunction
