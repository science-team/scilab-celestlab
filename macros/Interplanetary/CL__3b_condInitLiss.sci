//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [X0,omega,nu] = CL__3b_condInitLiss(Ax,phix,Az,phiz,env,t)
// Author:
// B. Meyssignac ( CNES, DCT/SB/MO)

//
//fonction qui genere les conditions initiales de lissajous en dynamique linearisee autour du point de
//Lagrange choisi a partir des amplitudes et phasages donnes en entree
//          :entree : amplitudes et phasages de la Lissajous desiree: Ax, Az, phix, phiz
//                    donne en Ax et phix tels que x = Ax*cos(omega*t+phix)+gl ( et donc y=-k*Ax*sin(omega*t+phix) )
//                    et en Az et phiz tels que z = Az*cos(nu*t+phiz)
//          :sortie : vecteur colonne des conditions initiales correspondantes dans le repere synodique normalise

// necessite: un environnement : le script 'environnement....sci'
//            des constantes de la dynamique lineaire et un RHS : script 'choix_point_col.sce'


// Declarations:


// Code:

gl = env.gl;
k = env.k;
omega = env.omega_init;
nu = env.nu;
B1=omega*t+phix
B2=nu*t+phiz
X0 = [Ax*cos(B1) + gl;
     -k*Ax*sin(B1);
      Az*cos(B2);
     -omega*Ax*sin(B1);
     -omega*k*Ax*cos(B1);
     -nu*Az*sin(B2)];

endfunction
