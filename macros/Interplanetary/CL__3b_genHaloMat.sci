//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [correction,XXtt,omegahalo] = CL__3b_genHaloMat(X0,omegacorr,date_init,ttot,env)
// Author:
// B. Meyssignac (CNES DCT/SB/MO)

//fonction qui calcule la trajectoire de la Halo que l'on peut obtenir avec les parametres en entree
//et les corrections numeriques en x et vy  necessaires pour la stabiliser a chaque demi tour:
//  entrees:date initiale
//             temps total de trajectoire sur la Halo a calculer
//  sortie:  matrice (2,n) correction qui contient dans chaque colonne
//     le module (en x et en vy) de la correction fournie pour la stabilisation
//     matrice (7,n+1) qui contient dans chaque colonne
//    point (positions-vitesses-date) ou on a fait une correction
//    avec les valeures deja stabilisees
//           omegahalo : la pulsation de l'orbite de halo

// calcul de l orbite de halo

// Declarations:


// Code:

XXtt = [];
correction = [];
date_fin = date_init+ttot;
tinit = date_init;
j=1;

while (date_fin-tinit)>=0

  [Xstab,corrnum,Xtraj,ttraj,omegatraj,deltaY] = CL__3b_stabHaloNewton(X0,omegacorr,tinit,env.pas,env.MU);
  if j==1 then omegahalo=omegatraj; end
  j=j+1;

  if abs(deltaY)<=%eps then break; end

  XXtti = [Xstab;tinit];
  XXtt = [XXtt XXtti];
  correction = [correction corrnum];

  X0 = Xtraj(:,$);
  tinit = ttraj($);

end

if abs(deltaY)<=%eps
  CL__message("halo non stabilized\n");
else
  CL__message("halo stabilized\n");
end

XXttfin = [Xtraj(:,$);ttraj($)];
XXtt = [XXtt XXttfin];

endfunction
