//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function X_t = CL__3b_shooting(X0,t,env,epsilon)
// This function uses a multiple shooting method to determinate the orbit from a reduce initial conditions
//
// Inputs: X0: Halo Initial conditions (columns of 6 dimension (position+velocity))
//         t: discretisation of the output orbit.
//         env:Result of tb_environement
//         epsilon: precision of the results.
// Output: X_t: Trajectorie stabilised
//

// Author:
// A. BLAZQUEZ (CNES DCT/SB/MO)

//parameters blocked by definition
//they could be modified but carefully


// Declarations:


// Code:

imax=50;
ecart_rel_max=0.5

//initialisation
[Xa,norm_F]=CL__3b_shootingEach(X0,t,env);

ecart_abs=max(norm_F);

if (ecart_abs<epsilon)
  CL__message("Stabilisation reach without iterations \n");
  Xfin=X0
else
  i=1;
  ecart_abs_prec=ecart_abs;
  ecart_rel=.1;//pour l'initialiser
  ecart_min=15;
  while ecart_abs>epsilon&i<imax&ecart_rel<ecart_rel_max
    //xclick()
    CL__message("Iteration: %i Error''s order: 10 %i  \n",i-1,int(log10(ecart_abs)))
    //ploting for validation pourpose
    //xclick()
    //plot2d(Xa(2,:),Xa(3,:),i)
    if ecart_abs<ecart_min
      ecart_min=ecart_abs
      i_min=i-1
      X_min=Xa
    end
    [Xb,norm_F]=CL__3b_shootingEach(Xa,t,env);
    ecart_abs=max(norm_F);//Error absolute
    ecart_rel=(ecart_abs-ecart_abs_prec)/ecart_abs_prec//Error relatif
    Xa=Xb;
    ecart_abs_prec=ecart_abs
    i=i+1;

  end

  Xfin=Xa;
  //ploting for validation pourpose
  //plot2d(Xa(2,:),Xa(3,:),i)
  CL__message("Iteration: %i Error''s order: 10 %i  \n",i-1,int(log10(ecart_abs)))

  if (ecart_rel>ecart_rel_max)
    CL__message("\n ecart_rel=%5.3f >%5.3f \n ",ecart_rel,ecart_rel_max)
    CL__message("---------------------\n Best Iteration: %i Error''s order: 10 %i  \n",i_min,int(log10(ecart_min)))
    Xfin=X_min
  end
  if (ecart_abs<=epsilon)
    CL__message("---------------------------------- \n Stabilisation required reached after %i \n ",i-1)
  else
    if i>=imax
      CL__message("Stabilisation required not reached after %i \n ",i-1)
    end
  end
end
X_t=[Xfin;t]

endfunction
