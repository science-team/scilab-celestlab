//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_3b_manifolds(env,orb,t_orb,period,epsilon,tint,pars)
// Manifolds (divergent and convergent) from Halo and Lissajous
//
// Calling Sequence
// manifolds = CL_3b_manifolds(env,lissajous_orb,epsilon,tint,pars)
// [manifold1,...,manifoldN] = CL_3b_manifolds(env,orb,t_orb,epsilon,tint,pars)
//
// Description
// <itemizedlist><listitem>
// <p>Computes <b>manifolds</b> for <link linkend="CL_3b_halo">Halo</link> 
// and <link linkend="CL_3b_lissajous">Lissajous</link> orbits. </p>
// <p>It can compute all four branches depending on the value of <b>pars</b>: </p>
// <p>convergent in <b>(pars="conv")</b>, convergent out <b>(pars="-conv")</b>
// divergent in <b>(pars="div")</b>, divergent out <b>(pars="-div")</b></p>
// <p>Output manifolds are then given in the same order as <b>pars</b>. </p>
// <p></p></listitem>
// <listitem>
// <p><b> Notes: </b></p> 
// <p>- Before using this function, it is needed to create an "environment" (<b>env</b>) for the chosen libration point and the chosen system (see <link linkend="CL_3b_environment">CL_3b_environment</link>).</p> 
// <p>- A Halo or Lissajous orbit must have been computed before using this function (see <link linkend="CL_3b_halo">CL_3b_halo</link> or <link linkend="CL_3b_lissajous">CL_3b_lissajous</link>). </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// env: Lagrangian point structure . (see <link linkend="CL_3b_environment">CL_3b_environment</link>)
// orb: Lissajous or Halo orbit [6xN] (see <link linkend="CL_3b_halo">CL_3b_halo</link> or <link linkend="CL_3b_lissajous">CL_3b_lissajous</link>)
// t_orb: Adimensional times of lissajous or halo orbit [1xN]
// epsilon: Epsilon. In the literature it is said to be about ~1e-9, but as the method is accurate enough, we recommended 1e-5
// period: Period used to estimate the monodromy. It corresponds to omegahalo for the halo orbits and nu for the Lissajous orbits 
// tint: Integration time (adimensional: use env.OMEGA) (See <link linkend="CL_3b_environment">CL_3b_environment</link>))
// pars: (string) Name(s) of manifolds to compute. Possible values are 'div','-div','conv' or '-conv'.
// manifolds: Generated manifolds (dimensions: 6,n,nb_points). Where first is position and velocities (6), n is extrapolation in time of each point of the original orbit, en nb_points depends on the discretisation of the original orbit( halo or lissajous)
//
// Bibliography
// 1) Introduction au probleme a trois corps et dynamique linearisee autour des points de Lagrange, G. Collange, Note Technique CCT Mecanique Orbitale num.7, CNES 2006
// 2) Estimation numerique des varietes stables et instables des orbites quasi-periodiques de Lissajous autour des points d'Euler (Lagrange L1, L2, L3), R. Alacevich, CNES septembre 2006
// 3) Exploration numerique d'orbites homoclines et heteroclines autour de L1 et L2 dans le probleme restreint a trois corps, rapport de stage, A. Martinez Maida, DCT/SB/MO 2007.0029301, CNES septembre 2007
//
// See also
// CL_3b_environment
// CL_3b_lissajous
// CL_3b_halo
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example with an Halo orbit:
// // Build environement around L2 point of system Sun-EarthMoon:
// env = CL_3b_environment('S-EM','L2');
// // Halo orbit : 
// Az = 150e6/env.D;
// sens = 0;
// t_orb=linspace(0,180,50)*24*3600*env.OMEGA; //180 days
// [orb,omega] = CL_3b_halo(env,Az,sens,t_orb);
// graph1 =figure();xtitle("Halo''s manifold" ,'X','Y');plot2d(orb(1,:),orb(2,:),5);
// // 4 branches manifolds :
// epsilon=1e-5;
// tint =120*24*3600*env.OMEGA//120 days;
// [div_in,conv_in,conv_out,div_out] =  ..
// CL_3b_manifolds(env,orb,t_orb,omega,epsilon,tint,['div','conv','-conv','-div']);
// for i=1:size(conv_out,3)
//  plot2d(conv_out(1,:,i),conv_out(2,:,i),2);
// end
// plot2d(orb(1,:),orb(2,:),5)
//
// // Just one branch :
// [div] = CL_3b_manifolds(env,orb,t_orb,omega,epsilon,tint,['div']);
//

// Declarations:


// Code:

Norb = size(orb,2);
Nt_orb = size(t_orb,2);
if(Norb ~= Nt_orb) then CL__error('orb and t_orb must be of same size'); end;

orb = [orb ; t_orb];  // compatibilite avec l'ancienne interface

CL__message("Convergent and divergent directions computation\n");

[orb_dir] = CL__3b_dirDirConv(orb,period/2,env);
CL__message("Manifolds generation\n");


manifolds = list();
Np = size(pars,'*');

for i=1:Np

  par = pars(i);
  select par
    case 'div'  //calcul de la variete divergente avec epsilon
      manifold = CL__3b_traceManifolds(orb_dir,epsilon,tint,env.MU,0);
    case '-div' //calcul de la variete divergente avec -epsilon
      manifold = CL__3b_traceManifolds(orb_dir,-epsilon,tint,env.MU,0);
    case 'conv' //calcul de la variete convergente avec epsilon
      manifold = CL__3b_traceManifolds(orb_dir,epsilon,tint,env.MU,1);
    case '-conv'//calcul de la variete convergente avec -epsilon
      manifold = CL__3b_traceManifolds(orb_dir,-epsilon,tint,env.MU,1);
    else
      CL__error('Unknown name of manifolds (pars)');
  end

  manifolds($+1) = manifold;

end

lhs = argn(1);

if lhs==1 & Np~=1
  varargout = list(manifolds);
elseif lhs==Np
  varargout = manifolds;
else
  CL__error('Bad size on input/output arguments');
end

endfunction
