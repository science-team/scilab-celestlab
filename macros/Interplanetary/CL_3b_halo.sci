//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [orb,omegahalo] = CL_3b_halo(env,Az,direction,t_orb)
// Halo orbit
//
// Calling Sequence
// [orb,omegahalo] = CL_3b_halo(env,Az,direction,t_orb)
//
// Description
// <itemizedlist><listitem>
// <p>Computes a halo orbit around a Lagrangian point.</p>
// <p></p></listitem>
// <listitem>
// <p><b> Notes: </b></p> 
// <p>- Before using this function, it is needed to create an "environment" (<b>env</b>) for 
// the chosen libration point and the chosen system (see <link linkend="CL_3b_environment">CL_3b_environment</link>).</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// env: Lagrangian point structure (See <link linkend="CL_3b_environment">CL_3b_environment</link>)
// Az: First order of amplitude along Z axis (adimensional: use env.D) (See <link linkend="CL_3b_environment">CL_3b_environment</link>)
// direction: Halo direction, 0 for North orientation or 1 for South orientation 
// t_orb: Times of the final orbit (adimensional: use env.OMEGA) (See <link linkend="CL_3b_environment">CL_3b_environment</link>))
// orb: Stabilized trajectory: [x;y;z;vx;vy;vz] (6xN)
// omegahalo: Halo orbit pulsation
//
// Bibliography
// 1) Introduction au probleme a trois corps et dynamique linearisee autour des points de Lagrange, G. Collange, Note Technique CCT Mecanique Orbitale num.7, CNES 2006
// 2) Estimation numerique des varietes stables et instables des orbites quasi-periodiques de Lissajous autour des points d'Euler (Lagrange L1, L2, L3), R. Alacevich, CNES septembre 2006
// 3) Exploration numerique d'orbites homoclines et heteroclines autour de L1 et L2 dans le probleme restreint a trois corps, rapport de stage, A. Martinez Maida, DCT/SB/MO 2007.0029301, CNES septembre 2007
//
// See also
// CL_3b_environment
// CL_3b_lissajous
// CL_3b_manifolds
//
// Authors
// CNES - DCT/SB
//
// Examples
// env = CL_3b_environment('S-EM','l2');
// Az = 150e6/env.D;
// direction = 0;
// t_orb=linspace(0,180,50)*24*3600*env.OMEGA; //180 days
// [orb,omega] = CL_3b_halo(env,Az,direction,t_orb);
// plot(orb(1,:),orb(2,:),'g');

// Declarations:


// Code:

CL__message("Calculating Initial Conditions \n");


[X0,omegacorr] = CL__3b_condInitHalo(Az,direction,env);

CL__message("Fitting every point \n");
[correction,XXtt,omegahalo] = CL__3b_genHaloMat(X0,omegacorr,t_orb(1),t_orb($),env);

CL__message("Formatting the orbit \n");
orb=CL__3b_formatOrbits(XXtt,t_orb,env);

// Nouvelle interface de sortie : sans la date :
orb = orb(1:6,:);
endfunction
