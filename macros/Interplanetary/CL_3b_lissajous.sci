//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [orb,omega,nu] = CL_3b_lissajous(env,Ax,phix,Az,phiz,epsilon,t_orb)
// Lissajous orbit
//
// Calling Sequence
// [orb,omega,nu] = CL_3b_lissajous(env,Ax,phix,Az,phiz,epsilon,t_orb)
//
// Description
// <itemizedlist><listitem>
// <p>Computes a Lissajous orbit around a Lagrangian point.</p>
// <p></p></listitem>
// <listitem>
// <p><b> Notes: </b> </p>
// <p>- Before using this function, it is needed to create an "environment" (<b>env</b>) 
// for the chosen libration point and the chosen system (see <link linkend="CL_3b_environment">CL_3b_environment</link>).</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// env: Lagrangian point structure (See <link linkend="CL_3b_environment">CL_3b_environment</link>)
// Ax: Amplitude in x (adimensional: use env.D, see <link linkend="CL_3b_environment">CL_3b_environment</link>)
// phix: Phase in x [rad]
// Az: Amplitude in z (adimensional: use env.D, see <link linkend="CL_3b_environment">CL_3b_environment</link>)
// phiz: Phase in z [rad]
// epsilon: Degree of accuracy 
// t_orb: Times of the final orbit (adimensional: use env.OMEGA, see <link linkend="CL_3b_environment">CL_3b_environment</link>)
// orb: Stabilized trajectory [x;y;z;vx;vy;vz] [6xN]
// omega: Lissajous orbit pulsation
// nu: nu
//
// Bibliography
// 1) Introduction au probleme a trois corps et dynamique linearisee autour des points de Lagrange, G. Collange, Note Technique CCT Mecanique Orbitale num.7, CNES 2006
// 2) Estimation numerique des varietes stables et instables des orbites quasi-periodiques de Lissajous autour des points d'Euler (Lagrange L1, L2, L3), R. Alacevich, CNES septembre 2006
// 3) Exploration numerique d'orbites homoclines et heteroclines autour de L1 et L2 dans le probleme restreint a trois corps, rapport de stage, A. Martinez Maida, DCT/SB/MO 2007.0029301, CNES septembre 2007
//
// See also
// CL_3b_environment
// CL_3b_halo
// CL_3b_lissajous
// CL_3b_manifolds
//
// Authors
// CNES - DCT/SB
//
// Examples
// env = CL_3b_environment('S-EM','l2');
// Ax = -30e6/env.D;
// phix = 0;
// Az = 100e6/env.D;
// phiz = 0;
// date_init = 0;
// epsilon=1e-10
// t_orb = linspace(0,4,100)*365.25*24*3600*env.OMEGA//4 years;
// orb = CL_3b_lissajous(env,Ax,phix,Az,phiz,epsilon,t_orb);
// plot(orb(1,:),orb(2,:),'g')
// f1 = scf();
// subplot(222); plot(orb(1,:),orb(3,:),'r'); xtitle('','X','Z'); plot2d(env.gl,0,-10);
// subplot(221);xgrid(); plot(orb(2,:),orb(3,:),'r'); xtitle('','Y','Z'); plot2d(0,0,-10);
//
// subplot(224); xgrid();plot(orb(1,:),orb(2,:),'r'); xtitle('','X','Y');plot2d(env.gl,0,-10);
//
// subplot(223);xstring(0.5,0.5,'Lissajous'' orbit');
//

// calcul des conditions initiales lineaires

// Declarations:


// Code:

CL__message("Calculating Initial Conditions \n");
points_per_orbit=5
//In order to avoid the first and last points with enough accuracy
//  we must add pi/omega at the beginning
t0=t_orb(1)-%pi/env.omega_init
tf=t_orb($)+%pi/env.omega_init
t_init=linspace(t0,tf,int(tf-t0/env.omega_init)*points_per_orbit);
[X0,omega,nu] = CL__3b_condInitLiss(Ax,phix,Az,phiz,env,t_init);

CL__message("Fitting every point \n");
[XXtt] = CL__3b_shooting(X0,t_init,env,epsilon);

CL__message("Formatting the orbit \n");
orb=CL__3b_formatOrbits(XXtt,t_orb,env);

// Nouvelle interface de sortie : sans la date :
orb = orb(1:6,:);
endfunction
