//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Traj_dir] = CL__3b_dirDirConv(orb,T,env)
//Author
// 12/08/07 R.Alacevich et B. Meyssignac ( CNES, DCT/SB/MO)
// 12/02/09 A. Blazquez( CNES, DCT/SB/MO)

//fonction qui donne, a chaque point de la trajectoire regeneree, le vecteur propre des varietes convergentes et divergentes a partir de la matrice de monodromie calculee au bout du temps T.
//  entree: orb: 7xn: sortie de la regeneration,
//                             chaque colonne est un point de l'orbite: position vitesse et date
//              T: 1 : temps au bout duquel est estime la matrice de monodromie
//  sortie: matrice Traj_dir:
//    premieres 7 lignes: les memes que orb (point et date)
//    ligne 8-13: direction divergeante normee du point correspondant
//    ligne 14-19: direction convergeante normee du point correspondant


// Declarations:


// Code:

sizeTL = size(orb,2);
Traj_dir=[]
div_dir = [];
conv_dir = [];
CL__message("Directions computation for " + msprintf("%d",sizeTL) + " points\n");

for i=1:sizeTL
  CL__message("%d ", i);
  //calcul de la monodromie a T pour la direction divergente
  //et a -T pour la direction convergente
  [XTpos,monodTpos] = CL__3b_monodromy(orb(1:6,i),T,env.pas,env.MU);
  [XTneg,monodTneg] = CL__3b_monodromy(orb(1:6,i),-T,env.pas,env.MU);
  //diagonalisation des matrices de monodromie
  // AB A modifier apartir d'ici il a besoin que de vecteur propre de Lambda
  VectPropPos = CL__3b_monodroVectProp(monodTpos);
  VectPropNeg = CL__3b_monodroVectProp(monodTneg);

  div=real(VectPropPos)
  signe_div = sign(div(1));
  div_direction = signe_div*div/norm(div);

  conv=real(VectPropNeg)
  signe_conv = sign(conv(1));
  conv_direction = signe_conv*conv/norm(conv);

  div_dir = [div_dir,div_direction];
  conv_dir = [conv_dir,conv_direction];

end
CL__message("\n");

Traj_dir = [orb;div_dir;conv_dir];
endfunction
