//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function gammal = CL__3b_gamma(MU,Lpoint)
//Author
// 20/09/10 E.Canalias ( CNES, DCT/SB/MO)

// Distance between libration point and closest primary
// Based on GR_GAM.F from Development of a software library for libration point mission analysis
//                        CNES Contract R-S06/VF-0001-052
// Input: MU mass parameter of corresponding problem (S-E, S-EM, S-J)
//        Lpoint libration point (1, 2 or 3)
// Output: gammal distance from the libration point to the closest primary in RTBP adimensional units

// Declarations:

// Code:


select Lpoint
case 'L1'
  X=(MU/3)^(1/3);
  deff("[u]=H(X,MU)",["mu2=2*MU";"a4=3-MU";"a3=3-mu2"; "u1=-MU+X*(mu2+X*(-MU+X*(a3+X*(-a4+X))))";"u2=mu2+X*(-mu2+X*(3*a3+X*(-4*a4+5*X)))";"u=u1/u2"]); 
case 'L2'
  X=(MU/3)^(1/3);
  deff("[u]=H(X,MU)",["mu2=2*MU";"a4=3-MU";"a3=3-mu2"; "u1=-MU+X*(-mu2+X*(-MU+X*(a3+X*(a4+X))))";"u2=-mu2+X*(-mu2+X*(3*a3+X*(4*a4+5*X)))";"u=u1/u2"]); 
case 'L3'
  X=1-7.D0*MU/12.D0;
  deff("[u]=H(X,MU)",["mum=1-MU";"a3=1+2*MU";"a4=2+MU";"u1=-mum+X*(-2*mum+X*(-mum+X*(a3+X*(a4+X))))";"u2=-2*mum+X*(-2*mum+X*(3*a3+X*(4*a4+5*X)))";"u=u1/u2"]);  
 end
 
 j=1;
 itmax=20;
 hh=H(X,MU);
 while (abs(hh)>1.E-15 & j <= itmax)
   j=j+1;
   X=X-hh;
   hh=H(X,MU);      
 end
 if (j > itmax ) then
     CL__message("CL__3b_gamma: %i iterations!! \n",itmax);
 end 
 gammal=X-hh;
 endfunction