//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [dv,rph,tanoh] = CL_ip_escapeDv(sma,ecc,vinf,tanoe,mu)
// Delta-V needed to escape from a planet
//
// Calling Sequence
// [dv,rph,tanoh] = CL_ip_escapeDv(sma,ecc,vinf [,tanoe,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the delta-V needed for the transfer from an elliptical orbit around a given planet to an 
// hyperbolic escape orbit.</p>
// <p>The escape maneuver is tangential, i.e. the delta-V is parallel to the velocity vector on the elliptical orbit. </p>
// <p>The initial orbit is defined by its semi-major axis (<b>sma</b>) and eccentricity (<b>ecc</b>).</p>
// <p>The final orbit is defined by its velocity at infinity (<b>vinf</b>).</p>
// <p>The true anomaly of the maneuver on the initial orbit (<b>tanoe</b>) can optionally be specified. By default tanoe=0 (meaning 'at the periapsis').</p>
// <p>The planet is defined by its gravitational constant mu (default is %CL_mu)</p></listitem>
// </itemizedlist>
//
// Parameters
// sma: Semi-major axis of initial orbit [m]. (1xN)
// ecc: Eccentricity of initial orbit. (1xN)
// vinf: Target velocity on hyperbolic orbit at infinity [m/s]. (1xN)
// tanoe: (optional) True anomaly of maneuver [rad]. Default value is 0. (1xN)
// mu: (optional) Gravitational constant of the planet considered [m3/s2]. Default value is %CL_mu.
// dv: Norm of the delta-v. (1xN)
// rph: Periapsis radius of hyperbolic orbit [m]. (1xN)
// tanoh: True anomaly on the hyperbolic orbit at the time of the maneuver [rad]. (1xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Escape from a LEO (circular) orbit :
// eqRad = 6378.e3;
// sma = [eqRad+350.e3 , eqRad+700.e3];
// ecc = zeros(sma);
// vinf = [5 , 6] * 1.e3;
// [dv, rph, tanoh] = CL_ip_escapeDv(sma, ecc, vinf)
//
// // Escape from an orbit around Mars :
// eqRad = CL_dataGet("body.Mars.eqRad");
// mu = CL_dataGet("body.Mars.mu");
//
// sma = [eqRad+350.e3 , eqRad+700.e3];
// ecc = [0.1 , 0.05];
// vinf = [5 , 6] * 1.e3;
// tanoe = [0, 0.1]; // radians
// [dv,rph,tanoh] = CL_ip_escapeDv(sma,ecc,vinf,tanoe,mu)
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('tanoe','local') then tanoe = 0; end
if ~exists('mu','local') then mu = %CL_mu; end

// check inputs
[sma,ecc,vinf,tanoe] = CL__checkInputs(sma,1,ecc,1,vinf,1,tanoe,1); 

I = find(sma <= 0);
if (I <> [])
  CL__error("Invalid sma: must be strictly positive");
end

I = find(ecc < 0 | ecc >= 1);
if (I <> [])
  CL__error("Invalid ecc: must be in [0,1[");
end

I = find(vinf <= 0);
if (I <> [])
  CL__error("Invalid vinf: must be strictly positive");
end
  
[nbarg_out] = argn(1);

// Initial velocity (elliptical orbit) :
p_ell = sma.*(1-ecc.^2);
r_ell = p_ell ./ (1 + ecc .* cos(tanoe));
v_ell = sqrt(mu * (2 ./ r_ell - 1 ./ sma));
  
  
// Final velocity (hyperbolic orbit) :
r_hyp = r_ell;
sma_hyp = mu ./ vinf.^2;
v_hyp = sqrt(mu * (2 ./ r_hyp + 1 ./ sma_hyp));
  
dv = v_hyp - v_ell;
  
if (nbarg_out >= 2)
  // We use the fact that the slope of the velocity is the same before/after maneuver. 
  // C_ell ./ (r_ell .* v_ell) = C_hyp ./ (r_hyp .* v_hyp)
  C_ell = sqrt(mu * p_ell);
  C_hyp = C_ell .* v_hyp ./ v_ell;
  ecc_hyp = sqrt( 1 + C_hyp.^2 ./ (mu*sma_hyp));
  rph = sma_hyp .* (ecc_hyp - 1);
end

if (nbarg_out >= 3)
  p_hyp = sma_hyp .* (ecc_hyp .^2 - 1);
  sin_gamma = ecc .* sqrt(mu ./ p_ell) .* sin(tanoe) ./ v_ell;
  
  sin_tanoh = sin_gamma ./ ecc_hyp ./ sqrt(mu ./ p_hyp) .* v_hyp;
  cos_tanoh = p_hyp ./ (r_hyp .* ecc_hyp) - 1 ./ ecc_hyp;
  
  tanoh = atan(sin_tanoh,cos_tanoh);
end
  
endfunction
