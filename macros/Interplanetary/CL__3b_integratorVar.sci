//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Y,t] = CL__3b_integratorVar(Y0,t0,tint,f,MU)


// Author:
// B. Meyssignac (CNES DCT/SB/MO)

//fonction qui utilise l'integration "root", pour le seul calcul de trajectoire
//  entrees: Y0: vecteur colonne des conditions initiales (six composantes: pos - vitesses)
//     t0: temps initial
//     tint: duree de l'integration
//               f: second membre: YCL_dot = f(t,Y). il n est pas gere ici en interne l'integration a rebours:
//Scilab dans sa version actuelle n'integre pas a rebours quand il fonctionne avec un pas variables.
//pour integrer a rebours il faut changer t en -t dans f
//Dans ce cas on integre non pas avec la fonction f mais avec f_reb
//qui doit etre definie dans la pile de variable. f_reb est  f dans laquelle t a ete change en -t.
//  sorties: Y: matrice resultat de l'integration: tous les points de la trajectoire
//     t: vecteur des dates associees a chaque point (colonne de Y)


// Declarations:


// Code:

%ODEOPTIONS = [2,0,0,%inf,0,2,500000,12,5,0,-1,-1];
sizeY = size(Y0);
rtol = 1.e-8*ones(sizeY(1),1);
atol = 1.e-9*ones(sizeY(1),1);
ng = 1;

if  tint>0

  deff("[u]=r(t,X)","u=t0+tint-t");
  [Yt,rd] = ode("root",Y0,t0,t0+tint,rtol,atol,f,ng,r);
  Y = Yt(2:$,:);
  t = Yt(1,:);

else

  deff("[u]=r(t,X)","u=t0+tint+t");
  Y0_reb = [Y0(1:3);-Y0(4:6)];
  [Yt,rd] = ode("root",Y0_reb,-t0,-(t0+tint),rtol,atol,f,ng,r);
  Y = [Yt(2:4,:);-Yt(5:7,:)];
  t = -Yt(1,:);

end

endfunction
