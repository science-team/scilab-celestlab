//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Xstab,corrnum,Xtraj,ttraj,omegatraj,deltaY] = CL__3b_stabHaloNewton(X0,omegacorr,tinit,pas,MU)
// This function stabilise the halo initial conditions ( From Richardson's method)
// Using Newton Method, the function assures the convergence of half an orbit arcs.
//
//  Inputs: X0: Halo Initial conditions (columns of 6 dimension (position+velocity))
//      omegacorr: Halo pulsation corrected
//          tinit: initial date
//          pas: from tb_environnement usually 0.01
//          MU: from tb_environnement
//
//  Outputs: Xstab: Initial conditions of the input orbit stabilise: vect (columns of 6 dimension (position+velocity))
//       corrnum: numeric corrections of the stabilisation process: vect colums of 2: dx dvy
//       Xtraj: Every point of the stabilise trajectory: matrix 6 lines (pos+vel) and n columns (dates)
//       ttraj: dates of each point of the trajectoire: vect n lines
//       deltaY: difference between the norms of the last 2 vectores Y of the Newton's method
//
// Author:
// B. MEYSSIGNAG (CNES DCT/SB/MO)

// Declarations:


// Code:

Yn = [];
Xn = [];
tn = [];
Xf = [];

// Initialisation of Newton's method
Yn = [X0(1);X0(5);%pi/omegacorr];
Xn = [Yn(1);X0(2);X0(3);X0(4);Yn(2);X0(6)];
tn = Yn(3);
dn=[];

[Xf,Mn] = CL__3b_monodromy(Xn,tn,pas,MU);

//Newton's method
while (abs(Xf(2)) + abs(Xf(4)) + abs(Xf(6)) > 3.e-14)

  r0f = sqrt((Xf(1)+MU)^2+Xf(2)^2+Xf(3)^2);
  r1f = sqrt((Xf(1)-1+MU)^2+Xf(2)^2+Xf(3)^2);

  DNewton = [Mn(2,1) Mn(2,5)  Xf(5);
             Mn(4,1) Mn(4,5)  2*Xf(5)+Xf(1)-(1-MU)*(Xf(1)+MU)/(r0f^3)-MU*(Xf(1)-1+MU)/(r1f^3);
             Mn(6,1) Mn(6,5) -(1-MU)*Xf(3)/(r0f^3)-MU*Xf(3)/(r1f^3)];

  dn = inv(DNewton)*[Xf(2);Xf(4);Xf(6)];

  deltaY = norm(dn);
  if deltaY<=%eps then break; end

  Yn = Yn - dn;
  Xn = [Yn(1);X0(2);X0(3);X0(4);Yn(2);X0(6)];
  tn = Yn(3);

  [Xf,Mn] = CL__3b_monodromy(Xn,tn,pas,MU);
end

if deltaY<=%eps
  CL__message("deltaY <= eps, impossible stabilisation\n");
else
  //format('v',24);
  yp = abs(Xf(2)) + abs(Xf(4)) + abs(Xf(6));
  CL__message("convergence of semi halo orbit estimation \n abs(y) + abs(vx) +abs(vz) = %e\n",yp);
end

//integration results
Xstab = Xn;
corrnum = [Xn(1)-X0(1);Xn(5)-X0(5)];
Z0 = [Xn;matrix(eye(6,6),36,1)];
deff("[u]=r(t,X)","u=t-tinit-tn");
[Ztraj,ttraj,rdtraj] = CL__3b_integratorFix(Z0,tinit,2*tn,CL__3b_RHSDP,r,pas,MU);
Xtraj = [Ztraj(1:6,:) (rdtraj(3:8))'];
ttraj = [ttraj,rdtraj(1)];
omegatraj = %pi/tn;

// Verifications plots

// param3d(Xtraj(1,:)',Xtraj(2,:)',Xtraj(3,:)');
// line = gce();
// line.foreground = 2;
// xgrid;

endfunction
