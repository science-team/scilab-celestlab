//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [X,M] = CL__3b_monodromy(X0,tint,pas,MU)
//fonction which computes the monodromy matrix for tint on X0

//Inputs: X0: initial point (columns of 6 dimension (position+velocity))
//              tint: Integration time
//Outputs:  X: Final point after integration(columns of 6 dimension (position+velocity)) 6x1 position vitesse a tint
//              M: Monodromy matrix (6x6)
// Author:
// R. Alacevich (Stage CNES DCT/SB/MO)
// B. Meyssignac (CNES DCT/SB/MO)


//Initialisation

// Declarations:


// Code:

Id = eye(6,6);
Z0 = [X0;matrix(Id,36,1)];

// Integration with fixed step
tfin = tint;
deff("[u]=r(t,Y)","u=t-tfin");
[Ztra,ttra,rdtra] = CL__3b_integratorFix(Z0,0,2*tfin,CL__3b_RHSDP,r,pas,MU);

sizerd = size(rdtra,2);
if sizerd>0
  monod = rdtra(9:$)';
  X = (rdtra(3:8))';
  M = matrix(monod,6,6);
else
  CL__message("tint = %f and tfin = %f\n",tint,ttra($));
  CL__error('CL__3b_monodromy.sci: monodromy integration is not completely done until the end!');
end

endfunction
