//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cjd_tcb]=CL_dat_tdb2tcb(cjd_tdb)
// TDB to TCB conversion
//
// Calling Sequence
// cjd_tcb = CL_dat_tdb2tcb(cjd_tdb)
//
// Description
// <itemizedlist><listitem>
// <p>Converts the time of a modified Julian date (from 1950.0) from TCB time scale to 
// TDB time scale.</p>
// <p>The conversion is compliant with the new definition for TDB (IAU 2006).</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// cjd_tdb: Modified (1950.0) Julian date in TDB time scale. (1xN)
// cjd_tcb: Modified (1950.0) Julian date in TCB time scale. (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// IAU 2006 Resolution 3 : Re-definition of Barycentric Dynamical Time, TDB (XXVIth International Astronomical Union General Assembly)
// U.S NAVAL OBSERVATORY CIRCULAR NO. 179, by G.H. Kaplan, 2005/10/20
//
// See also
// CL_dat_tcb2tdb
//
// Examples
// cjd_tdb = CL_dat_cal2cjd(2006,1,15,21,25,42.683799);
// cjd_tcb = CL_dat_tdb2tcb(cjd_tdb);

// Declarations:

// Code:

// See http://syrte.obspm.fr/iauJD16/IAU2006_Resol3.pdf or 
//     http://aa.usno.navy.mil/publications/docs/Circular_179.pdf for more details

// Parameters
Lb = 1.550519768e-8;
// modified julian days 1950.0 cjd_T0 (in TCB) for event 1977/1/1 0:00:00 TAI
// TCB is synchronized with TT (= TAI + 32.184 sec = TAI + 0.0003725 julian days of 86400 sec)
cjd_T0 = 9862.0003725;
// tdb0 value in sec
tdb0 = -6.55e-5;

// NB: The approximation made using cjd_tdb instead of cjd_tcb in the difference 
// with cjd_T0 leads to a very small maximum error of 0.9 microsec at the horizon of year 2100 
// (e.g. more than 1000 times lower than the 2 ms error made when assuming TT = TDB)
cjd_tcb = cjd_tdb + Lb * (cjd_tdb-cjd_T0) - tdb0 / 86400;

endfunction
