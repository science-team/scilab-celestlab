//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos2,vel2,jacob] = CL_fr_bodyConvert(body,frame1,frame2,cjd,pos1, vel1,tt_tref)
// Position and velocity transformation from one body frame to another
//
// Calling Sequence
// [pos2,vel2,jacob] = CL_fr_bodyConvert(body,frame1,frame2,cjd,pos1, [,vel1,tt_tref])
//
// Description
// <itemizedlist><listitem>
// <p>Converts position and (optionally) velocity vectors relative to "frame1" to position and (optionally) 
// velocity vectors relative to "frame2". </p>
// <p>The jacobian of the transformation is optionally computed.</p>
// <p></p></listitem>
// <listitem>
// <p>Available frames are : </p>
// <p>    'ICRS': International Celestial Reference System</p>
// <p>    'BCI': Body Centered Inertial</p>
// <p>    'BCF': Body Centered body Fixed</p>
// <p></p>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// <p></p>
// <p>Available bodies are: "Mercury","Venus","Mars","Jupiter","Saturn","Uranus", "Neptune", "Sun" and "Moon" </p>
// <p></p></listitem>
// <listitem>
// <p>Notes : </p>
// <p>- The date "cjd" is relative to the TREF time scale. </p>
// <p>- The frame names and body names are case sensitive</p>
// <p>- If only the position needs to be converted, vel1 can be omitted or set to []. (vel2 will then be set to []).</p>
// <p>- Earth is not a valid body for this function. See CL_fr_convert.</p>
// <p>- GCRS (identical to ICRS in CelestLab) is also accepted as a frame name.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// body: (string) Name of the body. ("Mercury","Venus","Mars","Jupiter","Saturn","Uranus", "Neptune", "Sun" or "Moon") (1x1)
// frame1: (string) Name of the initial frame. (1x1)
// frame2: (string) Name of the final frame. (1x1)
// cjd: Modified (1950.0) julian day (Time scale: TREF) (1xN or 1x1)
// pos1: Position vector in initial frame. (3xN or 3x1)
// vel1: Velocity vector in initial frame. (3xN or 3x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
// pos2: Position vector in final frame. (3xN or 3x1)
// vel2: (optional) Velocity vector in final frame. (3xN or 3x1)
// jacob: (optional) Jacobian of the transformation (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// Report of the IAU/IAG working group on cartographic coordinates and rotational elements: 2009
//
// See also
// CL_mod_IAUBodyAngles
// CL_fr_bodyConvertMat
// CL_fr_convert
//
// Examples
// cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
//
// // ICRS to BCF
// pos_EME2000 = [1e5;3e4;6e6];
// vel_EME2000 = [-1e3;3e3;6e3];
// [pos_BCF,vel_BCF] = CL_fr_bodyConvert("Mars","ICRS","BCI",cjd,pos_EME2000,vel_EME2000);


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_TT_TREF")); %CL_TT_TREF = %CL__PRIV.DATA.TT_TREF; end

// Code:
if (argn(2) < 5)
  CL__error('Not enough input arguments');
end

if (typeof(body) <> "string"); CL__error("Invalid input argument ''body''"); end;
if (typeof(frame1) <> "string"); CL__error("Invalid input argument ''frame1''"); end;
if (typeof(frame2) <> "string"); CL__error("Invalid input argument ''frame2''"); end;

vel_present = %t;
if ~exists("tt_tref", "local"); tt_tref = %CL_TT_TREF; end
if ~exists("vel1","local"); vel_present = %f; vel1 = []; end

// Check argument sizes, and resize if necessary:
if (vel_present)
  [cjd,pos1,vel1,tt_tref] = CL__checkInputs(cjd,1, pos1,3, vel1,3, tt_tref,1);
else
  [cjd,pos1,tt_tref] = CL__checkInputs(cjd,1, pos1,3, tt_tref,1);
end

// Conditions to compute velocity
cvel = %f;
if (argn(1) >= 2 & vel_present)
  cvel = %t;
end

// Condition to compute jacobian
cjac = %f;
if (argn(1) >= 3)
  cjac = %t;
end

// NB: validity of frame names and body is done by function CL_fr_bodyConvertMat
[M,omega] = CL_fr_bodyConvertMat(body,frame1,frame2,cjd, tt_tref);

// NB: vel2 and jacob will be set to [] if cvel and cjac are %f
[pos2,vel2,jacob] = CL_rot_pvConvert(pos1,vel1,M,omega,cvel,cjac)

endfunction
