//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2(I) = q1 
// I = integers (row vector)
// I can be empty in theory (but it does not work)

function [q2] = %CLquat_i_CLquat(I,q1,q2)

N1 = size(q1);
N2 = size(q2);

// q(:) <-> q(1:$)
if (isequal(I,:))
  I = 1:$;
end

// NB : I is of type "size implicit" if it contains the $ operator (ex : 1:$)
if (typeof(I) <> "size implicit")
  sI = size(I);
  if (sI(1) > 1 | min(I) <= 0.5 | max(I) >= N2+0.5)
    CL__error("Invalid index"); 
  end
  
else
  sI = size(q2(I));
  if (sI == 0)
    I = [];
    sI = [0, 0];
  else
    sI = [1,sI];
  end
end



if (sI(1) == 0 | N2 == 0)
  return; // no change

else

  if (N1 <> 1 & N1 <> sI(2))
    CL__error("Invalid size for q1"); 
  end

  r = q2.r;
  i = q2.i;

  r1 = q1.r; 
  i1 = q1.i; 

  if (N1 < sI(2))
    r1 = r1 * ones(1,sI(2));
    i1 = i1 * ones(1,sI(2));
  end

  r(:,I) = r1;
  i(:,I) = i1;

  q2 = CL__defQuat(r, i);

end

endfunction

