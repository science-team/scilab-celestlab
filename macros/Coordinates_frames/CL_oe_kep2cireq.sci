//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cireq,jacob] = CL_oe_kep2cireq(kep)
// Keplerian to circular equatorial orbital elements
//
// Calling Sequence
// [cireq,jacob] = CL_oe_kep2cireq(kep)
//
// Description
// <itemizedlist><listitem>
// <p>Converts classical Keplerian orbital elements to circular equatorial orbital elements.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// kep: Classical Keplerian orbital elements [sma;ecc;inc;pom;raan;anm] [m,rad] (6xN)
// cireq: Circular equatorial orbital elements [sma;ex;ey;ix;iy;L] [m,rad] (6xN)
// jacob: (optional) transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_cireq2kep
// CL_oe_kep2car
//
// Examples
// // Example 1
// kep = [7000.e3;0.01;1.8;0.1;0.2;0.3];
// cireq = CL_oe_kep2cireq(kep);
//
// // Example 2
// kep = [7000.e3;0.01;1.8;0.1;0.2;0.3];
// [cireq,jacob1] = CL_oe_kep2cireq(kep);
// [kep2,jacob2] = CL_oe_cireq2kep(cireq);
// kep - kep2 // zero
// jacob1*jacob2 // identity

// Declarations:
global %CL__PRIV; 
EPS_ORB = %CL__PRIV.DATA.epsOrb;

// Code:

// Handle [] cases
if (kep == [])
  cireq = [];
  jacob = [];
  return;
end

// Check validity of input
[isvalid,type_orbit] = CL__oe_isValid("kep",kep);
if (~isvalid); CL__error("Invalid orbital elements"); end;
if (find(type_orbit <> 1) <> []); CL__error("Invalid orbital elements (parabolic or hyperbolic orbit)"); end;

cireq = zeros(kep)

// Conversion formulas:
// a(cireq) = a(kep)
// ex = e*cos(pom+gom)
// ey = e*sin(pom+gom)
// ix = sin(i/2)cos(gom)
// iy = sin(i/2)sin(gom)
// L = M + pom + gom

cireq(1,:) = kep(1,:);
cireq(2,:) = kep(2,:) .* cos(kep(4,:)+kep(5,:));
cireq(3,:) = kep(2,:) .* sin(kep(4,:)+kep(5,:));
cireq(4,:) = sin(kep(3,:)/2) .* cos(kep(5,:));
cireq(5,:) = sin(kep(3,:)/2) .* sin(kep(5,:));
cireq(6,:) = kep(4,:) + kep(5,:) + kep(6,:);

// Jacobian computation (dcireq/dkep)
if (argn(1) == 2)
  // jacob(i,j) = d(cireq_i)/d(kep_j)
  //
  // Formulas used:
  // da/da = 1
  // dex/de = cos(pom+gom)
  // dex/dix = -ey
  // dex/diy = -ey
  // dey/de = sin(pom+gom)
  // dey/dix = ex
  // dey/diy = ex
  // dix/di = cos(i/2)*cos(gom)/2
  // dix/dgom = -iy
  // diy/di = cos(i/2)*sin(gom)/2
  // diy/dgom = ix
  // dL/dpom = 1
  // dL/dgom = 1
  // dL/dM = 1

  N = size(kep,2);
  jacob = zeros(6,6,N);
  
  jacob(1,1,:) = 1; // da/da
  
  jacob(2,2,:) = cos(kep(4,:)+kep(5,:)); // dex/de
  jacob(2,4,:) = - cireq(3,:); // dex/dix
  jacob(2,5,:) = - cireq(3,:); // dex/diy
  
  jacob(3,2,:) = sin(kep(4,:)+kep(5,:)); // dey/de
  jacob(3,4,:) = cireq(2,:); // dey/dix
  jacob(3,5,:) = cireq(2,:); // dey/diy
  
  jacob(4,3,:) = cos(kep(3,:)/2) .* cos(kep(5,:))/2; // dix/di
  jacob(4,5,:) = - cireq(5,:); // dix/dgom
  
  jacob(5,3,:) = cos(kep(3,:)/2) .* sin(kep(5,:))/2; // diy/di
  jacob(5,5,:) = cireq(4,:); // diy/dgom
  
  jacob(6,4,:) = 1; // dL/dpom
  jacob(6,5,:) = 1; // dL/dgom
  jacob(6,6,:) = 1; // dL/dM

end

endfunction
