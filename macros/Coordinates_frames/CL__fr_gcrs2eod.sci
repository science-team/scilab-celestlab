//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL__fr_gcrs2eod(jd, args, comega)
// GCRS to Ecliptic Of Date (EOD) transformation matrix and angular velocity vector
//
// Calling Sequence
// [M, omega] = CL__fr_gcrs2eod(jd [,args, comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from GCRS to EOD</p>
// <p>By convention, multiplying <b>M</b> by coordinates relative to GCRS yields coordinates relative to EOD.</p> 
// <p>Optionaly computes the angular velocity vector <b>omega</b> of EOD relative to GCRS, with coordinates relative to GCRS.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <p>The transformation consists of IAU2006 precession + IAU2006 mean obliquity of the ecliptic</p>
// <p>Note : args appears in the calling sequence but is not used.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// args: Not used.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// M: Transformation matrix (3x3xN)
// omega: (optional) Angular velocity vector [rad/s] (3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010

// Declarations:

// Code:

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

// IAU2006 precession and IAU2006 obliquity
// From GCRS to MOD: [3,1,3,1], [gamb;phib;-psib;-epsa]
// From MOD to EOD: [1], [epsa]
// --> From GCRS to EOD: [3,1,3], [gamb;phib;-psib]

[gamb,phib,psib,epsa, gambdot,phibdot,psibdot,epsadot] = CL__iers_prec2006(jd, comega);

M = CL_rot_angles2matrix([3,1,3], [ gamb; phib ; -psib]);

// Angular velocity vector
omega = [];
if (comega)
  omega = CL_rot_angVelocity([3,1,3], [gamb;phib;-psib], [gambdot;phibdot;-psibdot]);
end

endfunction
