//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_dat_cjd2cal(cjd)
// Modified (1950.0) Julian day to calendar date
//
// Calling Sequence
// [year,month,day,hour,minute,second] = CL_dat_cjd2cal(cjd)
// [year,month,day] = CL_dat_cjd2cal(cjd) // cjd = whole number
// [cal] = CL_dat_cjd2cal(cjd) // cal=[year;month;day;hour;minute;second]
//
// Description
// <itemizedlist><listitem>
// <p>Converts a 1950.0 Julian date (Julian day from 1950.0) into a calendar date.</p>
// <p>The output arguments are either: </p>
// <p> - year, month, day, hour, minute, second: 3 or 6 output arguments are expected. 3 is accepted only if the Julian date is a whole number. </p>
// <p> - cal: colum vector containing year, month, day, hour, minute, second. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: Modified Julian date from 1950.0 (1xN)
// year: Year (1xN)
// month: Month (1xN)
// day: Day (1xN)
// hour: Hours (1xN)
// minute: Minutes (1xN)
// second: Seconds (1xN)
// cal: Calendar date [year;month;day;hour;minute;second] (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_dat_cal2cjd
// CL_dat_convert
//
// Examples
// // Example 1
// cjd = 21965.50260416667;
// [year,month,day,hour,minute,second] = CL_dat_cjd2cal(cjd)
// cal = CL_dat_cjd2cal(cjd)
//
// // Example 2 (with cjd a whole number)
// cjd = 18262;
// [year,month,day] = CL_dat_cjd2cal(cjd)
// cal = CL_dat_cjd2cal(cjd)

// Declarations:


// Code:

// Check number of input arguments
if (argn(2) <> 1)
  CL__error("Wrong number of input arguments");
end

// Handle [] case
if (cjd == [])
  for k = 1 : argn(1)
    varargout(k) = [];
  end
  return; // <-- RETURN
end

// Check number of output arguments
if (argn(1) <> 1 & argn(1) <> 3 & argn(1) <> 6)
  CL__error("Wrong number of output arguments: 1, 3 or 6 expected.");
end
if (argn(1) == 3)
  if (find (cjd <> round(cjd)) <> [])
    CL__error("Wrong number of output arguments: 3 only for integer dates");
  end
end

if (size(cjd,1) <> 1)
  CL__error("Wrong dimension of input arguments");
end

cal = CL_dat_convert("cjd", "cal", cjd);

// Ouput
if (argn(1) == 1)
  varargout(1) = cal;

else
  varargout(1) = cal(1,:);
  varargout(2) = cal(2,:);
  varargout(3) = cal(3,:);
  if (argn(1) == 6)
     varargout(4) = cal(4,:);
     varargout(5) = cal(5,:);
     varargout(6) = cal(6,:);
  end
end

endfunction
