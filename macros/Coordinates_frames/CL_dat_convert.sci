//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [date_out] = CL_dat_convert(type_in,type_out,date_in)
// Date type conversion 
//
// Calling Sequence
// date_out = CL_dat_convert(type_in,type_out,date_in)
//
// Description
// <itemizedlist><listitem>
// <p>Converts a date from one type to another.</p>
// <p>The available types are:</p>
// <p>- "jd": Julian date </p>
// <p>- "cjd": Modified Julian date from 1950.0</p>
// <p>- "mjd": Modified Julian date: MJD = JD - 2400000.5 </p>
// <p>- "cal": Calendar date. [year;month;day] or [year;month;day;hours;minutes;seconds]</p>
// <p></p>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// <p></p>
// </listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>The input  and output dates are relative to the same time scale.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// type_in: (string) Type of input date: "jd", "cjd", "mjd" or "cal" (1x1)
// type_out: (string) Type of output date: "jd", "cjd", "mjd" or "cal" (1x1)
// date_in: Input date. (1xN or 3xN or 6xN). (3xN or 6xN only if type_in is "cal")
// date_out: Output date (1xN or 6xN). (6xN only if type_out is "cal")
//
// Authors
// CNES - DCT/SB
//
// Examples
// cjd = 21956.3;
//
// // Convert from cjd to jd
// jd = CL_dat_convert("cjd","jd",cjd);
// 
// // Convert from jd to calendar
// cal = CL_dat_convert("jd","cal",jd);
// 

// Declarations:

// Code:
if (argn(2) <> 3)
  CL__error("Wrong number of input arguments");
end

if (typeof(type_in) <> "string" | typeof(type_out) <> "string")
   CL__error("String expected for type_in and type_out"); 
end

if (size(type_in,"*") <> 1 | size(type_out,"*") <> 1); CL__error("Wrong dimension(s)"); end;
  
// Validity of strings
type_par_names = ["jd", "cjd", "mjd", "cal"];
if (find(type_in == type_par_names) == [] | find(type_out == type_par_names) == [])
  CL__error("Unrecognized string(s)");
end

// Handle [] case
if (date_in == [])
  date_out = [];
  return; // <-- RETURN
end

// Dimension of input date
if (type_in == "jd" | type_in == "cjd" | type_in == "mjd")
  if (size(date_in,1) <> 1); CL__error("Wrong dimension for input date"); end;
end

if (type_in == "cal" & size(date_in,1) <> 3 & size(date_in,1) <> 6)
  CL__error("Wrong dimension for input date");
end

if (type_in == type_out)
  // Special case
  data_out = date_in; 
  
else  
  // Compute an intermediate date (JD = two-part Julian date)
  // First part of the date (jd(1,:)) is a large integer
  // Second part of the date (jd(2,:)) is the remaining decimal part. (in [0,1.5[)
  // NB: accuracy of calculations: ~1.e-15 days
  select type_in
   case "jd"  ; jd = [floor(date_in); date_in-floor(date_in)];
   case "cjd" ; jd = [floor(date_in) + 2433282; date_in-floor(date_in) + 0.5];  
   case "mjd" ; jd = [floor(date_in) + 2400000; date_in-floor(date_in) + 0.5]; 
   case "cal" ; jd = CL__dat_cal2jd(date_in); 
  end

  select type_out
   case "jd"  ; date_out = jd(1,:) + jd(2,:); 
   case "cjd" ; date_out = (jd(1,:) - 2433282) + (jd(2,:) - 0.5);  
   case "mjd" ; date_out = (jd(1,:) - 2400000) + (jd(2,:) - 0.5); 
   case "cal" ; date_out = CL__dat_jd2cal(jd); 
  end
  
end
  
endfunction
