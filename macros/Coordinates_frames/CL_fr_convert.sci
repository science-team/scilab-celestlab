//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos2, vel2, jacob] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, ut1_tref, tt_tref, xp, yp, dX, dY, use_interp)
// Position and velocity conversion from one frame to another frame.
//
// Calling Sequence
// [pos2, vel2, jacob] = CL_fr_convert(frame1, frame2, cjd, pos1, [vel1, ut1_tref, tt_tref, xp, yp, dX, dY, use_interp])
//
// Description
// <itemizedlist><listitem>
// <p>Converts position and (optionally) velocity vectors relative to "frame1" to position and (optionally) 
// velocity vectors relative to "frame2". </p>
// <p>The jacobian of the transformation is optionally computed.</p>
// <p></p>
// <p>The available frames are : </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="frame_convert.gif"/></imageobject></inlinemediaobject></p>
// <p></p>
// <p>In addition, 2 other frames (or names) are defined: ECI and ECF. </p>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- The date "cjd" is relative to the TREF time scale. </p>
// <p>- Which optional arguments need to be supplied depends on the frame transformation. See 
//  the frame transformation diagram for more details. </p>
// <p>- The frame names are case sensitive.</p>
// <p>- If only the position needs to be converted, vel1 can be omitted or set to []. (vel2 will then be set to []).</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: Modified Julian date from 1950.0 (TREF time scale) (1xN or 1x1)
// frame1: (string) Initial frame (1x1)
// frame2: (string) Final frame (1x1)
// pos1: Position vector in initial frame [m] (3xN or 3x1)
// vel1: (optional) Velocity vector in initial frame [m/s] (3xN or 3x1)
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
// xp, yp: (optional) Position of the pole in ITRS [rad] (1xN or 1x1)
// dX, dY: (optional) Corrections to CIP coordinates [rad] (1xN or 1x1)
// use_interp: (optional, boolean) %t to use interpolation when computing CIP coordinates. Default is %t. (1x1)
// pos2: Position vector in final frame [m] (3xN)
// vel2: (optional) Velocity vector in final frame [m/s] (3xN)
// jacob: (optional) Jacobian of the transformation: (pos1,vel1) to (pos2,vel2) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Conversion of position from Veis to EME2000 (All optional arguments to default)
// pos1 = [3500.e3; 2500.e3; 5800.e3];
// cjd_tref = 21010;
// pos_EME2000 = CL_fr_convert("Veis", "EME2000", cjd_tref, pos1);
//
// // Conversion of position and velocity from GCRS to TIRS
// pos_GCRS = [3500.e3; 2500.e3; 5800.e3];
// vel_GCRS = [1.e3; 3.e3; 7.e3] ;
// cjd_tref = [21010, 21011];
// [pos_TIRS,vel_TIRS,jacob] = CL_fr_convert("GCRS","TIRS",cjd_tref,pos_GCRS,vel_GCRS,ut1_tref=-0.2);

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_UT1_TREF")); %CL_UT1_TREF = %CL__PRIV.DATA.UT1_TREF; end
if (~exists("%CL_TT_TREF")); %CL_TT_TREF = %CL__PRIV.DATA.TT_TREF; end

// Code:
if (argn(2) < 4)
  CL__error("Not enough input arguments");
end

vel_present = %t;
if ~exists("ut1_tref", "local"); ut1_tref = %CL_UT1_TREF; end
if ~exists("tt_tref", "local"); tt_tref = %CL_TT_TREF; end
if ~exists("xp", "local"); xp = 0; end
if ~exists("yp", "local"); yp = 0; end
if ~exists("dX", "local"); dX = 0; end
if ~exists("dY", "local"); dY = 0; end
if ~exists("use_interp", "local"); use_interp = %t; end
if ~exists("vel1","local"); vel_present = %f; vel1 = []; end

// Check argument sizes, and resize if necessary:
if (vel_present)
  [cjd,pos1,vel1,ut1_tref,tt_tref,xp,yp,dX,dY] = CL__checkInputs(cjd,1, pos1,3, vel1,3, ut1_tref,1, tt_tref,1, xp,1, yp,1, dX,1, dY,1);
else
  [cjd,pos1,ut1_tref,tt_tref,xp,yp,dX,dY] = CL__checkInputs(cjd,1, pos1,3, ut1_tref,1, tt_tref,1, xp,1, yp,1, dX,1, dY,1);
end

// opt = flag specifying to the internal function CL__fr_convert what needs to be computed
if (argn(1) == 1)
  opt = "pos";
elseif (argn(1) == 2)
  opt = "pos_vel";
  if (~vel_present)
    CL__error("Input velocity missing"); 
  end
else
  opt = "pos_vel_jac";
end


// Arguments structure to be used in all sub-functions :
if (use_interp)
  model = "interp"; 
else
  model = "classic";
end

args = struct( ...
  "model", model, ...
  "precession_model", "2006", ...
  "nutation_model", "2000AR06", ...
  "ut1_tt", ut1_tref-tt_tref, ...
  "xp", xp, ...
  "yp", yp, ...
  "dx06", dX, ...
  "dy06", dY);

// Convert input date from MJD1950.0 (TREF time scale) 
// to a two part JD (TT time scale)
// ttjda + ttjdb = cjd + 2433282.5;
ttjda = floor(cjd) + 2433282;
ttjdb = cjd - floor(cjd) + 0.5 + tt_tref / 86400;
ttjd = [ttjda;ttjdb]; 

[M,omega,pos2,vel2,jacob] = CL__fr_convert(frame1, frame2, ttjd, pos1, vel1, opt, args);


endfunction
