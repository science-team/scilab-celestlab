//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [B, omega] = CL__fr_gcrs2eme2000(jd, args, comega)
// GCRS to EME2000 (or J2000) transformation matrix and angular velocity vector. (IAU2006)
//
// Calling Sequence
// [B, omega] = CL__fr_gcrs2eme2000([jd, args, comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>B</b> from GCRS to EME2000 : frame bias
// <p>By convention, multiplying <b>B</b> by coordinates relative to GCRS yields coordinates relative to EME2000.</p> 
// <p>Optionaly computes the angular velocity vector <b>omega</b> of EME2000 relative to GCRS, with coordinates relative to GCRS.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <listitem>
// <p>NOTES :</p>
// <p>- The date jd is optional, if it is omitted, a (3x3) matrix and (3x1) angular veloticy vector is returned
//      If it is given, the outputs have the same size as jd</p>
// <p>- args appears in the calling sequence but is not used.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: (optional) Two-part julian day. Only used for its dimension (2xN)
// args: Not used.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// B: transformation matrix (3x3xN)
// omega: (optional) angular velocity vector [rad/s] (3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010

// Declarations:

// Code:

if ~exists('jd','local'); jd = [0;0]; end;

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

N = size(jd,2);

B = CL__iers_bias2006();
B = matrix(repmat(B,1,N), 3,3,N);

// Angular velocity vector
omega = [];
if (comega)
  // Zero since the bias is constant
  omega = zeros(3,N);
end

endfunction
