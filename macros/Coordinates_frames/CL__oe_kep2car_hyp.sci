//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as kepculated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos,vel,jacob] = CL__oe_kep2car_hyp(kep, mu,cjac)
// Classical Keplerian to cartesian orbital elements, for elliptic orbits only
//
// Calling Sequence
// [pos,vel,jacob] = CL__oe_kep2car_hyp(kep [,mu,cjac])
//
// Description
// <itemizedlist><listitem>
// <p>Converts Classical Keplerian orbital elements to cartesian orbital elements, for elliptic orbits only.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details. </p>
// <p>Notes: </p>
// <p>- This function only handles elliptical orbits</p>
// <p>- No tests on the validity of inputs arguments are done in this function (see higher level function)</p>
// <p>- The function does not handle [] input arguments</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// kep: Classical Keplerian orbital elements [sma;e;inc;pom;raan;M] [m,rad] (6xN)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// cjac: (optional) Set to %f to avoid computing jacobian. (default value is %t)
// pos: position [X;Y;Z] [m] (3xN)
// vel: velocity [Vx;Vy;Vz] [m/s] (3xN)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_car2kep
// CL_oe_kep2kep
//
// Examples
// // // Example 1
// kep = [7000.e3; 1.5; 1e-4; 1; 2; %pi/2];
// [pos,vel] = CL_oe_kep2car(kep);
//
// // Example 2
// kep = [7000.e3; 1.5; 1e-4; 1; 2; %pi/2];
// [pos,vel,jacob1] = CL_oe_kep2car(kep);
// [kep2,jacob2] = CL_oe_car2kep(pos,vel);
// kep2 - kep // zero
// jacob2 * jacob1 // identity

// Declarations:
global %CL__PRIV; 
EPS_ORB = %CL__PRIV.DATA.epsOrb;
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end
if ~exists('cjac','local'); cjac = %t; end

// Handle [] cases
if (kep == [])
  pos = [];
  vel = [];
  jacob = [];
  return;
end

// Avoid computing jacobian if not requested 
N = size(kep,2);
jacob = zeros(6,6,N);
if (argn(1) < 3)
  cjac = %f;
end

a = kep(1,:);
e = kep(2,:);

// H = eccentric argument of latitude
H = CL_kp_M2E(e,kep(6,:));
chH = cosh(H);
shH = sinh(H);

// Other needed quantities
r = a .* (e.* chH - 1);
n = sqrt(mu ./ a.^3);
eta = sqrt(e.^2 - 1);

// Position and velocity in "natural" frame
X = a .* (e - chH);
Y = a .* eta .* shH;
       
VX = -n .* a.^2 ./ r .* shH;
VY = n .* a.^2 ./ r .* eta .* chH;

// Position and velocity in cartesian frame

// Matrix R: from natural to cartesian frame
// R = R3(-gom)*R1(-i)*R3(-pom)
// pos = R*X and vel = R*VX

// NB: This code is simpler but slower:
// R = CL_rot_angles2matrix([3,1,3],[-pom; -i;-gom]);
// pos = R * [X;Y;0];
// vel = R * [VX;VY;0];
// There are three reasons for this:
// - CL_rot_angles2matrix is not optimized (it computes cosinus and sinus several times)
// - Using an hypermatrix is always slightly slower than direct computation)
// - The last column of the matrix is not needed!

// Using direct formulas is the fastest way to compute matrix R:
cp = cos(kep(4,:));
sp = sin(kep(4,:));
cg = cos(kep(5,:));
sg = sin(kep(5,:));
ci = cos(kep(3,:));
si = sin(kep(3,:));
// P = first column of matrix R
// Q = second column of matrix R
P = [(cg.*cp-sg.*ci.*sp) ; (sg.*cp+cg.*ci.*sp); (si.*sp)];
Q = [(-cg.*sp-sg.*ci.*cp); (-sg.*sp+cg.*ci.*cp); (si.*cp)];

pos = CL_dMult(P,X)  + CL_dMult(Q,Y);
vel = CL_dMult(P,VX) + CL_dMult(Q,VY);

// Jacobian computation (dcar/dkep)
if (cjac)
  // jacob(i,j) = d(car_i)/d(kep_j)

  // d(x,y,z,vx,vy,vz)/da
  jacob(:,1,:) = CL_dMult(1 ./ a, [pos;-vel/2]) ;
        
  // Partial derivatives of X,Y,VX,VY with respect to e:
  dXde = a.^2 ./ r .* (e .* chH - 1 + shH.^2);
  dYde = a.^2 .* shH ./ (r .* eta)  .* (chH - e);
  dVXde = n .* a.^4 ./ r.^3 .* (e - 2 * chH + e .* chH.^2) .* shH;
  dVYde = -n .* a.^4 ./ (r.^3 .* eta) .* (e.^2 - 1 - e .* chH + 2 * chH.^2 - e .* chH.^3);
  
  // Apply the same matrix R as before: 
  // d(x,y,z,vx,vy,vz)/de
  jacob(:,2,:) = [CL_dMult(P,dXde)  + CL_dMult(Q,dYde) ; ...
                  CL_dMult(P,dVXde) + CL_dMult(Q,dVYde)] ;
                  
  // d(x,y,z,vx,vy,vz)/dM
  jacob(:,6,:) = [ CL_dMult(1 ./ n, vel) ; ...
                   CL_dMult(-n .* a.^3 ./ r.^3, pos) ] ;
                  
  // The partial derivatives with respect to i,pom,gom 
  // are computed by differianting the matrix R
  
  // Pi,Pp,Pg = first column of matrix R differentiated with respect to i,pom,gom
  // Qi,Qp,Qg = second column of matrix R differentiated with respect to i,pom,gom
  Pi = [(sg.*si.*sp); (-cg.*si.*sp); (ci.*sp)];
  Qi = [(sg.*si.*cp); (-cg.*si.*cp); (ci.*cp)];

  Pp = [(-cg.*sp-sg.*ci.*cp); (-sg.*sp+cg.*ci.*cp); (si.*cp)];
  Qp = [(-cg.*cp+sg.*ci.*sp); (-sg.*cp-cg.*ci.*sp); (-si.*sp)];
  
  Pg = [(-sg.*cp-cg.*ci.*sp); (cg.*cp-sg.*ci.*sp); zeros(X)];
  Qg = [(sg.*sp-cg.*ci.*cp); (-cg.*sp-sg.*ci.*cp); zeros(X)];
  
  // d(x,y,z,vx,vy,vz)/di
  jacob(:,3,:) = [CL_dMult(Pi,X)  + CL_dMult(Qi,Y) ; ...
                  CL_dMult(Pi,VX) + CL_dMult(Qi,VY)] ;
                  
  // d(x,y,z,vx,vy,vz)/dpom
  jacob(:,4,:) = [CL_dMult(Pp,X)  + CL_dMult(Qp,Y) ; ...
                  CL_dMult(Pp,VX) + CL_dMult(Qp,VY)] ;
                  
  // d(x,y,z,vx,vy,vz)/dgom
  jacob(:,5,:) = [CL_dMult(Pg,X)  + CL_dMult(Qg,Y) ; ...
                  CL_dMult(Pg,VX) + CL_dMult(Qg,VY)] ;
 
  
end

endfunction
