//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL__fr_tirs2pef(jd, args, comega)
// TIRS to TIRS frame transformation matrix and angular velocity
//
// Calling Sequence
// [M, omega] = CL__fr_tirs2pef(jd [,args, comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from TIRS to TIRS.
// By convention, multiplying <b>M</b> by coordinates relative to TIRS yields coordinates relative to TIRS.</p> 
// <p>Optionaly computes the angular velocity vector <b>omega</b> of TIRS relative to TIRS, with coordinates relative to TIRS.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <p>Note: args appears in the calling sequence but is not used.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// args: (optional) Not used.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// M: Transformation matrix (3x3xN)
// omega: (optional) Angular velocity vector [rad/s] (3xN or 3x1)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010
//
// See also
// CL__fr_pef2itrs 
// CL__fr_cirs2pef
//

// Declarations:


// Code:

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

N = size(jd,2);

sp = CL__iers_sp2000(jd, comega);
M = CL_rot_angles2matrix(3, sp);

// Angular velocity vector
omega = [];
if (comega)
  omega = zeros(3,N);
end

endfunction
