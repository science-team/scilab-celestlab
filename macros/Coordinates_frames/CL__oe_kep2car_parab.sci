//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos,vel,jacob] = CL__oe_kep2car_parab(kep,mu,cjac)
// Classical Keplerian to cartesian orbital elements, for parabolic orbits only
//
// Calling Sequence
// [pos,vel,jacob] = CL__oe_kep2car_parab(kep [,mu,cjac])
//
// Description
// <itemizedlist><listitem>
// <p>Converts parabolic Keplerian orbital elements to cartesian orbital elements, for elliptic orbits only.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details. </p>
// <p>Notes: </p>
// <p>- This function only handles parabolic orbits (e stictly equal to 1)</p>
// <p>- No tests on the validity of inputs arguments are done in this function (see higher level function)</p>
// <p>- The function does not handle [] input arguments</p>
// <p>- WARNING: The first row of kep is not the semi-major axis! It is the parameter p.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// kep: Parabolic Keplerian orbital elements [p;e;inc;pom;raan;M] [m,rad] (6xN)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// cjac: (optional) Set to %f to avoid computing jacobian. (default value is %t)
// pos: position [X;Y;Z] [m] (3xN)
// vel: velocity [Vx;Vy;Vz] [m/s] (3xN)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_car2kep
// CL_oe_kep2kep
//
// Examples
// // // Example 1
// kep = [7000.e3; 1; 1e-4; 1; 2; %pi/2];
// [pos,vel] = CL_oe_kep2car(kep);
//
// // Example 2
// kep = [7000.e3; 1e-4; 1e-4; 1; 2; %pi/2];
// [pos,vel,jacob1] = CL_oe_kep2car(kep);
// [kep2,jacob2] = CL_oe_car2kep(pos,vel);
// kep2 - kep // zero
// jacob2 * jacob1 // identity

// Declarations:

// Declarations:
global %CL__PRIV; 
EPS_ORB = %CL__PRIV.DATA.epsOrb;
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end
if ~exists('cjac','local'); cjac = %t; end

// Handle [] cases
if (kep == [])
  pos = [];
  vel = [];
  jacob = [];
  return;
end

// Avoid computing jacobian if not requested 
N = size(kep,2);
jacob = zeros(6,6,N);
if (argn(1) < 3)
  cjac = %f;
end

N = size(kep,2)

rp   = kep(1,:)
ai   = kep(3,:)
apom = kep(4,:)
agom = kep(5,:)
am   = kep(6,:)

//--Barker equation solution
anom_M3 = 3*am
delta = sqrt(1 + anom_M3.^2)
aux = anom_M3 + delta
bux = anom_M3 - delta
uns3 = 1/3
d = sign(aux).*abs(aux).^uns3 + sign(bux).*abs(bux).^uns3
//--resolution de l'equation de Barker :6 * M = 3 * d + d**3 avec d  =  tan(theta/2) et  theta = anomalie vraie

// 1- calcul vecteur position r = rp (ld0*u + mu0*q)
//    et vecteur vitesse rv = 2*sqrt(mu/rp)(nu0*p+q)/(1.+d**2)

//calcul de variables intermediaires

cai = cos(ai)
sai = sin(ai)
cgo = cos(agom)
sgo = sin(agom)
cpo = cos(apom)            //     cosinus et sinus des angles i,grand omega,petit omega
spo = sin(apom)
cgocpo = cgo .* cpo
sgocpo = sgo .* cpo
sgospo = sgo .* spo
cgospo = cgo .* spo

p = zeros(3,N)
p(1,:) = cgocpo - (cai .* sgospo)
p(2,:) = sgocpo + (cai .* cgospo)
p(3,:) = sai .* spo                // calcul des composantes des vecteurs P et Q en fonction des
q(1,:) = - cgospo - (cai .* sgocpo)// angles i,petit omega grand omega
q(2,:) = - sgospo + (cai .* cgocpo)
q(3,:) = sai .* cpo

rld0 = (1 - d.^2)/2        // calcul de ld0 = (1. - d**2)/2 et de
rmu0 = d                   // mu0 = d

smusrp = sqrt(mu./rp)                         // calcul de smusrp = sqrt(mu/rp) , de
rnu0 = - d                                   // nu0 = -d et
coef = 2.0 .* smusrp ./ (1+d.^2)// de coef = 2.*smusrp / (1.+d**2)

pos = (rp.*.ones(3,1)) .* ((rld0.*.ones(3,1) .* p) + ((rmu0.*.ones(3,1)) .* q))// calcul des composantes des vecteurs position et
vel = (coef.*.ones(3,1)) .* ( ((rnu0.*.ones(3,1)) .* p) + q )       // vitesse

// Jacobian computation (dcar/dkep)
if (cjac)
  // jacob(i,j) = d(car_i)/d(kep_j)

  //calcul de variables intermediaires
   xdpai = zeros(3,N)
   xdpai(1,:) = sai  .* sgospo
   xdpai(2,:) = -sai .* cgospo
   xdpai(3,:) = cai  .* spo        // derivees des composantes de P et Q par rapport a i
   xdqai = zeros(3,N)
   xdqai(1,:) = sai .* sgocpo
   xdqai(2,:) = -sai .* cgocpo
   xdqai(3,:) = cai  .* cpo

   xdpgo = zeros(3,N)
   xdpgo(1,:) = - sgocpo - (cai .* cgospo)
   xdpgo(2,:) = cgocpo - (cai .* sgospo)
   xdpgo(3,:) = 0             //  derivees des composantes de P et Q par rapport a grand omega
   xdqgo = zeros(3,N)
   xdqgo(1,:) = sgospo - (cai .* cgocpo)
   xdqgo(2,:) = - cgospo - (cai .* sgocpo)
   xdqgo(3,:) = 0

   dcoefp  = - smusrp./(rp.*(1+d.^2))   // calcul derivee de coeff par rapport au parametre p

   xdldd = -d         // calcul des derivees de ld0 et de
   xdmud = 1 // mu0 par rapport a d

   xddam = 2 ./ (1 + d.^2)   // calcul derivee de d par rapport a M
                                             //    dd/dam = 2. / (1 + d**2)

   xdnud  = -1                   // calcul de la derivee de nu0 par rapport a d
   dcoefd = -smusrp .* d .* xddam .* xddam   // calcul de la derivee de coef par rapport a d = -4. * smusrp * d /((1.+d*d)**2)

   
   jacob = zeros(6,6,N);
   // calcul des derivees partielles des vecteurs position et
   // vitesse par rapport au parametre p,e,i,pomega,gomega,m
   //        ---par rapport au parametre p
   jacob(1:3,1,1:N) = (rld0.*.ones(3,1) .* p) + (rmu0.*.ones(3,1) .* q)
   jacob(4:6,1,1:N) =  dcoefp.*.ones(3,1) .* ((rnu0.*.ones(3,1) .* p) + q)

   //        ---par rapport a e ( toujours nulle)
   jacob(1:3,2,1:N) = 0
   jacob(4:6,2,1:N) = 0

   //        ---par rapport a i
   jacob(1:3,3,1:N) = rp.*.ones(3,1) .* ((rld0.*.ones(3,1) .* xdpai) + (rmu0.*.ones(3,1) .* xdqai))
   jacob(4:6,3,1:N) = coef.*.ones(3,1)  .* ((rnu0.*.ones(3,1) .* xdpai) + xdqai)

   //        ---par rapport a pomega (q = derivee de p /pomega)
   jacob(1:3,4,1:N) = rp.*.ones(3,1) .* ((rld0.*.ones(3,1) .* q) - (rmu0.*.ones(3,1) .* p))
   jacob(4:6,4,1:N) = coef.*.ones(3,1) .* ((rnu0.*.ones(3,1) .* q) - p)

   //        ---par rapport a gomega
   jacob(1:3,5,1:N) = rp.*.ones(3,1) .* ((rld0.*.ones(3,1) .* xdpgo) + (rmu0.*.ones(3,1) .* xdqgo))
   jacob(4:6,5,1:N) = coef.*.ones(3,1) .* ((rnu0.*.ones(3,1) .* xdpgo) + xdqgo)

   //        ---par rapport a M ( = derivee/d * derivee de d/m)
   jacob(1:3,6,1:N) = rp.*.ones(3,1) .* ((xdldd.*.ones(3,1) .* p) + (xdmud.*.ones(3,N) .* q)) .* (xddam.*.ones(3,1))
   jacob(4:6,6,1:N) = ( ((dcoefd .* rnu0).*.ones(3,1) + ...
                       coef.*.ones(3,1) .* (xdnud.*.ones(3,N))) .* p + ...
                       (dcoefd.*.ones(3,1) .* q) ...
                     ) .* (xddam.*.ones(3,1))
 
  
end

endfunction
