//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cjd] = CL_dat_cal2cjd(varargin)
// Calendar date to modified (1950.0) Julian day
//
// Calling Sequence
// cjd = CL_dat_cal2cjd(year,month,day,hour,minute,second)
// cjd = CL_dat_cal2cjd(year,month,day)
// cjd = CL_dat_cal2cjd(cal) // cal = [year;month;day;hour;minute;second]
// cjd = CL_dat_cal2cjd(cal) // cal = [year;month;day]
//
// Description
// <itemizedlist><listitem>
// <p>Converts a calendar date (year,month,day,hour,minute,second) into a 
// modified (1950.0) Julian day (julian day from 1950.0).</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// year: (integer) Year. (1xN or 1x1)
// month: (integer in [1,12]) Month. (1xN or 1x1). 
// day: (integer in [1,31]) Day. (1xN or 1x1) 
// hour: (optional) Hours. Default is 0. (1xN or 1x1)
// minute: (optional) Minutes. Default is 0. (1xN or 1x1)
// second: (optional) Seconds. Default is 0. (1xN or 1x1)
// cal: Calendar date (3xN or 6xN)
// cjd: Modified (1950.0) Julian day (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_dat_cjd2cal
// CL_dat_convert
//
// Examples
// // Example 1
// cjd = CL_dat_cal2cjd(2000,1,1)
// cjd = CL_dat_cal2cjd(2010,2,20,12,3,45)
// cjd = CL_dat_cal2cjd([2000;1;1])
// cjd = CL_dat_cal2cjd([2010;2;20;12;3;45])
//
// // Example 2
// year=[2000,2010]
// month=[1,2]
// day=[1,20]
// hour=[0,12]
// minute=[0,3]
// second=[0,45]
// cjd = CL_dat_cal2cjd(year,month,day,hour,minute,second)
// cjd = CL_dat_cal2cjd([year;month;day;hour;minute;second])


// Declarations:


// Code:

// N = max number of columns for all input arguments
N = 0;
for k = 1 : lstsize(varargin)
  N = max(N,size(varargin(k),2));
end

// Handle [] case (If all input arguments are [])
if (N == 0)
  cjd = [];
  return; // <-- RETURN
end

// Number of output arguments
if (argn(1) <> 1)
  CL__error("Wrong number of output arguments");
end

// Build a calendar date "cal" of size 6xN or 3xN:
nb_arg_in = argn(2);

// CL_dat_cal2cjd(year,month,day,hour,minute,second)
if (nb_arg_in == 6)
  // Check and resize input arguments
  [varargin(1),varargin(2),varargin(3),varargin(4),varargin(5),varargin(6)] = CL__checkInputs( ...
   varargin(1),1, varargin(2),1, varargin(3),1, varargin(4),1, varargin(5),1, varargin(6),1);
  cal = [varargin(1);varargin(2);varargin(3);varargin(4);varargin(5);varargin(6)];
  
// CL_dat_cal2cjd(year,month,day) 
elseif (nb_arg_in == 3)
  // Check and resize input arguments
  [varargin(1),varargin(2),varargin(3)] = CL__checkInputs(varargin(1),1, varargin(2),1, varargin(3),1);
  cal = [varargin(1);varargin(2);varargin(3)];
  
// CL_dat_cal2cjd(cal) with cal = [year;month;day;hour;minute;second]
//                       or cal = [year;month;day]
elseif (nb_arg_in == 1)
  if (size(varargin(1),1) <> 3 & size(varargin(1),1) <> 6)
    CL__error("Invalid argument dimension(s)");
  end
  cal = varargin(1);
end
  
// Output  
cjd = CL_dat_convert("cal","cjd",cal)

endfunction
