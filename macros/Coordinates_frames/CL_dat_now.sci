//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [dat] = CL_dat_now(form, ts, tt_tref)
// Current date/time
//
// Calling Sequence
// dat = CL_dat_now([form, ts, tt_tref])
//
// Description
// <itemizedlist><listitem>
// <p>Returns the current date/time in various time scales or formats. </p>
// <p>Accuracy: about 1 second (no fractional seconds in UTC).</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// form: (string, optional) Format: "cjd", "cal". Default is "cjd" (1x1)
// ts: (string, optional) Time scale: "UTC", "TAI", "TT", "TREF". Default is "TREF" (1x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF (1x1)
// dat: Current date/time (1x1)
//
// Authors
// CNES - DCT/SB
//
// Examples
// CL_dat_cal2str(CL_dat_now("cal"))
// 

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_TT_TREF")); %CL_TT_TREF = %CL__PRIV.DATA.TT_TREF; end

// Code:
if ~exists("form", "local"); form = "cjd"; end
if ~exists("ts", "local"); ts = "TREF"; end
if ~exists("tt_tref", "local"); tt_tref = %CL_TT_TREF; end


// possible formats
FORM = ["cjd", "cal"]; 

// possible time scales (NB: order matters)
TS = ["UTC", "TAI", "TT", "TREF"]; 


if (find(form == FORM) == [])
  CL__error("Invalid format"); 
end

if (find(ts == TS) == []) 
  CL__error("Invalid time scale"); 
end

// getdate("s"): number of seconds since 1970
// (number of leapseconds included => it is actually  UTC)

cjd = getdate("s")/86400 + CL_dat_cal2cjd(1970,1,1); // UTC

// Select correct result
// + round off to nearest milli-second
// (even if accuracy = second!) 

// convert to desired scale: 
cjd = CL_dat_scaleConvert("UTC", ts, cjd, ut1_tref=0, tt_tref=tt_tref); 

msecs = (cjd - floor(cjd)) * 86400.e3;  
cjd = floor(cjd) + round(msecs) / 86400.e3; 

// converts to desired format
dat = cjd; 
if (form == "cal")
  dat = CL_dat_cjd2cal(cjd); 
  dat(6) = round(dat(6)*1000)/1000; 
end

endfunction

