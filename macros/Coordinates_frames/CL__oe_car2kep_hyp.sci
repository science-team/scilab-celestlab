//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as kepculated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [kep,jacob] = CL__oe_car2kep_hyp(pos,vel, mu,cjac)
// Cartesian to classical Keplerian adapted orbital elements
//
// Calling Sequence
// [kep,jacob] = CL__oe_car2kep_hyp(pos,vel [,mu,cjac])
//
// Description
// <itemizedlist><listitem>
// <p>Converts cartesian orbital elements to classical Keplerian orbital elements.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details.</p>
// <p>Notes: </p>
// <p>- This function only handles hyperbolic orbits</p>
// <p>- No tests on the validity of inputs arguments are done in this function (see higher level function)</p>
// <p>- The function does not handle [] input arguments</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// pos: position [X;Y;Z] [m] (3xN)
// vel: velocity [Vx;Vy;Vz] [m/s] (3xN)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// cjac: (optional) Set to %f to avoid computing jacobian. (default value is %t)
// kep: Classical Keplerian orbital elements [sma;e;inc;pom;raan;M] [m,rad] (6xN)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_kep2car
// CL_oe_car2kep
//
// Examples
// // Example 1
// pos = [7000.e3;1000.e3;-500.e3];
// vel = [1.e3;2.e3;20e3];
// kep = CL_oe_car2kep(pos,vel);
//
// // Example 2
// pos = [7000.e3;1000.e3;-500.e3];
// vel = [1.e3;2.e3;7e3];
// [kep,jacob1] = CL_oe_car2kep(pos,vel);
// [pos2,vel2,jacob2] = CL_oe_kep2car(kep); 
// pos2 - pos // zero
// vel2 - vel // zero
// jacob2 * jacob1 // identity


// Declarations:
global %CL__PRIV; 
EPS_ORB = %CL__PRIV.DATA.epsOrb;
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end

// Avoid computing jacobian if not requested 
N = size(pos,2);
jacob = zeros(6,6,N);
if (argn(1) < 2)
  cjac = %f;
end


// Notes on particular cases handled: 
// 1) If the orbit is nearly equatorial, gom is not defined
// (Equatorial treshold : sin(inc) < EPS_ORB.equa)
// In that case gom is set to 0 (arbitrary choice)

// Semi major axis (positive!)
r = CL_norm(pos);
V = CL_norm(vel);
W = CL_cross(pos,vel);
pos_vel = CL_dot(pos,vel)
a = -r ./ (2 - r .* V.^2 ./ mu);

// Inclination
// inc = CL_vectAngle(W, [0;0;1]);
inc = atan(sqrt(W(1,:).^2+W(2,:).^2), W(3,:));

// Raan
node = CL_cross([0;0;1],W);
gom = atan(node(2,:), node(1,:));
Ieq = find(sin(inc) < EPS_ORB.equa); // equatorial orbit
// If W is colinear with [0;0;1], we have gom = atan(0,0) = 0, so node should be equal to [1;0;0]
gom(Ieq) = 0;
node(1,Ieq) = 1;
node(2,Ieq) = 0;
node(3,Ieq) = 0;

// Eccentricity
eshH = pos_vel ./ sqrt(mu .* a);
echH = r .* V.^2 ./ mu - 1;
e = sqrt(1 + CL_norm(W).^2 ./ (mu .* a));
// e = sqrt(echH.^2 - eshH.^2);

// Mean anomaly
H = atanh(eshH ./ echH);
M = eshH - H;  // Kepler's equation

// Argument of perigee
cos_alpha_v = CL_dot(pos, node);
sin_alpha_v = CL_dot(pos, CL_cross(W,node)) ./ CL_norm(W);
alpha_v = atan(sin_alpha_v, cos_alpha_v);
v = CL_kp_E2v(e,H);

pom = alpha_v - v, 2*%pi;

// Reduce pom,gom to [0,2pi[
kep = [a;e;inc;CL_rMod([pom;gom],2*%pi);M];

// Jacobian computation (dkep/dcar)
if (cjac)
  // jacob(i,j) = d(kep_i)/d(car_j)
  
  jacob = zeros(6,6,N);
  
  shH = sinh(H);
  chH = cosh(H);
  n = sqrt(mu ./ a.^3);
  
  // P = first column of matrix R
  P1 = chH ./ r .* pos(1,:) - shH ./ (n .* a) .* vel(1,:);
  P2 = chH ./ r .* pos(2,:) - shH ./ (n .* a) .* vel(2,:);
  P3 = chH ./ r .* pos(3,:) - shH ./ (n .* a) .* vel(3,:);
  
  // Q = second column of matrix R
  Q1 = 1 ./ sqrt(e.^2-1) .* (shH ./ r .* pos(1,:) - (chH - e) ./ (n .* a) .* vel(1,:));
  Q2 = 1 ./ sqrt(e.^2-1) .* (shH ./ r .* pos(2,:) - (chH - e) ./ (n .* a) .* vel(2,:));
  Q3 = 1 ./ sqrt(e.^2-1) .* (shH ./ r .* pos(3,:) - (chH - e) ./ (n .* a) .* vel(3,:));
  
  // Expressions of cos and sin of inc,pom,gom as a function of P and Q:
  cosi = P1.*Q2 - P2.*Q1;
  sini = sqrt(P3.^2 + Q3.^2);
  // sini = sin(inclination), set to %nan if inc is below the treshold
  // (Used all the time because sini is only used in divisions.)
  sini(Ieq) = %nan;
  cosp = Q3 ./ sini;
  sinp = P3 ./ sini;
  cosg = P1 .* cosp - Q1 .* sinp; 
  sing = P2 .* cosp - Q2 .* sinp;
  
  for j = 1 : 3
    // da/d(x,y,z):
    dadr = -2 * a.^2 ./ r.^3 .* pos(j,:);
    // da/d(vx,vy,vz):
    dadv = -2 ./ (n.^2 .* a) .* vel(j,:);

    // d(echH)/d(x,y,z)
    dechHdr = V.^2 ./ (r .* mu) .* pos(j,:);
    // d(eshH)/d(x,y,z)
    deshHdr = vel(j,:) ./ (n .* a.^2) + pos_vel ./ (n .* a .* r.^3) .* pos(j,:);
    // d(echH)/d(vx,vy,vz)
    dechHdv = 2 .* r ./ mu .* vel(j,:);
    // d(eshH)/d(vx,vy,vz)
    deshHdv = pos(j,:) ./ (n .* a.^2) + pos_vel ./ (n .* a .* mu) .* vel(j,:);
  
    // de/d(x,y,z)
    dedr = -shH .* deshHdr + chH .* dechHdr;
    // de/d(vx,vy,vz)
    dedv = -shH .* deshHdv + chH .* dechHdv;
    
    // dE/d(x,y,z)
    dHdr = 1 ./ e .* (chH .* deshHdr - shH .* dechHdr);
    // dE/d(vx,vy,vz)
    dHdv = 1 ./ e .* (chH .* deshHdv - shH .* dechHdv);
    
    // dM/d(x,y,z)
    dMdr = deshHdr - dHdr;
    // dM/d(vx,vy,vz)
    dMdv = deshHdv - dHdv;
    
    // P = first column of matrix R. P = [P1;P2;P3];
    // dPk/d(x,y,z)
    dPkdr = zeros(3,N);
    for k = 1 : 3
      dPkdr(k,:) = (j==k) * chH ./ r - pos(k,:) .* chH ./ r.^3 .* pos(j,:) - vel(k,:) .* shH ./ (2*n.*a.^2) .* dadr + ...
            (shH ./ r .* pos(k,:) - chH ./ (n .* a) .* vel(k,:)) .* dHdr;
    end
    // dPk/d(vx,vy,vz)
    dPkdv = zeros(3,N);
    for k = 1 : 3
      dPkdv(k,:) = (j==k) * -shH ./ (n.*a) - vel(k,:) .* shH ./ (2*n.*a.^2) .* dadv + ...
            (shH ./ r .* pos(k,:) - chH ./ (n .* a) .* vel(k,:)) .* dHdv;
    end
    
    // Q = Second column of matrix R. Q = [Q1;Q2;Q3];
    // dQk/d(x,y,z)
    dQkdr = zeros(3,N);
    for k = 1 : 3
      dQkdr(k,:) = 1 ./ (sqrt(e.^2-1)) .* ((j==k) * shH ./ r - pos(k,:) .* shH ./ r.^3 .* pos(j,:) - vel(k,:) .* (chH-e) ./ (2*n.*a.^2) .* dadr + ...
            (chH ./ r .* pos(k,:) - shH ./ (n .* a) .* vel(k,:)) .* dHdr - (eshH ./ (r .* (e.^2-1)) .* pos(k,:) - (echH-1) ./ (n.*a.*(e.^2-1)) .* vel(k,:)) .* dedr);
    end
    // dQk/d(vx,vy,vz)
    dQkdv = zeros(3,N);
    for k = 1 : 3
      dQkdv(k,:) = 1 ./ (sqrt(e.^2-1)) .* ((j==k) * -(chH-e) ./ (n.*a) - vel(k,:) .* (chH-e) ./ (2*n.*a.^2) .* dadv + ...
            (chH ./ r .* pos(k,:) - shH ./ (n .* a) .* vel(k,:)) .* dHdv - (eshH ./ (r .* (e.^2-1)) .* pos(k,:) - (echH-1) ./ (n.*a.*(e.^2-1)) .* vel(k,:)) .* dedv);
    end
    
    // di/d(x,y,z)
    didr = cosi ./ sini .* (dPkdr(3,:) .* P3 + dQkdr(3,:) .* Q3) - ...
           (P1 .* dQkdr(2,:) + Q2 .* dPkdr(1,:) - P2 .* dQkdr(1,:) - Q1 .* dPkdr(2,:)) .* sini;
    // di/d(vx,vy,vz)
    didv = cosi ./ sini .* (dPkdv(3,:) .* P3 + dQkdv(3,:) .* Q3) - ...
           (P1 .* dQkdv(2,:) + Q2 .* dPkdv(1,:) - P2 .* dQkdv(1,:) - Q1 .* dPkdv(2,:)) .* sini;
           
    // dpom/d(x,y,z)
    dpomdr = 1 ./ sini .* (dPkdr(3,:) .* cosp - dQkdr(3,:) .* sinp);
    // dpom/d(vx,vy,vz)
    dpomdv = 1 ./ sini .* (dPkdv(3,:) .* cosp - dQkdv(3,:) .* sinp);
    
    // dgom/d(x,y,z)
    dgomdr = (dPkdr(2,:) .* cosp - dQkdr(2,:) .* sinp) .* cosg - ...
           (dPkdr(1,:) .* cosp - dQkdr(1,:) .* sinp) .* sing - dpomdr .* cosi;
    // dgom/d(vx,vy,vz)
    dgomdv = (dPkdv(2,:) .* cosp - dQkdv(2,:) .* sinp) .* cosg - ...
           (dPkdv(1,:) .* cosp - dQkdv(1,:) .* sinp) .* sing - dpomdv .* cosi;
           
    // Assign results to the output jacobian:
    jacob(1,j,:) = dadr;
    jacob(2,j,:) = dedr;
    jacob(3,j,:) = didr;
    jacob(4,j,:) = dpomdr;
    jacob(5,j,:) = dgomdr;
    jacob(6,j,:) = dMdr;
    
    jacob(1,j+3,:) = dadv;
    jacob(2,j+3,:) = dedv;
    jacob(3,j+3,:) = didv;
    jacob(4,j+3,:) = dpomdv;
    jacob(5,j+3,:) = dgomdv;
    jacob(6,j+3,:) = dMdv;
    
  end
    
end

endfunction
