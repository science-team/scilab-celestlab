//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as kepculated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [kep,jacob] = CL__oe_car2kep_parab(pos,vel, mu,cjac)
// Cartesian to classical Keplerian adapted orbital elements
//
// Calling Sequence
// [kep,jacob] = CL__oe_car2kep_parab(pos,vel [,mu,cjac])
//
// Description
// <itemizedlist><listitem>
// <p>Converts cartesian orbital elements to classical Keplerian orbital elements.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details.</p>
// <p>Notes: </p>
// <p>- This function only handles hyperbolic orbits</p>
// <p>- No tests on the validity of inputs arguments are done in this function (see higher level function)</p>
// <p>- The function does not handle [] input arguments</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// pos: position [X;Y;Z] [m] (3xN)
// vel: velocity [Vx;Vy;Vz] [m/s] (3xN)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// cjac: (optional) Set to %f to avoid computing jacobian. (default value is %t)
// kep: Classical Keplerian orbital elements [sma;e;inc;pom;raan;M] [m,rad] (6xN)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_kep2car
// CL_oe_car2kep
//
// Examples
// // Example 1
// pos = [7000.e3;1000.e3;-500.e3];
// vel = [1.e3;2.e3;20e3];
// kep = CL_oe_car2kep(pos,vel);
//
// // Example 2
// pos = [7000.e3;1000.e3;-500.e3];
// vel = [1.e3;2.e3;7e3];
// [kep,jacob1] = CL_oe_car2kep(pos,vel);
// [pos2,vel2,jacob2] = CL_oe_kep2car(kep); 
// pos2 - pos // zero
// vel2 - vel // zero
// jacob2 * jacob1 // identity


// Declarations:
global %CL__PRIV; 
EPS_ORB = %CL__PRIV.DATA.epsOrb;
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local'); mu = %CL_mu; end

// Avoid computing jacobian if not requested 
N = size(pos,2);
jacob = zeros(6,6,N);
if (argn(1) < 2)
  cjac = %f;
end


// Notes on particular cases handled: 
// 1) If the orbit is nearly equatorial, gom is not defined
// (Equatorial treshold : sin(inc) < EPS_ORB.equa)
// In that case gom is set to 0 (arbitrary choice)

N = size(pos,2)

kep = zeros(6,N)

r = CL_norm(pos); //norme vecteur position
kinetic_moment = CL_cross(pos,vel);
pos_vel = CL_dot(pos,vel);

rxv1 = kinetic_moment(1,:) //
rxv2 = kinetic_moment(2,:) //  produit vectoriel (position x vitesse)
rxv3 = kinetic_moment(3,:) //

rxv  = CL_norm(kinetic_moment); //norme du vecteur produit vectoriel (position x vitesse)

nrxv1 = rxv1 ./ rxv
nrxv2 = rxv2 ./ rxv
nrxv3 = rxv3 ./ rxv

//parameter p=a de la parabole =  = 2 * distance au perigee
parameter = rxv.^2 ./ mu
kep(1,:) = parameter;

//eccentricite
kep(2,:) = e

//inclinaison sur le plan equatorial kep%i
rxvsxi = sqrt(rxv1.^2+rxv2.^2)  //avec rxvsxi non nul si inclinaison non nulle
sxi = rxvsxi./rxv                    //sinus(inclinaison)
cxi = nrxv3                         //cosinus(inclinaison)
if find(sxi < EPS_ORB.equa)~=[] //orbite equatoriale
  CL__error('equatorial orbit');
end
kep(3,:) = CL__sc2angle(cxi,sxi)  //calcul de l'inclinaison (resultat dans [0,pi] car sin(inclinaison) > 0)

//longitude du noeud ascendant
rsicgo = - rxv2
rsisgo =   rxv1
kep(5,:) = CL__sc2angle(rsicgo,rsisgo)

//argument du perigee
//perigee + anomalie vraie
aspov = pos(3,:)
acpov = nrxv1.*pos(2,:)-nrxv2.*pos(1,:)
pov = CL__sc2angle(acpov,aspov)
//anomalie vraie
rcav  = parameter - r
rsav  = pos_vel.*sqrt(parameter./mu)
av = CL__sc2angle(rcav, rsav) //anomalie vraie
po = pov - av
kep(4,:) = pmodulo(po,2*%pi)  //d'ou l'argument du perigee

//anomalie moyenne
d = pos_vel./rxv  //equation de Barker : 6 * xm = 3*d + d**3
                      //d = tan(anomalie vraie /2)

kep(6,:) = d.*(0.5 + d.*d./6.0)

// Jacobian computation (dkep/dcar)
if (cjac)
  // jacob(i,j) = d(kep_i)/d(car_j)
  
  //calcul derivees
  jacob = hypermat([6 6 N])
  drdx = zeros(6,N)
  drxv1 = zeros(6,N)
  drxv2 = zeros(6,N)
  drxv3 = zeros(6,N)
  dpos_vel = zeros(6,N)
  //calcul derivees intermediaires
  //derivees normes vecteurs position, vitesse, et r x v
   drdx(1:3,:) = pos ./ (r.*.ones(3,1))
   drdx(4:6,:) = 0

   drxv1(1,:) = 0
   drxv1(2,:) =  vel(3,:)
   drxv1(3,:) = -vel(2,:)
   drxv1(4,:) = 0
   drxv1(5,:) = -pos(3,:)
   drxv1(6,:) =  pos(2,:)

   drxv2(1,:) = -vel(3,:)
   drxv2(2,:) = 0
   drxv2(3,:) =  vel(1,:)
   drxv2(4,:) =  pos(3,:)
   drxv2(5,:) = 0
   drxv2(6,:) = -pos(1,:)

   drxv3(1,:) =  vel(2,:)
   drxv3(2,:) = -vel(1,:)
   drxv3(3,:) = 0
   drxv3(4,:) = -pos(2,:)
   drxv3(5,:) =  pos(1,:)
   drxv3(6,:) = 0

   drxv = drxv1.*(nrxv1.*.ones(6,1)) + drxv2.*(nrxv2.*.ones(6,1)) + drxv3.*(nrxv3.*.ones(6,1))

   //derivees produit scalaire
   dpos_vel(1:3,:) = vel
   dpos_vel(4:6,:) = pos

   rxvsmu = 2*rxv./mu
   dpdx = rxvsmu.*.ones(6,1) .* drxv   //calcul derivees du parameter p
   dedx = zeros(6,N)                //calcul derivees de l'eccentricite e
   dxidx = ( cxi.*.ones(6,1) .* drxv - drxv3 ) ./ (rxvsxi.*.ones(6,1))   //calcul derivees inclinaison
   rxvsi2 = rxvsxi.^2
   dgodx = ( rxv1.*.ones(6,1) .* drxv2 - rxv2.*.ones(6,1) .* drxv1 ) ./ (rxvsi2.*.ones(6,1))   //calcul derivees noeud ascendant

   //calcul derivees perigee
   //po = pov - av --> dpodx(:)=dpovdx(:)-davdx(:)

   //r*sxi * spov = aspov = pos(3)
   //r*sxi * cpov = acpov

   rsxi2 = (r.*sxi).^2
   dacpov  = ( drxv1.*(pos(2,:).*.ones(6,1)) - drxv2.*(pos(1,:).*.ones(6,1)) - acpov.*.ones(6,1) .* drxv ) ./ (rxv.*.ones(6,1))
   dacpov(1,:)  = dacpov(1,:) - nrxv2
   dacpov(2,:)  = dacpov(2,:) + nrxv1

   dpovdx  = -pos(3,:).*.ones(6,1) .* dacpov ./ (rsxi2.*.ones(6,1))   //avec rsxi2 non nul si inclinaison non nulle et r non nul
   dpovdx(3,:) = dpovdx(3,:) + acpov./rsxi2

   //r*cav = rcav = p - r
   //r*sav = rsav = pos_vel*sqrt(p/mu) = pos_vel*rxv/mu

   r2 = r.^2
   drsav = ( dpos_vel.*(rxv.*.ones(6,1)) + pos_vel.*.ones(6,1) .*drxv ) ./mu
   davdx = ( -rsav.*.ones(6,1) .* (dpdx-drdx) + rcav.*.ones(6,1) .*drsav ) ./(r2.*.ones(6,1))
   dpodx = dpovdx - davdx

   //calcul derivees anomalie moyenne
   //derivees de d = pos_vel/rxv
   dddx = (dpos_vel - d.*.ones(6,1) .*drxv) ./ (rxv.*.ones(6,1))

   cof = (1+ d.^2)*0.5
   dxmdx= cof.*.ones(6,1) .* dddx

   //affectation de la jacobienne de la transformation
  jacob = hypermat([6,6,N],[dpdx; dedx; dxidx; dpodx; dgodx; dxmdx]);
  jacob = jacob';
  if (N==1) then jacob=jacob(:,:,1); end
    
end

endfunction
