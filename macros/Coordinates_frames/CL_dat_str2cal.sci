//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cal] = CL_dat_str2cal(str)
// String to calendar date 
//
// Calling Sequence
// cal = CL_dat_str2cal(str)
//
// Description
// <itemizedlist><listitem>
// <p>Decodes a string into a calendar date.</p>
// <p>The input format must have the form: "year / month / day  [hours : minutes : seconds]" </p>
// <p>The calendar date is a column vector [year;month;day;hours,minutes;seconds]. </p>
// <p><b>Note: </b></p>
// <p>%nan is returned if the input string format is not correct. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// str: Formatted calendar date (1xN)
// cal: Calendar date (6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// cal = CL_dat_str2cal("2012/1/1 12:0:0.1")
// 

// Declarations:

// read next value until separator "separ"; 
// strsuiv = string following separ
// separ = " " if even if end of string found. 
// x = %nan if anomaly (e.g. separator found <> separ)
function [x, strsuiv] = nextval(str, separ)
  str = str + " "; 
  [x, strsuiv] = strtod(str);

  // no character read
  I = find(length(strsuiv) == length(str)); 
  x(I) = %nan; 

  if (separ == " ")
    sep = part(strsuiv, 1);
    I = find(sep <> " " & sep <> ""); 
    x(I) = %nan; 
    strsuiv = stripblanks(strsuiv,%t);
  end

  if (separ <> " ")
    strsuiv = stripblanks(strsuiv,%t); 
  sep = part(strsuiv, 1);
    I = find(sep <> separ); 
    x(I) = %nan; 
    strsuiv = stripblanks(part(strsuiv, 2:max(length(strsuiv))),%t);
  end
endfunction



// Code:

str = stripblanks(str,%t);

if (size(str,1) <> 1)
  CL__error("Invalid size for str"); 
end
  
if (str == [] | str == ""); 
  cal = []; 
  return;
end
 
   
N = size(str,2); 
cal = zeros(6,N); 

// day part (year/month/day)

[cal(1,:), str] = nextval(str, "/")
[cal(2,:), str] = nextval(str, "/")
[cal(3,:), str] = nextval(str, " ")

// hour part (hours:minutes:seconds)

I = find(length(str) <> 0); 
if (I <> [])
  [cal(4,I), str(I)] = nextval(str(I), ":")
  [cal(5,I), str(I)] = nextval(str(I), ":")
  [cal(6,I), str(I)] = nextval(str(I), " ")
end

I = find(length(str) <> 0); 
cal(:,I) = %nan; 
  
  
// A few additional checks ...
  
[i,j] = find(cal < 0 | isnan(cal)); 
cal(:,j) = %nan; 

[i,j] = find(cal <> round(cal)); 
I = find(i <> 6); 
cal(:,j(I)) = %nan; 

// check values:
// month in [1, 12], day in [1, 31], hours in [0, 24], mn and sec in [0,60]
// ([0,24] and [0,60] to ensure reversivility) 
[i,j] = find(cal(2,:) < 1  | .. 
             cal(2,:) > 12 | ..
             cal(3,:) < 1  | ..
       cal(3,:) > 31 | ..
             cal(4,:) > 24 | ..
             cal(5,:) > 60 | ..
             cal(6,:) > 60 ); 
cal(:,j) = %nan; 

  
endfunction


