//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cir,jacob] = CL_oe_kep2cir(kep)
// Keplerian to circular adapted orbital elements
//
// Calling Sequence
// [cir,jacob] = CL_oe_kep2cir(kep)
//
// Description
// <itemizedlist><listitem>
// <p>Converts classical Keplerian orbital elements to orbital elements adapted to near-circular orbits.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// kep: Keplerian orbital elements [sma;ecc;inc;pom;raan;anm] [m,rad] (6xN)
// cir: Orbital elements adapted to near-circular orbits [sma;ex;ey;inc;raan;alpha] [m,rad] (6xN)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_oe_cir2kep
// CL_oe_kep2car
//
// Examples
// // Example 1
// kep = [7000.e3;0.01;1.8;0.1;0.2;0.3];
// cir = CL_oe_kep2cir(kep);
//
// // Example 2
// kep = [7000.e3;0.01;1.8;0.1;0.2;0.3];
// [cir,jacob1] = CL_oe_kep2cir(kep);
// [kep2,jacob2] = CL_oe_cir2kep(cir); 
// kep2 - kep // zero
// jacob2 * jacob1 // identity


// Declarations:
global %CL__PRIV; 
EPS_ORB = %CL__PRIV.DATA.epsOrb;

// Code:
// Handle [] cases
if (kep == [])
  cir = [];
  jacob = [];
  return;
end

// Check validity of input (must be an elliptical orbit too!)
[isvalid,type_orbit] = CL__oe_isValid("kep",kep);
if (~isvalid); CL__error("Invalid orbital elements"); end;
if (find(type_orbit <> 1) <> []); CL__error("Invalid orbital elements (parabolic or hyperbolic orbit)"); end;

// Conversion formulas:
// a = a
// ex = e*cos(pom)
// ey = e*sin(pom)
// i = i
// gom = gom
// L = pom+M

cir = zeros(kep);

cir(1,:) = kep(1,:);
cir(2,:) = kep(2,:) .* cos(kep(4,:));
cir(3,:) = kep(2,:) .* sin(kep(4,:));
cir(4,:) = kep(3,:);
cir(5,:) = kep(5,:);
cir(6,:) = kep(4,:) + kep(6,:);


// Jacobian computation (dcir/dkep)
if (argn(1) == 2)
  // jacob(i,j) = d(cir_i)/d(kep_j)
  //
  // Formulas used:
  // da/da = 1
  // d(ex)/d(e) = cos(w)
  // d(ey)/d(e) = sin(w)
  // d(ex)/d(w) = -ey
  // d(ey)/d(w) = ex
  // di/di = 1
  // dgom/dgom = 1
  // d(pso)/d(w) = 1
  // d(pso)/d(M) = 1
  
  N = size(kep,2);
  jacob = zeros(3,3,N);

  jacob(1,1,:) = 1; // da
  
  jacob(2,2,:) = cos(kep(4,:)); // dex/de
  jacob(2,4,:) = - cir(3,:); // dex/w
  
  jacob(3,2,:) = sin(kep(4,:)); // dey/de
  jacob(3,4,:) = cir(2,:); // dey/dw
  
  jacob(4,3,:) = 1 ; // di/di
  
  jacob(5,5,:) = 1; // dgom/dgom
  
  jacob(6,4,:) = 1; // dpso/dw
  jacob(6,6,:) = 1; // dpso/dM
  
end

endfunction
