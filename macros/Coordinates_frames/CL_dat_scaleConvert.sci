//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cjd] = CL_dat_scaleConvert(ts_in, ts_out, cjd ,ut1_tref, tt_tref)
// Convert time scale 
//
// Calling Sequence
// cjd_out = CL_dat_scaleConvert(ts_in, ts_out, cjd_in [,ut1_tref, tt_tref])
//
// Description
// <itemizedlist><listitem>
// <p>Converts a date from one time scale to another.</p>
// <p>The available time scales are "TREF", "TT", "TAI", "UT1", "UTC", "TDB", "TCB".</p>
// <p></p>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// <p></p>
// </listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- The input and output dates must be of the same type ("cjd").</p>
// <p>- If both ts_in and ts_out are different from UT1 and TREF, the value of ut1_tref has no influence on the results. </p>
// <p>- If both ts_in and ts_out are different from TT and TREF, the value of tt_tref has no influence on the results. </p>
// <p>- TDB is supposed identical to TT. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// type_in: (string) Input time scale: "TREF", "TT", "TAI", "UT1", "UTC", "TDB", "TCB" (1x1)
// type_out: (string) Output time scale: "TREF", "TT", "TAI", "UT1", "UTC", "TDB", "TCB" (1x1)
// cjd_in: Input date (modified julian day from 1950.0). (1xN)
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1x1 or 1xN)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF (1x1 or 1xN)
// cjd_out: Output date (modified julian day from 1950.0).(1xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1: UTC to TAI
// cjd_utc = CL_dat_cal2cjd(2013,1,1);
// cjd_tai = CL_dat_scaleConvert("UTC","TAI",cjd_utc); 
// cjd_tai = CL_dat_scaleConvert("UTC","TAI",cjd_utc,0,0); // => same
//
// // Example 2: Offsets between TREF and various time scales (sec)
// cjd_tref = CL_dat_cal2cjd(2000,1,1) : 10 : CL_dat_now("cjd"); 
// utc_tref = (CL_dat_scaleConvert("TREF","UTC",cjd_tref) - cjd_tref) *  86400;
// tai_tref = (CL_dat_scaleConvert("TREF","TAI",cjd_tref) - cjd_tref) *  86400;
// tt_tref = (CL_dat_scaleConvert("TREF","TT",cjd_tref) - cjd_tref) *  86400;
// tcb_tref = (CL_dat_scaleConvert("TREF","TCB",cjd_tref) - cjd_tref) *  86400;
// scf(); 
// plot(cjd_tref, [utc_tref; tai_tref; tt_tref; tcb_tref]); 
// h = CL_g_select(gca(), "Polyline"); h.thickness = 2; 
// CL_g_legend(gca(), ["UTC", "TAI", "TT", "TCB"]); 
// CL_g_stdaxes(); 


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_TT_TREF")); %CL_TT_TREF = %CL__PRIV.DATA.TT_TREF; end
if (~exists("%CL_UT1_TREF")); %CL_UT1_TREF = %CL__PRIV.DATA.UT1_TREF; end

// Code:
if (~exists("tt_tref", "local")); tt_tref = %CL_TT_TREF; end
if (~exists("ut1_tref", "local")); ut1_tref = %CL_UT1_TREF; end
tt_tai = CL_dataGet("TT_TAI"); 

if (typeof(ts_in) <> "string" | typeof(ts_out) <> "string")
   CL__error("String expected for ts_in and ts_out"); 
end

if (size(ts_in,"*") <> 1 | size(ts_out,"*") <> 1) 
  CL__error("Wrong dimension(s)"); 
end
  
// check sizes / resize  
[cjd, ut1_tref, tt_tref] = CL__checkInputs(cjd, 1, ut1_tref, 1, tt_tref, 1); 


// possible time scales
TS = ["TREF", "TT", "TAI", "UT1", "UTC", "TDB", "TCB"]; 

if (find(ts_in == TS) == [] | find(ts_out == TS) == [])
  CL__error("Unrecognized time scales");
end


// Handle [] case
if (cjd == [])
  return; // <-- RETURN
end

// no change
if ("ts_in" == "ts_out")
  return; 
end


// Note: 
// converts ts_in -> tai and tai -> ts_out

// step 1: ts_in -> tai
// dt = cjd_tai - cjd_in (seconds)

if (ts_in == "TREF") 
  // dt = tai-tref = tai-tt + tt-tref
  dt = -tt_tai + tt_tref;
elseif (ts_in == "TT" | ts_in == "TDB") 
  // dt = tai-tt
  dt = -tt_tai;
elseif (ts_in == "TAI") 
  // dt = tai-tai
  dt = 0;
elseif (ts_in == "UT1") 
  // dt = tai-ut1 = tai-tt + tt-tref + tref-ut1 
  dt = -tt_tai + tt_tref - ut1_tref;
elseif (ts_in == "TCB") 
  // tai_tcb = tai_tt + tt_tcb
  tdb_tcb = (CL_dat_tcb2tdb(cjd) - cjd)*86400;  
  dt = -tt_tai + tdb_tcb; 
else // UTC
  // dt = tai - utc 
  dt = (CL_dat_utc2tai(cjd) -cjd) * 86400;
end


// step 2: tai -> ts_out
// dt2 = cjd_out - cjd_tai (seconds)
// => cjd_out = cjd_out-cjd_tai + cjd_tai-cjd_in = dt2 + dt

if (ts_out == "TREF") 
  // dt2 = tref-tai = tref-tt + tt-tai
  dt = dt - tt_tref + tt_tai;
elseif (ts_out == "TT" | ts_out == "TDB") 
  // dt2 = tt-tai 
  dt = dt + tt_tai;
elseif (ts_out == "TAI") 
  // dt2 = tai-tai
  dt = dt;
elseif (ts_out == "UT1") 
  // dt2 = ut1-tai = ut1-tref + tref-tt + tt-tai 
  dt = dt + ut1_tref - tt_tref + tt_tai;
elseif (ts_out == "TCB") 
  // tcb_tai = tcb_tdb + tt_tai
  cjd_tt = cjd + (dt + tt_tai)/86400; 
  tcb_tdb = (CL_dat_tdb2tcb(cjd_tt) - cjd_tt) * 86400;  
  dt = dt + tcb_tdb + tt_tai; 
else // UTC
  // dt2 = utc-tai
  cjd_tai = cjd + dt / 86400; 
  dt = dt + (CL_dat_tai2utc(cjd_tai) - cjd_tai) * 86400;
end


// date_out 
cjd = cjd + dt/86400; 

  
endfunction
