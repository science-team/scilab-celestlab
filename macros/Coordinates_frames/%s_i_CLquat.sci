//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q(I) = [] 
// I = integers (row vector)
// I can be empty in theory (but it does not work)

function [q] = %s_i_CLquat(I,a,q)

// q(:) <-> q(1:$)
if (isequal(I,:))
  I = 1:$;
end

// NB : I is of type "size implicit" if it contains the $ operator (ex : 1:$)
if (typeof(I) <> "size implicit")
  sI = size(I);
else
  sI = size(q(I));
end

if (sI(1) == 0 | size(q) == 0)
  return; // no change

elseif (a == [])
  r = q.r;
  i = q.i;
  r(I) = [];
  i(:,I) = [];
  q = CL__defQuat(r, i);
  
else
  CL__error("Invalid insertion");
end

endfunction
