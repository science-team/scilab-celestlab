//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as kepculated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [oe2,jacob] = CL_oe_convert(type_oe1,type_oe2,oe1, mu)
// Conversion of orbital elements
//
// Calling Sequence
// [oe2,jacob] = CL_oe_convert(type_oe1,type_oe2,oe1 [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Converts orbital elements from a given type to orbital elements of another type.</p>
// <p>Available types of orbital elements are: "kep", "cir", "cireq", "equin" and "pv".</p>
// <p>Orbital elements of type "pv" are position and velocity in a 6xN vector ([pos;vel])</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details on orbital elements.</p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- mu is only needed when converting to or from "pv" orbital elements.</p>
// <p>- Only the conversions available in functions CL_oe_xxx are available. 
// (i.e conversion to and from keplerian orbital element or cartesian orbital elements)</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// type_oe1: (string) Type of input orbital elements ("kep", "cir", "cireq", "equin" or "pv") (1x1)
// type_oe2: (string) Type of output orbital elements ("kep", "cir", "cireq", "equin" or "pv") (1x1)
// oe1: Input orbital elements (6xN)
// oe2: Output orbital elements (6xN)
// mu: (optional) Gravitational constant [m^3/s^2]. (default value is %CL_mu)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1
// pos = [7000.e3;1000.e3;-500.e3];
// vel = [1.e3;2.e3;7e3];
// kep = CL_oe_convert("pv","kep",[pos;vel]);
//
// // Example 2
// pos = [7000.e3;1000.e3;-500.e3];
// vel = [1.e3;2.e3;7e3];
// [kep,jacob1] = CL_oe_convert("pv","kep",[pos;vel]);
// [pv2,jacob2] = CL_oe_convert("kep","pv",kep); 
// pv2 - [pos;vel] // zero
// jacob2 * jacob1 // identity

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if (argn(2) < 3 | argn(2) > 4)
  CL__error("Invalid number of input argument");
end

if ~exists('mu','local'); mu = %CL_mu; end

type_oe = ["kep", "cir", "cireq", "equin", "pv"];

I1 = find(type_oe1 == type_oe);
I2 = find(type_oe2 == type_oe);
if (I1 == [] | I2 == [])
  CL__error("Invalid type of orbital elements");
end

if (size(oe1,1) <> 6)
  CL__error("Invalid orbital elements (6 rows expected)");
end

// Replace "pv" by "car"
if (type_oe1 == "pv"); type_oe1 = "car"; end;
if (type_oe2 == "pv"); type_oe2 = "car"; end;

if (type_oe1 == type_oe2)
  oe2 = oe1;
  N = size(oe1, 2);
  jacob = matrix(repmat(eye(6,6),1,N),6,6,N); // identity hypermatrix
  return; // <-- RETURN !
end

// Only conversion to and from "kep" or "car" are available!
if (I1 <> 1 & I1 <> 5 & I2 <> 1 & I2 <> 5)
  CL__error("Conversion not handled");
end

// Function name
fname = "CL_oe_" + type_oe1 + "2" + type_oe2;

// Check existence of function (should always exist! see tests above)
if (~exists("fname"))
  CL__error("Function "+fname+ " does not exist!");
end

// Function (guaranteed to exist at this point)
f = evstr(fname);

// Cases without jacobian
if (argn(1) == 1)
  // Special case: input = cartesian
  if (type_oe1 == "car")
    [oe2] = f(oe1(1:3,:), oe1(4:6,:),mu);
  // Special case: output = cartesian
  elseif (type_oe2 == "car")
    oe2 = zeros(oe1);
    [oe2(1:3,:), oe2(4:6,:)] = f(oe1,mu);
  else
    // Standard case
    [oe2] = f(oe1);
  end
end

// Cases with jacobian
if (argn(1) == 2)
  // Special case: input = cartesian
  if (type_oe1 == "car")
    [oe2, jacob] = f(oe1(1:3,:), oe1(4:6,:),mu);
  // Special case: output = cartesian
  elseif (type_oe2 == "car")
    oe2 = zeros(oe1);
    [oe2(1:3,:), oe2(4:6,:), jacob] = f(oe1,mu);
  else
    // Standard case
    [oe2, jacob] = f(oe1);
  end
end
endfunction
