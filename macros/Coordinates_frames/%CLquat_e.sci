//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2 = q1(I)
// I = integers (row vector) - can be []

function [q2] = %CLquat_e(I,q1)

// q(:) <-> q(1:$)
if (isequal(I,:))
  I = 1:$;
end

// NB : I is of type "size implicit" if it contains the $ operator (ex : 1:$)
if (typeof(I) <> "size implicit")
  sI = size(I);
  N = size(q1);

  if (I <> [] & (sI(1) > 1 | max(I) >= N+0.5 | min(I) <= 0.5))
    CL__error("Invalid index"); 
  end
end

q2 = CL__defQuat(q1.r(1,I), q1.i(1:3,I));

endfunction
