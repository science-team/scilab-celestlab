//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q3 = [q1,q2] 
// (concatenation)
// OK if q1 or q2 is empty

function [q3] = %CLquat_c_CLquat(q1,q2)

q3 = CL__defQuat([q1.r, q2.r], [q1.i, q2.i]);

endfunction

