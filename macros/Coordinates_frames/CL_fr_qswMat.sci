//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M] = CL_fr_qswMat (pos_car,vel_car)
// Inertial frame to "qsw" local orbital frame transformation matrix
//
// Calling Sequence
// M = CL_fr_qswMat(pos_car,vel_car)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from the inertial reference frame to 
// the "qsw" local orbital frame.</p>
// <p> The inertial frame is the frame relative to which the satellite's position and velocity are defined. The "qsw" local frame is built using these position and velocity vectors. </p>
// <p> By convention, multiplying <b>M</b> by coordinates relative to the inertial frame yields coordinates relative to the "qsw" frame. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Local frames">Local frames</link> for  more details on the definition of local frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// pos_car: satellite's position vector relative to the inertial frame [m] (3xN)
// vel_car: satellite's velocity vector relative to the inertial frame [m/s] (3xN)
// M : inertial to "qsw" local frame transformation matrix (3x3xN)
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 10.2.2.3 (Definition du Repere orbital local)
// 2) CNES - MSLIB FORTRAN 90, Volume O (mo_qsw_geo)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_qsw2inertial
// CL_fr_inertial2qsw
// CL_fr_tnwMat
// CL_fr_inertial2tnw
// CL_fr_tnw2inertial
//
// Examples
// // Inertial to "qsw" :
// pos_car = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// vel_car = [[1.e3;3.e3;7.e3] , [2.e3;3.e3;6.e3]];
// [M] = CL_fr_qswMat(pos_car,vel_car)
// pos_qsw = M*pos_car;
//
// // "qsw" to inertial :
// pos_car = M'*pos_qsw;

// Declarations:


// Code:

[lhs,rhs] = argn()

if rhs~=2 then CL__error('check number of input arguments'); end

N = size(pos_car,2);

//calcul du vecteur q
q = CL_unitVector(pos_car);

// calcul du produit vectoriel pos^vel
w = CL_unitVector(CL_cross(pos_car,vel_car));

// calcul du produit vectoriel w^q
s = CL_cross(w,q)

// Matrix M :
M = hypermat([3 3 N] , [q(1,:); s(1,:); w(1,:); ...
            q(2,:); s(2,:); w(2,:); ...
            q(3,:); s(3,:); w(3,:)]);
if(N == 1)
    M = M(:,:,1);
end

endfunction
