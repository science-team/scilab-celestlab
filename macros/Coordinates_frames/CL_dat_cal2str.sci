//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [str] = CL_dat_cal2str(cal)
// Calendar date to string 
//
// Calling Sequence
// str = CL_dat_cal2str(cal)
//
// Description
// <itemizedlist><listitem>
// <p>Converts a calendar date into a string.</p>
// <p>The calendar date is a column vector [year;month;day] or
// [year;month;day;hours,minutes;seconds]. </p>
// <p><b>Notes:</b></p>
// <p>- The dates are rounded to the nearest microsecond. </p>
// <p>- The calendar date in input is supposed to be a valid calendar date, 
// otherwise the standard format may not be well adapted. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cal: Calendar date (3x1 or 6xN)
// str: Formatted calendar date (1xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// CL_dat_cal2str([2012;1;1;12;0;0])
// 

// Declarations:

// Code:

if (cal == []); 
  str = ""; 
  return;
end

if (size(cal,1) <> 3 & size(cal,1) <> 6)
  CL__error("Invalid argument"); 
end 

if (typeof(cal) <> "constant")
  CL__error("Invalid calendar date argument"); 
end 
  
if (size(cal,1) == 3)
  cal = [cal; zeros(cal)]; 
end


// round sec and recompute calendar date
// (while trying to keep accuracy)
nd = 6 * ones(cal(1,:)); //digits

j1 = CL_dat_cal2cjd(cal(1:3,:)); 
j2 = (cal(4,:)*3600 + cal(5,:)*60 + cal(6,:)) / 86400; 
  
cjd = j1 + floor(j2); 
secs = round((j2 - floor(j2)) .* 86400 .* 10.^nd) ./ 10.^nd;

// if millisec = whole number => only 3 digits
I = find(secs*1000 == round(secs*1000)); 
nd(I) = 3; 

if (secs == 86400)
  cjd = cjd+1; 
  secs = 0; 
end

// regerate cal
// only integer part for the day for accuracy
// seconds : separately

cal = CL_dat_cjd2cal(cjd); // whole number
cal(4:6,:) = CL_sec2hms(secs); // seconds

// formatting
strcal = emptystr(cal'); 

strcal(:,1) = msprintf("%d\n", cal(1,:)');   // year
strcal(:,2) = msprintf("%02d\n", cal(2,:)'); // month
strcal(:,3) = msprintf("%02d\n", cal(3,:)'); // day
strcal(:,4) = msprintf("%02d\n", cal(4,:)'); // hours
strcal(:,5) = msprintf("%02d\n", cal(5,:)'); // minutes

secs = round(cal(6,:) .* 10.^nd) ./ 10.^nd; 
isec = floor(secs);
xsec = round((secs - isec) .* 10.^nd);

I = find(nd == 3)
if (I <> [])
  strcal(I,6) = msprintf("%02d.%03d\n", isec(I)', xsec(I)'); 
end

I = find(nd == 6)
if (I <> [])
  strcal(I,6) = msprintf("%02d.%06d\n", isec(I)', xsec(I)'); 
end
  
str = ( strcal(:,1) + "/" + strcal(:,2) + "/" + strcal(:,3) + .. 
        " " + ..
        strcal(:,4) + ":" + strcal(:,5) + ":" + strcal(:,6) )'
  

endfunction


