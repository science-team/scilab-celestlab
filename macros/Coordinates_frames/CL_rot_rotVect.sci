//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [w] = CL_rot_rotVect(q,v)
// Image of vector by a rotation defined by a quaternion
//
// Calling Sequence
// w = CL_rot_rotVect(q,v)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the image of a vector by a rotation defined by a quaternion. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - The quaternion should have a norm equal to 1 (not checked). </p>
// <p> - See <link linkend="Data types">Data types</link> or <link linkend="CL_rot_defQuat">CL_rot_defQuat</link> for more details on quaternions. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// q : Quaternion (that defines the rotation) (dim N or dim 1)
// v : Vector (3xN or 3x1)
// w : Rotated vector (3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_rot_defQuat
// CL_rot_matrix2quat
// CL_rot_quat2matrix
//
// Examples
// q = CL_rot_axAng2quat([0;0;1], %pi/6) // rotation around Z
// M = CL_rot_quat2matrix(q) // corresponding matrix
// v = [1; 0; 0] // some vector
// w = CL_rot_rotVect(q, v) // rotated vector
// w = M' * v // same
// 
// // Same with 2 quaternions
// q2 = [q, q] 
// M2 = CL_rot_quat2matrix(q2) // corresponding matrix
// w = CL_rot_rotVect(q2, v) // rotated vector
// w = M2' * v // same
//
// // Same with 2 vectors
// v2 = [v, v] 
// w = CL_rot_rotVect(q, v2) // rotated vector
// w = M' * v2 // same


// Declarations:


// Code:

[lhs rhs]=argn(0);

if (rhs <> 2)
   CL__error("Invalid number of input arguments"); 
end

if (typeof(q) <> "CLquat")
  CL__error("Wrong type of input argument. Quaternion expected");
end

if (typeof(v) <> "constant") 
   CL__error("v is not of type constant"); 
end

[li, co] = size(v);

if (li ~= 3) 
   CL__error("Invalid size for v"); 
end

if (co == 1 & size(q)>=1) 

  // rotation de v (unique) sur le quaternion(s) q
  w = imag(q*CL_rot_defQuat(zeros(q.r),v*ones(1,size(q)))*conj(q));

elseif (co == size(q)) 

  // rotation de v(:,I) sur le quaternion q(I)
  w = imag(q*CL_rot_defQuat(zeros(q.r),v)*conj(q));

elseif (co > 1 & size(q)==1)
  Q = CL_rot_defQuat(q.r*ones(1,co),q.i*ones(1,co));
  V = CL_rot_defQuat(zeros(1,co),v);
  w = imag( Q*V*conj(Q) );

else
  CL__error("Invalid size of input arguments");
end

endfunction
