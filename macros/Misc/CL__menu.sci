//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Displays CelestLab menu 
function CL__menu()

  // sub menu labels
  labels = [ "Demos"; ..
             "Help"; ..
             "Data files (predefined variables)"; ..
             "---"; ..
             "Release notes (latest version)"; ..
             "Release notes (previous versions)"; ..
             "Edit configuration file"; ..
             "About CelestLab" ..
             ]; 
   
  // delete menu in case
  delmenu("CelestLab")

  // create menu
  addmenu("CelestLab", labels, list(0,"CL__menu_action"));

  // activate menu
  setmenu("CelestLab");
 
endfunction
