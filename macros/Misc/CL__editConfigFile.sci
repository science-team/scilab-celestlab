//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Edit CelestLab configuration file
// NB:  do not use configSet or configGet in this function ! 
 
function CL__editConfigFile()

// get the configuration description
config_desc = CL__configGetDesc(); 

// load configuration file (guaranteed valid)
conf = CL__configLoad(); 

// prepares the menu options for "x_choices"
// -> uses info from config_desc + conf
// Ival0: indices of "initial" values
// (relative to proposed list of values)

list_opt = list(); 
Ival0 = []; 

for (name = config_desc.names)
  // value index
  ind = find(name == config_desc.names); 

  // possible values
  accval = config_desc.accval(ind); 

  // index of value in accval
  k = find(conf(name) == accval); 

  // if parameter not editable: only one choice
  if (~config_desc.editable(ind)) 
    accval = accval(k); 
    k = 1; 
  end

  Ival0 = [Ival0, k]; 

  // list(name, index, possible values)
  list_opt($+1) = list(name, k, accval); 
end


// edit values => index of values in accval or [] 
Ival = x_choices('Configuration options (see help for details)', list_opt);

// "cancel" pressed => return
if (Ival == []); 
  return; 
end

// no change => return
if (find(Ival <> Ival0) == []); 
  return; 
end

// Something has changed => user must confirms file update
rep = messagebox("Confirm update of configuration file", "modal", "warning", ["Update file!" "Cancel"])
if (rep == 2); 
  return; 
end
  
 
// file will be updated

str = [ "// -----------------------------------------";
        "// CelestLab configuration (preferences)";
        "// See dedicated help page for more details";
        "// -----------------------------------------" ];    

for k = 1 : length(list_opt)
  elem = list_opt(k); 
  str($+1) = msprintf("conf.%s = ""%s""", elem(1), elem(3)(Ival(k))); 
end
  
// write file
mputl(str, config_desc.fpath); 

// notice 
messagebox(["You have to restart Scilab for the "; ..
            "changes to be taken into account ! "], "modal", ..
            "warning", ["OK"])


endfunction
