//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Menu CelestLab : Data files
// => shows the list of ".scd" files in CL_home()/data 
// => edit selected file

function [] = CL__editDataFiles()

  // interface to "editor"
  function Editor(fpath)
    try
      editor(fpath, "readonly");
    catch
      messagebox("This function is not available: upgrade Scilab!", "CelestLab message");
    end
  endfunction

  d = fullfile(CL_home(),"data");
  lfiles = findfiles(d, "*.scd");
  if (lfiles <> [])
    n = x_choose(lfiles, d + ":"); 
    if (n > 0)
      Editor(fullfile(d, lfiles(n)));
    end
  end

endfunction
