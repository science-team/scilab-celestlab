//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Menu CelestLab : Release Notes
// option = 1 => latest (default)
// option = 2 => history

function [] = CL__releaseNotes(option)

  if (~exists("option", "local")); option = 1; end

  if (option == 1)
    fname = "detailed_changelog_latest.txt"; 
  elseif (option == 2)
    fname = "detailed_changelog_history.txt"; 
  else
    CL__error("Invalid option"); 
  end

  // load file
  path = fullfile(CL_home(), fname); 
  txt = mgetl(path);

  // show 
  messagebox(txt, fname);
 
endfunction
