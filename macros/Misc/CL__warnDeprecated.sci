//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Prints a message if a deprecated function is called. 
// ("CL__warnDeprecated()" has to be inserted in the 
// deprecated function)
// The message is printed once. 
// The list of called deprecated functions is stored in the 
// field "DEPRECATED" of the global variable "%CL__PRIV"
//
// NB: works for high level functions only. 

function [] = CL__warnDeprecated()

  global %CL__PRIV; 

  if (CL_configGet("warn_deprecated") == "no")
    return;
  end

  if (isempty(%CL__PRIV))
    %CL__PRIV = struct(); 
  end

  if (~isfield(%CL__PRIV, "DEPRECATED"))
    %CL__PRIV.DEPRECATED = []; 
  end

  // tree of function calls
  // if only one row => does nothing (no higher level)
  [numlin,mac] = where(); 
  if (size(mac, "*") <= 1); 
    return; 
  end

  // highest level call
  funcname = mac(2); 

  I = find(%CL__PRIV.DEPRECATED == funcname); 

  if (I == [])
    // function name not yet stored
    mprintf("*** CelestLab (warning): Function %s is deprecated!\n", funcname); 
    %CL__PRIV.DEPRECATED($+1) = funcname; 
  end

endfunction


  
