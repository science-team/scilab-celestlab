//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

//  Interface pour demos CelestLab
//  Auteur : A. Lamy


// -------------------------------------------------------------
// main
// -------------------------------------------------------------
function [] = CL__demos()

// Code: 
printf("\n");
printf("*******************************************\n");
printf("*   CelestLab Demo Tool - CNES - DCT/SB   *\n");
printf("*******************************************\n");

// repertoire "racine des demos"
rep_demos = fullfile(CL_home(), "demos");

// fichier logo CelestLab
nomfic_logo = fullfile(CL_home(), "data", "misc", "logo_celestlab_small.png");

CL__demos_main(rep_demos, nomfic_logo);

endfunction


// -------------------------------------------------------------
// fonction principale
// -------------------------------------------------------------
function CL__demos_main(rep_base, nomfic_logo)

  frame_w      = 200;     // Frame width
  frame_h      = 350;     // Frame height
  margin_x     = 15;      // Horizontal margin between each elements
  margin_y     = 17;      // Vertical margin between each elements
  dy_titre     = 15;      // Vertical margin between each elements
  dy_logo      = 136;     // dimension exacte logo y 
  dx_logo      = 121;     // dimension exacte logo x
  dy_box       = 20;      // dimension zone "checkbox"

  dx_ref = [margin_x, frame_w, margin_x, frame_w+50, margin_x];
  dy_ref = [margin_y, frame_h, margin_y+dy_titre];

  px_ref = [0,cumsum(dx_ref)];
  py_ref = [0,cumsum(dy_ref)];

  dim = [px_ref($), py_ref($)]; // axes figure

  cadre_1 = [ px_ref(2), py_ref(2)+dy_logo+10, dx_ref(2), dy_ref(2)-dy_logo-10 ];
  cadre_2  = [ px_ref(4), py_ref(2)+10, dx_ref(4), dy_ref(2)-10 ];

  cadre_logo = [ px_ref(2), py_ref(2), dx_ref(2), dy_logo ];
  cadre_box = [ px_ref(4), py_ref(2)-10, dx_ref(4), dy_box ];

  figdef = gdf(); // default figure
  figure_position_def = get(figdef, "figure_position"); // sauvegarde valeur
  figure_size_def = get(figdef, "figure_size"); // sauvegarde valeur
  axes_size_def = get(figdef, "axes_size"); // sauvegarde valeur
  set(figdef, "figure_position", [20,20]);
  set(figdef, "figure_size", [10,10]);
  set(figdef, "axes_size", [10,10]);
  set(figdef, "visible", "off");
  set(figdef, "immediate_drawing", "off");

  fig = figure("Figure_name", "CelestLab Demo Tool", "Tag", "CL_demoTool");
  
  sdf(); // sets default values

  delmenu(fig.figure_id, gettext("&File"));
  delmenu(fig.figure_id, gettext("&Tools"));
  delmenu(fig.figure_id, gettext("&Edit"));
  delmenu(fig.figure_id, gettext("&?"));
  toolbar(fig.figure_id, "off");

  hm1 = uimenu( "parent", fig, "label" , 'File');
  hm2 = uimenu( "parent", fig, "label" , 'Options');
  hm3 = uimenu( "parent", fig, "label" , '?');

  uimenu( "parent"    , hm1, ...
          "label"     , 'Quit', ...
          "callback"  , "CL__demos_cb_quit()" );

  uimenu( "parent"    , hm2, ...
          "label"     , 'Debug mode ...', ...
          "callback"  , "CL__demos_cb_debug_mode()" );

  uimenu( "parent"    , hm3, ...
          "label"     , 'Help ...', ...
          "callback"  , "CL__demos_cb_help()" );

  uimenu( "parent"    , hm3, ...
          "label"     , 'About ...', ...
          "callback"  , "CL__demos_cb_about()" );

  fig.background      = -2;
  fig.axes_size       = dim;

  // lecture des donnees
  [topics, contents] = CL__demos_gen_data(rep_base);

  // liste des "topics"
  h1 = CL__demos_createListe(fig, "Topics", cadre_1, "liste1");

  CL__demos_createLogo(fig, nomfic_logo, cadre_logo, dx_logo, dy_logo);

  // liste des contenus
  h2 = CL__demos_createListe(fig, "Applications", cadre_2, "liste2");

  // options additionnelles
  h3 = CL__demos_createCheckbox(fig, "Keep Plots", cadre_box, "Keep");
  h4 = CL__demos_createCheckbox(fig, "Show Info", cadre_box+[90,0,0,0], "View");

  cb_actif = 0;     // 1 = call back en cours
  listfen = list(); // liste des fenetres crees
  numtopic = 0;     // numero du choix courant "topic"
  numchoix = 0;     // numero du choic courant "application"

  // activation
  demo = tlist(['demo', 'topics', 'contents', 'h1', 'h2', 'h3', 'h4', 'fig', ...
                'numtopic', 'numchoix', 'listfen', 'cb_actif', 'db_mode'], ...
                 topics, contents, h1, h2, h3, h4, fig, ...
                 numtopic, numchoix, listfen, cb_actif);

  set(demo.h1, "string", strcat(topics(:,1),"|"));
  set(demo.h1, "user_data", topics);
  set(demo.h3, "value", 0);
  set(demo.h4, "value", 0);

  demo.fig.immediate_drawing = "on";
  demo.fig.visible = "off"; // to be sure that no plot can appear in the plot

  demo.cb_actif = 0; // 1 = call back en cours
  demo.listfen = list();
  demo.db_mode = 0; // 1 = mode actif

  set(fig,"user_data",demo); // place les donnees internes dans la zone "user_data"; 

endfunction


// -------------------------------------------------------------
// cree une liste de choix
// renvoie le "handle" de la liste
// -------------------------------------------------------------
function [h] = CL__demos_createListe(fig, titre, cadre, tag)

  // frame
  uicontrol( ...
  "parent"              , fig,...
  "relief"              , "groove",...
  "style"               , "frame",...
  "units"               , "pixels",...
  "position"            , cadre,...
  "horizontalalignment" , "center", ...
  "background"          , [1 1 1], ...
  "visible"             , "on", ...
  "tag"                 , "frame");

  // Frame title
  margey = 3;
  htxt = uicontrol( ...
  "parent"              , fig,...
  "style"               , "text",...
  "string"              , "",...
  "units"               , "pixels",...
  "position"            , [ cadre(1), cadre(2)+cadre(4)+margey-2, cadre(3), 20],...
  "fontunits"           , "points",...
  "fontSize"            , 11,...
  "fontWeight"          , "normal",...
  "horizontalAlignment" , "center", ...
  "background"          , [1 1 1], ...
  "visible"             , "on", ...
  "tag"                 , "title_frame");

  // List Box
  margex = 6;
  margey = 6;

  h = uicontrol( ...
  "parent"              , fig,...
  "style"               , "listbox",...
  "units"               , "pixels",...
  "position"            , [ cadre(1)+margex cadre(2)+margey cadre(3)-2*margex cadre(4)-2*margey ],...
  "fontunits"           , "points",...
  "fontSize"            , 12,...
  "horizontAlalignment" , "left", ...
  "BackgroundColor"     , [255/255 , 255/255 , 255/255 ], ...
  "callback"            , "CL__demos_cb()",...
  "visible"             , "on", ...
  "tag"                 , tag);

  // refait ici car bug scilab (lie a la taille pas toujours correctement affichee)
  set(htxt, "string", titre); 
  
endfunction


// -------------------------------------------------------------
// Logo
// -------------------------------------------------------------
function [h] = CL__demos_createLogo(fig,nomfic,cadre, dimx, dimy)

  str = "<html>";
  str = str + "<img src=""file:///" + nomfic + """";
  str = str + " width=" + msprintf("%d",dimx);
  str = str + " height=" + msprintf("%d",dimy);
  str = str + " alt=""CelestLab"" />";
  str = str + "</html>";

  h = uicontrol( ...
  "parent"              , fig,...
  "style"               , "text",...
  "string"              , str,...
  "units"               , "pixels",...
  "position"            , cadre,...
  "background"          , [1 1 1], ...
  "tag"                 , "my_text", ...
  "horizontalalignment" , "center", ...
  "verticalalignment"   , "middle" ...
  );

endfunction

// -------------------------------------------------------------
// checkbox pour garder les traces
// renvoie le "handle" du checkbox
// -------------------------------------------------------------
function [h] = CL__demos_createCheckbox(fig, titre, cadre, tag)

  h = uicontrol( ...
  "parent"              , fig,...
  "style"               , "checkbox",...
  "string"              , titre,...
  "units"               , "pixels",...
  "position"            , cadre,...
  "background"          , [1 1 1], ...
  "foregroundColor"     , [0 0 0], ...
  "tag"                 , tag, ...
  "callback"            , "CL__demos_cb()",...
  "horizontalalignment" , "left", ...
  "verticalalignment"   , "bottom" ...
  );

endfunction

// -------------------------------------------------------------
// fonction "call-back" des listes
// -------------------------------------------------------------
function CL__demos_cb()

  // recuperation de la structure de donnees 'demo'
  fig = get(gcbo, "parent"); // figure principale (parent) - gcbo = objet appelant

  demo = get(fig, "user_data"); // donnees internes
  tag = get(gcbo,"tag"); // tag de l'appelant

  if (demo.cb_actif == 1); return; end // rien si cb() en cours
  demo.cb_actif = 1; // bloque les activations suivantes
  set(fig,"user_data",demo); // maj pour cb_actif

  keep = get(demo.h3, "value");
  view = get(demo.h4, "value");
  debug_mode = demo.db_mode;

  if ((tag == "liste1" | tag == "liste2") & keep == 0)
     for k = 1:length(demo.listfen);
        f = demo.listfen(k);
        try, delete(f), end
     end
     demo.listfen = list();
  end

  scf_save = scf;
  funcprot(0);     // pas de message de redefinition de scf()

 // redefinition of scf : 
 // - enable to store the figure id
 // - initializes the default event handler
 // - NB : initializes user_data as a structure with 2 fields: 
 //        -> eventHandler : name of event handler function
 //        -> PointerLocationData : reference information for slope
 function [f] = scf(varargin)
   rhs=argn(2);
   if (rhs == 0)
     demo = get(fig, "user_data");
     f = scf_save();
     f.figure_size = [600,500];
     demo.listfen(length(demo.listfen)+1) = f;
     set(fig,"user_data",demo);

     ud = struct();
     // ud.eventHandler = CL__demos_defUserHandler;
     ud.eventHandler = ""; 
     ud.PointerLocationData = [];
     set(f, 'user_data', ud);
     f.event_handler = "CL__demos_userEvtHandler";
     f.event_handler_enable = "off";

   else
     f = scf_save(varargin(1));
   end
 endfunction

  // fonction d'execution du script (pour masquage)
  function [ierr] = exec_script(nomfic)
    oldfmt=format(); // save format for string()
    format('v', 10); // 10 digits
    CL_init(init_glob = %f); // initialisation des constantes au cas ou
    ierr = exec(nomfic, 'errcatch', mode=-1);
    if (ierr <> 0)
       [str,n,lig,func] = lasterror(); // derniere erreur
       if (debug_mode == 1 & ~isempty(str))
          fprintf(%io(2), "*** STOP ***\n");
          for s=str; fprintf(%io(2), "=> Message: \n%s\n",s); end
          if (func <> ""); fprintf(%io(2), "=> Function: \n%s\n", func); end
       end
    end
    format(oldfmt(2), oldfmt(1)); // restore format
  endfunction

  // print info if available
  // info file = same name as script (but .info instead of .sce)
  function disp_info(nomfic)
    try
      lignes = mgetl(nomfic);
    catch
      lignes = [];
    end
    if isempty(lignes); return; end
    printf("\nInfo:\n");
    w = 0;
    infoex = %f;
    for k = 1:length(length(lignes))
      if (length(lignes(k)) <= 3); continue; end
      lig1 = strstr(lignes(k), '//>');
      if (length(lig1) == 0); continue; end
      lig = strstr(lig1, '>');
      infoex = %t;
      printf("%s\n", lig);
    end
    if (~infoex); printf("None\n"); end
  endfunction

  if (tag == "liste1")
     demo.numtopic = get(gcbo, "value"); // affecte la liste des applis
     appli = demo.contents(demo.numtopic).noms;
     set(demo.h2, "string", strcat(appli(:,1),"|"));
     set(demo.h2, "user_data", appli);
     demo.numchoix = 0; // pas d'appli courante

  elseif (tag == "liste2")
     demo.numchoix = get(gcbo, "value");

     scf(fig);
     nomchoix = demo.contents(demo.numtopic).noms(demo.numchoix); 
     fig.info_message = nomchoix + ": in progress...";

     nomfic = demo.contents(demo.numtopic).fics(demo.numchoix); // nom du script
     
     set(fig,"user_data",demo); // car modifie par scf()

     if (view == 1)
        printf("\n");
        printf("-------------------------------------------\n");
        printf("Script path:\n");
        script_path = nomfic; 
        if (getos() == "Windows")
          script_path = getlongpathname(script_path); 
        end
        printf("%s\n", script_path);
        disp_info(nomfic);
        deff('$I(str)', 'printf(''%s\n'', str)');
        printf("-------------------------------------------\n");
     end

     ierr = exec_script(nomfic);

     demo = get(fig, "user_data"); // recup modif eventuelles
     set(demo.h1, "value", demo.numtopic); // force numero courant en cas de changement
     set(demo.h2, "value", demo.numchoix);

     scf(fig);
     fig.info_message = " ";

  elseif (tag == "keep")
     value = get(demo.h3, "value");
  end

  demo.cb_actif = 0;
  set(fig,"user_data",demo);

endfunction


// -------------------------------------------------------------
// fonction "call-back" 'quit'
// -------------------------------------------------------------
function CL__demos_cb_quit()
  // recuperation de la structure de donnees 'demo'

  fig = get(get(gcbo, "parent"),"parent");
  demo = get(fig, "user_data");

  for k = 1:length(demo.listfen);
    f = demo.listfen(k);
    try
      delete(f);
    end
  end

  delete(fig);
endfunction


// -------------------------------------------------------------
// fonction "call-back" 'about'
// -------------------------------------------------------------
function CL__demos_cb_about()
  // recuperation de la structure de donnees 'demo'

  messagebox(["CelestLab Demo Tool" "CNES - DCT/SB"]);
endfunction


// -------------------------------------------------------------
// fonction "call-back" 'debug mode'
// -------------------------------------------------------------
function CL__demos_cb_debug_mode()

  fig = get(get(gcbo, "parent"),"parent");
  demo = get(fig, "user_data");

  if (demo.db_mode == 1) // on
     old = "ON";
     new = "OFF";
  else
     old = "OFF";
     new = "ON";
  end

  l1=list(' Current value is: ',1, [old]);
  l2=list(' Change to: ',1, [new]);
  choix = x_choices("Debug mode: ", list(l1,l2));
  if (~isempty(choix))
    demo.db_mode = 1 - demo.db_mode;
    set(fig,"user_data",demo);
  end
endfunction

// -------------------------------------------------------------
// fonction "call-back" 'debug mode'
// -------------------------------------------------------------
function CL__demos_cb_help()
  // path to demos directory
  path = fullfile(CL_home(), "demos"); 
  if (getos() == "Windows"); path = getlongpathname(path); end

  msg = [
    "Select an item in the ''Topics'' list first, then an" 
    "application."
    " "
    "Each application corresponds to a specific script (''.sce'')"
    "in the CelestLab ''demos'' directory."
    "The ''demos'' root path is: "
    path
    " "
    "Special features: "
    " "
    " - Keep Plots: "
    "   If selected, graphic windows are not deleted"
    "   when a new application is selected."
    " "
    " - Show Info:"
    "   If selected, additional information is printed. "
    "   (path of application selected + any addition information "
    "   present in the script)"
    " "
    " - Option -> Debug mode: "
    "   If ''ON'', error/info messages are printed, if any. "
    "   Default is ''OFF''."
    " "
    " "
    "Note: "
    " - The CelestLab demo tool calls CL_init before executing "
    " the scripts. It follows that changing some of the values of the "
    " local variables %CL_xxx has no impact on the demo results."
    "------------------------------------------------------------------------- "
  ];

  messagebox(msg, "CelestLab Demo Tool - Help", "info")

endfunction


// -------------------------------------------------------------
// explore les sous-repertoire de path qui contiennent des fichiers '.sce'
// topics : liste des repertoires de 1er niveau
// contents : liste de tlist(noms, fics)
// -------------------------------------------------------------
function [topics, contents] = CL__demos_gen_data(path)

 topics = [];
 contents = list();
 sep = filesep();

 // Code:
 listed = gsort(listfiles(path), 'lr', 'i'); // liste des sous-repertoires du repertoire 'demos'

 for d = listed'
   nomrep = path+sep+d;
   if isempty(strindex(d,'.')) & length(findfiles(nomrep, "*.sce")) <> 0
      topics = [topics ; strsubst(d,"_", " ")];

      fics = gsort(nomrep + sep + findfiles(nomrep,"*.sce"), 'lr', 'i');
      noms = basename(fics);

      cont = tlist(["contents", "noms", "fics"], strsubst(noms,"_", " "), fics);
      contents = lstcat(contents, list(cont));
   end
 end

endfunction


// -------------------------------------------------------------
// event handler pour les fenetre crees par les applications
// appelle le handler eventHandler de user_data (meme interface)
// -------------------------------------------------------------
function CL__demos_userEvtHandler(win, x, y, ibut)

 f = get_figure_handle(win);
 if isempty(f); return; end

 ud = f.user_data; 
 if isstruct(ud)
   if isfield(ud, 'eventHandler')
     ud.eventHandler(win, x, y, ibut);
   end
 end 

endfunction


// -------------------------------------------------------------
// Default event handler
// -> pointer location in user coordinates (left button)
//    and reference for slope
// -> slope (right button)
// -------------------------------------------------------------
function CL__demos_defUserHandler(win, x, y, ibut)
// NB : x,y : pixels

function [s] = format_reel(x)
  s = sprintf("%.6g", x); 
  s = strsubst(s, "e+00", "e+0"); 
  s = strsubst(s, "e-00", "e-0"); 
endfunction

function [s] = format_xy(x,y)
  s1 = "x: " + format_reel(x); 
  s2 = "y: " + format_reel(y); 
  s = sprintf("%-10s   %-10s", s1, s2);
endfunction

// protection against interferences
// check that all figures are visible and drawn or else does nothing
list_fig_id = winsid();
for id = list_fig_id 
  f = get_figure_handle(id);
  if ~is_handle_valid(f); return; end
  if ~(f.immediate_drawing == "on" & f.visible == "on"); return; end
end

f = get_figure_handle(win);
if isempty(f); return; end
if ~is_handle_valid(f); return; end

if (f.immediate_drawing == "off" | f.visible == "off")
   return; // avoids interferences if plot not finished
end 

// save current figure and axes
f_save=gcf();
a_save=gca();

scf(f);

ud = get(f, "user_data");
pos_ref = ud.PointerLocationData(1:2);
ind_ref = ud.PointerLocationData(3);

//xinfo(sprintf("win=%d x=%d y=%d but=%d", win,x,y,ibut));

if (ibut == -1)
  if isempty(pos_ref)
    // f.info_message = 'Left click: pointer location,  Right click: slope';
    f.info_message = " "; 
  end
  return;
end

left = intersect(ibut, [0,3]); // 3 = left click
right = intersect(ibut, [2,5]); // 5 = right click

if (isempty(left) & isempty(right)); return; end

h = CL_g_select(f, "Axes");
ok = 0;

for k = 1:length(h)
  a=sca(h(k)); // selects h(k) as current axes
  if (a.view <> "2d"); continue; end  // does nothing for 3D axes

  // xu, yu : user coordinates
  // rect : frame coordinates in pixels
  [xu, yu, rect] = xchange(x,y,"i2f"); 

  if (x>=rect(1) & x<=rect(1)+rect(3) & y>=rect(2) & y<=rect(2)+rect(4))
     ok = 1;
     s1 = format_xy(xu, yu);
     
     if ~isempty(left)
        pos_ref = [xu, yu]; // reference position stored
        ind_ref = k;
        s2 = "";

     else
        if isempty(pos_ref)
           s2 = sprintf("slope: *** No reference position ***");

        elseif k <> ind_ref
           s2 = sprintf("slope: *** Not the same axes ***");

        else
           s1ref = format_xy(pos_ref(1), pos_ref(2));
           s2ref = "<= reference position";
           f.info_message = sprintf("%-25s   %s", s1ref, s2ref);
           sleep(1000);
           if xu == pos_ref(1)
              if yu == pos_ref(2)
                 s2 = "=> slope: undefined"
              else
                 s2 = "=> slope: infinity"
              end
           else
              pente = (pos_ref(2)-yu)/(pos_ref(1)-xu);
              s2 = "=> slope: " + format_reel(pente); 
           end
        end
     end
     f.info_message = sprintf("%-25s   %s", s1, s2);
     break;

   end

end

if (ok == 0)  // out of frame => reinitializes
   f.info_message = ' '; 
   pos_ref= [];
   ind_ref = [];
end

// store ref location
ud.PointerLocationData = [pos_ref, ind_ref];
set(f, "user_data", ud);

// restore cuurent figure and axes
scf(f_save);
sca(a_save);

endfunction


