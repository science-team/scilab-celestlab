//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Actions associated with CelestLab menu
// => see CL__menu 
function [action] = CL__menu_action(k)

  // actions associated with each label
  actions = [ ..
    "CL__demos();" ; ..
    "help(''CelestLab'');" ; ..
    "CL__editDataFiles()"; ..
    ""; ..
    "CL__releaseNotes(1);" ; ..
    "CL__releaseNotes(2);" ; ..
    "CL__editConfigFile()"; ..
    "CL__about();" ..
    ];   


  // scilab version (in case...) 
  sciver = getversion('scilab'); 
  scinum = sciver(1)*10 + sciver(2); 

  if (k >= 1 & k <= size(actions, "*"))
    action = actions(k); 
  else
    action = "";  
  end
  
endfunction


