//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [acc] = CL_fo_apparentAcc(pos, vel, omega, omegadot)
// Apparent acceleration (non-inertial frame)
//
// Calling Sequence
// [acc] = CL_fo_apparentAcc(pos, vel, omega, omegadot)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Acceleration due to the "non inertiallity" of the frame. ("rotation" accelerations only, i.e. Coriolis + centrifugal force)</p> 
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The origin for the position vector must be the central body (there is no acceleration of the origin 
// of the frame considered).</p>
// <p>- The coordinates frame can be any frame.</p>
// <p>- omega is the angular velocity vector of the (current) frame (noted R) with respect to an inertial 
// (i.e. not rotating) frame (noted R0). </p>
// <p>- All vectors have coordinates in frame R. </p> 
// <p>- If vel or omegadot are empty, they are considered as [0;0;0].</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector [m]. (3xN or 3x1)
// vel: Velocity vector (relative to frame R) [m/s]. (3xN or 3x1)
// omega: Angular velocity vector of frame R with respect to frame R0 [rad/s]. (3xN or 3x1)
// omegadot: (optional) Time derivative (in frame R0) of omega [rad/s^2]. Default is [0;0;0] (3xN or 3x1)
// acc: Acceleration [m/s^2]. (3xN) 
//
// Authors
// CNES - DCT/SB
//
// Examples
// pos = [6378.e3; 0; 0];
// vel = [0; 7000.; 0];
// omega = [0; 0; 1] * 5.e-12; // 1 deg / century
// CL_fo_apparentAcc(pos, vel, omega)


// Declarations:


// Code:
if (~exists("omegadot", "local")); omegadot = [0;0;0]; end
if (omegadot == []); omegadot = [0;0;0]; end
if (vel == []); vel = [0;0;0]; end

// checks sizes
[pos, vel, omega, omegadot] = CL__checkInputs(pos, 3, vel, 3, omega, 3, omegadot, 3); 

acc = -2 * CL_cross(omega, vel) - CL_cross(omega, CL_cross(omega, pos)) - CL_cross(omegadot, pos); 

endfunction
