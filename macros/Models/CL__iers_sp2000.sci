//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [sp, spdot] = CL__iers_sp2000(jd, cder)
// Computation of TIO locator (s') (IAU 2000)
//
// Calling Sequence
// [sp, spdot] = CL__iers_sp2000(jd [,cder])
//
// Description
// <itemizedlist><listitem>
// <p> TIO locator = position of the Terrestrial Intermediate Origin on the 
// equator of the Celestial Intermediate Pole. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// cder: (boolean, optional) Option to compute derivative. If cder is %f, the derivative will be set to []. Default is %t. (1x1)
// sp: TIO locator [rad] (1xN)
// spdot: Time derivative of TIO locator [rad/s] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS2010


// Declarations:

// Code:

if (~exists("cder","local")); cder = %t; end;
if (argn(1) <= 1); cder = %f; end;

ARCSEC_TO_RAD = %pi / (180*3600);

// Julian centuries since J2000.0
t = ((jd(1,:) - 2451545) + jd(2,:)) / 36525;

// eq 5.13, p52, paragraph 5.5.2
sp = -47.e-6 * t * ARCSEC_TO_RAD;

// Time derivative
spdot = [];
if (cder)
  spdot = -47.e-6 * ARCSEC_TO_RAD / (36525 * 86400) * ones(t); // in rad/s
end

endfunction
