//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos_sun, vel_sun] = CL_eph_sun(cjd,tt_tref,frame,model)
// Earth-Sun position and velocity vectors
//
// Calling Sequence
// [pos_sun, vel_sun] = CL_eph_sun(cjd [,tt_tref,frame,model])
//
// Description
// <itemizedlist><listitem>
// <p>Computes Earth-Sun position and velocity vectors in the ECI or EOD frame</p>
// <p></p>
// <p>The default output frame is ECI.</p>
// <p>The model used can optionaly be modified:</p>
// <p> - model = "med": MEEUS "med" model is used.</p>
// <p> - model = "high": VSOP87 "high" model is used.</p>
// <p></p></listitem>
// <listitem>
// <p>Note: If the output frame is ECI and the model is "med", a simplified frame conversion model is used.</p>
// <p>The accuracy of this simplified frame conversion is better than 1 arcsecond in the [2000-2100] period, 
// which is way below the accuracy of the model itself.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the ECI and EOD frames,
// and <link linkend="Ephemerides">Ephemerides</link> for more details on the models.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// cjd: Modified (1950.0) julian day (Time scale: TREF) (1xN)
// tt_tref: TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
// frame: (optional, string) Name of the output frame. Can be "EOD" or "ECI". Default is "ECI" (1x1)
// model : (optional, string) Accuracy of the model. "med" or "high". Default is "med". (1x1)
// pos_sun : Earth-Sun position vector in output frame [m] (3xN)
// vel_sun : Earth-Sun velocity vector in output frame [m/s] (3xN)
//
// Bibliography
// Astronomical Algorithms J.Meeus - Second edition 1998, Chapter 25
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_eph_planet
// CL_eph_de405
//
// Examples
// cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
//
// // Default = standard precision, in ECI frame
// pos_sun_ECI = CL_eph_sun(cjd)
//
// // Higher precision, in EOD frame, with velocity
// [pos_sun_EOD, vel_sun_EOD] = CL_eph_sun(cjd , frame="EOD", model="high");


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_TT_TREF")); %CL_TT_TREF = %CL__PRIV.DATA.TT_TREF; end

// Code:
if (argn(2) < 1 | argn(2) > 4)
  CL__error("Invalid number of input arguments");
end

if ~exists("tt_tref","local"); tt_tref = %CL_TT_TREF; end;
if ~exists("frame","local"); frame = "ECI"; end;
if ~exists("model","local"); model = "med"; end;

if (typeof(model) <> "string"); CL__error("Invalid input argument ''model''"); end
if (typeof(frame) <> "string"); CL__error("Invalid input argument ''frame''"); end

if (frame <> "EOD" & frame <> "ECI"); CL__error("Unrecognized frame name"); end;
  
// Compute velocity only if needed
cvel = %f;
if (argn(1) == 2)
  cvel = %t;
end

// Conversion from cjd (TREF) to jd (TT)
jd = CL_dat_convert("cjd","jd",cjd+tt_tref/86400);

// MEEUS model (output is in EOD frame, centered on Earth)
if (model == "med")
  [pos_sun,vel_sun] = CL__eph_sun_Meeus(jd, cvel);
  
// VSOP87 model (output is in EOD frame, centered on Sun)
elseif (model == "high")
  if (cvel)
    [pos_sun,vel_sun] = CL_eph_planet("Earth", cjd, tt_tref, "high");
  else
    pos_sun = CL_eph_planet("Earth", cjd, tt_tref, "high");
    vel_sun = [];
  end
  pos_sun = -pos_sun; // Earth-Sun vector
  vel_sun = -vel_sun;
else
  CL__error("Invalid ''model''");
end


// Convert in ECI frame (if needed)
// NB: Use a frame conversion model in agreeance with the precision of the model!
if (frame == "ECI")
  if (model == "high")
    [pos_sun,vel_sun] = CL_fr_convert("EOD", "ECI", cjd, pos_sun, vel_sun, tt_tref=tt_tref);
  elseif (model == "med")
    [pos_sun,vel_sun] = CL__fr_fastConvert("EOD", CL_configGet("ECI_frame"), jd, pos_sun, vel_sun);
  end
end

endfunction
