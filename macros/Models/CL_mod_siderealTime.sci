//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [sidt,sidtdot] = CL_mod_siderealTime(cjd, ut1_tref,frame)
// Sidereal time (ECI to ECF rotation angle)
//
// Calling Sequence
// [sidt,sidtdot] = CL_mod_siderealTime(cjd [,ut1_tref,frame])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the sidereal time from an inertial frame to a rotating frame.</p>
// <p></p>
// <p>Optional argument <b>frame</b> can take on three values:</p>
// <p>- "ECI": (default) sidereal time is from ECI to ECF</p>
// <p>- "CIRS": sidereal time is from CIRS to TIRS</p>
// <p>- "Veis": sidereal time is from Veis to PEF</p>
// <p></p>
// <p>The time derivative can also be optionally computed.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> and 
// <link linkend="Configuration">Configuration</link> for more details</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: Modified (1950.0) Julian day (Time scale: TREF) (1xN)
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
// frame: (string, optional) Name of the inertial frame. Default is "ECI" (1x1)
// sidt : Sidereal time [rad]
// sidtdot : Sidereal time first derivative [rad/s]
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Use default time scale (TREF), and default frame (ECI)
// [sidt,sidtdot] = CL_mod_siderealTime(21915.3252)
//
// // TREF = UTC, UT1-UTC = 0.2 and Frame = Veis 
// cjd_utc = CL_dat_cal2cjd(2008,10,25,15,30,25);
// ut1_tref = 0.2;
// [sidt,sidtdot] = CL_mod_siderealTime(cjd_utc, ut1_tref, "Veis")
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_UT1_TREF")); %CL_UT1_TREF = %CL__PRIV.DATA.UT1_TREF; end

// Code:
if (~exists("ut1_tref", "local")); ut1_tref = %CL_UT1_TREF; end
if (~exists("frame", "local")); frame = "ECI"; end

if (typeof(frame) <> "string"); CL__error("Invalid input argument: frame"); end;

if (frame == "ECI"); frame = CL_configGet("ECI_frame"); end;

// Convert from cjd (TREF) to a two-part julian date (UT1)
// Warning: Do not use function CL_dat_convert in order to avoid losing accuracy
jd = [floor(cjd) + 2433282; ...
      cjd-floor(cjd) + 0.5 + ut1_tref/86400]

// CIRS --> TIRS (Earth Rotation Angle - ERA)
if (frame == "CIRS")
  [sidt, sidtdot] = CL__iers_era2000(jd);
  
// Veis --> PEF (Veis sidereal time)
elseif (frame == "Veis")
  [sidt, sidtdot] = CL__mod_sidTimeVeis(jd);

else
  CL__error("Invalid inertial frame");
end

endfunction
