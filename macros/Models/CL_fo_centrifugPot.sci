//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pot] = CL_fo_centrifugPot(pos, omega)
// Apparent potential due to the centrigural force (non-inertial frame)
//
// Calling Sequence
// [pot] = CL_fo_centrifugPot(pos, omega)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Potential due to the centrifugal force (rotating frame). </p> 
// <p>By definition, the acceleration derived from the potential is +grad(potential).</p> 
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The origin for the position vector must be the central body (there is no acceleration of the origin 
// of the frame considered).</p>
// <p>- The coordinates frame can be any frame.</p>
// <p>- omega is the angular velocity vector of the (current) frame with respect to a reference (inertial) 
// frame. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector [m]. (3xN or 3x1)
// omega: Angular velocity vector of current frame with respect to reference frame [rad/s]. (3xN or 3x1)
// pot: Potential [m^2/s^2]. (1xN) 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fo_centrifugAcc
//
// Examples
// pos = [6378.e3; 0; 0];
// omega = [0; 0; 1] * 2.e-7; // ~ 1 deg / day
// CL_fo_centrifugPot(pos, omega)


// Declarations:


// Code:

// checks sizes
[pos, omega] = CL__checkInputs(pos, 3, omega, 3); 

pot = 0.5 * CL_dot(CL_cross(omega, pos)); 


endfunction
