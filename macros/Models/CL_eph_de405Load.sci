//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ephem] = CL_eph_de405Load(fname, incel)
// Loads a DE405 ephemeris file
//
// Calling Sequence
// ephem = CL_eph_de405Load(fname, [, incel])
//
// Description
// <itemizedlist><listitem>
// <p>Loads a DE405 ephemeris file.</p>
// <p>The result is returned in a Scilab stucture <b>ephem</b> that can be used by the function
// <link linkend="CL_eph_de405">CL_eph_de405</link>.</p>
// <p></p>
// <p>If the <b>incel</b> ("in CelestLab") argument is true, the files are looked for in 
// the directory: CL_home()/data/ephem/de405. </p>
// <p></p></listitem>
// <listitem>
// <p>Note: The format of the ephemeris file is specific to CelestLab.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// fname: (string) Name (or path) of ephemeris file (1x1)
// incel: (boolean, optional) %t if the file is looked for in CelestLab database. Default is %f.
// ephem: Scilab structure containing the data (1x1)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_eph_de405
//
// Examples
// // Load a DE405 ephemeris file
// fname = "1960.dat";
// ephem = CL_eph_de405Load(fname, %t);
//
// // Use it! 
// cjd = CL_dat_convert("cal", "cjd", [1962;1;1]);
// [pos, vel, acc] = CL_eph_de405("Mars",cjd, "Earth", ephem=ephem); 

if (~exists("incel", "local")); incel = %f; end

if (incel)
  fname = fullfile(CL_home(), "data", "ephem", "de405", fname); 
end

if (~isfile(fname))
  CL__error("File " + fname + " not found");
end

load(fname, "ephem");

endfunction
