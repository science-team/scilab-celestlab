//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pot] = CL_fo_thirdBodyPot(pos, pos_b, mu_b)
// Potential due to gravity (third body)
//
// Calling Sequence
// [pot] = CL_fo_thirdBodyPot(pos, pos_b, mu_b)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Potential due to third body.</p> 
// <p>By definition, the acceleration derived from the potential is +grad(potential).</p> 
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The origin for all position vectors must be the central body.</p>
// <p>- The coordinates frame can be any frame.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector (from central body) [m]. (3xN or 3x1)
// pos_b: Position vector of third body (from central body) [m]. (3xN or 3x1)
// mu_b: Gravitational constant of third body [m^3/s^2]. (1x1)
// pot: Potential [m^2/s^2]. (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fo_thirdBodyAcc
//
// Examples
// pos = [7000.e3; 0; 0]; 
// pos_sun = CL_eph_sun(CL_dat_cal2cjd(2000,3,21)); 
// mu_sun = CL_dataGet("body.Sun.mu"); 
// CL_fo_thirdBodyPot(pos, pos_sun, mu_sun)

// Declarations:


// Code:
[pos, pos_b] = CL__checkInputs(pos, 3, pos_b, 3); 

d = CL_norm(pos_b - pos); 
D = CL_norm(pos_b); 

pot = mu_b * (1 ./ d - CL_dot(pos, pos_b) ./ (D.^3)); 

endfunction
 