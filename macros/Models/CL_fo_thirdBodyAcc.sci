//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [acc] = CL_fo_thirdBodyAcc(pos, pos_b, mu_b)
// Acceleration due to gravity (third body)
//
// Calling Sequence
// [acc] = CL_fo_thirdBodyAcc(pos, pos_b, mu_b)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Perturbing (differential) acceleration (relative to an inertial frame) due to third body.</p> 
// <p>It is equal to the gravity acceleration acting on the satellite minus the gravity acceleration acting
// on the central body. </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The origin for all position vectors must be the central body.</p>
// <p>- The coordinates frame can be any frame.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector (from central body) [m]. (3xN or 3x1)
// pos_b: Position vector of third body (from central body) [m]. (3xN or 3x1)
// mu_b: Gravitational constant of third body [m^3/s^2]. (1x1)
// acc: Acceleration [m/s^2]. (3xN) 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fo_thirdBodyPot
//
// Examples
// pos = [7000.e3; 0; 0]; 
// pos_sun = CL_eph_sun(CL_dat_cal2cjd(2000,3,21)); 
// mu_sun = CL_dataGet("body.Sun.mu"); 
// CL_fo_thirdBodyAcc(pos, pos_sun, mu_sun)

// Declarations:


// Code:
[pos, pos_b] = CL__checkInputs(pos, 3, pos_b, 3); 

d3 = CL_norm(pos_b - pos).^3; 
D3 = CL_norm(pos_b).^3; 

acc = mu_b * [ (pos_b(1,:) - pos(1,:)) ./ d3 - pos_b(1,:) ./ D3; 
               (pos_b(2,:) - pos(2,:)) ./ d3 - pos_b(2,:) ./ D3; 
               (pos_b(3,:) - pos(3,:)) ./ d3 - pos_b(3,:) ./ D3 ]; 

endfunction
 