function [pos_moon, vel_moon] = CL_eph_moon(cjd,tt_tref,frame,model)
// Earth-Moon position and velocity vectors
//
// Calling Sequence
// [pos_moon, vel_moon] = CL_eph_moon(cjd [,tt_tref,frame,model])
//
// Description
// <itemizedlist><listitem>
// <p>Computes Earth-Moon position and velocity vectors in the ECI or EOD frame using 
//    Meeus algorithm or ELP/MPP02 lunar theory.</p>
// <p></p>
// <p>The default output frame is ECI.</p>
// <p>The model used can optionaly be modified:</p>
// <p> - model = "med": MEEUS "med" model is used.</p>
// <p> - model = "high": MEEUS "high" model is used.</p>
// <p> - model = "ELP":  ELP/MPP02 model is used.</p>
// <p></p></listitem>
// <listitem>
// <p>Note: If the output frame is ECI and the model is "med", a simplified frame conversion model is used.</p>
// <p>The accuracy of this simplified frame conversion is better than 1 arcsecond in the [2000-2100] period, 
// which is way below the accuracy of the model itself.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the ECI and EOD frames,
// and <link linkend="Ephemerides">Ephemerides</link> for more details on the models.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// cjd: Modified (1950.0) julian day (Time scale: TREF) (1xN)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
// frame: (optional, string) Name of the output frame. Can be "EOD" or "ECI". Default is "ECI" (1x1)
// model : (optional, string) Name of model. "med", "high" or "ELP". Default is "med". (1x1)
// pos_moon : Earth-Moon position vector in output frame [m] (3xN)
// vel_moon : Earth-Moon velocity vector in output frame [m/s] (3xN)
//
// Bibliography
// Astronomical Algorithms J.Meeus - Second edition 1998, Chapter 47
// LUNAR SOLUTION ELP version ELP/MPP02 - Jean CHAPRONT and Gerard FRANCOU Observatoire de Paris - SYRTE department - UMR 8630/CNRS - October 2002
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_eph_de405
//
// Examples
// cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
//
// // Default = standard precision, in ECI frame
// pos_moon_ECI = CL_eph_moon(cjd)
//
// // Higher precision, in EOD frame, and with velocity
// [pos_moon_EOD, vel_moon_EOD] = CL_eph_moon(cjd , frame="EOD", model="high");
//
// // Maximum precision (ELP/MPP02 lunar theory)
// [pos_moon_EOD, vel_moon_EOD] = CL_eph_moon(cjd , frame="EOD", model="ELP");

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_TT_TREF")); %CL_TT_TREF = %CL__PRIV.DATA.TT_TREF; end

// Code:
if (argn(2) < 1 | argn(2) > 4)
  CL__error("Invalid number of input arguments");
end

if ~exists("tt_tref","local"); tt_tref = %CL_TT_TREF; end;
if ~exists("frame","local"); frame = "ECI"; end;
if ~exists("model","local"); model = "med"; end;

if (typeof(model) <> "string"); CL__error("Invalid input argument ''model''"); end
if (typeof(frame) <> "string"); CL__error("Invalid input argument ''frame''"); end

if (frame <> "EOD" & frame <> "ECI"); CL__error("Unrecognized frame name"); end;

// Compute velocity only if needed
cvel = %f;
if (argn(1) == 2)
  cvel = %t;
end
  
// Conversion from cjd (TREF) to jd (TT)
jd = CL_dat_convert("cjd","jd",cjd+tt_tref/86400);  
  

if (model == "med")
  // Number of coefficients to be used in MEEUS model
  nbcoef = [26,13,13];
  [pos_moon,vel_moon] = CL__eph_moon_Meeus(jd, nbcoef, cvel);
  
elseif (model == "high")
  // Number of coefficients to be used in MEEUS model
  nbcoef = [62,66,46];
  [pos_moon,vel_moon] = CL__eph_moon_Meeus(jd, nbcoef, cvel);
  
elseif (model == "ELP")
  [pos_moon,vel_moon] = CL__eph_moon_ELP(jd, cvel);
  
else
  CL__error("Invalid ''model''");
end

// Convert in ECI frame (if needed)
// NB: Use a frame conversion model in agreeance with the precision of the model!
if (frame == "ECI")
  if (model == "high" | model == "ELP")
    [pos_moon,vel_moon] = CL_fr_convert("EOD", "ECI", cjd, pos_moon, vel_moon, tt_tref=tt_tref);
  elseif (model == "med")
    [pos_moon,vel_moon] = CL__fr_fastConvert("EOD", CL_configGet("ECI_frame"), jd, pos_moon, vel_moon);
  end
end

endfunction
