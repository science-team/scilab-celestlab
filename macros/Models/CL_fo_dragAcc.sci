//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [acc] = CL_fo_dragAcc(vel, rho, coefd)
// Acceleration due to atmospheric drag
//
// Calling Sequence
// [acc] = CL_fo_dragAcc(vel, rho, coefd)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Perturbing acceleration (relative to an inertial frame) due to atmospheric drag.</p> 
// <p>It is parallel to the velocity vector (opposite direction), and is proportional to the velocity 
// norm squared.</p> 
// <p></p> 
// <p>The drag coefficient (<b>coefd</b>) is defined by: coefd = cd * area / mass (with cd around 2.7).</p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The velocity vector (<b>vel</b>) is the velocity relative to the atmosphere.</p> 
// <p>- The coordinates frame can be any frame.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// vel: Velocity vector (relative to the atmosphere) [m/s]. (3xN or 3x1)
// rho: Density of the atmosphere [kg/m^3]. (1xN or 1x1)
// coefd: Drag coefficient (cd*area/mass) [m^2/kg]. (1xN or 1x1)
// acc: Acceleration [m/s^2]. (3xN) 
//
// Authors
// CNES - DCT/SB
//
// Examples
// mu = CL_dataGet("mu"); 
// eqRad = CL_dataGet("eqRad"); 
// alt = (400:20:800)*1.e3; // altitude
// vel = [sqrt(mu./(eqRad+alt)); zeros(alt); zeros(alt)];  
// rho = CL_mod_atmUS76Density(alt); 
// coefd = 2.7 * 10 ./ 1000; 
// acc = CL_fo_dragAcc(vel, rho, coefd)
// scf(); 
// plot(alt/1000, CL_norm(acc)); 

// Declarations:


// Code:
[vel, rho, coefd] = CL__checkInputs(vel, 3, rho, 1, coefd, 1); 

K = -0.5 .* rho .* coefd .* CL_norm(vel); 

acc = CL_dMult(vel, K); 

endfunction
 