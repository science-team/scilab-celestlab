//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [acc] = CL_fo_centralAcc(pos, mu)
// Acceleration due to gravity (central force) 
//
// Calling Sequence
// [acc] = CL_fo_centralAcc(pos [, mu])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Acceleration (relative to an inertial frame) due to the central force (Newtonian acceleration).</p> 
// <p>It is directed from the central body to the satellite, and is inversely proportional 
// to the distance squared.</p> 
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The origin for the position vector must be the central body.</p>
// <p>- The coordinates frame can be any frame.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector (from the central body) [m]. (3xN)
// mu: (optional) Gravitational constant [m^3/s^2]. Default value is %CL_mu. (1x1)
// acc: Acceleration [m/s^2]. (3xN) 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fo_centralPot
//
// Examples
// pos = [6378.e3; 0; 0];
// CL_fo_centralAcc(pos)


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end


// Code:
if ~exists('mu','local'); mu = %CL_mu; end

acc = -mu * CL_dMult(pos, 1.0 ./ (CL_norm(pos).^3)); 
    
endfunction
 