//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [dens] = CL_mod_atmUS76Density(alt)
// US76 atmospheric model density 
//
// Calling Sequence
// [dens] = CL_mod_atmUS76Density(alt)
//
// Description
// <itemizedlist><listitem>
// <p>Computes density at a given altitude
// using US76 atmospheric model (convenience function).</p>
// </listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- The US76 atmospheric model can be used for altitudes in [0 - 1000] km. </p>
// <p>- Altitudes below 0 km are silently enforced to 0 km. </p>
// <p>- Results for altitudes above 1000 km are set to %nan. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// alt : Altitude [m] (1xN)
// dens : Density [kg/m^3] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_mod_atmUS76
//
// Examples
// alt = 700.e3;
// dens = CL_mod_atmUS76Density(alt)

// Declarations:

// Code:

[temp, pres, dens] = CL_mod_atmUS76(alt); 

endfunction
