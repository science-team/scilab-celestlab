//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [B] = CL__iers_bias2006()
// Bias matrix (IAU2006)
//
// Description
// B is the matrix from the GCRS frame to the EME2000 frame.
// u_EME2000 = B * u_GCRS
//
// Calling Sequence
// B = CL__iers_bias2006()
//
// Parameters
// B: Bias matrix (3x3)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010


// Declarations:

// Code:

// Precession angles at date J2000.0 (ref1, p61-63)
[gamb,phib,psib,epsa] = CL__iers_prec2006([2451545;0])

// Frame bias matrix: GCRS to J2000.0.
B = CL_rot_angles2matrix([3,1,3,1], [gamb;phib;-psib;-epsa]);

endfunction
