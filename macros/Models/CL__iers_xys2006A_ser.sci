//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [x, y, s, xdot, ydot, sdot] = CL__iers_xys2006A_ser(jd, cder)
// X,Y,s using series based on IAU 2006 precession and IAU 2000AR06 nutation models
//
// Calling Sequence
// [x, y, s, xdot, ydot, sdot] = CL__iers_xys2006A_ser(jd [,cder])
//
// Description
// <itemizedlist><listitem>
// <p>X,Y coordinates of the Celestial Intermediate Pole and CIO locator s 
// using series based on IAU 2006 precession and IAU 2000AR06 nutation models</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// cder: (boolean, optional) Option to compute derivatives. If cder is %f, the derivatives will be set to []. Default is %t. (1x1)
// x: X coordinate of CIP (1xN)
// y: Y coordinate of CIP (1xN)
// s: CIO locator (1xN)
// xdot: Time derivative of X coordinate of CIP (1xN)
// ydot: Time derivative of Y coordinate of CIP (1xN)
// sdot: Time derivative of CIO locator (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010


// Declarations:


// Code:

if (~exists("cder","local")); cder = %t; end;
if (argn(1) <= 3); cder = %f; end;

[x, y, xdot, ydot] = CL__iers_xy2006A_ser(jd, cder);
[s, sdot] = CL__iers_s2006(jd, x, y, xdot, ydot, cder);

endfunction

