//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pot] = CL_fo_centralPot(pos, mu)
// Potential due to gravity (central force) 
//
// Calling Sequence
// pot = CL_fo_centralPot(pos [, mu])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Potential due to the central force.</p> 
// <p>By definition, the acceleration derived from the potential is +grad(potential).</p> 
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- The origin for the position vector must be the central body.</p>
// <p>- The coordinates frame can be any frame.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Force models">Force models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector (from the central body) [m]. (3xN)
// mu: (optional) Gravitational constant [m^3/s^2]. Default value is %CL_mu. (1x1)
// pot: Potential [m^2/s^2]. (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fo_centralAcc
//
// Examples
// pos = [6378.e3; 0; 0];
// CL_fo_centralPot(pos)


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end


// Code:
if ~exists('mu','local'); mu = %CL_mu; end

r = CL_norm(pos);  

pot = mu ./ r; 

endfunction
 