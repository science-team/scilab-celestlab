//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [era, eradot] = CL__iers_era2000(jd, cder)
// Earth rotation angle (IAU 2000)
//
// Calling Sequence
// [era, eradot] = CL__iers_era2000(jd [,cder])
//
// Description
// <itemizedlist><listitem>
// <p>Compute the Earth rotation angle (radians) </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: UT1) (2xN)
// cder: (boolean, optional) Option to compute derivative. If cder is %f, the derivative will be set to []. Default is %t. (1x1)
// era: Earth Rotation Angle [rad] (1xN)
// eradot: (optional) Earth Rotation angular velocity [rad/s] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010


// Declarations:


// Code:

if (~exists("cder","local")); cder = %t; end;
if (argn(1) <= 1); cder = %f; end;

// Julian days since J2000.0
// NB: jd(1,:) - 2451545: should be exact for maximum precision
t = ((jd(1,:) - 2451545) + jd(2,:));

// Fractional part of t (days) (no problem if f is > 1 because of CL_rMod in ERA)
f = (jd(1,:) - floor(jd(1,:))) + (jd(2,:) - floor(jd(2,:)));

// Earth rotation angle at this UT1. (eq 5.15, p52, paragraph 5.5.3)
era = CL_rMod(2*%pi * (f + 0.7790572732640 + 0.00273781191135448 * t) , 2*%pi);

// Time derivative
eradot = [];
if (cder)
  eradot = 2*%pi*1.00273781191135448/86400 * ones(t);  // rad/s
end

endfunction
