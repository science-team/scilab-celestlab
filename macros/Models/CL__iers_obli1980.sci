//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [epsa, epsadot] = CL__iers_obli1980(jd, cder)
// Mean obliquity of the ecliptic (IAU 1980 model).
//
// Calling Sequence
// [epsa, epsadot] = CL__iers_obli1980(jd [,cder])
//
// Description
// <itemizedlist><listitem>
// <p>Compute the mean obliquity of the ecliptic (IAU 1980 model).</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// cder: (boolean, optional) Option to compute derivative. If cder is %f, the derivative will be set to []. Default is %t. (1x1)
// epsa: Mean obliquity of the ecliptic [rad] (1xN)
// epsadot: Time derivative of mean obliquity of the ecliptic [rad/s] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) 1980 IAU theory of nutation: The final report of 
//    the IAU working group on nutation, P.K.Seidelmann, 1981
//


// Declarations:


// Code:

if (~exists("cder","local")); cder = %t; end;
if (argn(1) <= 1); cder = %f; end;

ARCSEC_TO_RAD = %pi / (180*3600);

// Julian centuries since J2000.0
t = ((jd(1,:) - 2451545) + jd(2,:)) / 36525;

// p98, paragraph 10
// (original equation: first term = 23deg 26' 21.448" = 84381.448")
epsa =  (84381.448 + (-46.8150 + (-0.00059 + ...
        (0.001813) .* t) .* t) .* t) * ARCSEC_TO_RAD;
 
 
// ---------------
// Time derivative   
// ---------------
epsadot = [];  
if (cder)
  epsadot = (-46.8150 + (2*-0.00059 + (3*0.001813) .* t) .* t) * ARCSEC_TO_RAD / (36525*86400);
end 
 
endfunction
