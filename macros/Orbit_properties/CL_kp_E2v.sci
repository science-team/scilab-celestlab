//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [v] = CL_kp_E2v(ecc,E)
// Eccentric anomaly to true anomaly
//
// Calling Sequence
// v = CL_kp_E2v(ecc,E)
//
// Description
// <itemizedlist><listitem>
// <p>Computes true anomaly from eccentric anomaly and eccentricity for elliptical and hyperbolic orbits.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ecc: Eccentricity (PxN or Px1)
// E: Eccentric anomaly [rad] (PxN or Px1)
// v: True anomaly [rad] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, equation 3.10a
// 2) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 4.6.6, page 179
//
// See also
// CL_kp_M2Ecir
// CL_kp_M2E
// CL_kp_v2E
//
// Examples
// // Elliptic and hyperbolic orbit: 
// E = [1.7, 5.7];
// ecc = [0.3, 1.5];
// v = CL_kp_E2v(ecc,E);
//
// // Consistency test:
// E2 = CL_kp_v2E(ecc,v);

// Declarations:

// Code:

I = find(ecc < 0); 
if (I <> []) 
  CL__error("Invalid eccentricity (<0)");
end

I = find(ecc == 1); 
if (I <> []) 
  CL__error("Invalid eccentricity: parabola case not handled");
end

// check / resize
nr = size(ecc,1); 
[ecc,E] = CL__checkInputs(ecc,nr,E,nr); 

v = zeros(E);

// ------------------------
// Ellipse
// ------------------------
I = find(ecc < 1); 
if (I <> [])
  betaa = ecc(I)./(1+sqrt(1-ecc(I).^2));
  v(I) = E(I) + 2*atan(betaa.*sin(E(I)),1-betaa.*cos(E(I)));
end

// ------------------------
// Hyperbola
// ------------------------
I = find(ecc > 1);  
if (I <> [])
  w = sqrt((ecc(I)+1)./(ecc(I)-1)).*tanh(E(I)./2);
  v(I) = 2.0.*atan(w);
end


endfunction


