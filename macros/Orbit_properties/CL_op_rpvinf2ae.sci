//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [sma,ecc] = CL_op_rpvinf2ae(rp,vinf, mu)
// Periapsis radius and velocity at infinity to semi major axis and eccentricity (hyperbolic orbits)
//
// Calling Sequence
// [sma,ecc] = CL_op_rpvinf2ae(rp,vinf [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes semi major axis and eccentricity from
// periapsis radius and velocity at infinity (hyperbolic orbits only).</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// rp: Periapsis radius [m] (1xN or 1x1)
// vind: Velocity at infinity [rad] (1xN or 1x1)
// mu : (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// sma: Semi major axis [m] (1xN)
// ecc: Eccentricity (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_op_rpvp2ae
//
// Examples
// rp = 7078.e3;
// vinf = 12.e3;
// [sma,ecc] = CL_op_rpvinf2ae(rp,vinf)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local') then mu=%CL_mu; end

Nr = size(rp,1); 
[rp, vinf] = CL__checkInputs(rp, Nr, vinf, Nr); 

if (find(rp <= 0 | vinf <= 0) <> []); CL__error("Positive arguments expected"); end;

sma = mu ./ (vinf.^2); 
ecc = rp ./ sma + 1; 

endfunction
