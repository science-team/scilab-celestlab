//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
 
function [v] = CL_kp_M2v(ecc,M)
// Mean anomaly to true anomaly
//
// Calling Sequence
// v = CL_kp_M2v(ecc,M)
//
// Description
// <itemizedlist><listitem>
// <p>Computes true anomaly from mean anomaly and eccentricity for elliptical and hyperbolic orbits.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ecc: Eccentricity (PxN or Px1)
// M: Mean anomaly [rad] (PxN or Px1)
// v: True anomaly [rad] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, equation 3.10a
// 2) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 4.6.6, page 179
//
// See also
// CL_kp_v2M
//
// Examples
// // Elliptic and hyperbolic orbit: 
// M = [0.1, 0.2];
// ecc = [0.3, 1.5];
// v = CL_kp_M2v(ecc,M);
//
// // Consistency test:
// M2 = CL_kp_v2M(ecc,v);


// Declarations:


// Code:

// No error checking (done by called functions)
E = CL_kp_M2E(ecc,M);
v = CL_kp_E2v(ecc,E);


endfunction


