//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
 
function [M] = CL_kp_v2M(ecc,v)
// True anomaly to mean anomaly
//
// Calling Sequence
// M = CL_kp_v2M(ecc,v)
//
// Description
// <itemizedlist><listitem>
// <p>Computes mean anomaly from true anomaly and eccentricity for elliptical and 
// hyperbolic orbits.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ecc: Eccentricity (PxN or Px1)
// v: True anomaly [rad] (PxN or Px1)
// M: Mean anomaly [rad] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, equation 3.10a
// 2) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 4.6.6, page 179
//
// See also
// CL_kp_M2v
//
// Examples
// // Elliptic and hyperbolic orbit: 
// v = [0.1, 0.2];
// ecc = [0.3, 1.5];
// M = CL_kp_v2M(ecc,v);
//
// // Consistency test:
// v2 = CL_kp_M2v(ecc,M);


// Declarations:


// Code:

// No error checking (done by called functions)

E = CL_kp_v2E(ecc,v);
M = CL_kp_E2M(ecc,E);

endfunction


