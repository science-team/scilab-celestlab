//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_kp_params(type_output,sma, mu,rotr_pla)
// Orbit properties for a Keplerian motion (mean motion, period, ...)
//
// Calling Sequence
// [par] = CL_kp_params(type_output,sma [,mu,rotr_pla])
// [par1,par2] = CL_kp_params([type_output1,type_output2],sma [,mu,rotr_pla])
// [par1,...,parN] = CL_kp_params([type_output1,... type_outputN],sma [,mu,rotr_pla])
//
// Description
// <itemizedlist><listitem>
// <p>Computes quantities related to the Keplerian mean motion:</p> 
// <p>  - 'mm' : mean motion : </p>
// <p> <inlinemediaobject><imageobject><imagedata fileref="meanmotion.gif"/></imageobject></inlinemediaobject></p>
// <p>  - 'per' : period : </p>
// <p> <inlinemediaobject><imageobject><imagedata fileref="period.gif"/></imageobject></inlinemediaobject></p>
// <p>  - 'lgap' : gap in longitude between 2 consecutive ground tracks : </p>
// <p> <inlinemediaobject><imageobject><imagedata fileref="longitudegap.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// type_output : (string) Name(s) of outputs : 'mm','per','lgap' (1 x Noutput)
// sma: Semi-major axis [m] (1xN)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// rotr_pla: (optional) Rotation rate of the planet (default is %CL_rotrBody)
// par1,...,park: Output values [rad/s, s, rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_op_paramsJ2
//
// Examples
// // Longitude gap between 2 consecutive ground tracks :
// sma = 7078.e3 ;
// [lgap] = CL_kp_params('lgap',sma)
//
// // Mean motion and period : 
// sma = 7078.e3 ;
// [mm,per] = CL_kp_params(['mm','per'],sma)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_rotrBody")); %CL_rotrBody = %CL__PRIV.DATA.rotrBody; end

// Compatibility: old constant must not exist
if (exists("%CL_rotrEarth")); CL__warning("Variable %CL_rotrEarth exists: use %CL_rotrBody instead"); %CL_rotrBody = %CL_rotrEarth; end

// Code:
if ~exists('mu','local') then mu=%CL_mu; end
if ~exists('rotr_pla','local') then rotr_pla=%CL_rotrBody; end

varargout = list();

mm = (mu./(sma.^3)).^(0.5);
Noutput = max(size(type_output));
for k = 1 : Noutput
  if(type_output(k) == 'mm')
    varargout(k) = mm;
  elseif(type_output(k) == 'per')
    varargout(k) = 2*%pi ./ mm;
  elseif(type_output(k) == 'lgap')
    varargout(k) = 2*%pi .* (rotr_pla) ./ mm;
  else
    CL__error('Unrecognized parameter name');
  end
end

endfunction
