//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
 
function [E] = CL_kp_v2E(ecc,v)
// True anomaly to eccentric anomaly
//
// Calling Sequence
// E = CL_kp_v2E(ecc,v)
//
// Description
// <itemizedlist><listitem>
// <p>Computes eccentric anomaly from true anomaly and eccentricity for 
// elliptical and hyperbolic orbits.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// v: True anomaly [rad] (PxN or Px1)
// ecc: Eccentricity (PxN or Px1)
// E: Eccentric anomaly. Undefined (%nan) if the true anomaly is not in the correct range.  [rad] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, equation 3.10a
// 2) Mecanique spatiale, CNES - Cepadues 1995, Tome I, section 4.6.6, page 179
//
// See also
// CL_kp_M2Ecir
// CL_kp_M2E
// CL_kp_E2v
//
// Examples
// // Elliptic and hyperbolic orbit: 
// v = [0.1, 0.2];
// ecc = [0.3, 1.5];
// E = CL_kp_v2E(ecc,v);
//
// // Consistency test:
// v2 = CL_kp_E2v(ecc,E);

// Declarations:

// Code:

I = find(ecc < 0); 
if (I <> []) 
  CL__error("Invalid eccentricity (<0)");
end

I = find(ecc == 1); 
if (I <> []) 
  CL__error("Invalid eccentricity: parabola case not handled");
end

// check / resize
nr = size(ecc,1); 
[ecc,v] = CL__checkInputs(ecc,nr,v,nr); 

E = zeros(v)

// -------------------------------
// Ellipse
// -------------------------------

I = find(ecc < 1);

if (I <> [])
  betaa = ecc(I) ./ (1 + sqrt(1-ecc(I).^2))
  E(I) = v(I) - 2*atan(betaa .* sin(v(I)), 1 + betaa .* cos(v(I)))
end

// -------------------------------
// Hyperbola
// -------------------------------

I = find(ecc > 1);

if (I <> [])

  E(I) = %nan; 

  if (find(abs(v(I)) > %pi) <> [])
    CL__error('Hyperbola: true anomaly > pi'); 
  end

  // check v < vinf 
  // vinf = acos(-1/e) : invalid value (E would be infinity)
  k = find(cos(v(I)) .* ecc(I) > -1+%eps & abs(v(I)) < %pi-%eps); 
  I = I(k); 

  E(I) = 2*atanh( sqrt((ecc(I)-1) ./ (ecc(I)+1)) .* tan(v(I)/2) ); 

end

endfunction
