//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function x = CL__E_esinE(e,E)
// Accurate computation of E - e*sin(E).
// (useful when E is close to 0 and e close to 1,
// i.e. near the perigee of almost parabolic orbits)

x = (1 - e) .* sin(E);
mE2 = -E .* E;
term = E;
d = zeros(x);
x0 = %nan * ones(x);

K = 1:length(x);// indices to be dealt with

nb_iter = 0;

while (K <> [])
  d(K) = d(K) + 2;
  term(K) = term(K) .* mE2(K) ./ (d(K) .* (d(K) + 1));
  x0(K) = x(K);
  x(K) = x(K) - term(K);
  // the inequality test below IS intentional and should NOT be replaced by a check with a small tolerance
  K = find(x <> x0);
  nb_iter = nb_iter + 1;
end

endfunction
