//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [sma,ecc] = CL_op_rava2ae(ra,va, mu)
// Apoapsis radius and velocity to semi major axis and eccentricity (elliptic orbits)
//
// Calling Sequence
// [sma,ecc] = CL_op_rava2ae(ra,va [,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes semi major axis and eccentricity from apoapsis radius and 
// velocity at apoapsis (elliptic orbit only).</p>
// <p></p></listitem>
// <listitem>
// <p><b>Note:</b></p>
// <p>The results are correct even if (ra,va) are data that actually correspond to the periapsis.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ra: Apoapsis radius [m] (1xN or 1x1)
// va: Velocity at apoapsis [m/s] (1xN or 1x1)
// mu : (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// sma: Semi major axis [m] (1xN)
// ecc: Eccentricity (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_op_rpvp2ae
// CL_op_rarp2ae
//
// Examples
// ra = 7078.e3;
// va = 7.e3;
// [sma,ecc] = CL_op_rava2ae(ra,va)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('mu','local') then mu=%CL_mu; end

Nr = size(ra,1); 
[ra, va] = CL__checkInputs(ra, Nr, va, Nr); 

if (find(ra <= 0 | va <= 0) <> []); CL__error("Positive arguments expected"); end;

sma = (mu .* ra) ./ (2*mu - ra .* va.^2); 
ecc = abs(mu - sma .* va.^2) ./ (mu + sma .* va.^2); 

endfunction
