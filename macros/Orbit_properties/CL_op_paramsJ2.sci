//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_op_paramsJ2(type_output,sma,ecc,inc, er,mu,j2,rotr_pla)
// Orbit properties depending on J2 (mean motion, period, nodal period, ...)
//
// Calling Sequence
// [par] = CL_op_paramsJ2(type_output,sma,ecc,inc [,er,mu,j2,rotr_pla])
// [par1,par2] = CL_op_paramsJ2([type_output1,type_output2],sma,ecc,inc [,er,mu,j2,rotr_pla])
// [par1,par2,par3] = CL_op_paramsJ2([type_output1,type_output2 type_output3],sma,ecc,inc [,er,mu,j2,rotr_pla])
//
// Description
// <itemizedlist><listitem>
// <p>Computes quantities related to the mean motion taking the effect of J2 into account. </p>
// <p><b>type_output</b> can be : </p>
// <p>  - 'mm' : mean motion </p>
// <inlinemediaobject><imageobject><imagedata fileref="meanmotionJ2.gif"/></imageobject></inlinemediaobject>
// <p>  - 'per' : period (from periapsis to the next one) </p>
// <inlinemediaobject><imageobject><imagedata fileref="periodJ2.gif"/></imageobject></inlinemediaobject>
// <p>  - 'nodper' : nodal period (from ascending node to the next one) </p>
// <inlinemediaobject><imageobject><imagedata fileref="nodalperiodJ2.gif"/></imageobject></inlinemediaobject>
// <p>  - 'lgap' : gap in longitude between 2 consecutive ground tracks </p>
// <inlinemediaobject><imageobject><imagedata fileref="longitudegapJ2.gif"/></imageobject></inlinemediaobject>
// </listitem>
// </itemizedlist>
//
// Parameters
// type_output : Name(s) of the quantities to be computed. Possible names are: 'mm','per','nodper','lgap' (1 x Noutput)
// sma: semi major axis [m] (1xN)
// ecc: eccentricity  (1xN)
// inc: inclination [rad] (1xN)
// er: (optional) equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// j2: (optional) zonal coefficient (second zonal harmonic) (default is %CL_j1jn(2))
// rotr_pla: (optional) rotation rate of the planet (default is %CL_rotrBody)
// par: mean motion, period, nodal period or longitude gap between 2 ground tracks [rad/s,s,s,rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_kp_params
//
// Examples
// // Longitude gap between 2 consecutive ground tracks :
// sma = 7078.e3 ;
// ecc = 0.001 ;
// inc = CL_deg2rad(98);
// [lgap] = CL_op_paramsJ2('lgap',sma,ecc,inc)
//
// // Mean motion and nodal period
// sma = 7078.e3 ;
// ecc = 0.001 ;
// inc = CL_deg2rad(98);
// [mm,nodper] = CL_op_paramsJ2(['mm' 'nodper'],sma,ecc,inc)

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end
if (~exists("%CL_j1jn")); %CL_j1jn = %CL__PRIV.DATA.j1jn; end
if (~exists("%CL_rotrBody")); %CL_rotrBody = %CL__PRIV.DATA.rotrBody; end

if (~isequal(size(%CL_j1jn), size(%CL__PRIV.DATA.j1jn)))
  CL__error("Invalid size for %CL_j1jn");
end

// Compatibility: old constant must not exist
if (exists("%CL_j2")); CL__warning("Variable %CL_j2 exists: use %CL_j1jn instead"); %CL_j1jn(2) = %CL_j2; end
if (exists("%CL_rotrEarth")); CL__warning("Variable %CL_rotrEarth exists: use %CL_rotrBody instead"); %CL_rotrBody = %CL_rotrEarth; end

// Code:
if ~exists('er','local'); er = %CL_eqRad; end
if ~exists('mu','local'); mu = %CL_mu; end
if ~exists('j2','local'); j2 = %CL_j1jn(2);end
if ~exists('rotr_pla','local'); rotr_pla = %CL_rotrBody; end

varargout = list();

[dpom,dgom,dM] = CL_op_driftJ2(sma,ecc,inc,er,mu,j2);
Noutput = max(size(type_output));
for k = 1 : Noutput
  if(type_output(k) == 'mm')
    varargout(k) = dpom + dM;
  elseif(type_output(k) == 'per')
    varargout(k) = 2*%pi ./ dM;
  elseif(type_output(k) == 'nodper')
    varargout(k) = 2*%pi ./ (dpom +dM);
  elseif(type_output(k) == 'lgap')
    varargout(k) = 2*%pi .* (rotr_pla-dgom) ./ (dpom +dM);
  else
    CL__error('Unknown type of type_output');
  end
end

endfunction
