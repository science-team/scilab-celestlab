//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [E] = CL_kp_M2E(ecc,M)
// Mean anomaly to eccentric anomaly (Kepler's equation)
//
// Calling Sequence
// E = CL_kp_M2E(ecc,M)
//
// Description
// <itemizedlist><listitem>
// <p>Solves Kepler's equation for hyperbolic or elliptical orbits.</p>
// <p>Given the eccentricity <b>ecc</b> and the mean anomaly 
// <b>M</b>, this function computes the eccentric anomaly 
// <b>E</b>, such that : </p>
// <p> - Elliptical orbits ( ecc < 1 ) : <b>E - ecc * sin(E) = M</b> </p>
// <p> - Hyperbolic orbits ( ecc > 1 ) : <b>ecc * sinh(E) - E = M</b> </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// M: Mean anomaly [rad] (PxN or Px1)
// ecc: Eccentricity (PxN or Px1)
// E: Eccentric anomaly [rad] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) CNES - MSLIB FORTRAN 90, Volume V (mvi_kepler_hyperb)
// 2) Procedures for solving Kepler's Equation - A.W. Odell and R.H Gooding - 1986
//
// See also
// CL_kp_M2Ecir
//
// Examples
// // Elliptic and hyperbolic orbit: 
// M = [0.1, 0.2];
// ecc = [0.3, 1.5];
// E = CL_kp_M2E(ecc,M);
//
// // Consistency test:
// M2 = CL_kp_E2M(ecc,E);


// Declarations:

// Code:
I = find(ecc < 0); 
if (I <> []) 
  CL__error("Invalid eccentricity (<0)");
end

I = find(ecc == 1); 
if (I <> []) 
  CL__error("Invalid eccentricity: parabola case not handled");
end

// check / resize
nr = size(ecc,1); 
[ecc,M] = CL__checkInputs(ecc,nr,M,nr); 


E = zeros(M); // initialization needed !

// ------------------------
// Ellipse
// ------------------------
I = find(ecc < 1);  

if (I <> []) 
  E(I) = CL__kp_M2Eell(ecc(I), M(I));
end

// ------------------------
// Hyperbola
// ------------------------
I = find(ecc > 1); 

if (I <> []) 
  E(I) = CL__kp_M2Ehyp(ecc(I), M(I));
end


endfunction



