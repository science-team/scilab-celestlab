//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of angribution of free software.  You can  use,
//  modify and/ or reangribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ang] = CL_gm_sphericDist(p1,p2)
// Angle (angular distance) between 2 directions
//
// Calling Sequence
// [ang] = CL_gm_sphericDist(p1,p2)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the angle between 2 vectors. Each vector is described by 2 spherical 
// coordinates (analogous to longitude and latitude).</p> 
// <p>The three following formulas give the spherical distance between 
// two vectors (where lambda: longitude, phi:latitude): </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="GCdist.gif"/></imageobject></inlinemediaobject></p>
// <p>However, the first formula is used in this function as the two other ones may 
// lead to rounding errors for small angles or antipodal points.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// p1: First direction in spherical coordinates: [longitude;latitude] [rad] (2xN)
// p2: Second direction in spherical coordinates: [longitude;latitude] [rad] (2xN)
// ang: Angle between p1 and p2 [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) R.W. Sinnott, "Virtues of the Haversine", Sky and Telescope, vol. 68, no. 2, 1984, p. 159
// 2) Weisstein, Eric W. "Great Circle." From MathWorld. <ulink url="http://mathworld.wolfram.com/GreatCircle.html">http://mathworld.wolfram.com/GreatCircle.html</ulink>
//
// See also
// CL_co_car2sph
// CL_co_sph2car
//
// Examples
// // Spherical distance between Toulouse (43deg 36' 19"N, 1deg 26' 34"E)
// // and Barcelona (41deg 24' 7"N, 2deg 10' 17"E)
// Toulouse = [43 + 36/60 + 19/3600; 1 + 26/60 + 34/3600] * 180/%pi;
// Barcelone = [41 + 24/60 + 7/3600; 2 + 10/60 + 17/3600] * 180/%pi;
// ang = CL_gm_sphericDist(Toulouse,Barcelone)
//
// // Distance (meters)
// ang * 6378.e3
//

// Declarations:

// Code:

lon1 = p1(1,:);
lon2 = p2(1,:);
lat1 = p1(2,:);
lat2 = p2(2,:);


ang = atan(sqrt( ( cos(lat2).*sin(lon2-lon1) ).^2 + ( cos(lat1).*sin(lat2)-sin(lat1).*cos(lat2).*cos(lon2-lon1) ).^2 ),..//y
             sin(lat1).*sin(lat2)+cos(lat1).*cos(lat2).*cos(lon2-lon1).. //x
            );

endfunction
