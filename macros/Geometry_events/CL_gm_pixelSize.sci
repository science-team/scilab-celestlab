//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [f1,f2] = CL_gm_pixelSize(sat_alt,tilt,target_alt, er)
// Footprint dimensions for a conic sensor at an angle to the vertical
//
// Calling Sequence
// [f1,f2] = CL_gm_pixelSize(sat_alt,tilt,target_alt [,er])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the size factors of the footprint of a conic sensor at an 
// angle to the descending vertical. </p>
// <p>If the footprint is small enough, it can be considered as
// the intersection between a cone of revolution and the plane tangent to the planet surface (and containing the target location). </p>
// <p>Then: </p>
// <p> - The footprint outline is a circle with radius D when the sensor is looking to the descending vertical. </p>
// <p> - It is an ellipse with semi-major axis <b>f1</b>*D and semi-minor axis <b>f2</b>*D 
// when the sensor is tilted at an angle (<b>tilt</b>) to the vertical. </p>
// <p> Two effects are responsible for the change of size: increase of distance 
// (reflected in f1 and f2) and increase of incidence angle (reflected in f1 only). </p>
// <p><inlinemediaobject><imageobject><imagedata fileref="pixel_size.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p><b>Note:</b></p>
// <p> - A spherical planet is assumed. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// sat_alt: Satellite altitude [m] (1xN)
// tilt: Angle to the descending vertical [rad] (1xN)
// target_alt: (optional) Target altitude [m]  (default is 0)(1xN)
// er : (optional) Planet radius (default is %CL_eqRad) [m]
// f1: Size factor in tilt plane(1xN)
// f2: Size factor in direction perpendicular to tilt plane(1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_gm_visiParams
//
// Examples
// // Example 1
// sat_alt = 700.e3;
// tilt = CL_deg2rad(20);
// [f1,f2] = CL_gm_pixelSize(sat_alt,tilt)
//
// // Same computation, with CL_gm_visiParams :
// eqRad = CL_dataGet("eqRad");
// sat_radius = eqRad + sat_alt;
// target_radius = eqRad;
//
// // distance for the given tilt
// [dist] = CL_gm_visiParams(sat_radius,target_radius,'sat',tilt,'dist')
//
// // elevation for the given tilt
// [elev] = CL_gm_visiParams(sat_radius,target_radius,'sat',tilt,'elev')
//
// f1 = dist/(sat_alt * sin(elev))
// f2 = dist/sat_alt
//

// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end

// Code:
if ~exists('er','local') then er=%CL_eqRad; end
if ~exists('target_alt','local') then target_alt=0; end

sat_radius = er + sat_alt;
target_radius = er + target_alt;
[dist] = CL_gm_visiParams(sat_radius,target_radius,'sat',tilt,'dist') // distance for the given tilt
[elev] = CL_gm_visiParams(sat_radius,target_radius,'sat',tilt,'elev') // elevation for the given tilt
f1 = dist./(sat_alt .* sin(elev))
f2 = dist./sat_alt

endfunction
