//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [rat_ecl] = CL_gm_eclipseCheck(pos_obs, pos1, pos2, sr1, sr2, output)
// Hidden fraction of a body (eclipsed by another body), as seen from an observer
//
// Calling Sequence
// rat_ecl = CL_gm_eclipseCheck(pos_obs, pos1, pos2, sr1, sr2)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the hidden fraction of a body (eclipsed by another body), as seen from an observer:</p>
// <p>- 1.0 : 100% of the first body is eclipsed </p>
// <p>- 0.7 : 70% of the first body is eclipsed </p>
// <p>- 0.0 : 0% of the first body is eclipsed </p>
// <p></p>
// <p>The two bodies are assumed spherical</p>
// <p><inlinemediaobject><imageobject><imagedata fileref="eclipse_check.gif"/></imageobject></inlinemediaobject></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// pos_obs: Position of observer (3xN or 3x1)
// pos1: Position of centre of sphere1 (3xN or 3x1)
// pos2: Position of centre of sphere2 (3xN or 3x1)
// sr1: Radius of sphere1 (1x1 or 1xN)
// sr2: Radius of sphere2 (1x1 or 1xN)
// rat_ecl: Hidden fraction of body1, as seen from pos_obs (3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_gm_inters3dConesSa
//
// Examples
// // Fraction of Sun eclipsed by Earth (from an observer in space):
// radiusSun = CL_dataGet("body.Sun.eqRad");
// radiusEarth = CL_dataGet("body.Earth.eqRad");
// au = CL_dataGet("au");
//
// theta = linspace(2.88, 3.05, 100); 
// pos_obs = 42000.e3 * [cos(theta); sin(theta); zeros(theta)];
// pos1 = au * [1; 0; 0]; // Sun
// sr1 = radiusSun;
// pos2 = [0; 0; 0]; // Earth
// sr2 = radiusEarth;
// rat_ecl = CL_gm_eclipseCheck(pos_obs, pos1, pos2, sr1, sr2); 
// scf();
// plot(theta*180/%pi, rat_ecl, "o"); 


// Declarations:  

// Code :
  
[lhs,rhs]=argn();
  
[pos_obs, pos1, pos2, sr1, sr2, N] = CL__checkInputs(pos_obs,3, pos1,3, pos2,3, sr1,1 ,sr2,1);

// Check that radii are positive
I = find(sr1 < 0 | sr2 < 0);
if (I~=[]); CL__error("Radius of spheres must be positive"); end;
  
// Check that observer is outside both spheres :
norm1 = CL_norm(pos_obs-pos1);
norm2 = CL_norm(pos_obs-pos2);
I = find(norm1 < sr1 | norm2 < sr2);
if (I~=[]); CL__error("Observer must be outside both spheres"); end;
  
// -------------------------------------------------------------

alpha1 = asin( sr1 ./ norm1 ); // apparent radius of sphere1
alpha2 = asin( sr2 ./ norm2 ); // apparent radius of sphere2

// separation angle between the two spheres (as seen from obs)
alpha = CL_vectAngle( pos1 - pos_obs , pos2 - pos_obs ); 
  
// Solid angle of body1 occulted by body2
ang_inters = CL_gm_inters3dConesSa(alpha,alpha1,alpha2);
  
// If body2 was behind body1 : body1 is not occulted
I = find( norm2 > norm1 );
ang_inters(I) = 0;
  
// Solid angle of body1 : 
ang_tot = 2*%pi * (1 - cos(alpha1));
 
// ratio of body1 eclipsed
rat_ecl = %nan * ones(ang_inters); 
I = find(ang_tot > 0); 
rat_ecl(I) = ang_inters(I) ./ ang_tot(I);


endfunction






