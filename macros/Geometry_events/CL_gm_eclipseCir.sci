//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [res] = CL_gm_eclipseCir(sma,inc,raan,alpha_sun,delta_sun, er,mu)
// Analytical eclipse calculation for circular orbits
//
// Calling Sequence
// res = CL_gm_eclipseCir(sma,inc,raan,alpha_sun,delta_sun [,er,mu])
//
// Description
// <itemizedlist><listitem>
// <p>Computes various ress that characterize the portion of the orbit path where the satellite is 
// in the shadow of the planet.</p> 
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>This function is actually an interface to CL_gm_eclipse,
// which is called with zero values for eccentricity and argument of periapsis).</p>
// <p>See <link linkend="CL_gm_eclipse">CL_gm_eclipse</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// sma: Semi major axis [m] (1xN)
// inc: Inclination [rad] (1xN)
// raan: Right ascension of ascending node [rad] (1xN)
// alpha_sun: Sun right ascension [rad] (1xN)
// delta_sun: Sun declination [rad] (1xN)
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// res: (structure) Various quantities that define the eclipse [rad,sec] (each field is 1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_gm_eclipse
// CL_gm_betaEclipse
//
// Examples
// cjd = CL_dat_cal2cjd(2009,03,21,6,0,0);
// pos_sun = CL_eph_sun(cjd);
// pos_sun_sph = CL_co_car2sph(pos_sun);
// alpha_sun = pos_sun_sph(1);
// delta_sun = pos_sun_sph(2);
// raan = 0;
// sma = 7300.e3;
// inc = CL_deg2rad(98);
// res = CL_gm_eclipseCir(sma,inc,raan,alpha_sun,delta_sun);
// res.start
// res.end
// res.sun_orb
// res.angle
// res.duration


// Declarations:
global %CL__PRIV; 
if (~exists("%CL_eqRad")); %CL_eqRad = %CL__PRIV.DATA.eqRad; end
if (~exists("%CL_mu")); %CL_mu = %CL__PRIV.DATA.mu; end

// Code:
if ~exists('er','local'); er = %CL_eqRad; end
if ~exists('mu','local'); mu = %CL_mu; end

ecc = 0;  // eccentricity
argp = 0; // argument of perigee (value has no impact)

res = CL_gm_eclipse(sma, ecc, inc, argp, raan, alpha_sun, delta_sun, er, mu);  


endfunction
