//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [raan1,raan2] = CL_gm_beta2raan(betaa,inc,alpha_sun,delta_sun)
// Beta angle to right ascension of ascending node 
//
// Calling Sequence
// [raan1,raan2] = CL_gm_beta2raan(betaa,inc,alpha_sun,delta_sun)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the right ascension of the ascending node from the beta angle (beta angle = angle 
// between the satellite's orbit plane and the Sun direction). </p>
// <p></p>
// <p>There are 2 solutions (<b>raan1</b>) and (<b>raan2</b>) such that: </p>
// <p>- raan2 = pi - raan1 + 2*alpha_sun </p>
// <p>- raan1 is in the interval [ alpha_sun - pi/2 ; alpha_sun + pi/2] modulo 2*pi</p>
// <p>- raan2 is in the interval [ alpha_sun + pi/2 ; alpha_sun + 3*pi/2] modulo 2*pi</p>
// <p></p>
// <inlinemediaobject><imageobject><imagedata fileref="beta.gif"/></imageobject></inlinemediaobject></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- The beta angle is a signed quantity. If it is positive then the Sun direction is less
// than 90 degrees from the orbit's angular momentum vector (vector perpendicular to the
// orbit plane and oriented according to right hand rule). </p>
// <p>- Rhe right ascension can be undefined (%nan) if there are no solutions. </p>
// <p>- Be careful about the order of arguments: beta angle, inclination... </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// betaa : Beta angle [rad] (1xN)
// inc : Inclination of the orbit [rad] (1xN)
// alpha_sun : Sun right ascension [rad] (1xN)
// delta_sun : Sun declination [rad] (1xN)
// raan1 : Right ascension of the ascending node (solution 1)  [rad] (1xN)
// raan2 : Right ascension of the ascending node (solution 2) [rad] (1xN)
//
// See also
// CL_op_locTimeG50
// CL_gm_raan2beta
// CL_mod_moonSunG50
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Get alpha_sun and delta_sun
// cjd = 20050;
// pos_sun = CL_eph_sun(cjd);
// pos_sun_sph = CL_co_car2sph(pos_sun);
// alpha_sun = pos_sun_sph(1);
// delta_sun = pos_sun_sph(2);
// betaa = %pi/4;
// inc = CL_deg2rad(98.7);
// [raan1,raan2] = CL_gm_beta2raan(betaa,inc,alpha_sun,delta_sun)

// Coherence des entrees

// Declarations:


// Code:

// check inputs 
I = find(inc < 0 | inc > %pi + 2*%eps)
if (I <> [])
  CL__error("Invalid inclination"); 
end

[betaa,inc,alpha_sun,delta_sun] = CL__checkInputs(betaa,1,inc,1,alpha_sun,1,delta_sun,1); 

sin_inc = sin(inc); 
eps = 1.e-15; 

// result will be undefined (%nan) if inc is close to 0 or pi
I = find(abs(sin_inc) < eps);
sin_inc(I) = %nan; 

f = (sin(betaa) - sin(delta_sun) .* cos(inc)) ./ (cos(delta_sun) .* sin_inc);

raan1 = CL_rMod(real(asin(f)) + alpha_sun, 0, 2*%pi);
raan2 = CL_rMod(%pi - raan1 + 2*alpha_sun, 0, 2*%pi);

// if f > 1 or f < -1 => raan undefined (greater than maximum value)
I = find(abs(f) > 1); 
raan1(I) = %nan; 
raan2(I) = %nan; 


endfunction
