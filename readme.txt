===================================================
CelestLab - CNES Space mechanics toolbox for Scilab
===================================================

CelestLab contents
------------------
Here are the main topics:
+ Coordinates and frames
+ Trajectory and maneuvers
+ Geometry and events
+ Models
+ Orbit properties
+ Interplanetary
+ Relative motion
+ Math
+ Utilities



Building CelestLab 
------------------

- Manual installation:
Execute the "builder.sce" script (located in CelestLab root directory): exec('builder.sce',mode=-1);

- Installation via ATOMS:
Follow ATOMS modules standard installation procedure.



Loading CelestLab 
-----------------

- Manual loading:
Execute the "loader.sce" script (located in CelestLab root directory): exec('loader.sce',mode=-1);
If you want CelestLab to be automatically loaded when you start Scilab, just include the previous line 
in your "scilab.ini" file ("scilab.ini" is located in the SCIHOME directory). 

- Loading via ATOMS:
Follow ATOMS modules standard procedure. 

- Read the help pages for more details. 



